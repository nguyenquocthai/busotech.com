<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class ActiveEmail1 extends Mailable
{
    use Queueable, SerializesModels;
     
    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $obj;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($obj)
    {
        $this->obj = $obj;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('khongkhong1990@gmail.com')
                    ->view('frontend.mails.activeemail_plain')
                    ->subject('Website wistman.asia');
                      // ->attach(public_path('/images').'/demo.jpg', [
                      //         'as' => 'demo.jpg',
                      //         'mime' => 'image/jpeg',
                      // ]);
    }
}