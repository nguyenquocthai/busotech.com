<?php

namespace App\Exceptions;

use Intervention\Image\ImageManagerStatic as Image;
use App\Exceptions\ShopCommon;
use Illuminate\Http\Request;

class ShopUpload
{
    //-------------------------------------------------------------------------------
    public static function upload($file, $link, $options = []) {

        // $file->isValid('files') != 1
        if ( $file->extension('files') != 'jpeg' && $file->extension('files') != 'png' && $file->extension('files') != 'gif' && $file->extension('files') != 'svg') {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }

        $extension = ShopCommon::getExtension($file->getClientOriginalName('files'));

        $name_file = md5(gmdate('Y-m-d H:i:s') . $file->getClientOriginalName('files')) . $extension;

        if (!empty($options['file_name'])){
            $image_path = $link['path'] . $options['file_name'];
            if(file_exists($image_path)) {
                unlink($image_path);
            }
        }

        // if (empty($options['file_name']))
                
        // else
        //     $name_file = $options['file_name'];

        $image_resize = Image::make($file->getRealPath());
        if ($file->getClientSize() >= 10000000) {
           return 'error';
        }
        if ($file->getClientSize() >= 8000000) {
           $image_resize->resize($image_resize->width(), $image_resize->height());
        }

        //$result = $file->move($link['path'], $name_file);
        $result = $image_resize->save($link['path'] . $name_file);

        if ($result) {
            return [
                'result' => 0,
                'file_name' => $name_file,
                'size' => $file->getClientSize(),
                //'file_name' => $link['url'] . $name_file,
            ];

        } else {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }
    }

    //-------------------------------------------------------------------------------
    public static function upload_avatar($file, $link, $options = []) {

        // $file->isValid('files') != 1
        if ( $file->extension('files') != 'jpeg' && $file->extension('files') != 'png' && $file->extension('files') != 'gif' && $file->extension('files') != 'svg') {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }

        $extension = ShopCommon::getExtension($file->getClientOriginalName('files'));

        if (!empty($options['file_name'])){
            $image_path = $link['path'] . $options['file_name'];
            if(file_exists($image_path)) {
                unlink($image_path);
            }
        }
        // if (empty($options['file_name']))
        //         $name_file = md5(gmdate('Y-m-d H:i:s') . $file->getClientOriginalName('files')) . $extension;
        // else
        //     $name_file = $options['file_name'];

        $image_resize = Image::make($file->getRealPath());
        if ($file->getClientSize() >= 10000000) {
           return 'error';
        }
        if ($file->getClientSize() >= 8000000) {
           $image_resize->resize($image_resize->width(), $image_resize->height());
        }

        //$result = $file->move($link['path'], $name_file);
        $result = $image_resize->save($link['path'] . $name_file);

        if ($result) {
            return [
                'result' => 0,
                'file_name' => $name_file,
                'size' => $file->getClientSize(),
                //'file_name' => $link['url'] . $name_file,
            ];

        } else {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }
    }

    //-------------------------------------------------------------------------------
    public static function upload_file($file, $link, $options = []) {

        $extension = ShopCommon::getExtension($file->getClientOriginalName('files'));

        $name_file = md5(gmdate('Y-m-d H:i:s') . $file->getClientOriginalName('files')) . $extension;

        if (!empty($options['file_name'])){
            $image_path = $link['path'] . $options['file_name'];
            if(file_exists($image_path)) {
                unlink($image_path);
            }
        }

        // if (empty($options['file_name']))
                
        // else
        //     $name_file = $options['file_name'];


        $result = $file->move($link['path'], $name_file);

        if ($result) {
            return [
                'result' => 0,
                'file_name' => $name_file,
                'size' => $file->getClientSize(),
                //'file_name' => $link['url'] . $name_file,
            ];

        } else {
            return false;
            /*return [
                'result' => 1,
                'string' => $option,
            ];*/
        }
    }
}