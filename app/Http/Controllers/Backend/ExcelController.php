<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model;
use DB;
use Excel;

class ExcelController extends BaseAdminController
{
    public function index () {
        $arr_excel[] = ['email', 'fullname', 'phone', 'address'];
        $contacts = DB::table('shop_contacts')->get()->toArray();
        foreach($contacts as $contact) {
            $arr_excel[] = [
                'email' => $contact->email,
                'fullname' => $contact->fullname,
                'phone' => $contact->phone,
                'address' => $contact->address,
            ];
        }
        Excel::create('Contact', function($excel) use ($arr_excel) {
            $excel->setTitle('Contact');
            $excel->sheet('Contact', function($sheet) use ($arr_excel){
                $sheet->fromArray($arr_excel, null, 'A1', false, false);
            });
        })->download('xlsx');
        die;
    }



}
