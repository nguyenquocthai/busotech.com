<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Model;
use App\Model\FunctionAuthority;

class SessionController extends BaseAdminController
{
    public function index (Request $request) {
        $data['code'] = 300;

        if($request->url == '/admin') {
            $data['code'] = 200;
            return response()->json($data, 200);
        }

        if (Auth::user()->role == 1) {
            $data['code'] = 200;
        } else {
            $url = str_replace('/', '_', $request->url);
            $split = explode("_",$url);
            
            $url_mod = '_'.$split[1].'_'.$split[2].'_'.@$split[3];

            $list_rules = json_decode(Auth::user()->list_rule, true);
            foreach($list_rules as $list_rule) {
                $fucntion = FunctionAuthority::where([
                    ['id', '=', $list_rule['id']],
                    ['role_json' , 'like', '%' . $url_mod . '%']
                ])->first();

                if($fucntion) {
                    $data['code'] = 200;
                    break;
                }
            }
        }
        return response()->json($data, 200);
    }

}
