<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\library;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminlibrarysController extends BaseAdminController
{

    public function index (Request $request) {

        
        $librarys = DB::table('librarys')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($librarys as $key => $library) {
            $vitri = $key+1;
            $row = $this->GetRow($library, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = library::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'library');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = library::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_library_cat1'] = $request->id_library_cat1;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'librarys', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'library', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $library = DB::table('librarys')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$library) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            library::where([
                ['id', $library->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.library_path').$library->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($library, $vitri)
    {
        $row = [];
        $row[] = $library->id;
        $row[] = $library->position;
        $row[] = $vitri;
        $row[] = $library->title;
        $row[] = $this->GetImg([
            'avatar'=> $library->avatar,
            'data'=> 'library',
            'time'=> $library->updated_at
        ]);
        $row[] = '<span class="hidden">'.$library->updated_at.'</span>'.date('d/m/Y', strtotime($library->updated_at));
        $view = View::make('Backend/library/_status', ['status' => $library->status]);
        $row[] = $view->render();
        $view = View::make('Backend/library/_actions', ['id' => $library->id,'page' => 'library']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $library = library::findOrFail($id);
            $library['avatar'] = config('general.library_url') . $library['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $library);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
