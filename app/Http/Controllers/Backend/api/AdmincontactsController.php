<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\contact;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdmincontactsController extends BaseAdminController
{

    public function index (Request $request) {

        $contacts = DB::table('contacts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $contacts = json_decode($contacts, true);
        $export = View::make('Backend/contact/_export', ['contacts' => $contacts]);
        
        $contacts = DB::table('contacts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $giatenmiens = DB::table('giatenmiens')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])
            ->get();
        $output = [];
        foreach ($contacts as $key => $contact) {
            

            $vitri = $key+1;
            $row = $this->GetRow($contact, $vitri);
            $output[] = $row;
        }
        $data['code'] = 200;
        $data['export'] = $export->render();
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = contact::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'contact');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = contact::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'contact', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $contact = DB::table('contacts')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$contact) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            contact::where([
                ['id', $contact->id]
            ])->update(['del_flg' => 1]);

            if (isset($contact->avatar)) {
                $image_path = config('general.contact_path').$contact->avatar;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                
                }
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($contact, $vitri)
    {
        $row = [];
        $row[] = $contact->id;
        $row[] = $contact->name;
        $row[] = $contact->email;
        $row[] = $contact->tel;
        $row[] = $contact->created_at;
        $view = View::make('Backend/contact/_status', ['status' => $contact->status]);
        $row[] = $view->render();
        $view = View::make('Backend/contact/_actions', ['id' => $contact->id,'id_price' => $contact->id_price,'page' => 'contact']);
        $row[] = $view->render();

        return $row;
    }
}
