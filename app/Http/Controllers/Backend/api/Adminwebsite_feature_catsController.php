<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\website_feature_cat;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminwebsite_feature_catsController extends BaseAdminController
{

    public function index (Request $request) {

        $website_feature_cats = DB::table('website_feature_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'DESC')
            ->get();

        $output = [];
        foreach ($website_feature_cats as $key => $website_feature_cat) {
            $vitri = $key+1;
            $row = $this->GetRow($website_feature_cat, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = website_feature_cat::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'website_feature_cat');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = website_feature_cat::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'website_feature_cat', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $website_feature_cat = DB::table('website_feature_cats')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$website_feature_cat) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            website_feature_cat::where([
                ['id', $website_feature_cat->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.website_feature_cat_path').$website_feature_cat->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($website_feature_cat, $vitri)
    {
        $row = [];
        $row[] = $website_feature_cat->id;
        $row[] = $website_feature_cat->position;
        $row[] = $vitri;
        $row[] = $website_feature_cat->title;
        $row[] = '<span class="hidden">'.$website_feature_cat->updated_at.'</span>'.date('d/m/Y', strtotime($website_feature_cat->updated_at));
        $view = View::make('Backend/website_feature_cat/_status', ['status' => $website_feature_cat->status]);
        $row[] = $view->render();
        $view = View::make('Backend/website_feature_cat/_actions', ['id' => $website_feature_cat->id,'page' => 'website_feature_cat']);
        $row[] = $view->render();

        return $row;
    }
}
