<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\subscribe;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminsubscribesController extends BaseAdminController
{

    public function index (Request $request) {

        $subscribes = DB::table('subscribes')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $subscribes = json_decode($subscribes, true);
        $export = View::make('Backend/subscribe/_export', ['subscribes' => $subscribes]);
        
        $subscribes = DB::table('subscribes')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $output = [];
        foreach ($subscribes as $key => $subscribe) {
            

            $vitri = $key+1;
            $row = $this->GetRow($subscribe, $vitri);
            $output[] = $row;
        }
        $data['code'] = 200;
        $data['export'] = $export->render();
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = subscribe::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'subscribe');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = subscribe::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'subscribe', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $subscribe = DB::table('subscribes')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$subscribe) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            subscribe::where([
                ['id', $subscribe->id]
            ])->update(['del_flg' => 1]);

            if (isset($subscribe->avatar)) {
                $image_path = config('general.subscribe_path').$subscribe->avatar;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                
                }
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($subscribe, $vitri)
    {
        $row = [];
        $row[] = $subscribe->id;
        $row[] = $subscribe->email;
        $row[] = $subscribe->created_at;
        
        return $row;
    }
}
