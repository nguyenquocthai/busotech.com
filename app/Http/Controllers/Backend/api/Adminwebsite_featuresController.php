<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\website_feature;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminwebsite_featuresController extends BaseAdminController
{

    public function index (Request $request) {

        $website_features = DB::table('website_features')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'DESC')
            ->get();

        $output = [];
        foreach ($website_features as $key => $website_feature) {
            $vitri = $key+1;
            $row = $this->GetRow($website_feature, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = website_feature::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'website_feature');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = website_feature::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'website_feature', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $website_feature = DB::table('website_features')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$website_feature) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            website_feature::where([
                ['id', $website_feature->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.website_feature_path').$website_feature->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($website_feature, $vitri)
    {
        $row = [];
        $row[] = $website_feature->id;
        $row[] = $website_feature->position;
        $row[] = $vitri;
        $row[] = $website_feature->title;
        $row[] = '<span class="hidden">'.$website_feature->updated_at.'</span>'.date('d/m/Y', strtotime($website_feature->updated_at));
        $view = View::make('Backend/website_feature/_status', ['status' => $website_feature->status]);
        $row[] = $view->render();
        $view = View::make('Backend/website_feature/_actions', ['id' => $website_feature->id,'id_website_feature' => $website_feature->id_website_feature_cat,'page' => 'website_feature']);
        $row[] = $view->render();

        return $row;
    }
}
