<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\intro;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminintrosController extends BaseAdminController
{

    public function index (Request $request) {

        $intros = DB::table('intros')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '!=', 10],
            ])
            ->orderBy('id', 'asc')
            ->get();

        $output = [];
        foreach ($intros as $key => $intro) {
            $vitri = $key+1;
            $row = $this->GetRow($intro, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = intro::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'intro');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = intro::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                

                $update_values['title'] = $request->title;
                $update_values['title_en'] = $request->title_en;
                $update_values['content'] = $request->content;
                $update_values['content_en'] = $request->content_en;
                $update_values['summary'] = $request->summary;
                $update_values['summary_en'] = $request->summary_en;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                $update_values['meta_title'] = $request->meta_title;
                $update_values['meta_description'] = $request->meta_description;
                $this->DB_update($update_values, 'intros', $id);
                $edit_db = $this->EditDB($request->all(),'intro', $id);
        
                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'intro', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $intro = DB::table('intros')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$intro) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            intro::where([
                ['id', $intro->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.intro_path').$intro->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($intro, $vitri)
    {
        $row = [];
        $row[] = $intro->id;
        $row[] = $intro->position;
        $row[] = $vitri;
        $row[] = $intro->title;
        $row[] = '<span class="hidden">'.$intro->updated_at.'</span>'.date('d/m/Y', strtotime($intro->updated_at));
        $view = View::make('Backend/intro/_type', ['type' => $intro->type]);
        $row[] = $view->render();
        $view = View::make('Backend/intro/_actions', ['id' => $intro->id,'page' => 'intro']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $intro = intro::findOrFail($id);
            $intro['avatar'] = config('general.intro_url') . $intro['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $intro);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
