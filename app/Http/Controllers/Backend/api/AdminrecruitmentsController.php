<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\recruitment;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminrecruitmentsController extends BaseAdminController
{

    public function index (Request $request) {

        
        $recruitments = DB::table('recruitments')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($recruitments as $key => $recruitment) {
            $vitri = $key+1;
            $row = $this->GetRow($recruitment, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = recruitment::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'recruitment');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = recruitment::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_recruitment_cat1'] = $request->id_recruitment_cat1;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'recruitments', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'recruitment', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $recruitment = DB::table('recruitments')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$recruitment) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            recruitment::where([
                ['id', $recruitment->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.recruitment_path').$recruitment->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($recruitment, $vitri)
    {
        $row = [];
        $row[] = $recruitment->id;
        $row[] = $recruitment->position;
        $row[] = $vitri;
        $row[] = $recruitment->title;
        $row[] = $this->GetImg([
            'avatar'=> $recruitment->avatar,
            'data'=> 'recruitment',
            'time'=> $recruitment->updated_at
        ]);
        $row[] = '<span class="hidden">'.$recruitment->updated_at.'</span>'.date('d/m/Y', strtotime($recruitment->updated_at));
        $view = View::make('Backend/recruitment/_status', ['status' => $recruitment->status]);
        $row[] = $view->render();
        $view = View::make('Backend/recruitment/_actions', ['id' => $recruitment->id,'page' => 'recruitment']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $recruitment = recruitment::findOrFail($id);
            $recruitment['avatar'] = config('general.recruitment_url') . $recruitment['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $recruitment);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
