<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\intro;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminintro_countrysController extends BaseAdminController
{

    public function index (Request $request) {

        $intro_countrys = DB::table('intros')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '!=', 9],
            ])
            ->orderBy('id', 'desc')
            ->get();

        $output = [];
        foreach ($intro_countrys as $key => $intro_country) {
            $vitri = $key+1;
            $row = $this->GetRow($intro_country, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = intro::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'intro');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = intro::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];

                $update_values['title'] = $request->title;
                $update_values['title_en'] = $request->title_en;
                $update_values['slug'] = $request->slug;
                $update_values['content'] = $request->content;
                $update_values['content_en'] = $request->content_en;
                $update_values['summary'] = $request->summary;
                $update_values['summary_en'] = $request->summary_en;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                $update_values['meta_title'] = $request->meta_title;
                $update_values['meta_description'] = $request->meta_description;
                $this->DB_update($update_values, 'intros', $id);
                $edit_db = $this->EditDB($request->all(),'intro', $id);
        
                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'intro', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $intro_country = DB::table('intros')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$intro_country) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            intro::where([
                ['id', $intro_country->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.intro_path').$intro_country->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($intro_country, $vitri)
    {
        $row = [];
        $row[] = $intro_country->id;
        $row[] = $intro_country->position;
        $row[] = $vitri;
        $row[] = $intro_country->title;
        $row[] = '<span class="hidden">'.$intro_country->updated_at.'</span>'.date('d/m/Y', strtotime($intro_country->updated_at));
        $view = View::make('Backend/intro_country/_type', ['type' => $intro_country->type]);
        $row[] = $view->render();
        $view = View::make('Backend/intro_country/_actions', ['id' => $intro_country->id,'page' => 'intro_country']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $intro_country = intro::findOrFail($id);
            $intro_country['avatar'] = config('general.intro_url') . $intro_country['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $intro_country);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
