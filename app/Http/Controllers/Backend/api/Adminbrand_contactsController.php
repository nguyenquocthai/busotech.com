<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\brand_contact;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminbrand_contactsController extends BaseAdminController
{

    public function index (Request $request) {

        $brand_contacts = DB::table('brand_contacts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $brand_contacts = json_decode($brand_contacts, true);
        $export = View::make('Backend/brand_contact/_export', ['brand_contacts' => $brand_contacts]);
        
        $brand_contacts = DB::table('brand_contacts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $giatenmiens = DB::table('giatenmiens')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])
            ->get();
        $output = [];
        foreach ($brand_contacts as $key => $brand_contact) {
            

            $vitri = $key+1;
            $row = $this->GetRow($brand_contact, $vitri);
            $output[] = $row;
        }
        $data['code'] = 200;
        $data['export'] = $export->render();
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = brand_contact::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'brand_contact');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = brand_contact::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'brand_contact', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $brand_contact = DB::table('brand_contacts')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$brand_contact) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            brand_contact::where([
                ['id', $brand_contact->id]
            ])->update(['del_flg' => 1]);

            if (isset($brand_contact->avatar)) {
                $image_path = config('general.brand_contact_path').$brand_contact->avatar;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                
                }
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($brand_contact, $vitri)
    {
        $row = [];
        $row[] = $brand_contact->id;
        $row[] = $brand_contact->name;
        $row[] = $brand_contact->email;
        $row[] = $brand_contact->tel;
        $row[] = $brand_contact->created_at;
        $view = View::make('Backend/brand_contact/_status', ['status' => $brand_contact->status]);
        $row[] = $view->render();
        $view = View::make('Backend/brand_contact/_actions', ['id' => $brand_contact->id,'page' => 'brand_contact']);
        $row[] = $view->render();

        return $row;
    }
}
