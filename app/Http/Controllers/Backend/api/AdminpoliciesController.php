<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\policie;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminpoliciesController extends BaseAdminController
{

    public function index (Request $request) {

        
        $policies = DB::table('policies')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($policies as $key => $policie) {
            $vitri = $key+1;
            $row = $this->GetRow($policie, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = policie::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'policie');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = policie::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_policie_cat1'] = $request->id_policie_cat1;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'policies', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'policie', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $policie = DB::table('policies')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$policie) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            policie::where([
                ['id', $policie->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.policie_path').$policie->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($policie, $vitri)
    {
        $row = [];
        $row[] = $policie->id;
        $row[] = $policie->position;
        $row[] = $vitri;
        $row[] = $policie->title;
        // $row[] = $this->GetImg([
        //     'avatar'=> $policie->avatar,
        //     'data'=> 'policie',
        //     'time'=> $policie->updated_at
        // ]);
        $row[] = '<span class="hidden">'.$policie->updated_at.'</span>'.date('d/m/Y', strtotime($policie->updated_at));
        $view = View::make('Backend/policie/_status', ['status' => $policie->status]);
        $row[] = $view->render();
        $view = View::make('Backend/policie/_actions', ['id' => $policie->id,'page' => 'policie']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $policie = policie::findOrFail($id);
            $policie['avatar'] = config('general.policie_url') . $policie['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $policie);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
