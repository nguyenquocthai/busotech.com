<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\resource;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminresourcesController extends BaseAdminController
{

    public function index (Request $request) {

        
        $resources = DB::table('resources')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($resources as $key => $resource) {
            $vitri = $key+1;
            $row = $this->GetRow($resource, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = resource::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'resource');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = resource::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_resource_cat'] = $request->id_resource_cat;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'resources', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'resource', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $resource = DB::table('resources')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$resource) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            resource::where([
                ['id', $resource->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.resource_path').$resource->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($resource, $vitri)
    {
        $row = [];
        $row[] = $resource->id;
        $row[] = $resource->position;
        $row[] = $vitri;
        $row[] = $resource->title;
        $row[] = $this->GetImg([
            'avatar'=> $resource->avatar,
            'data'=> 'resource',
            'time'=> $resource->updated_at
        ]);
        $row[] = '<span class="hidden">'.$resource->updated_at.'</span>'.date('d/m/Y', strtotime($resource->updated_at));
        $view = View::make('Backend/resource/_status', ['status' => $resource->status]);
        $row[] = $view->render();
        $view = View::make('Backend/resource/_actions', ['id' => $resource->id,'page' => 'resource']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $resource = resource::findOrFail($id);
            $resource['avatar'] = config('general.resource_url') . $resource['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $resource);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
