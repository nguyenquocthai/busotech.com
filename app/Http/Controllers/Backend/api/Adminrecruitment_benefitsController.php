<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\recruitment_benefit;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminrecruitment_benefitsController extends BaseAdminController
{

    public function index (Request $request) {

        
        $recruitment_benefits = DB::table('recruitment_benefits')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($recruitment_benefits as $key => $recruitment_benefit) {
            $vitri = $key+1;
            $row = $this->GetRow($recruitment_benefit, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = recruitment_benefit::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'recruitment_benefit');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = recruitment_benefit::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_recruitment_benefit_cat1'] = $request->id_recruitment_benefit_cat1;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'recruitment_benefits', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'recruitment_benefit', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $recruitment_benefit = DB::table('recruitment_benefits')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$recruitment_benefit) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            recruitment_benefit::where([
                ['id', $recruitment_benefit->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.recruitment_benefit_path').$recruitment_benefit->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($recruitment_benefit, $vitri)
    {
        $row = [];
        $row[] = $recruitment_benefit->id;
        $row[] = $recruitment_benefit->position;
        $row[] = $vitri;
        $row[] = $recruitment_benefit->title;
        $row[] = $this->GetImg([
            'avatar'=> $recruitment_benefit->avatar,
            'data'=> 'recruitment_benefit',
            'time'=> $recruitment_benefit->updated_at
        ]);
        $row[] = '<span class="hidden">'.$recruitment_benefit->updated_at.'</span>'.date('d/m/Y', strtotime($recruitment_benefit->updated_at));
        $view = View::make('Backend/recruitment_benefit/_status', ['status' => $recruitment_benefit->status]);
        $row[] = $view->render();
        $view = View::make('Backend/recruitment_benefit/_actions', ['id' => $recruitment_benefit->id,'page' => 'recruitment_benefit']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $recruitment_benefit = recruitment_benefit::findOrFail($id);
            $recruitment_benefit['avatar'] = config('general.recruitment_benefit_url') . $recruitment_benefit['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $recruitment_benefit);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
