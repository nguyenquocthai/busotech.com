<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\brand_duan;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminbrand_duansController extends BaseAdminController
{

    public function index (Request $request) {

        
        $brand_duans = DB::table('brand_duans')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($brand_duans as $key => $brand_duan) {
            $vitri = $key+1;
            $row = $this->GetRow($brand_duan, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = brand_duan::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'brand_duan');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = brand_duan::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            // else if ($request->status_code == "edit_b1") {

            //     $update_values = [];
                
            //     $update_values['title'] = $request->title;
            //     $update_values['content'] = $request->content;
            //     $update_values['summary'] = $request->summary;
            //     $update_values['id_brand_duan_cat'] = $request->id_brand_duan_cat;
            //     $update_values['updated_at'] = date("Y-m-d H:i:s");
                

            //     $edit_db = $this->DB_update($update_values, 'brand_duans', $id);

            //     $data['code'] = 200;
            //     $data['message'] = 'Update ok.';
            //     return response()->json($data, 200);
            // }

            $edit_db = $this->EditDB($request->all(),'brand_duan', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $brand_duan = DB::table('brand_duans')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$brand_duan) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            brand_duan::where([
                ['id', $brand_duan->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.brand_duan_path').$brand_duan->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($brand_duan, $vitri)
    {
        $row = [];
        $row[] = $brand_duan->id;
        $row[] = $brand_duan->position;
        $row[] = $vitri;
        $row[] = $brand_duan->title;
        $row[] = $this->GetImg([
            'avatar'=> $brand_duan->avatar,
            'data'=> 'brand_duan',
            'time'=> $brand_duan->updated_at
        ]);
        $row[] = '<span class="hidden">'.$brand_duan->updated_at.'</span>'.date('d/m/Y', strtotime($brand_duan->updated_at));
        $view = View::make('Backend/brand_duan/_status', ['status' => $brand_duan->status]);
        $row[] = $view->render();
        $view = View::make('Backend/brand_duan/_actions', ['id' => $brand_duan->id,'page' => 'brand_duan']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $brand_duan = brand_duan::findOrFail($id);
            $brand_duan['avatar'] = config('general.brand_duan_url') . $brand_duan['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $brand_duan);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
