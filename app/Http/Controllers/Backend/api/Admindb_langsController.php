<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\db_lang;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Admindb_langsController extends BaseAdminController
{

    public function index (Request $request) {

        $db_langs = DB::table('db_langs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($db_langs as $key => $db_lang) {
            $vitri = $key+1;
            $row = $this->GetRow($db_lang, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = db_lang::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'db_lang');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = db_lang::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $db_value = $request->all();
            unset($db_value['value']['slug']);

            $edit_db = $this->EditDB($db_value,'db_lang', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $db_lang = DB::table('db_langs')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$db_lang) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            db_lang::where([
                ['id', $db_lang->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.db_lang_path').$db_lang->avatar;
            rename($image_path, config('general.db_lang_path')."del_".$db_lang->avatar);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($db_lang, $vitri)
    {
        $row = [];
        $row[] = $db_lang->id;
        $row[] = $db_lang->position;
        $row[] = $vitri;
        $row[] = $db_lang->title;
        $row[] = $db_lang->title_en;
        $row[] = $db_lang->slug;
        // $view = View::make('Backend/db_lang/_status', ['status' => $db_lang->status]);
        // $row[] = $view->render();
        $view = View::make('Backend/db_lang/_actions', ['id' => $db_lang->id,'page' => 'db_lang']);
        $row[] = $view->render();

        return $row;
    }
}
