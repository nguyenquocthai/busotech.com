<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\product_cat;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminproduct_catsController extends BaseAdminController
{

    public function index (Request $request) {

        $product_cats = DB::table('product_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        

        $output = [];
        foreach ($product_cats as $key => $product_cat) {
            $vitri = $key+1;
            $row = $this->GetRow($product_cat, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = product_cat::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'product_cat');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = product_cat::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                

                $update_values['title'] = $request->title;
                
                $update_values['summary'] = $request->summary;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'product_cats', $id);
                 
                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'product_cat', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {
            $product_cat = product_cat::findOrFail($id);
            $product_cat['avatar'] = config('general.product_cat_url') . $product_cat['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $product_cat);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $product_cat = DB::table('product_cats')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$product_cat) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            product_cat::where([
                ['id', $product_cat->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.product_cat_path').$product_cat->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($product_cat, $vitri)
    {
        $row = [];
        $row[] = $product_cat->id;
        $row[] = $product_cat->position;
        $row[] = $vitri;
        $row[] = $product_cat->title;
        
        $row[] = '<span class="hidden">'.$product_cat->updated_at.'</span>'.date('d/m/Y', strtotime($product_cat->updated_at));
        $view = View::make('Backend/product_cat/_status', ['status' => $product_cat->status]);
        $row[] = $view->render();
        $view = View::make('Backend/product_cat/_actions', ['id' => $product_cat->id,'page' => 'product_cat']);
        $row[] = $view->render();

        return $row;
    }
}
