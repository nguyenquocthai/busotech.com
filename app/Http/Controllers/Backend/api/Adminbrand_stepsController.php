<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\brand_step;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminbrand_stepsController extends BaseAdminController
{

    public function index (Request $request) {

        
        $brand_steps = DB::table('brand_steps')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($brand_steps as $key => $brand_step) {
            $vitri = $key+1;
            $row = $this->GetRow($brand_step, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = brand_step::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'brand_step');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = brand_step::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            // else if ($request->status_code == "edit_b1") {

            //     $update_values = [];
                
            //     $update_values['title'] = $request->title;
            //     $update_values['content'] = $request->content;
            //     $update_values['summary'] = $request->summary;
            //     $update_values['id_brand_step_cat'] = $request->id_brand_step_cat;
            //     $update_values['updated_at'] = date("Y-m-d H:i:s");
                

            //     $edit_db = $this->DB_update($update_values, 'brand_steps', $id);

            //     $data['code'] = 200;
            //     $data['message'] = 'Update ok.';
            //     return response()->json($data, 200);
            // }

            $edit_db = $this->EditDB($request->all(),'brand_step', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $brand_step = DB::table('brand_steps')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$brand_step) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            brand_step::where([
                ['id', $brand_step->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.brand_step_path').$brand_step->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($brand_step, $vitri)
    {
        $row = [];
        $row[] = $brand_step->id;
        $row[] = $brand_step->position;
        $row[] = $vitri;
        $row[] = $brand_step->title;
        $row[] = $this->GetImg([
            'avatar'=> $brand_step->avatar,
            'data'=> 'brand_step',
            'time'=> $brand_step->updated_at
        ]);
        $row[] = '<span class="hidden">'.$brand_step->updated_at.'</span>'.date('d/m/Y', strtotime($brand_step->updated_at));
        $view = View::make('Backend/brand_step/_status', ['status' => $brand_step->status]);
        $row[] = $view->render();
        $view = View::make('Backend/brand_step/_actions', ['id' => $brand_step->id,'page' => 'brand_step']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $brand_step = brand_step::findOrFail($id);
            $brand_step['avatar'] = config('general.brand_step_url') . $brand_step['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $brand_step);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
