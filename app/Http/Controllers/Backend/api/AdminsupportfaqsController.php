<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\supportfaq;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminsupportfaqsController extends BaseAdminController
{

    public function index (Request $request) {

        
        $supportfaqs = DB::table('supportfaqs')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($supportfaqs as $key => $supportfaq) {
            $vitri = $key+1;
            $row = $this->GetRow($supportfaq, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = supportfaq::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'supportfaq');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = supportfaq::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_supportfaq_cat1'] = $request->id_supportfaq_cat1;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'supportfaqs', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'supportfaq', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $supportfaq = DB::table('supportfaqs')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$supportfaq) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            supportfaq::where([
                ['id', $supportfaq->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.supportfaq_path').$supportfaq->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($supportfaq, $vitri)
    {
        $row = [];
        $row[] = $supportfaq->id;
        $row[] = $supportfaq->position;
        $row[] = $vitri;
        $row[] = $supportfaq->title;
        $row[] = $this->GetImg([
            'avatar'=> $supportfaq->avatar,
            'data'=> 'supportfaq',
            'time'=> $supportfaq->updated_at
        ]);
        $row[] = '<span class="hidden">'.$supportfaq->updated_at.'</span>'.date('d/m/Y', strtotime($supportfaq->updated_at));
        $view = View::make('Backend/supportfaq/_status', ['status' => $supportfaq->status]);
        $row[] = $view->render();
        $view = View::make('Backend/supportfaq/_actions', ['id' => $supportfaq->id,'page' => 'supportfaq']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $supportfaq = supportfaq::findOrFail($id);
            $supportfaq['avatar'] = config('general.supportfaq_url') . $supportfaq['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $supportfaq);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
