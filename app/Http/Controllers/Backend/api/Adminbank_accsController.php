<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\bank_acc;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminbank_accsController extends BaseAdminController
{

    public function index (Request $request) {

        $bank_accs = DB::table('bank_accs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'DESC')
            ->get();

        $output = [];
        foreach ($bank_accs as $key => $bank_acc) {
            $vitri = $key+1;
            $row = $this->GetRow($bank_acc, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = bank_acc::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'bank_acc');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = bank_acc::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'bank_acc', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $bank_acc = DB::table('bank_accs')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$bank_acc) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            bank_acc::where([
                ['id', $bank_acc->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.bank_acc_path').$bank_acc->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($bank_acc, $vitri)
    {
        $row = [];
        $row[] = $bank_acc->id;
        $row[] = $bank_acc->position;
        $row[] = $vitri;
        $row[] = $bank_acc->title;
        $row[] = '<span class="hidden">'.$bank_acc->updated_at.'</span>'.date('d/m/Y', strtotime($bank_acc->updated_at));
        $view = View::make('Backend/bank_acc/_status', ['status' => $bank_acc->status]);
        $row[] = $view->render();
        $view = View::make('Backend/bank_acc/_actions', ['id' => $bank_acc->id,'page' => 'bank_acc']);
        $row[] = $view->render();

        return $row;
    }
}
