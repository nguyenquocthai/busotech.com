<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\service;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminservicesController extends BaseAdminController
{

    public function index (Request $request) {

        
        $services = DB::table('services')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($services as $key => $service) {
            $vitri = $key+1;
            $row = $this->GetRow($service, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = service::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'service');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = service::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_service_cat'] = $request->id_service_cat;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'services', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'service', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $service = DB::table('services')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$service) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            service::where([
                ['id', $service->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.service_path').$service->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($service, $vitri)
    {
        $row = [];
        $row[] = $service->id;
        $row[] = $service->position;
        $row[] = $vitri;
        $row[] = $service->title;
        $row[] = $this->GetImg([
            'avatar'=> $service->avatar,
            'data'=> 'service',
            'time'=> $service->updated_at
        ]);
        $row[] = '<span class="hidden">'.$service->updated_at.'</span>'.date('d/m/Y', strtotime($service->updated_at));
        $view = View::make('Backend/service/_status', ['status' => $service->status]);
        $row[] = $view->render();
        $view = View::make('Backend/service/_actions', ['id' => $service->id,'page' => 'service']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $service = service::findOrFail($id);
            $service['avatar'] = config('general.service_url') . $service['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $service);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
