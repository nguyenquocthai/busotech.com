<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\slider_blog;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminslider_blogsController extends BaseAdminController
{

    public function index (Request $request) {
        
        $slider_blogs = DB::table('slider_blogs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();


        $output = [];
        foreach ($slider_blogs as $key => $slider_blog) {
            $vitri = $key+1;
            $row = $this->GetRow($slider_blog, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = slider_blog::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'slider_blog');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = slider_blog::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'slider_blog', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $slider_blog = DB::table('slider_blogs')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$slider_blog) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            slider_blog::where([
                ['id', $slider_blog->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.slider_blog_path').$slider_blog->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($slider_blog, $vitri)
    {
        $row = [];
        $row[] = $slider_blog->id;
        $row[] = $slider_blog->position;
        $row[] = $vitri;
        $row[] = $slider_blog->title;
        $row[] = $this->GetImg([
            'avatar'=> $slider_blog->avatar,
            'data'=> 'slider_blog',
            'time'=> $slider_blog->updated_at
        ]);
        $row[] = '<span class="hidden">'.$slider_blog->updated_at.'</span>'.date('d/m/Y', strtotime($slider_blog->updated_at));

        
        $view = View::make('Backend/slider_blog/_status', ['status' => $slider_blog->status]);
        $row[] = $view->render();
        $view = View::make('Backend/slider_blog/_actions', ['id' => $slider_blog->id,'page' => 'slider_blog']);
        $row[] = $view->render();
        

        return $row;
    }
}
