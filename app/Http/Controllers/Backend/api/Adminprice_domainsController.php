<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\price_domain;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminprice_domainsController extends BaseAdminController
{

    public function index (Request $request) {

        $price_domains = DB::table('price_domains')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'DESC')
            ->get();

        $output = [];
        foreach ($price_domains as $key => $price_domain) {
            $vitri = $key+1;
            $row = $this->GetRow($price_domain, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = price_domain::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'price_domain');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = price_domain::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'price_domain', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $price_domain = DB::table('price_domains')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$price_domain) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            price_domain::where([
                ['id', $price_domain->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.price_domain_path').$price_domain->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($price_domain, $vitri)
    {
        $row = [];
        $row[] = $price_domain->id;
        $row[] = $price_domain->position;
        $row[] = $vitri;
        $row[] = $price_domain->title;
        $row[] = $price_domain->gia;
        $row[] = '<span class="hidden">'.$price_domain->updated_at.'</span>'.date('d/m/Y', strtotime($price_domain->updated_at));
        $view = View::make('Backend/price_domain/_status', ['status' => $price_domain->status]);
        $row[] = $view->render();
        $view = View::make('Backend/price_domain/_actions', ['id' => $price_domain->id,'page' => 'price_domain']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $price_domain = price_domain::findOrFail($id);
            $price_domain['avatar'] = config('general.price_domain_url') . $price_domain['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $price_domain);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
