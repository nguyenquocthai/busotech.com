<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\album;
use App\Model\ItemAlbum;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Extensions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminalbumsController extends BaseAdminController
{

    public function index (Request $request) {

        
        $albums = DB::table('albums')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($albums as $key => $album) {
            $vitri = $key+1;
            $row = $this->GetRow($album, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = album::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'album');
           
            $files = $request->albums;
            if ($files != "undefined" && $files != "") {
                foreach ($files as $file) {
                    $avatar = $file;
                    $option['file_name'] = '';
                    $option['path'] = config('general.item_album_path');
                    $option['url'] = config('general.item_album_url');

                    $callback = ShopUpload::upload_image($option, $avatar);

                    DB::table('item_albums')->insert(
                        [
                            'name' => $callback['name'],
                            'item_id' => $save_db['id'],
                            'type' => 1
                        ]
                    );
                }
            }

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {
            
            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = album::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_album_cat'] = $request->id_album_cat;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'albums', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            
            //upload item album
            
            $files = $request->albums;
            if ($files != "undefined" && $files != "") {
                foreach ($files as $file) {
                    $avatar = $file;
                    $option['file_name'] = '';
                    $option['path'] = config('general.item_album_path');
                    $option['url'] = config('general.item_album_url');

                    $callback = ShopUpload::upload_image($option, $avatar);

                    DB::table('item_albums')->insert(
                        [
                            'name' => $callback['name'],
                            'item_id' => $id,
                            'type' => 1
                        ]
                    );
                }
            }
            

            $edit_db = $this->EditDB($request->all(),'album', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $album = DB::table('albums')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$album) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            album::where([
                ['id', $album->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.album_path').$album->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($album, $vitri)
    {
        $row = [];
        $row[] = $album->id;
        $row[] = $album->position;
        $row[] = $vitri;
        $row[] = $album->title;
        $row[] = $this->GetImg([
            'avatar'=> $album->avatar,
            'data'=> 'album',
            'time'=> $album->updated_at
        ]);
        $row[] = '<span class="hidden">'.$album->updated_at.'</span>'.date('d/m/Y', strtotime($album->updated_at));
        $view = View::make('Backend/album/_status', ['status' => $album->status]);
        $row[] = $view->render();
        $view = View::make('Backend/album/_actions', ['id' => $album->id,'page' => 'album']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $album = album::findOrFail($id);
            $album['avatar'] = config('general.album_url') . $album['avatar'];

            $item_albums = DB::table('item_albums')
                ->select('*')
                ->where([
                    ['item_id', '=', $id],
                    ['del_flg', '=', 0],
                    ['type', '=', 1],
                ])
                ->orderBy('id', 'desc')
                ->get();

            $view_albums = View::make('/Backend/album/_album', ['item_albums' => $item_albums]);

            $album['albums'] = $view_albums->render();

            $this->resp(ErrorCodes::E_OK, null, $album);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }

    
}
