<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\duan;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminduansController extends BaseAdminController
{

    public function index (Request $request) {
        
        $duans = DB::table('duans')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();


        $output = [];
        foreach ($duans as $key => $duan) {
            $vitri = $key+1;
            $row = $this->GetRow($duan, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = duan::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'duan');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = duan::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'duan', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $duan = DB::table('duans')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$duan) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            duan::where([
                ['id', $duan->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.duan_path').$duan->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($duan, $vitri)
    {
        $row = [];
        $row[] = $duan->id;
        $row[] = $duan->position;
        $row[] = $vitri;
        $row[] = $duan->title;
        $row[] = $this->GetImg([
            'avatar'=> $duan->avatar,
            'data'=> 'duan',
            'time'=> $duan->updated_at
        ]);
        $row[] = '<span class="hidden">'.$duan->updated_at.'</span>'.date('d/m/Y', strtotime($duan->updated_at));
        
        $view = View::make('Backend/duan/_status', ['status' => $duan->status]);
        $row[] = $view->render();
        $view = View::make('Backend/duan/_actions', ['id' => $duan->id,'page' => 'duan','id_cate' => $duan->id_duan_cat]);
        $row[] = $view->render();
        

        return $row;
    }
}
