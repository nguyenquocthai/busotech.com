<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\giatenmien;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdmingiatenmiensController extends BaseAdminController
{

    public function index (Request $request) {

        
        $giatenmiens = DB::table('giatenmiens')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($giatenmiens as $key => $giatenmien) {
            $vitri = $key+1;
            $row = $this->GetRow($giatenmien, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = giatenmien::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'giatenmien');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = giatenmien::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_giatenmien_cat'] = $request->id_giatenmien_cat;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'giatenmiens', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'giatenmien', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $giatenmien = DB::table('giatenmiens')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$giatenmien) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            giatenmien::where([
                ['id', $giatenmien->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.giatenmien_path').$giatenmien->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($giatenmien, $vitri)
    {
        $row = [];
        $row[] = $giatenmien->id;
        $row[] = $giatenmien->position;
        $row[] = $vitri;
        $row[] = $giatenmien->title;
        $row[] = $giatenmien->gia;
        $row[] = '<span class="hidden">'.$giatenmien->updated_at.'</span>'.date('d/m/Y', strtotime($giatenmien->updated_at));
        $view = View::make('Backend/giatenmien/_status', ['status' => $giatenmien->status]);
        $row[] = $view->render();
        $view = View::make('Backend/giatenmien/_actions', ['id' => $giatenmien->id,'page' => 'giatenmien']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $giatenmien = giatenmien::findOrFail($id);
            $giatenmien['avatar'] = config('general.giatenmien_url') . $giatenmien['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $giatenmien);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
