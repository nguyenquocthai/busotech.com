<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\item_album;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminitem_albumsController extends BaseAdminController
{

    public function index (Request $request) {

        $item_albums = DB::table('item_albums')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();

        $output = [];
        foreach ($item_albums as $key => $item_album) {
            $vitri = $key+1;
            $row = $this->GetRow($item_album, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = item_album::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'item_album');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = item_album::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];

                $update_values['title'] = $request->title;
                $update_values['link_youtube'] = $request->link_youtube;
                $update_values['content'] = $request->content;
                $update_values['short_content'] = $request->short_content;
                $update_values['updated_at'] = date("Y-m-d H:i:s");

                $this->DB_update($update_values, 'item_albums', $id);
                $edit_db = $this->EditDB($request->all(),'item_album', $id);

                
        
                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'item_album', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $item_album = DB::table('item_albums')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$item_album) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            item_album::where([
                ['id', $item_album->id]
            ])->update(['del_flg' => 1]);
            
             $image_path = config('general.item_album_path').$item_album->name;
             if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            // rename($image_path, config('general.item_album_path')."del_".$item_album->name);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($item_album, $vitri)
    {
        $row = [];
        $row[] = $item_album->id;
        $row[] = $item_album->position;
        $row[] = $vitri;
        $row[] = $item_album->title;
        
        $row[] = $this->GetImg([
            'avatar'=> $item_album->link_youtube,
            'data'=> 'item_album',
            'time'=> $item_album->updated_at
        ]);
        $row[] = '<span class="hidden">'.$item_album->updated_at.'</span>'.date('d/m/Y', strtotime($item_album->updated_at));
        $view = View::make('Backend/item_album/_status', ['status' => $item_album->status]);
        $row[] = $view->render();
        $view = View::make('Backend/item_album/_actions', ['id' => $item_album->id,'page' => 'item_album']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $item_album = item_album::findOrFail($id);
            $item_album['avatar'] = config('general.item_album_url') . $item_album['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $item_album);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
