<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\customer;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdmincustomersController extends BaseAdminController
{

    public function index (Request $request) {

        $customers = DB::table('customers')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();

        $output = [];
        foreach ($customers as $key => $customer) {
            $vitri = $key+1;
            $row = $this->GetRow($customer, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = customer::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'customer');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = customer::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'customer', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $customer = DB::table('customers')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$customer) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            customer::where([
                ['id', $customer->id]
            ])->update(['del_flg' => 1]);

            if (isset($customer->avatar)) {
                $image_path = config('general.customer_path').$customer->avatar;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                    
                }
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($customer, $vitri)
    {
        $row = [];
        $row[] = $customer->id;
        $row[] = $customer->id_khach;
        $row[] = $customer->email_contact;
        $row[] = $customer->phone_contact;
        $row[] = $customer->created_at;
        $view = View::make('Backend/customer/_status', ['status' => $customer->status]);
        $row[] = $view->render();
        $view = View::make('Backend/customer/_actions', ['id' => $customer->id,'page' => 'customer']);
        $row[] = $view->render();

        return $row;
    }
}
