<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\video;
use App\Model\ItemAlbum;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Extensions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminvideosController extends BaseAdminController
{

    public function index (Request $request) {

        
        $videos = DB::table('videos')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($videos as $key => $video) {
            $vitri = $key+1;
            $row = $this->GetRow($video, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = video::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'video');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = video::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['slug'] = $request->slug;
                $update_values['content'] = @$request->content;
                $update_values['summary'] = @$request->summary;
                $update_values['id_video_cat'] = @$request->id_video_cat;
                $update_values['updated_at'] = date("Y-m-d H:i:s");

                $edit_db = $this->DB_update($update_values, 'videos', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'video', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $video = DB::table('videos')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$video) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            video::where([
                ['id', $video->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.video_path').$video->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($video, $vitri)
    {
        $row = [];
        $row[] = $video->id;
        $row[] = $video->position;
        $row[] = $vitri;
        $row[] = $video->title;
        
        $row[] = '<span class="hidden">'.$video->updated_at.'</span>'.date('d/m/Y', strtotime($video->updated_at));
        $view = View::make('Backend/video/_status', ['status' => $video->status]);
        $row[] = $view->render();
        $view = View::make('Backend/video/_actions', ['id' => $video->id,'page' => 'video']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $video = video::findOrFail($id);
            $video['avatar'] = config('general.video_url') . $video['avatar'];
            
            $this->resp(ErrorCodes::E_OK, null, $video);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
