<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\partner_contact;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminpartner_contactsController extends BaseAdminController
{

    public function index (Request $request) {

        $partner_contacts = DB::table('partner_contacts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $partner_contacts = json_decode($partner_contacts, true);
        $export = View::make('Backend/partner_contact/_export', ['partner_contacts' => $partner_contacts]);
        
        $partner_contacts = DB::table('partner_contacts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        
        $output = [];
        foreach ($partner_contacts as $key => $partner_contact) {
            $vitri = $key+1;
            $row = $this->GetRow($partner_contact, $vitri);
            $output[] = $row;
        }
        $data['code'] = 200;
        $data['export'] = $export->render();
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = partner_contact::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'partner_contact');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = partner_contact::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'partner_contact', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $partner_contact = DB::table('partner_contacts')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$partner_contact) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            partner_contact::where([
                ['id', $partner_contact->id]
            ])->update(['del_flg' => 1]);

            if (isset($partner_contact->avatar)) {
                $image_path = config('general.partner_contact_path').$partner_contact->avatar;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                
                }
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($partner_contact, $vitri)
    {
        $row = [];
        $row[] = $partner_contact->id;
        $row[] = $partner_contact->name;
        $row[] = $partner_contact->email;
        $row[] = $partner_contact->tel;
        $row[] = $partner_contact->created_at;
        $view = View::make('Backend/partner_contact/_status', ['status' => $partner_contact->status]);
        $row[] = $view->render();
        $view = View::make('Backend/partner_contact/_actions', ['id' => $partner_contact->id,'page' => 'partner_contact']);
        $row[] = $view->render();

        return $row;
    }
}
