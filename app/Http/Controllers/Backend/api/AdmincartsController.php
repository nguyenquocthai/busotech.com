<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\cart;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdmincartsController extends BaseAdminController
{

    public function index (Request $request) {

        $carts = DB::table('carts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();

        $output = [];
        foreach ($carts as $key => $cart) {
            $vitri = $key+1;
            $row = $this->GetRow($cart, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {

            $khachhang = DB::table('carts')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id', '=', $id]
                ])
                ->first();

            // DB::table('carts')
            //     ->where('id', $id)
            //     ->update(['status' => 1]);

            $carts = DB::table('cart_details')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['id_cart', '=', $id]
                    
                ])
                ->orderBy('id', 'desc')
                ->get();

            foreach ($carts as $key => $cart) {
                $product = DB::table('products')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['status', '=', 0],
                        ['id', '=', $cart->id_product]
                    ])
                    ->first();

                $carts[$key]->product = $product;
            }

            $view = View::make('Backend/cart/_items', ['carts' => $carts, 'khachhang' => $khachhang]);

            $data['code'] = 200;
            $data['data'] = $view->render();
            return response()->json($data, 200);
            
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
   

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                // $error_validate = cart::validate($id);
                // $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                // if ($validator->fails()) {
                //     $data['code'] = 300;
                //     $data['error'] = $validator->errors();
                //     return response()->json($data, 200);
                // }
            }

            $edit_db = $this->EditDB($request->all(),'cart', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $cart = DB::table('carts')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$cart) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            cart::where([
                ['id', $cart->id]
            ])->update(['del_flg' => 1]);

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($cart, $vitri)
    {
        $row = [];
        $row[] = $cart->id;
        $row[] = $cart->position;
        $row[] = $vitri;
        $row[] = '<a href="/admin/cart/edit/'.$cart->id.'" title="">'.$cart->name.'</a>';
        $row[] = $cart->phone;
        $row[] = $cart->email;
        $row[] = '<span class="hidden">'.$cart->updated_at.'</span>'.date('d/m/Y', strtotime($cart->updated_at));
        $row[] = $cart->payment;
        $view = View::make('Backend/cart/_status', ['status' => $cart->status]);
        $row[] = $view->render();
        $view = View::make('Backend/cart/_actions', ['id' => $cart->id,'page' => 'cart']);
        $row[] = $view->render();

        return $row;
    }
}
