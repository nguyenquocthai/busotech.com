<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\support;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminsupportsController extends BaseAdminController
{

    public function index (Request $request) {

        
        $supports = DB::table('supports')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($supports as $key => $support) {
            $vitri = $key+1;
            $row = $this->GetRow($support, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = support::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'support');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = support::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_support_cat1'] = $request->id_support_cat1;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'supports', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'support', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $support = DB::table('supports')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$support) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            support::where([
                ['id', $support->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.support_path').$support->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($support, $vitri)
    {
        $row = [];
        $row[] = $support->id;
        $row[] = $support->position;
        $row[] = $vitri;
        $row[] = $support->title;
        $row[] = $this->GetImg([
            'avatar'=> $support->avatar,
            'data'=> 'support',
            'time'=> $support->updated_at
        ]);
        $row[] = '<span class="hidden">'.$support->updated_at.'</span>'.date('d/m/Y', strtotime($support->updated_at));
        $view = View::make('Backend/support/_status', ['status' => $support->status]);
        $row[] = $view->render();
        $view = View::make('Backend/support/_actions', ['id' => $support->id,'page' => 'support']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $support = support::findOrFail($id);
            $support['avatar'] = config('general.support_url') . $support['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $support);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
