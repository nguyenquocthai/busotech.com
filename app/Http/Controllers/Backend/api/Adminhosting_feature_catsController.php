<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\hosting_feature_cat;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminhosting_feature_catsController extends BaseAdminController
{

    public function index (Request $request) {

        $hosting_feature_cats = DB::table('hosting_feature_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'DESC')
            ->get();

        $output = [];
        foreach ($hosting_feature_cats as $key => $hosting_feature_cat) {
            $vitri = $key+1;
            $row = $this->GetRow($hosting_feature_cat, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = hosting_feature_cat::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'hosting_feature_cat');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = hosting_feature_cat::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'hosting_feature_cat', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $hosting_feature_cat = DB::table('hosting_feature_cats')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$hosting_feature_cat) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            hosting_feature_cat::where([
                ['id', $hosting_feature_cat->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.hosting_feature_cat_path').$hosting_feature_cat->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($hosting_feature_cat, $vitri)
    {
        $row = [];
        $row[] = $hosting_feature_cat->id;
        $row[] = $hosting_feature_cat->position;
        $row[] = $vitri;
        $row[] = $hosting_feature_cat->title;
        $row[] = '<span class="hidden">'.$hosting_feature_cat->updated_at.'</span>'.date('d/m/Y', strtotime($hosting_feature_cat->updated_at));
        $view = View::make('Backend/hosting_feature_cat/_status', ['status' => $hosting_feature_cat->status]);
        $row[] = $view->render();
        $view = View::make('Backend/hosting_feature_cat/_actions', ['id' => $hosting_feature_cat->id,'page' => 'hosting_feature_cat']);
        $row[] = $view->render();

        return $row;
    }
}
