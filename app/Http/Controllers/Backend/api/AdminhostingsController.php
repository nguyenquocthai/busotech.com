<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\hosting;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminhostingsController extends BaseAdminController
{

    public function index (Request $request) {

        $hostings = DB::table('hostings')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'DESC')
            ->get();

        $output = [];
        foreach ($hostings as $key => $hosting) {
            $vitri = $key+1;
            $row = $this->GetRow($hosting, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = hosting::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'hosting');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = hosting::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'hosting', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $hosting = DB::table('hostings')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$hosting) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            hosting::where([
                ['id', $hosting->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.hosting_path').$hosting->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($hosting, $vitri)
    {
        $row = [];
        $row[] = $hosting->id;
        $row[] = $hosting->position;
        $row[] = $vitri;
        $row[] = $hosting->title;
        $row[] = $hosting->gia;
        $row[] = '<span class="hidden">'.$hosting->updated_at.'</span>'.date('d/m/Y', strtotime($hosting->updated_at));
        $view = View::make('Backend/hosting/_status', ['status' => $hosting->status]);
        $row[] = $view->render();
        $view = View::make('Backend/hosting/_actions', ['id' => $hosting->id,'page' => 'hosting']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $hosting = hosting::findOrFail($id);
            $hosting['avatar'] = config('general.hosting_url') . $hosting['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $hosting);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
