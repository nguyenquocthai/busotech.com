<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\payment_guide;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminpayment_guidesController extends BaseAdminController
{

    public function index (Request $request) {

        
        $payment_guides = DB::table('payment_guides')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'asc')
            ->get();

        $output = [];
        foreach ($payment_guides as $key => $payment_guide) {
            $vitri = $key+1;
            $row = $this->GetRow($payment_guide, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = payment_guide::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'payment_guide');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = payment_guide::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            

            $edit_db = $this->EditDB($request->all(),'payment_guide', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $payment_guide = DB::table('payment_guides')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$payment_guide) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            payment_guide::where([
                ['id', $payment_guide->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.payment_guide_path').$payment_guide->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($payment_guide, $vitri)
    {
        $row = [];
        $row[] = $payment_guide->id;
        $row[] = $payment_guide->position;
        $row[] = $vitri;
        $row[] = $payment_guide->title;
        // $row[] = $this->GetImg([
        //     'avatar'=> $payment_guide->avatar,
        //     'data'=> 'payment_guide',
        //     'time'=> $payment_guide->updated_at
        // ]);
        $row[] = '<span class="hidden">'.$payment_guide->updated_at.'</span>'.date('d/m/Y', strtotime($payment_guide->updated_at));
        $view = View::make('Backend/payment_guide/_status', ['status' => $payment_guide->status]);
        $row[] = $view->render();
        $view = View::make('Backend/payment_guide/_actions', ['id' => $payment_guide->id,'page' => 'payment_guide']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $payment_guide = payment_guide::findOrFail($id);
            $payment_guide['avatar'] = config('general.payment_guide_url') . $payment_guide['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $payment_guide);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
