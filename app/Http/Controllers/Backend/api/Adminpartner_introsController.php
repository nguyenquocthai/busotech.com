<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\partner_intro;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminpartner_introsController extends BaseAdminController
{

    public function index (Request $request) {

        $partner_intros = DB::table('partner_intros')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'DESC')
            ->get();

        $output = [];
        foreach ($partner_intros as $key => $partner_intro) {
            $vitri = $key+1;
            $row = $this->GetRow($partner_intro, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = partner_intro::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'partner_intro');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = partner_intro::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'partner_intro', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $partner_intro = DB::table('partner_intros')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$partner_intro) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            partner_intro::where([
                ['id', $partner_intro->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.partner_intro_path').$partner_intro->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($partner_intro, $vitri)
    {
        $row = [];
        $row[] = $partner_intro->id;
        $row[] = $partner_intro->position;
        $row[] = $vitri;
        $row[] = $partner_intro->title;
        $row[] = '<span class="hidden">'.$partner_intro->updated_at.'</span>'.date('d/m/Y', strtotime($partner_intro->updated_at));
        $view = View::make('Backend/partner_intro/_status', ['status' => $partner_intro->status]);
        $row[] = $view->render();
        $view = View::make('Backend/partner_intro/_actions', ['id' => $partner_intro->id,'page' => 'partner_intro']);
        $row[] = $view->render();

        return $row;
    }
}
