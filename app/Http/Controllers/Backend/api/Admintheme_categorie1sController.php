<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\theme_categorie1;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Admintheme_categorie1sController extends BaseAdminController
{

    public function index (Request $request) {

        
        $theme_categorie1s = DB::table('theme_categorie1s')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($theme_categorie1s as $key => $theme_categorie1) {
            $vitri = $key+1;
            $row = $this->GetRow($theme_categorie1, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = theme_categorie1::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'theme_categorie1');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = theme_categorie1::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_theme_categorie1_cat'] = $request->id_theme_categorie1_cat;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'theme_categorie1s', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'theme_categorie1', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $theme_categorie1 = DB::table('theme_categorie1s')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$theme_categorie1) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            theme_categorie1::where([
                ['id', $theme_categorie1->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.theme_categorie1_path').$theme_categorie1->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($theme_categorie1, $vitri)
    {
        $row = [];
        $row[] = $theme_categorie1->id;
        $row[] = $theme_categorie1->position;
        $row[] = $vitri;
        $row[] = $theme_categorie1->title;
        
        $row[] = '<span class="hidden">'.$theme_categorie1->updated_at.'</span>'.date('d/m/Y', strtotime($theme_categorie1->updated_at));
        $view = View::make('Backend/theme_categorie1/_status', ['status' => $theme_categorie1->status]);
        $row[] = $view->render();
        $view = View::make('Backend/theme_categorie1/_actions', ['id' => $theme_categorie1->id,'page' => 'theme_categorie1','id_theme_cat' => $theme_categorie1->id_theme_cat]);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $theme_categorie1 = theme_categorie1::findOrFail($id);
            $theme_categorie1['avatar'] = config('general.theme_categorie1_url') . $theme_categorie1['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $theme_categorie1);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
