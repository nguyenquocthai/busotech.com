<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\product;
use App\Model\ItemAlbum;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Extensions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminproductsController extends BaseAdminController
{

    public function index (Request $request) {

        
        $products = DB::table('products')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($products as $key => $product) {
            $vitri = $key+1;
            $row = $this->GetRow($product, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = product::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'product');
           
            $files = $request->albums;
            if ($files != "undefined" && $files != "") {
                foreach ($files as $file) {
                    $avatar = $file;
                    $option['file_name'] = '';
                    $option['path'] = config('general.item_album_path');
                    $option['url'] = config('general.item_album_url');

                    $callback = ShopUpload::upload_image($option, $avatar);

                    DB::table('item_albums')->insert(
                        [
                            'name' => $callback['name'],
                            'item_id' => $save_db['id'],
                            'type' => 0
                        ]
                    );
                }
            }

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {
            
            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = product::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                // validate
                $error_validate = product::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['noibat'] = $request->noibat;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_product_cat'] = $request->id_product_cat;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'products', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            
            //upload item album
            
            $files = $request->albums;
            if ($files != "undefined" && $files != "") {
                foreach ($files as $file) {
                    $avatar = $file;
                    $option['file_name'] = '';
                    $option['path'] = config('general.item_album_path');
                    $option['url'] = config('general.item_album_url');

                    $callback = ShopUpload::upload_image($option, $avatar);

                    DB::table('item_albums')->insert(
                        [
                            'name' => $callback['name'],
                            'item_id' => $id,
                            'type' => 0
                        ]
                    );
                }
            }
            

            $edit_db = $this->EditDB($request->all(),'product', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $product = DB::table('products')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$product) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            product::where([
                ['id', $product->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.product_path').$product->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($product, $vitri)
    {
        $row = [];
        $row[] = $product->id;
        $row[] = $product->position;
        $row[] = $vitri;
        $row[] = $product->title;
        $row[] = $this->GetImg([
            'avatar'=> $product->avatar,
            'data'=> 'product',
            'time'=> $product->updated_at
        ]);
        $row[] = '<span class="hidden">'.$product->updated_at.'</span>'.date('d/m/Y', strtotime($product->updated_at));
        $view = View::make('Backend/product/_status', ['status' => $product->status]);
        $row[] = $view->render();
        $view = View::make('Backend/product/_actions', ['id' => $product->id,'page' => 'product']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $product = product::findOrFail($id);
            $product['avatar'] = config('general.product_url') . $product['avatar'];

            $item_albums = DB::table('item_albums')
                ->select('*')
                ->where([
                    ['item_id', '=', $id],
                    ['del_flg', '=', 0],
                    ['type', '=', 0],
                ])
                ->orderBy('id', 'desc')
                ->get();

            $view_albums = View::make('/Backend/product/_album', ['item_albums' => $item_albums]);

            $product['albums'] = $view_albums->render();

            $this->resp(ErrorCodes::E_OK, null, $product);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }

    
}
