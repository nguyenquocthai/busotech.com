<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\hosting_feature;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminhosting_featuresController extends BaseAdminController
{

    public function index (Request $request) {

        $hosting_features = DB::table('hosting_features')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'DESC')
            ->get();

        $output = [];
        foreach ($hosting_features as $key => $hosting_feature) {
            $vitri = $key+1;
            $row = $this->GetRow($hosting_feature, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = hosting_feature::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'hosting_feature');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = hosting_feature::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'hosting_feature', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $hosting_feature = DB::table('hosting_features')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$hosting_feature) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            hosting_feature::where([
                ['id', $hosting_feature->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.hosting_feature_path').$hosting_feature->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($hosting_feature, $vitri)
    {
        $row = [];
        $row[] = $hosting_feature->id;
        $row[] = $hosting_feature->position;
        $row[] = $vitri;
        $row[] = $hosting_feature->title;
        $row[] = '<span class="hidden">'.$hosting_feature->updated_at.'</span>'.date('d/m/Y', strtotime($hosting_feature->updated_at));
        $view = View::make('Backend/hosting_feature/_status', ['status' => $hosting_feature->status]);
        $row[] = $view->render();
        $view = View::make('Backend/hosting_feature/_actions', ['id' => $hosting_feature->id,'id_hosting_feature' => $hosting_feature->id_hosting_feature_cat,'page' => 'hosting_feature']);
        $row[] = $view->render();

        return $row;
    }
}
