<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\event;
use App\Model\ItemAlbum;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Extensions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdmineventsController extends BaseAdminController
{

    public function index (Request $request) {

        
        $events = DB::table('events')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($events as $key => $event) {
            $vitri = $key+1;
            $row = $this->GetRow($event, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = event::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'event');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = event::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['slug'] = $request->slug;
                $update_values['content'] = @$request->content;
                $update_values['summary'] = @$request->summary;
                $update_values['id_event_cat'] = @$request->id_event_cat;
                $update_values['updated_at'] = date("Y-m-d H:i:s");

                $edit_db = $this->DB_update($update_values, 'events', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'event', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $event = DB::table('events')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$event) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            event::where([
                ['id', $event->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.event_path').$event->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($event, $vitri)
    {
        $row = [];
        $row[] = $event->id;
        $row[] = $event->position;
        $row[] = $vitri;
        $row[] = $event->title;
        $row[] = $this->GetImg([
            'avatar'=> $event->avatar,
            'data'=> 'event',
            'time'=> $event->updated_at
        ]);
        $row[] = '<span class="hidden">'.$event->updated_at.'</span>'.date('d/m/Y', strtotime($event->updated_at));
        $view = View::make('Backend/event/_status', ['status' => $event->status]);
        $row[] = $view->render();
        $view = View::make('Backend/event/_actions', ['id' => $event->id,'page' => 'event']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $event = event::findOrFail($id);
            $event['avatar'] = config('general.event_url') . $event['avatar'];
            
            $this->resp(ErrorCodes::E_OK, null, $event);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
