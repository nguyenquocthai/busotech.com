<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\theme_categorie;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Admintheme_categoriesController extends BaseAdminController
{

    public function index (Request $request) {

        
        $theme_categories = DB::table('theme_categories')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($theme_categories as $key => $theme_categorie) {
            $vitri = $key+1;
            $row = $this->GetRow($theme_categorie, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = theme_categorie::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'theme_categorie');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = theme_categorie::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_theme_categorie_cat'] = $request->id_theme_categorie_cat;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'theme_categories', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'theme_categorie', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $theme_categorie = DB::table('theme_categories')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$theme_categorie) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            theme_categorie::where([
                ['id', $theme_categorie->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.theme_categorie_path').$theme_categorie->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($theme_categorie, $vitri)
    {
        $row = [];
        $row[] = $theme_categorie->id;
        $row[] = $theme_categorie->position;
        $row[] = $vitri;
        $row[] = $theme_categorie->title;
        
        $row[] = '<span class="hidden">'.$theme_categorie->updated_at.'</span>'.date('d/m/Y', strtotime($theme_categorie->updated_at));
        $view = View::make('Backend/theme_categorie/_status', ['status' => $theme_categorie->status]);
        $row[] = $view->render();
        $view = View::make('Backend/theme_categorie/_actions', ['id' => $theme_categorie->id,'page' => 'theme_categorie']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $theme_categorie = theme_categorie::findOrFail($id);
            $theme_categorie['avatar'] = config('general.theme_categorie_url') . $theme_categorie['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $theme_categorie);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
