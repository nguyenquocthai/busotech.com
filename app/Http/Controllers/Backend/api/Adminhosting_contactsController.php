<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\hosting_contact;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminhosting_contactsController extends BaseAdminController
{

    public function index (Request $request) {

        $hosting_contacts = DB::table('hosting_contacts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $hosting_contacts = json_decode($hosting_contacts, true);
        $export = View::make('Backend/hosting_contact/_export', ['hosting_contacts' => $hosting_contacts]);
        
        $hosting_contacts = DB::table('hosting_contacts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $hostings = DB::table('hostings')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])
            ->get();
        $output = [];
        foreach ($hosting_contacts as $key => $hosting_contact) {
            $vitri = $key+1;
            $row = $this->GetRow($hosting_contact, $vitri);
            $output[] = $row;
        }
        
        $data['code'] = 200;
        $data['export'] = $export->render();
        $data['data'] = $output;
        $data['hostings'] = $hostings;
        // echo "<pre>"; print_r($data);die; echo "</pre";
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = hosting_contact::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'hosting_contact');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = hosting_contact::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'hosting_contact', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $hosting_contact = DB::table('hosting_contacts')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$hosting_contact) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            hosting_contact::where([
                ['id', $hosting_contact->id]
            ])->update(['del_flg' => 1]);

            if (isset($hosting_contact->avatar)) {
                $image_path = config('general.hosting_contact_path').$hosting_contact->avatar;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                
                }
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($hosting_contact, $vitri)
    {
        $row = [];
        $row[] = $hosting_contact->id;
        $row[] = $hosting_contact->name;
        $row[] = $hosting_contact->email;
        $row[] = $hosting_contact->tel;
        $row[] = $hosting_contact->created_at;
        $view = View::make('Backend/hosting_contact/_status', ['status' => $hosting_contact->status]);
        $row[] = $view->render();
        $view = View::make('Backend/hosting_contact/_actions', ['id' => $hosting_contact->id,'id_host' => $hosting_contact->id_host,'page' => 'hosting_contact']);
        $row[] = $view->render();

        return $row;
    }
}
