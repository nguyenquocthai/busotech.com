<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\book;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminbooksController extends BaseAdminController
{

    public function index (Request $request) {

        
        $books = DB::table('books')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($books as $key => $book) {
            $vitri = $key+1;
            $row = $this->GetRow($book, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = book::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'book');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = book::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            // else if ($request->status_code == "edit_b1") {

            //     $update_values = [];
                
            //     $update_values['title'] = $request->title;
            //     $update_values['content'] = $request->content;
            //     $update_values['summary'] = $request->summary;
            //     $update_values['id_book_cat'] = $request->id_book_cat;
            //     $update_values['updated_at'] = date("Y-m-d H:i:s");
                

            //     $edit_db = $this->DB_update($update_values, 'books', $id);

            //     $data['code'] = 200;
            //     $data['message'] = 'Update ok.';
            //     return response()->json($data, 200);
            // }

            $edit_db = $this->EditDB($request->all(),'book', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $book = DB::table('books')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$book) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            book::where([
                ['id', $book->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.book_path').$book->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($book, $vitri)
    {
        $row = [];
        $row[] = $book->id;
        $row[] = $book->position;
        $row[] = $vitri;
        $row[] = $book->title;
        $row[] = $this->GetImg([
            'avatar'=> $book->avatar,
            'data'=> 'book',
            'time'=> $book->updated_at
        ]);
        $row[] = '<span class="hidden">'.$book->updated_at.'</span>'.date('d/m/Y', strtotime($book->updated_at));
        $view = View::make('Backend/book/_status', ['status' => $book->status]);
        $row[] = $view->render();
        $view = View::make('Backend/book/_actions', ['id' => $book->id,'page' => 'book']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $book = book::findOrFail($id);
            $book['avatar'] = config('general.book_url') . $book['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $book);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
