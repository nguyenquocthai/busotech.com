<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\dinhhuong;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdmindinhhuongsController extends BaseAdminController
{

    public function index (Request $request) {

        $dinhhuongs = DB::table('dinhhuongs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'DESC')
            ->get();

        $output = [];
        foreach ($dinhhuongs as $key => $dinhhuong) {
            $vitri = $key+1;
            $row = $this->GetRow($dinhhuong, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = dinhhuong::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'dinhhuong');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = dinhhuong::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'dinhhuong', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $dinhhuong = DB::table('dinhhuongs')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$dinhhuong) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            dinhhuong::where([
                ['id', $dinhhuong->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.dinhhuong_path').$dinhhuong->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($dinhhuong, $vitri)
    {
        $row = [];
        $row[] = $dinhhuong->id;
        $row[] = $dinhhuong->position;
        $row[] = $vitri;
        $row[] = $dinhhuong->title;
        $row[] = '<span class="hidden">'.$dinhhuong->updated_at.'</span>'.date('d/m/Y', strtotime($dinhhuong->updated_at));
        $view = View::make('Backend/dinhhuong/_status', ['status' => $dinhhuong->status]);
        $row[] = $view->render();
        $view = View::make('Backend/dinhhuong/_actions', ['id' => $dinhhuong->id,'page' => 'dinhhuong']);
        $row[] = $view->render();

        return $row;
    }
}
