<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\nhuongquyen;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminnhuongquyensController extends BaseAdminController
{

    public function index (Request $request) {
        
        $nhuongquyens = DB::table('nhuongquyens')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();


        $output = [];
        foreach ($nhuongquyens as $key => $nhuongquyen) {
            $vitri = $key+1;
            $row = $this->GetRow($nhuongquyen, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = nhuongquyen::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'nhuongquyen');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = nhuongquyen::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                $update_values['meta_title'] = $request->meta_title;
                $update_values['meta_description'] = $request->meta_description;
                $this->DB_update($update_values, 'nhuongquyens', $id);
                $edit_db = $this->EditDB($request->all(),'nhuongquyen', $id);
                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'nhuongquyen', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $nhuongquyen = DB::table('nhuongquyens')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$nhuongquyen) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            nhuongquyen::where([
                ['id', $nhuongquyen->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.nhuongquyen_path').$nhuongquyen->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($nhuongquyen, $vitri)
    {
        $row = [];
        $row[] = $nhuongquyen->id;
        $row[] = $nhuongquyen->position;
        $row[] = $vitri;
        $row[] = $nhuongquyen->title;
        $row[] = $this->GetImg([
            'avatar'=> $nhuongquyen->avatar,
            'data'=> 'nhuongquyen',
            'time'=> $nhuongquyen->updated_at
        ]);
        $row[] = '<span class="hidden">'.$nhuongquyen->updated_at.'</span>'.date('d/m/Y', strtotime($nhuongquyen->updated_at));

        
        $view = View::make('Backend/nhuongquyen/_status', ['status' => $nhuongquyen->status]);
        $row[] = $view->render();
        
        $view = View::make('Backend/nhuongquyen/_actions', ['id' => $nhuongquyen->id,'page' => 'nhuongquyen']);
        $row[] = $view->render();
        

        return $row;
    }
    public function show($id)
    {
        try {
            $nhuongquyen = nhuongquyen::findOrFail($id);
            $nhuongquyen['avatar'] = config('general.nhuongquyen_url') . $nhuongquyen['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $nhuongquyen);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
