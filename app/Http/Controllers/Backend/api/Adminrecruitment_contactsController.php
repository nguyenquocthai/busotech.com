<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\recruitment_contact;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminrecruitment_contactsController extends BaseAdminController
{

    public function index (Request $request) {

        $recruitment_contacts = DB::table('recruitment_contacts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $recruitment_contacts = json_decode($recruitment_contacts, true);
        $export = View::make('Backend/recruitment_contact/_export', ['recruitment_contacts' => $recruitment_contacts]);
        
        $recruitment_contacts = DB::table('recruitment_contacts')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        $giatenmiens = DB::table('giatenmiens')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])
            ->get();
        $output = [];
        foreach ($recruitment_contacts as $key => $recruitment_contact) {
            

            $vitri = $key+1;
            $row = $this->GetRow($recruitment_contact, $vitri);
            $output[] = $row;
        }
        $data['code'] = 200;
        $data['export'] = $export->render();
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = recruitment_contact::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'recruitment_contact');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = recruitment_contact::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'recruitment_contact', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $recruitment_contact = DB::table('recruitment_contacts')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$recruitment_contact) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            recruitment_contact::where([
                ['id', $recruitment_contact->id]
            ])->update(['del_flg' => 1]);

            if (isset($recruitment_contact->avatar)) {
                $image_path = config('general.recruitment_contact_path').$recruitment_contact->avatar;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                
                }
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($recruitment_contact, $vitri)
    {
        $row = [];
        $row[] = $recruitment_contact->id;
        $row[] = $recruitment_contact->name;
        $row[] = $recruitment_contact->email;
        $row[] = $recruitment_contact->tel;
        $row[] = $recruitment_contact->created_at;
        $view = View::make('Backend/recruitment_contact/_status', ['status' => $recruitment_contact->status]);
        $row[] = $view->render();
        $view = View::make('Backend/recruitment_contact/_actions', ['id' => $recruitment_contact->id,'page' => 'recruitment_contact']);
        $row[] = $view->render();

        return $row;
    }
}
