<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\banner;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminbannersController extends BaseAdminController
{

    public function index (Request $request) {
        
        $banners = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('id', 'asc')
            ->get();

        $output = [];
        foreach ($banners as $key => $banner) {
            $vitri = $key+1;
            $row = $this->GetRow($banner, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = banner::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'banner');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = banner::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                

                $update_values['title'] = $request->title;
                $update_values['title_en'] = $request->title_en;
                $update_values['content'] = $request->content;
                $update_values['content_en'] = $request->content_en;
                $update_values['summary'] = $request->summary;
                $update_values['summary_en'] = $request->summary_en;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                $update_values['meta_title'] = $request->meta_title;
                $update_values['meta_description'] = $request->meta_description;
                $this->DB_update($update_values, 'banners', $id);
                $edit_db = $this->EditDB($request->all(),'banner', $id);

                
        
                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'banner', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $banner = DB::table('banners')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$banner) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            banner::where([
                ['id', $banner->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.banner_path').$banner->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($banner, $vitri)
    {
        $row = [];
        $row[] = $banner->id;
        $row[] = $banner->position;
        $row[] = $vitri;
        $row[] = $banner->title;
        $row[] = $this->GetImg([
            'avatar'=> $banner->avatar,
            'data'=> 'banner',
            'time'=> $banner->updated_at
        ]);
        $row[] = '<span class="hidden">'.$banner->updated_at.'</span>'.date('d/m/Y', strtotime($banner->updated_at));

        
        $view = View::make('Backend/banner/_status', ['status' => $banner->status]);
        $row[] = $view->render();
        $view = View::make('Backend/banner/_type', ['type' => $banner->type]);
        $row[] = $view->render();
        $view = View::make('Backend/banner/_actions', ['id' => $banner->id,'page' => 'banner']);
        $row[] = $view->render();
        

        return $row;
    }
    public function show($id)
    {
        try {
            $banner = banner::findOrFail($id);
            $banner['avatar'] = config('general.banner_url') . $banner['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $banner);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
