<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\buso_step;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminbuso_stepsController extends BaseAdminController
{

    public function index (Request $request) {

        
        $buso_steps = DB::table('buso_steps')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($buso_steps as $key => $buso_step) {
            $vitri = $key+1;
            $row = $this->GetRow($buso_step, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = buso_step::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'buso_step');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = buso_step::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            // else if ($request->status_code == "edit_b1") {

            //     $update_values = [];
                
            //     $update_values['title'] = $request->title;
            //     $update_values['content'] = $request->content;
            //     $update_values['summary'] = $request->summary;
            //     $update_values['id_buso_step_cat'] = $request->id_buso_step_cat;
            //     $update_values['updated_at'] = date("Y-m-d H:i:s");
                

            //     $edit_db = $this->DB_update($update_values, 'buso_steps', $id);

            //     $data['code'] = 200;
            //     $data['message'] = 'Update ok.';
            //     return response()->json($data, 200);
            // }

            $edit_db = $this->EditDB($request->all(),'buso_step', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $buso_step = DB::table('buso_steps')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$buso_step) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            buso_step::where([
                ['id', $buso_step->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.buso_step_path').$buso_step->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($buso_step, $vitri)
    {
        $row = [];
        $row[] = $buso_step->id;
        $row[] = $buso_step->position;
        $row[] = $vitri;
        $row[] = $buso_step->title;
        
        $row[] = '<span class="hidden">'.$buso_step->updated_at.'</span>'.date('d/m/Y', strtotime($buso_step->updated_at));
        $view = View::make('Backend/buso_step/_status', ['status' => $buso_step->status]);
        $row[] = $view->render();
        $view = View::make('Backend/buso_step/_actions', ['id' => $buso_step->id,'page' => 'buso_step']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $buso_step = buso_step::findOrFail($id);
            $buso_step['avatar'] = config('general.buso_step_url') . $buso_step['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $buso_step);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
