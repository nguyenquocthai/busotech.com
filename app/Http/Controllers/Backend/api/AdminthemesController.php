<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\theme;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminthemesController extends BaseAdminController
{

    public function index (Request $request) {

        
        $themes = DB::table('themes')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($themes as $key => $theme) {
            $vitri = $key+1;
            $row = $this->GetRow($theme, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = theme::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'theme');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = theme::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            else if ($request->status_code == "edit_b1") {

                $update_values = [];
                
                $update_values['title'] = $request->title;
                $update_values['content'] = $request->content;
                $update_values['summary'] = $request->summary;
                $update_values['id_theme_cat'] = $request->id_theme_cat;
                $update_values['updated_at'] = date("Y-m-d H:i:s");
                

                $edit_db = $this->DB_update($update_values, 'themes', $id);

                $data['code'] = 200;
                $data['message'] = 'Update ok.';
                return response()->json($data, 200);
            }

            $edit_db = $this->EditDB($request->all(),'theme', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $theme = DB::table('themes')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$theme) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            theme::where([
                ['id', $theme->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.theme_path').$theme->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($theme, $vitri)
    {
        $row = [];
        $row[] = $theme->id;
        $row[] = $theme->position;
        $row[] = $vitri;
        $row[] = $theme->title;
        $row[] = $this->GetImg([
            'avatar'=> $theme->avatar,
            'data'=> 'theme',
            'time'=> $theme->updated_at
        ]);
        $row[] = '<span class="hidden">'.$theme->updated_at.'</span>'.date('d/m/Y', strtotime($theme->updated_at));
        $view = View::make('Backend/theme/_status', ['status' => $theme->status]);
        $row[] = $view->render();
        $view = View::make('Backend/theme/_actions', ['id' => $theme->id,'page' => 'theme']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $theme = theme::findOrFail($id);
            $theme['avatar'] = config('general.theme_url') . $theme['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $theme);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
