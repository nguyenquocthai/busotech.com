<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\giatenmien_cat;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Admingiatenmien_catsController extends BaseAdminController
{

    public function index (Request $request) {

        $giatenmien_cats = DB::table('giatenmien_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'DESC')
            ->get();

        $output = [];
        foreach ($giatenmien_cats as $key => $giatenmien_cat) {
            $vitri = $key+1;
            $row = $this->GetRow($giatenmien_cat, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // validate
            $error_validate = giatenmien_cat::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'giatenmien_cat');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit') {
                // validate
                $error_validate = giatenmien_cat::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }

            $edit_db = $this->EditDB($request->all(),'giatenmien_cat', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $giatenmien_cat = DB::table('giatenmien_cats')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$giatenmien_cat) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            giatenmien_cat::where([
                ['id', $giatenmien_cat->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.giatenmien_cat_path').$giatenmien_cat->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }

            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($giatenmien_cat, $vitri)
    {
        $row = [];
        $row[] = $giatenmien_cat->id;
        $row[] = $giatenmien_cat->position;
        $row[] = $vitri;
        $row[] = $giatenmien_cat->title;
        $row[] = '<span class="hidden">'.$giatenmien_cat->updated_at.'</span>'.date('d/m/Y', strtotime($giatenmien_cat->updated_at));
        $view = View::make('Backend/giatenmien_cat/_status', ['status' => $giatenmien_cat->status]);
        $row[] = $view->render();
        $view = View::make('Backend/giatenmien_cat/_actions', ['id' => $giatenmien_cat->id,'page' => 'giatenmien_cat']);
        $row[] = $view->render();

        return $row;
    }
}
