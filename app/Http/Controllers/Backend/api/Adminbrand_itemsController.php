<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use File;
use App\Model\brand_item;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class Adminbrand_itemsController extends BaseAdminController
{

    public function index (Request $request) {

        
        $brand_items = DB::table('brand_items')
            
            ->select('*')
            ->where([
                ['del_flg', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $output = [];
        foreach ($brand_items as $key => $brand_item) {
            $vitri = $key+1;
            $row = $this->GetRow($brand_item, $vitri);
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {


            // validate
            $error_validate = brand_item::validate(0);
            $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);
            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            $save_db = $this->SaveDB($request->all(),'brand_item');

            return response()->json($save_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }

    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == 'edit' ) {
                // validate
                $error_validate = brand_item::validate($id);
                $validator = \Validator::make($request->value, $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

                if ($validator->fails()) {
                    $data['code'] = 300;
                    $data['error'] = $validator->errors();
                    return response()->json($data, 200);
                }
            }
            // else if ($request->status_code == "edit_b1") {

            //     $update_values = [];
                
            //     $update_values['title'] = $request->title;
            //     $update_values['content'] = $request->content;
            //     $update_values['summary'] = $request->summary;
            //     $update_values['id_brand_item_cat'] = $request->id_brand_item_cat;
            //     $update_values['updated_at'] = date("Y-m-d H:i:s");
                

            //     $edit_db = $this->DB_update($update_values, 'brand_items', $id);

            //     $data['code'] = 200;
            //     $data['message'] = 'Update ok.';
            //     return response()->json($data, 200);
            // }

            $edit_db = $this->EditDB($request->all(),'brand_item', $id);

            if ($request->status_code == 'edit') {
                $edit_db['row'] = $this->GetRow($edit_db['row'], $request->vitri);
            }

            return response()->json($edit_db, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {

            $brand_item = DB::table('brand_items')
                ->select('*')
                ->where([
                    ['id', '=', $id],
                ])
                ->first();

            if (!$brand_item) {
                $data['code'] = 300;
                $data['error'] = 'Không tìm thấy.';
                return response()->json($data, 200);
            }

            brand_item::where([
                ['id', $brand_item->id]
            ])->update(['del_flg' => 1]);

            $image_path = config('general.brand_item_path').$brand_item->avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
                
            }
            $data['code'] = 200;
            $data['message'] = 'Xóa thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {

            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);

        }
    }

    //-------------------------------------------------------------------------------
    public function GetRow($brand_item, $vitri)
    {
        $row = [];
        $row[] = $brand_item->id;
        $row[] = $brand_item->position;
        $row[] = $vitri;
        $row[] = $brand_item->title;
        $row[] = $this->GetImg([
            'avatar'=> $brand_item->avatar,
            'data'=> 'brand_item',
            'time'=> $brand_item->updated_at
        ]);
        $row[] = '<span class="hidden">'.$brand_item->updated_at.'</span>'.date('d/m/Y', strtotime($brand_item->updated_at));
        $view = View::make('Backend/brand_item/_status', ['status' => $brand_item->status]);
        $row[] = $view->render();
        $view = View::make('Backend/brand_item/_actions', ['id' => $brand_item->id,'page' => 'brand_item']);
        $row[] = $view->render();

        return $row;
    }
    public function show($id)
    {
        try {
            $brand_item = brand_item::findOrFail($id);
            $brand_item['avatar'] = config('general.brand_item_url') . $brand_item['avatar'];

            $this->resp(ErrorCodes::E_OK, null, $brand_item);
        } catch (Exception $e) {
            $this->exception($e);
        }

        $this->response();
    }
}
