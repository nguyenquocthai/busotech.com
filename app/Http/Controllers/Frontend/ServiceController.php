<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;
use App\Model\theme;
use App\Model\theme_categorie;
use App\Model\theme_categorie1;

class ServiceController extends BaseFrontendController
{
    public function busoweb()
    {   
        $busowebs = DB::table('busowebs')
        ->select($this->array_select('busowebs'))
            ->where([
                ['del_flg', '=', 0],
                ['type', '=', 1]
            ])
            ->get();
        
        $theme_cates = theme_categorie::where([
            ['del_flg', '=', 0],
            ['status', '=', 0]
        ])->orderBy('position', 'desc')->get();

        $theme_cate1s = theme_categorie1::where([
            ['del_flg', '=', 0],
            ['status', '=', 0]
        ])->orderBy('position', 'desc')->get();

        $themes = theme::where([
            ['del_flg', '=', 0],
            ['status', '=', 0]
        ])->orderBy('position', 'desc')->paginate(20);
        $doitacs = DB::table('doitacs')
        ->select($this->array_select('doitacs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(15)->orderBy('position','desc')
            ->get();
        $duans = DB::table('duans')
        ->select($this->array_select('duans'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $price_domain_cats = DB::table('price_domain_cats')
        ->select('*')
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $price_domains = DB::table('price_domains')
        ->select('*')
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 5]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.service.busoweb')->with(compact('seopage','busowebs','theme_cate1s','theme_cates','themes','doitacs','duans','price_domain_cats','price_domains'));
    }
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}