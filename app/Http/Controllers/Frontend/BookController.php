<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class BookController extends BaseFrontendController
{
    public function index()
    {   
        
        $books = DB::table('books')
            ->select($this->array_select('books'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->paginate(10);
        $booknbs = DB::table('books')
            ->select($this->array_select('books'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['noibat', '=', 0],
            ])
            ->orderBy('id', 'desc')
            ->limit(6)
            ->get();

        $banner = DB::table('banners')
            ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 7],
            ])
            ->first();
        $events = DB::table('events')
            ->select($this->array_select('events'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 8]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.book.index')->with(compact('seopage','books','banner','booknbs','events'));
    }
    
    public function detail($slug)
    {   
        $bookdt = DB::table('books')
            ->select($this->array_select('books'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        $booknbs = DB::table('books')
            ->select($this->array_select('books'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['noibat', '=', 0],
            ])
            ->orderBy('id', 'desc')
            ->limit(6)
            ->get();
        
        $events = DB::table('events')
            ->select($this->array_select('events'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        $banner = DB::table('banners')
            ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 7],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 8]
            ])
            ->first();
        $seopage->meta_title = @$bookdt->meta_title;
        $seopage->meta_description = @$bookdt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$bookdt->avatar,
            'data'=>'book',
            'time'=>$bookdt->updated_at
        ]);

        return view('frontend.book.detail')->with(compact('seopage','bookdt','banner','booknbs','events'));
    }

    public function cat($slug)
    {   
        $book_cat = DB::table('book_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        $books = DB::table('books')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id_book_cat', '=', $book_cat->id],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 4],
            ])
            ->first();
        $book_cats = DB::table('book_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', $book_cat->id]
            ])
            ->orderBy('position', 'desc')
            ->limit(5)
            ->get();
        $blogs = DB::table('blogs')
            ->select('*')
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        $events = DB::table('events')
            ->select('*')
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 5]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.book.cat')->with(compact('book_cat','seopage','books','book_cats','banner','blogs','events'));
        }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}