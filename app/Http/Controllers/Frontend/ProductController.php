<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Model\contact;

class ProductController extends BaseFrontendController
{
    public function index()
    {   
        $product_cats = DB::table('product_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $products = DB::table('products')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                 ['status', '=', 0],
            ])
            ->orderBy('position', 'desc')
            ->paginate(9);
        $productnbs = DB::table('products')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                 ['status', '=', 0],
                 ['noibat', '=', 0],
            ])
            ->orderBy('position', 'desc')
            ->paginate(3);

        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();
        $album = DB::table('albums')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])
            ->orderBy('position', 'desc')
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.product.index')->with(compact('seopage','products','product_cats','banner','productnbs','album'));
    }
    
    public function detail($slug)
    {   
        // Session::flush();
        // echo "<pre>";
        //         print_r(session('cart'));die();
        //         echo "</pre>";
        $productdt = DB::table('products')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();

        $product_cat = DB::table('product_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', @$productdt->id_product_cat],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        
         $products = DB::table('products')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', @$productdt->id],
                ['id_product_cat', '=', @$product_cat->id],
            ])
            ->orderBy('position', 'desc')
            ->get();
        $item_albums = DB::table('item_albums')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['type', '=', 0],
                ['item_id', '=', @$productdt->id],
            ])
            ->orderBy('id', 'desc')
            ->get();
        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();

        if (session('cart')) {
            $carts = session('cart');
        } else {
            $carts = [];
        }

        usort($carts, function($a, $b) {
            return $b['time'] - $a['time'];
        });
        
        foreach ($carts as $key => $cart) {
            $product1 = DB::table('products')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id', '=', $cart['id']]
                ])
                ->first();

            $carts[$key]['product'] = $product1;
        }
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.product.detail')->with(compact('seopage','productdt','products','product_cat','item_albums','banner','carts'));
    }
    public function cat($slug)
    {   
        
        $product_cat = DB::table('product_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        $product_cats = DB::table('product_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $products = DB::table('products')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                 ['status', '=', 0],
                 ['id_product_cat', '=', $product_cat->id],
            ])
            ->orderBy('position', 'desc')
            ->paginate(9);
        $productnbs = DB::table('products')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                 ['status', '=', 0],
                 ['noibat', '=', 0],
            ])
            ->orderBy('position', 'desc')
            ->paginate(3);

        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();
        $album = DB::table('albums')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])
            ->orderBy('position', 'desc')
            ->first();
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.product.cat')->with(compact('seopage','product_cat','products','product_cats','banner','productnbs','album'));
    }

    //-------------------------------------------------------------------------------
    public function viewcart()
    {
        if (session('cart')) {
            $carts = session('cart');
        } else {
            $carts = [];
        }

        usort($carts, function($a, $b) {
            return $b['time'] - $a['time'];
        });
        foreach ($carts as $key => $cart) {
            $product = DB::table('products')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id', '=', $cart['id']]
                ])
                ->first();

            $carts[$key]['product'] = $product;
        }
        // echo "<pre>";
        // print_r($carts);
        // echo "</pre>";die;

        $view = View::make('frontend/product/viewcart', ['carts' => $carts]);
        $data = $view->render();
        return $data;
    }
    //----------------------
    public function add_country(Request $request)
    {

        $za_provinces = DB::table('za_provinces')
            ->select('*')
            ->where([
                    
                    ['name', '=', $request->name]
                ])
            ->first();
        $zb_districts = DB::table('zb_districts')
            ->select('*')
            ->where([
                    
                    ['province_id', '=', $za_provinces->id]
                ])
            ->get();
        $view = View::make('frontend/product/country', ['zb_districts' => $zb_districts]);
        $data = $view->render();
        return response()->json($data, 200);
    }
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}