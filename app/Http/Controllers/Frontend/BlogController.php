<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class BlogController extends BaseFrontendController
{
    public function index()
    {   
        
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $blognb = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['hight_flg', '=', 1],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        $sliders = DB::table('slider_blogs')
        ->select($this->array_select('slider_blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])
            ->get();
        $blog_cats = DB::table('blog_cats')
        ->select($this->array_select('blog_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 4]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.blog.index')->with(compact('seopage','blognb','blogs','sliders','blog_cats'));
    }
    public function cat($slug)
    {   
        
        $blog_catdt = DB::table('blog_cats')
        ->select($this->array_select('blog_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['id_blog_cat', '=', $blog_catdt->id],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->paginate(10);
        $blog_news = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $blog_cats = DB::table('blog_cats')
        ->select($this->array_select('blog_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 4]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.blog.cat')->with(compact('seopage','blog_news','blogs','blog_catdt','blog_cats'));
    }
    public function catall()
    {   
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->paginate(10);

        $blog_cats = DB::table('blog_cats')
        ->select($this->array_select('blog_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 4]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.blog.catall')->with(compact('seopage','blogs','blog_cats'));
    }
    public function detail($slug)
    {   
        $blogdt = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['id', '!=', $blogdt->id],
                ['id_blog_cat', '=', $blogdt->id_blog_cat],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $blog_news = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['id', '!=', $blogdt->id],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $blognbs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['del_flg', '=', 0],
                ['id', '!=', $blogdt->id],
                ['hight_flg', '=', 1],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $blog_cats = DB::table('blog_cats')
        ->select($this->array_select('blog_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $blog_catdt = DB::table('blog_cats')
        ->select($this->array_select('blog_cats'))
            ->where([
                ['status', '=', 0],
                ['id', '=', $blogdt->id_blog_cat],
                ['del_flg', '=', 0]
            ])
            ->first();
        
        $banner = DB::table('banners')
        ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 3],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 4]
            ])
            ->first();
        $seopage->meta_title = @$blogdt->meta_title;
        $seopage->meta_description = @$blogdt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$blogdt->avatar,
            'data'=>'blog',
            'time'=>$blogdt->updated_at
        ]);

        return view('frontend.blog.detail')->with(compact('seopage','blogdt','banner','blogs','blog_news','blognbs','blog_cats','blog_catdt'));
    }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}