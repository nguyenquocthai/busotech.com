<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class VideoController extends BaseFrontendController
{
    public function index()
    {   
        
        $videos = DB::table('videos')
            ->select($this->array_select('videos'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        foreach ($videos as $key => $video) {

            $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
             $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

            if (preg_match($longUrlRegex, $video->link_youtube, $matches)) {
                $youtube_id = $matches[count($matches) - 1];
            }

            if (preg_match($shortUrlRegex, $video->link_youtube, $matches)) {
                $youtube_id = $matches[count($matches) - 1];
            }
            $embed = 'https://www.youtube.com/embed/' . $youtube_id ;
            $video->linkyoutube = $embed;
        }
        $banner = DB::table('banners')
            ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 8],
            ])
            ->first();
        $blogs = DB::table('blogs')
            ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        $events = DB::table('events')
            ->select($this->array_select('events'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 9]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.video.index')->with(compact('videos','seopage','blogs','banner','events'));
    }
    
    public function detail($slug)
    {   
        $eventdt = DB::table('events')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        $events = DB::table('events')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->limit(5)->get();

        $blogs = DB::table('blogs')
            ->select('*')
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        
        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 2],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.event.detail')->with(compact('seopage','eventdt','banner','blogs','events'));
    }
    public function cat($slug)
    {   
        $nhahang_cat_active = DB::table('nhahang_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();
        $nhahang_cats = DB::table('nhahang_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $all_nhahang = DB::table('nhahangs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id_nhahang_cat', '=', $nhahang_cat_active->id],
            ])
            ->orderBy('position', 'desc')
            ->get();

        $page_start = 0;
        $max_page = count($all_nhahang);


        $nhahangs = DB::table('nhahangs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                 ['status', '=', 0],
                 ['id_nhahang_cat', '=', $nhahang_cat_active->id],
            ])
            ->offset($page_start)
            ->limit(6)
            ->orderBy('position', 'desc')
            ->get();
        $page_current = count($nhahangs) + $page_start;

        if($max_page > $page_current)
            $next_page=1;
        else
            $next_page= 0;

        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.nhahang.cat')->with(compact('seopage','nhahangs','nhahang_cats','banner','all_nhahang','page_current','next_page','nhahang_cat_active'));
    }
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}