<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Exceptions\OnePayPayment;
use App\Mail\CartEmail;

class cartController extends BaseFrontendController
{

    public function __construct(OnePayPayment $onePayPayment)
    {
        $this->boot();
        $this->onePayPayment = $onePayPayment;
    }

    public function index()
    {
        return view('frontend.cart.index');
    }

    //-------------------------------------------------------------------------------
    public function add_cart(Request $request)
    {
        try {
            // echo "<pre>";
            // print_r($request->all());
            // echo "</pre>";die;
            $item_cart = $request->all();
            $item_cart['time'] = time();

            if (session('cart')) {
                $carts = session('cart');
                $check = 0;
                foreach ($carts as $key => $value) {
                    if($value['id'] == $request->id &&isset($carts[$key]['soluong'])){
                        
                        $carts[$key]['soluong'] = $carts[$key]['soluong'] + $request->soluong;
                        $check = 1;
                        
                    }    
                    // else($value['id'] == $request->id){
                        
                    //     $carts[$key]['soluong'] = $request->soluong;
                    //     $carts[$key]['sizeAo'] = $request->sizeAo;
                    //     $carts[$key]['time'] = time();
                    //     $check = 1;
                        
                    // }    
                    
                }

                if ($check == 0) {
                    $carts[] = $item_cart;
                }

                session(['cart' => $carts]);

            } else {
                $carts = [];
                $carts[] = $item_cart;
                session(['cart' => $carts]);
            }

            $data['code'] = 200;
            $data['viewcart'] = $this->viewcart();
            $data['count'] = count($carts);
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function update_cart(Request $request)
    {
        try {
            
            if (session('cart')) {
                $carts = session('cart');

                foreach ($carts as $key => $value) {
                    if($value['id'] == $request->id ){
                        $carts[$key]['soluong'] = $request->soluong;
                        $carts[$key]['total'] = $request->soluong * $request->price;
                    }
                }

                session(['cart' => $carts]);

            } else {
                $carts = [];
                $carts[] = $item_cart;
                session(['cart' => $carts]);
            }
            
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function remove_cart(Request $request)
    {
        try {

            if ($request->status_code == 'all') {
                session()->forget('cart');
            }

            if (session('cart')) {
                $carts = session('cart');
                foreach ($carts as $key => $value) {
                    if($value['id'] == $request->id ){
                        unset($carts[$key]);
                    }
                }

                session(['cart' => $carts]);
            } else {
                $carts = [];
            }
    
            $data['code'] = 200;
            $data['count'] = count($carts);
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function show_cart()
    {
        try {

            if (session('cart')) {
                $carts = session('cart');
            } else {
                $carts = [];
            }

            $data['code'] = 200;
            $data['viewcart'] = $this->viewcart1();
            $data['count'] = count($carts);
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function viewcart()
    {
        if (session('cart')) {
            $carts = session('cart');
        } else {
            $carts = [];
        }

        usort($carts, function($a, $b) {
            return $b['time'] - $a['time'];
        });
        foreach ($carts as $key => $cart) {
            $product = DB::table('products')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id', '=', $cart['id']]
                ])
                ->first();

            $carts[$key]['product'] = $product;
        }

        $view = View::make('frontend/product/viewcart', ['carts' => $carts]);
        $data = $view->render();
        return $data;
    }
    //-------------------------------------------------------------------------------
    public function viewcart1()
    {
        if (session('cart')) {
            $carts = session('cart');
        } else {
            $carts = [];
        }

        usort($carts, function($a, $b) {
            return $b['time'] - $a['time'];
        });
        foreach ($carts as $key => $cart) {
            $product = DB::table('products')
                ->select('*')
                ->where([
                    ['del_flg', '=', 0],
                    ['status', '=', 0],
                    ['id', '=', $cart['id']]
                ])
                ->first();

            $carts[$key]['product'] = $product;
        }
        $za_provinces = DB::table('za_provinces')
            ->select('*')
            ->get();
        $zb_districts = DB::table('zb_districts')
            ->select('*')
            ->get();

        $view = View::make('frontend/product/thanhtoan', ['carts' => $carts],['za_provinces' => $za_provinces],['zb_districts' => $zb_districts]);
        $data = $view->render();
        return $data;
    }

    //-------------------------------------------------------------------------------
    public function send_cart(Request $request)
    {
        try {

            if (session('cart')) {
                $carts = session('cart');
            } else {
                return redirect('/');
            }

            if (count($carts) == 0) {
                $data['code'] = 300;
                $data['error'] = 'Chưa chọn sản phẩm';
                return response()->json($data, 200);
            }

            $insert_value = $request->all();

            unset($insert_value['admin_email']);
            $insert_value['position'] = $this->GetPos('carts',"");
            $insert_value['created_at'] = date("Y-m-d H:i:s");
            $insert_value['updated_at'] = date("Y-m-d H:i:s");
            $insert_value['status'] = 0;
            $id_carts = DB::table('carts')->insertGetId($insert_value);

            foreach ($carts as $key => $cart) {
                DB::table('cart_details')->insert(
                    [
                        'id_cart' => $id_carts,
                        'id_product' => $cart['id'],
                        'soluong' => $cart['soluong'],
                        'updated_at' => date("Y-m-d H:i:s"),
                        'created_at' => date("Y-m-d H:i:s")
                    ]
                );
            }
            session()->forget('cart');
            // if ($request->payment == 'payment') {
            //     $rq = $request->all();
            //     $rq['id_carts'] = $id_carts;
            //     // 'return_url' => url('/') . '/reponse_payment/' . $id_carts;
            //     return view('frontend/pay_nganluong/index')->with(compact('rq'));
            // }
            
            
            // $obj = new \stdClass();
            // Mail::to($request->admin_email)->send(new CartEmail($obj));

            $cart = DB::table('carts')
                ->select('*')
                ->where([
                    ['id', '=', $id_carts]
                ])
                ->first();

            $view = View::make('frontend/product/dathangthanhcong', ['cart' => $cart]);
            $data = $view->render();
            return $data;
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function checkout(Request $request){
        $data = $request->all();
        return view('frontend/pay_nganluong/checkout')->with(compact('data'));
    }

    //-------------------------------------------------------------------------------
    public function payment_success(Request $request){
        $data = $request->all();
        return view('frontend/pay_nganluong/payment_success')->with(compact('data'));
    }

    public function reponse_payment($id, Request $request)
    {

        //thanh toan online onepage
        // $checkpay = $this->onePayPayment->resultValidate([
        //     'vpc_TxnResponseCode' => $request->vpc_TxnResponseCode,
        //     'vpc_SecureHash' => $request->vpc_SecureHash
        // ]);

        // thanh toan ngan luong
        $transaction_info = $request->transaction_info;
        $order_code = $request->order_code;
        $price = $request->price;
        $payment_id = $request->payment_id;
        $payment_type = $request->payment_type;
        $error_text = $request->error_text;
        $secure_code = $request->secure_code;

        $checkpay = $this->nganluongPayment->verifyPaymentUrl($transaction_info, $order_code, $price, $payment_id, $payment_type, $error_text, $secure_code);

        if ($checkpay) {
            DB::table('carts')
                ->where('id', $id)
                ->update(['pay_status' => 1]);
                session()->forget('cart');
                return view('frontend/product/paythanhcong');

        }
        else{
            session()->forget('cart');
            return view('frontend/product/paythatbai');
        }

        
    }

    public function pay_ok(Request $request)
    {
        
        $id = $request->id;
        $status = $request->status;
        if ($status == 1) {
            DB::table('carts')
                ->where('id', $id)
                ->update(['pay_status' => 1]);
                session()->forget('cart');
                return view('frontend/product/paythanhcong');

        }
        else{
            return view('frontend/product/paythatbai');
        }
    }
}