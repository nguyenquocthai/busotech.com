<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class ResourceController extends BaseFrontendController
{
    public function index()
    {   
        
        $resources = DB::table('resources')
        ->select($this->array_select('resources'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $banner = DB::table('banners')
        ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 4],
            ])
            ->first();
        $resource_cats = DB::table('resource_cats')
        ->select($this->array_select('resource_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 6]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.dongy.index')->with(compact('seopage','resources','banner','resource_cats'));
    }
    
    public function detail($slug)
    {   
        $resourcedt = DB::table('resources')
        ->select($this->array_select('resources'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        $events = DB::table('events')
        ->select($this->array_select('events'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        $banner = DB::table('banners')
        ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 4],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 6]
            ])
            ->first();
        $seopage->meta_title = @$resourcedt->meta_title;
        $seopage->meta_description = @$resourcedt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$resourcedt->avatar,
            'data'=>'resource',
            'time'=>$resourcedt->updated_at
        ]);

        return view('frontend.resource.detail')->with(compact('seopage','resourcedt','banner','blogs','events'));
    }

    public function cat($slug)
    {   
        $resource_cat = DB::table('resource_cats')
        ->select($this->array_select('resource_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        $resources = DB::table('resources')
        ->select($this->array_select('resources'))
            ->where([
                ['del_flg', '=', 0],
                ['id_resource_cat', '=', $resource_cat->id],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $banner = DB::table('banners')
        ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 4],
            ])
            ->first();
        $resource_cats = DB::table('resource_cats')
        ->select($this->array_select('resource_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', $resource_cat->id]
            ])
            ->orderBy('position', 'desc')
            ->limit(5)
            ->get();
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        $events = DB::table('events')
        ->select($this->array_select('events'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 6]
            ])
            ->first();
        $seopage->meta_title = @$resource_cat->meta_title;
        $seopage->meta_description = @$resource_cat->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$resource_cat->avatar,
            'data'=>'resource',
            'time'=>$resource_cat->updated_at
        ]);

        return view('frontend.resource.cat')->with(compact('resource_cat','seopage','resources','resource_cats','banner','blogs','events'));
        }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}