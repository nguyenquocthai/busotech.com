<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\comment;

class CommentController extends BaseFrontendController
{
   
    public function add_comment(Request $request)
    {
        try {
            $data = [];
            if($request->id_khach == $request->id_chu)
            {
                $data['code'] = 300;
                $data['error'] = 'This post is yours';
                return response()->json($data, 200);
            }

            DB::table('customers')->insert([
                'name_contact' => $request->name_contact,
                'phone_contact' => $request->phone_contact,
                'email_contact' => $request->email_contact,
                'comment' => $request->comment,
                'id_chu' => $request->id_chu,
                'id_khach' => $request->id_khach,
                'id_itemproject' => $request->id_itemproject,
                'trang_thai' => 0,
                'status' => 0,
                'del_flg' => 0,
                'created_at' => date("Y-m-d H:i:s")
            ]);

            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Connection errors';
            return response()->json($data, 200);
        }
        
    }

    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Connection errors';
            return response()->json($data, 200);
        }
    }
}