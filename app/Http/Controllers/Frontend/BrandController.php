<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class BrandController extends BaseFrontendController
{
    public function index()
    {   
        
        $brand_items = DB::table('brand_items')
            ->select($this->array_select('brand_items'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $brand_steps = DB::table('brand_steps')
            ->select($this->array_select('brand_steps'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $brand_duans = DB::table('brand_duans')
        ->select($this->array_select('brand_duans'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $doitacs = DB::table('doitacs')
        ->select($this->array_select('doitacs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(15)->orderBy('position','desc')
            ->get();
        $banner = DB::table('banners')
            ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 7],
            ])
            ->first();

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 6]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.brand.index')->with(compact('seopage','brand_items','banner','brand_steps','blogs','brand_duans','doitacs'));
    }
    public function brandingform()
    {   
        
        $brand_items = DB::table('brand_items')
            ->select($this->array_select('brand_items'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $brand_steps = DB::table('brand_steps')
            ->select($this->array_select('brand_steps'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $brand_duans = DB::table('brand_duans')
        ->select($this->array_select('brand_duans'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $doitacs = DB::table('doitacs')
        ->select($this->array_select('doitacs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(15)->orderBy('position','desc')
            ->get();
        $banner = DB::table('banners')
            ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 7],
            ])
            ->first();

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 6]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.brand.form')->with(compact('seopage','brand_items','banner','brand_steps','blogs','brand_duans','doitacs'));
    }
    public function brandingprojects()
    {   
        
        $brand_items = DB::table('brand_items')
            ->select($this->array_select('brand_items'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $brand_steps = DB::table('brand_steps')
            ->select($this->array_select('brand_steps'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $brand_duans = DB::table('brand_duans')
        ->select($this->array_select('brand_duans'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $doitacs = DB::table('doitacs')
        ->select($this->array_select('doitacs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(15)->orderBy('position','desc')
            ->get();
        $banner = DB::table('banners')
            ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 7],
            ])
            ->first();

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 6]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.brand.project')->with(compact('seopage','brand_items','banner','brand_steps','blogs','brand_duans','doitacs'));
    }
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}