<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class StoreController extends BaseFrontendController
{
    public function index()
    {
        return view('frontend.store.index')->with(compact(''));
    }

    public function datatable_store(Request $request) {

        $stores = DB::table('stores')
            ->select('*')
            ->where([
                ['id_code', '=', 0],
                ['del_flg', '=', 0]
            ])
            ->get();

        $output = [];
        foreach ($stores as $store) {
            $store->data = str_replace("2","",$store->data);
            
            $row = [];

            $row[] = '<label class="mt-checkbox mt-checkbox-outline"><input class="check_player" value="'.$store->data.'" type="checkbox" data-id="'.$store->id.'" data-title="'.$store->title.'" data-title_en="'.$store->title_en.'" data-value="'.$store->value.'" data-value_en="'.$store->value_en.'"><span></span></label>';

            $row[] = $store->id;

            $row[] = '<i class="' . $store->data . '"</i>';

            $row[] = $store->title;

            $row[] = $store->value;

            $row[] = $store->title_en;

            $row[] = $store->value_en;

            $output[] = $row;
        }

        $data['status'] = 200;
        $data['value'] = $output;
        return response()->json($data, 200);
    }

    public function datatable_store2(Request $request) {

        $stores = DB::table('stores')
            ->select('*')
            ->where([
                ['id_code', '=', 1],
                ['del_flg', '=', 0]
            ])
            ->get();

        $output = [];
        foreach ($stores as $store) {
            if(!isset($request->admin))
            $store->data = str_replace("2","",$store->data);
            
            $row = [];

            $row[] = '<label class="mt-checkbox mt-checkbox-outline"><input class="check_player2" value="'.$store->data.'" type="checkbox" data-id="'.$store->id.'" data-title="'.$store->title.'" data-title_en="'.$store->title_en.'"><span></span></label>';

            $row[] = $store->id;

            $row[] = '<i class="' . $store->data . '"</i>';

            $row[] = $store->title;

            $row[] = $store->title_en;

            $output[] = $row;
        }

        $data['status'] = 200;
        $data['value'] = $output;
        return response()->json($data, 200);
    }

}