<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class SearchController extends BaseFrontendController
{
    public function index(Request $request)
    {

        $query_serach = $request->all();

        $where = [
            ['del_flg', '=', 0],
            ['active', '=', 0]
        ];

        $sl_item_project = [
            'title',
            'slug',
            'avatar',
            'address',
            'id',
            'updated_at',
            'map',
            'area',
            'phongngu',
            'succhua',
            'price'
        ];

        $key_slug = $this->slug($request->keyword);

        if ($request->keyword != '') {
            $where[] = ['search_slug', 'like', '%'.$key_slug.'%'];
        }

        if ($request->location != '') {
            $where[] = ['location', '=', $request->location];
        }

        if ($request->category != '') {
            $where[] = ['category_id', '=', $request->category];
        }

        if ($request->bathroom != '') {
            if ($request->bathroom == '>6') {
                $where[] = ['phongtam', '>', 6];
            } else{
                $where[] = ['phongtam', '=', $request->bathroom];
            }
        }

        if ($request->bedroom != '') {
            if ($request->bedroom == '>6') {
                $where[] = ['phongngu', '>', 6];
            } else{
                $where[] = ['phongngu', '=', $request->bedroom];
            }
        }

        if ($request->price) {

            if ($request->price == '<500') {
                $where[] = ['price', '<', 500];
            } else if($request->price == '>2000'){
                $where[] = ['price', '>', 2000];
            } else {
                $price = substr($request->price, 0, -1); //bỏ dấu , cuối cùng
                $price = explode(",",$request->price); //Chuyển thành array
                $price_min = $price[0];
                $price_max = $price[1];

                $where[] = ['price', '>=', $price_min];
                $where[] = ['price', '<=', $price_max];
            }
        }

        if ($request->area) {

            if ($request->area == '<500') {
                $where[] = ['area', '<', 500];
            } else if($request->area == '>2000'){
                $where[] = ['area', '>', 2000];
            } else {
                $area = substr($request->area, 0, -1); //bỏ dấu , cuối cùng
                $area = explode(",",$request->area); //Chuyển thành array
                $area_min = $area[0];
                $area_max = $area[1];

                $where[] = ['area', '>=', $area_min];
                $where[] = ['area', '<=', $area_max];
            }
        }

        $p_max = DB::table('item_projects')
            ->where($where)
            ->count();

        $item_projects = DB::table('item_projects')
            ->select('*')
            ->where($where)
            ->orderBy('position', 'desc')
            ->get();

        if ($p_max == 0) {
            $where = [];
            $where[] = ['del_flg', '=', 0];
            $where[] = ['active', '=', 0];
            $key_slug = $this->slug($request->keyword);

            $arrays = explode('-', $key_slug);
            $db_search_tags = DB::table('db_search_tags')
                ->select('title', 'slug')
                ->Where(function ($query) use($arrays) {
                    foreach ($arrays as $array) {
                        $query->orwhere('slug', 'like', '%'.$array.'%');
                    }       
                })
                ->get();

            $array_slugs = [];
            if ($key_slug != null) {
                foreach ($db_search_tags as $key => $value) {
                    if (strpos($key_slug, $value->slug) !== false) {
                        $array_slugs[] = $value->slug;
                    }
                }

                $array_slugs[] = $key_slug;
            }

            if ($request->location != '') {
                $where[] = ['location', '=', $request->location];
            }

            if ($request->category != '') {
                $where[] = ['category_id', '=', $request->category];
            }

            if ($request->bathroom != '') {
                if ($request->bathroom == '>6') {
                    $where[] = ['phongtam', '>', 6];
                } else{
                    $where[] = ['phongtam', '=', $request->bathroom];
                }
            }

            if ($request->bedroom != '') {
                if ($request->bedroom == '>6') {
                    $where[] = ['phongngu', '>', 6];
                } else{
                    $where[] = ['phongngu', '=', $request->bedroom];
                }
            }

            if ($request->price) {

                if ($request->price == '<500') {
                    $where[] = ['price', '<', 500];
                } else if($request->price == '>2000'){
                    $where[] = ['price', '>', 2000];
                } else {
                    $price = substr($request->price, 0, -1); //bỏ dấu , cuối cùng
                    $price = explode(",",$request->price); //Chuyển thành array
                    $price_min = $price[0];
                    $price_max = $price[1];

                    $where[] = ['price', '>=', $price_min];
                    $where[] = ['price', '<=', $price_max];
                }
            }

            if ($request->area) {

                if ($request->area == '<500') {
                    $where[] = ['area', '<', 500];
                } else if($request->area == '>2000'){
                    $where[] = ['area', '>', 2000];
                } else {
                    $area = substr($request->area, 0, -1); //bỏ dấu , cuối cùng
                    $area = explode(",",$request->area); //Chuyển thành array
                    $area_min = $area[0];
                    $area_max = $area[1];

                    $where[] = ['area', '>=', $area_min];
                    $where[] = ['area', '<=', $area_max];
                }
            }

            $p_max = DB::table('item_projects')
                ->where($where)
                ->Where(function ($query) use($array_slugs) {
                    foreach ($array_slugs as $array_slug) {
                        if ($array_slug != '') {
                            $query->orwhere('search_slug', 'like', '%'.$array_slug.'%');
                        }
                    }       
                })
                ->count();

            $item_projects = DB::table('item_projects')
                ->select($sl_item_project)
                ->where($where)
                ->Where(function ($query) use($array_slugs) {
                    foreach ($array_slugs as $array_slug) {
                        if ($array_slug != '') {
                            $query->orwhere('search_slug', 'like', '%'.$array_slug.'%');
                        }
                    }         
                })
                ->orderBy('position', 'desc')
                ->get();
            

        }
        foreach ($item_projects as $key => $item_project) {

                $item_album = DB::table('item_albums')
                    ->select('*')
                    ->where([
                        ['del_flg', '=', 0],
                        ['item_id', '=', $item_project->id]
                    ])
                    ->orderBy('id', 'asc')
                    ->first();
                if($item_album){
                    $item_projects[$key]->album = $item_album;
                }

            }
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 2]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.search.index')->with(compact('seopage', 'item_projects', 'query_serach'));
    }

    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}