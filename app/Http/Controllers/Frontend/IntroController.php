<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;


class IntroController extends BaseFrontendController
{
    public function index()
    {
        $intros = DB::table('intros')
            ->select($this->array_select('intros'))
            ->where([
                ['del_flg', '=', 0],
                ['type', '=', 1]
            ])
            ->get();
        $dinhhuongs = DB::table('dinhhuongs')
            ->select($this->array_select('dinhhuongs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->get();
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 2]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.intro.index')->with(compact('seopage','intros','dinhhuongs'));
    }
    public function personnel()
    {
        $personnel_values = DB::table('personnel_values')
            ->select($this->array_select('personnel_values'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->get();
        $personnel_count = DB::table('personnel_values')
            ->select($this->array_select('personnel_values'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->count();
        $personnels = DB::table('personnels')
            ->select($this->array_select('personnels'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->get();
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 2]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.intro.personnel')->with(compact('seopage','personnel_values','personnel_count','personnels'));
    }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}