<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class NhuongquyenController extends BaseFrontendController
{
    public function index()
    {   
        
        $nhuongquyens = DB::table('nhuongquyens')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->paginate(10);

        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.nhuongquyen.index')->with(compact('seopage','nhuongquyens','banner'));
    }
    
    public function detail($slug)
    {   
        $nhuongquyendt = DB::table('nhuongquyens')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        $nhuongquyen_cats = DB::table('nhuongquyen_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        
        $nhuongquyenlqs = DB::table('nhuongquyens')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', $nhuongquyendt->id],
                ['id_nhuongquyen_cat', '=', $nhuongquyendt->id_nhuongquyen_cat],
            ])
            ->orderBy('position', 'desc')
            ->get();

        $nhuongquyen_news = DB::table('nhuongquyens')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', $nhuongquyendt->id],
            ])
            ->orderBy('position', 'desc')
            ->get();
        
        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.nhuongquyen.detail')->with(compact('seopage','nhuongquyendt','nhuongquyenlqs','nhuongquyen_news','nhuongquyen_cats','banner'));
    }

    public function cat($slug)
    {   
        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();

        $nhuongquyen_cat_active = DB::table('nhuongquyen_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        
         $nhuongquyens = DB::table('nhuongquyens')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id_nhuongquyen_cat', '=', $nhuongquyen_cat_active->id],
            ])
            ->orderBy('position', 'desc')
            ->paginate(12);
        $nhuongquyen_cats = DB::table('nhuongquyen_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.nhuongquyen.cat')->with(compact('seopage','nhuongquyens','nhuongquyen_cat_active','banner','nhuongquyen_cats'));
    }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}