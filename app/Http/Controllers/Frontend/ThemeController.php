<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\ShopPartnerSupport;
use App\Model\theme;
use App\Model\theme_categorie;
use App\Model\theme_categorie1;
use App\Model\duan;

class ThemeController extends BaseFrontendController
{
    public function category($slug)
    {
        $theme_catedt = theme_categorie1::where([
            ['del_flg', '=', 0],
            ['slug', '=', $slug],
            ['status', '=', 0]
        ])->first();

        $themes = theme::where([
            ['id_theme_cat1', '=', $theme_catedt->id],
            ['del_flg', '=', 0],
            ['status', '=', 0]
        ])->orderBy('position', 'desc')->paginate(12);

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$theme_catedt->meta_title;
        $seopage->meta_description = @$theme_catedt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$theme_catedt->avatar,
            'data'=>'theme_categorie1',
            'time'=>$theme_catedt->updated_at
        ]);

        return view('frontend.theme.category')->with(compact('themes','theme_catedt','seopage'));
    }
    public function index()
    {
        $theme_cates = theme_categorie::where([
            ['del_flg', '=', 0],
            ['status', '=', 0]
        ])->orderBy('position', 'desc')->get();

        $theme_cate1s = theme_categorie1::where([
            ['del_flg', '=', 0],
            ['status', '=', 0]
        ])->orderBy('position', 'desc')->get();

        $themes = theme::where([
            ['del_flg', '=', 0],
            ['status', '=', 0]
        ])->orderBy('position', 'desc')->paginate(20);

        $duans = duan::where([
            ['del_flg', '=', 0],
            ['status', '=', 0]
        ])->orderBy('position', 'desc')->get();

        $theme_noibats = theme::where([
            ['del_flg', '=', 0],
            ['status', '=', 0],
            ['highlight', '=', 1],
        ])->orderBy('position', 'desc')->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.theme.index')->with(compact('themes','theme_cates','theme_cate1s','duans','theme_noibats','seopage'));
    }

    public function detail($slug)
    {
        $themedt = theme::where([
            ['del_flg', '=', 0],
            ['slug', '=', $slug]
        ])->first();

        $theme_catedt = theme_categorie1::where([
            ['del_flg', '=', 0],
            ['id', '=', $themedt->id_theme_cat1]
        ])->first();

        $theme_same_types = theme::where([
            ['del_flg', '=', 0],
            ['status', '=', 0],
            ['id_theme_cat1', '=', $themedt->id_theme_cat1],
            ['id', '!=', $themedt->id],
        ])->orderBy('position', 'desc')->limit(4)->get();

        $theme_noibats = theme::where([
            ['del_flg', '=', 0],
            ['status', '=', 0],
            ['highlight', '=', 1],
            ['id', '!=', $themedt->id],
        ])->orderBy('position', 'desc')->get();

        if(!$themedt) {
            return redirect('/');
        }

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$themedt->meta_title;
        $seopage->meta_description = @$themedt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$themedt->avatar,
            'data'=>'theme',
            'time'=>$themedt->updated_at
        ]);

        return view('frontend.theme.detail')->with(compact('seopage','themedt','theme_catedt','theme_same_types','theme_noibats'));
    }
    public function trial($slug, Request $request)
    {
        // echo "<pre>"; print_r($request->priceID);die; echo "</pre>";
        $themedt = theme::where([
            ['del_flg', '=', 0],
            ['slug', '=', $slug]
        ])->first();

        $price_domain_cats = DB::table('price_domain_cats')
        ->select('*')
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $price_domains = DB::table('price_domains')
        ->select('*')
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();

        $id_price = $request->priceID;
        
        if(!$themedt) {
            return redirect('/');
        }

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$themedt->meta_title;
        $seopage->meta_description = @$themedt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$themedt->avatar,
            'data'=>'theme',
            'time'=>$themedt->updated_at
        ]);

        return view('frontend.theme.trial')->with(compact('seopage','themedt','id_price','price_domain_cats','price_domains'));
    }
    public function demo($slug)
    {
        
        $themedt = theme::where([
            ['del_flg', '=', 0],
            ['slug', '=', $slug]
        ])->first();

        if(!$themedt) {
            return redirect('/');
        }

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$themedt->meta_title;
        $seopage->meta_description = @$themedt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$themedt->avatar,
            'data'=>'theme',
            'time'=>$themedt->updated_at
        ]);

        return view('frontend.theme.themedemo')->with(compact('seopage','themedt'));
    }

}