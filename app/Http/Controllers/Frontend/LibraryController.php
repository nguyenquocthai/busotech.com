<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class LibraryController extends BaseFrontendController
{
    public function index()
    {   
        
        $librarys = DB::table('librarys')
        ->select($this->array_select('librarys'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $librarynb = DB::table('librarys')
        ->select($this->array_select('librarys'))
            ->where([
                ['del_flg', '=', 0],
                ['hight_flg', '=', 1],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        $sliders = DB::table('sliders')
        ->select($this->array_select('sliders'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])
            ->get();
        $library_cats = DB::table('library_cats')
        ->select($this->array_select('library_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 11]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.library.index')->with(compact('seopage','librarynb','librarys','sliders','library_cats'));
    }
    public function cat($slug)
    {   
        
        $library_catdt = DB::table('library_cats')
        ->select($this->array_select('library_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        
        $librarys = DB::table('librarys')
        ->select($this->array_select('librarys'))
            ->where([
                ['del_flg', '=', 0],
                ['id_library_cat', '=', $library_catdt->id],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->paginate(10);
        $library_news = DB::table('librarys')
        ->select($this->array_select('librarys'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $library_cats = DB::table('library_cats')
        ->select($this->array_select('library_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 11]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.library.cat')->with(compact('seopage','library_news','librarys','library_catdt','library_cats'));
    }
    public function catall()
    {   
        $librarys = DB::table('librarys')
        ->select($this->array_select('librarys'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->paginate(10);

        $library_cats = DB::table('library_cats')
        ->select($this->array_select('library_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 11]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.library.catall')->with(compact('seopage','librarys','library_cats'));
    }
    public function detail($slug)
    {   
        $librarydt = DB::table('librarys')
        ->select($this->array_select('librarys'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        $librarys = DB::table('librarys')
        ->select($this->array_select('librarys'))
            ->where([
                ['del_flg', '=', 0],
                ['id', '!=', $librarydt->id],
                ['id_library_cat', '=', $librarydt->id_library_cat],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $library_news = DB::table('librarys')
        ->select($this->array_select('librarys'))
            ->where([
                ['del_flg', '=', 0],
                ['id', '!=', $librarydt->id],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $librarynbs = DB::table('librarys')
        ->select($this->array_select('librarys'))
            ->where([
                ['del_flg', '=', 0],
                ['id', '!=', $librarydt->id],
                ['hight_flg', '=', 1],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $library_cats = DB::table('library_cats')
        ->select($this->array_select('library_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $library_catdt = DB::table('library_cats')
        ->select($this->array_select('library_cats'))
            ->where([
                ['status', '=', 0],
                ['id', '=', $librarydt->id_library_cat],
                ['del_flg', '=', 0]
            ])
            ->first();
        
        $banner = DB::table('banners')
        ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 3],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 11]
            ])
            ->first();
        $seopage->meta_title = @$librarydt->meta_title;
        $seopage->meta_description = @$librarydt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$librarydt->avatar,
            'data'=>'library',
            'time'=>$librarydt->updated_at
        ]);

        return view('frontend.library.detail')->with(compact('seopage','librarydt','banner','librarys','library_news','librarynbs','library_cats','library_catdt'));
    }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}