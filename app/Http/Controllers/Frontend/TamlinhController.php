<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class TamlinhController extends BaseFrontendController
{
    public function index()
    {   
        
        $blogs = DB::table('tamlinhs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->paginate(12);

        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();
        $blog_cats = DB::table('tamlinh_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.tamlinh.index')->with(compact('seopage','blogs','banner','blog_cats'));
    }
    
    public function detail($slug)
    {   
        $tamlinhdt = DB::table('tamlinhs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        $tamlinh_cats = DB::table('tamlinh_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        
        $tamlinhlqs = DB::table('tamlinhs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', $tamlinhdt->id],
                ['id_tamlinh_cat', '=', $tamlinhdt->id_tamlinh_cat],
            ])
            ->orderBy('position', 'desc')
            ->get();

        $tamlinh_news = DB::table('tamlinhs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', $tamlinhdt->id],
            ])
            ->orderBy('position', 'desc')
            ->get();
        
        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.tamlinh.detail')->with(compact('seopage','tamlinhdt','tamlinhlqs','tamlinh_news','tamlinh_cats','banner'));
    }

    public function cat($slug)
    {   
        $blog_cat = DB::table('tamlinh_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();
        $blogs = DB::table('tamlinhs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id_tamlinh_cat', '=', $blog_cat->id],
            ])
            ->orderBy('position', 'desc')
            ->paginate(10);
        $blogall = DB::table('tamlinhs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])
            ->orderBy('position', 'desc')
            ->paginate(10);
        $blog_cats = DB::table('tamlinh_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.tamlinh.cat')->with(compact('seopage','blogs','blog_cat','banner','blog_cats','blogall'));
    }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}