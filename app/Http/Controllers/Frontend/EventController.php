<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class EventController extends BaseFrontendController
{
    public function index()
    {   
        
        $events = DB::table('events')
        ->select($this->array_select('events'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->paginate(10);
        $banner = DB::table('banners')
        ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 2],
            ])
            ->first();
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.event.index')->with(compact('seopage','blogs','banner','events'));
    }
    
    public function detail($slug)
    {   
        $eventdt = DB::table('events')
        ->select($this->array_select('events'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        $events = DB::table('events')
        ->select($this->array_select('events'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->limit(5)->get();

        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        
        $banner = DB::table('banners')
        ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 2],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$eventdt->meta_title;
        $seopage->meta_description = @$eventdt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$eventdt->avatar,
            'data'=>'event',
            'time'=>$eventdt->updated_at
        ]);

        return view('frontend.event.detail')->with(compact('seopage','eventdt','banner','blogs','events'));
    }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}