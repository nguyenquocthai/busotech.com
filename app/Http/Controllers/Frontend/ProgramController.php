<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class ProgramController extends BaseFrontendController
{
    public function index()
    {   
        
        $programs = DB::table('programs')
        ->select($this->array_select('programs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $banner = DB::table('banners')
        ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 4],
            ])
            ->first();
        $program_cats = DB::table('program_cats')
        ->select($this->array_select('program_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 5]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.dongy.index')->with(compact('seopage','programs','banner','program_cats'));
    }
    
    public function detail($slug)
    {   
        $programdt = DB::table('programs')
        ->select($this->array_select('programs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        $events = DB::table('events')
        ->select($this->array_select('events'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        $banner = DB::table('banners')
        ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 4],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 5]
            ])
            ->first();
        $seopage->meta_title = @$programdt->meta_title;
        $seopage->meta_description = @$programdt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$programdt->avatar,
            'data'=>'program',
            'time'=>$programdt->updated_at
        ]);

        return view('frontend.program.detail')->with(compact('seopage','programdt','banner','blogs','events'));
    }

    public function cat($slug)
    {   
        $program_cat = DB::table('program_cats')
        ->select($this->array_select('program_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->first();
        $programs = DB::table('programs')
        ->select($this->array_select('programs'))
            ->where([
                ['del_flg', '=', 0],
                ['id_program_cat', '=', @$program_cat->id],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $banner = DB::table('banners')
        ->select($this->array_select('banners'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 4],
            ])
            ->first();
        $program_cats = DB::table('program_cats')
        ->select($this->array_select('program_cats'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', @$program_cat->id]
            ])
            ->orderBy('position', 'desc')
            ->limit(5)
            ->get();
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        $events = DB::table('events')
        ->select($this->array_select('events'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(5)->orderBy('id','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 5]
            ])
            ->first();
        $seopage->meta_title = @$program_cat->meta_title;
        $seopage->meta_description = @$program_cat->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$program_cat->avatar,
            'data'=>'program_cat',
            'time'=>$program_cat->updated_at
        ]);

        return view('frontend.program.cat')->with(compact('program_cat','seopage','programs','program_cats','banner','blogs','events'));
        }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}