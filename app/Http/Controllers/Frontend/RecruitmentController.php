<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class RecruitmentController extends BaseFrontendController
{
    public function index()
    {   
        $recruitments = DB::table('recruitments')
        ->select($this->array_select('recruitments'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $departments = DB::table('departments')
        ->select($this->array_select('departments'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $recruitment_images = DB::table('recruitment_images')
        ->select($this->array_select('recruitment_images'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $recruitment_benefits = DB::table('recruitment_benefits')
        ->select($this->array_select('recruitment_benefits'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 9]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.recruitment.index')->with(compact('seopage','recruitment_images','recruitment_benefits','recruitments','departments'));
    }
    
    public function detail($slug)
    {   
        $recruitmentdt = DB::table('recruitments')
        ->select($this->array_select('recruitments'))
            ->where([
                ['status', '=', 0],
                ['slug', '=', $slug],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->first();
        $recruitments = DB::table('recruitments')
        ->select($this->array_select('recruitments'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$recruitmentdt->meta_title;
        $seopage->meta_description = @$recruitmentdt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$recruitmentdt->avatar,
            'data'=>'recruitment',
            'time'=>$recruitmentdt->updated_at
        ]);

        return view('frontend.recruitment.detail')->with(compact('seopage','recruitmentdt','recruitments'));
    }
    public function cat($slug)
    {   
        $nhahang_cat_active = DB::table('nhahang_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();
        $nhahang_cats = DB::table('nhahang_cats')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $all_nhahang = DB::table('nhahangs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id_nhahang_cat', '=', $nhahang_cat_active->id],
            ])
            ->orderBy('position', 'desc')
            ->get();

        $page_start = 0;
        $max_page = count($all_nhahang);


        $nhahangs = DB::table('nhahangs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                 ['status', '=', 0],
                 ['id_nhahang_cat', '=', $nhahang_cat_active->id],
            ])
            ->offset($page_start)
            ->limit(6)
            ->orderBy('position', 'desc')
            ->get();
        $page_current = count($nhahangs) + $page_start;

        if($max_page > $page_current)
            $next_page=1;
        else
            $next_page= 0;

        $banner = DB::table('banners')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 1],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 3]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.nhahang.cat')->with(compact('seopage','nhahangs','nhahang_cats','banner','all_nhahang','page_current','next_page','nhahang_cat_active'));
    }
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}