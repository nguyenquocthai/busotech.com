<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ActiveEmail;
use App\Mail\ActiveEmail1;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;
use \stdClass;

class ContactController extends BaseFrontendController
{
    public function index()
    {
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 7]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.contact.index')->with(compact('seopage'));
    }
    public function add_contact(Request $request)
    {
        $data = [];

        DB::table('contacts')->insert([
            'name' => @$request->name,
            'firstname' => @$request->firstname,
            'email' => @$request->email,
            'tel' => @$request->tel,
            'content' => @$request->content,
            'status' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);

        $data['code'] = 200;
        return response()->json($data, 200);
    }
    public function add_subscribe(Request $request)
    {
        $data = [];

        DB::table('subscribes')->insert([
            'email' => @$request->email,
            'status' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);

        $data['code'] = 200;
        return response()->json($data, 200);
    }
    public function add_contact_brand(Request $request)
    {
        $data = [];

        DB::table('brand_contacts')->insert([
            'name' => @$request->name,
            'email' => @$request->email,
            'tel' => @$request->tel,
            'branding_type' => @$request->branding_type,
            'status' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);

        $data['code'] = 200;
        return response()->json($data, 200);
    }
    public function add_contact_partner(Request $request)
    {
        $data = [];

        DB::table('partner_contacts')->insert([
            'name' => @$request->name,
            'email' => @$request->email,
            'tel' => @$request->tel,
            'name_shop' => @$request->name_shop,
            'address_shop' => @$request->address_shop,
            'status' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);

        $data['code'] = 200;
        return response()->json($data, 200);
    }
    public function add_contact_hosting(Request $request)
    {
        $data = [];

        DB::table('hosting_contacts')->insert([
            'name' => @$request->name,
            'email' => @$request->email,
            'tel' => @$request->tel,
            'address' => @$request->address,
            'city' => @$request->city,
            'zip_code' => @$request->zip_code,
            'country' => @$request->country,
            'user' => @$request->user,
            'password' => @$request->password,
            'check_invoice' => @$request->check_invoice,
            'name_co' => @$request->name_co,
            'address_co' => @$request->address_co,
            'tax_code' => @$request->tax_code,
            'id_host' => @$request->id_host,
            'name_host' => @$request->name_host,
            'time_host' => @$request->time_host,
            'price_host' => @$request->price_host,
            'status' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);

        $data['code'] = 200;
        $data['ten_goi_host'] = @$request->ten_goi_host;
        $data['price_host'] = @$request->price_host;
        $data['time_host'] = @$request->time_host;
        return response()->json($data, 200);
    }
    public function add_tuyendung(Request $request)
    {
        $data = [];
       
        $filetuyendung = @$request->file_tuyendung;
        $params = json_decode(@$request->params);
        // echo "<pre>"; print_r($params->name); echo "</pre"; die;
        $filetuyendung->move('file_tuyendung', $filetuyendung->getClientOriginalName());
        DB::table('recruitment_contacts')->insert([
            'name' => @$params->name,
            'email' => @$params->email,
            'tel' => @$params->tel,
            'vitri' => @$params->vitri,
            'file' => @$filetuyendung->getClientOriginalName(),
            'status' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);


        $data['code'] = 200;
        return response()->json($data, 200);
    }
    //------------------------------------------
    public function add_contact_theme(Request $request)
    {
        $data = [];

        
        if ($request->status_code === 'search_domain') {
            // echo "<pre>"; print_r($request->all());die; echo "</pre";
            // $arr_domain = ['.vn','.com','.asia','.net','.com.vn','.org','.info','.online','.xyz','.top','.vip','.site','.pro','.club'];
            
            $string_search = $request->name_domain_search;
            $string_search = explode(".",$string_search);
            $string_search = $string_search[0];
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://daily.pavietnam.vn/interface.php?username=sysnet&apikey=36d08f9730aa0e555fa2b71f69896114&cmd=check_whois&domain=".$request->name_domain_search."&responsetype=json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET"
            ));
            $response_domain = curl_exec($curl);
            curl_close($curl);
            $item_check = new stdClass();
            $item_check->val = $response_domain;
            $item_check->domain = $request->name_domain_search;
            
            // echo '<pre>';print_r($item_check);echo '</pre>';die;
            
            if($response_domain == 1){
                $data['code'] = 200;
                $data['domain_check'] = $request->name_domain_search;
                $request->domain_name = $request->name_domain_search;
                $data['value'] = 'Có thể đăng ký';
                $data['color'] = '#92da92b3';
                return response()->json($data, 200);
            }
            else{
                
                $data['code'] = 300;
                $data['domain_check'] = $request->name_domain_search;
                $data['value'] = 'Đã được đăng ký';
                $data['color'] = 'red';
                return response()->json($data, 200);
            }
            
        }
        
       
        $obj = new \stdClass();
        $obj->name = $request->fullname;
        $obj->email = $request->email;
        $obj->phone = $request->phone;
        $obj->address = $request->address;
        $obj->domain_old = $request->domain_old;
        if($request->status_check == 3 ){
            $obj->domain_name = $request->domain_name.'.busotech.site';
        }
        else {
            $obj->domain_name = $request->domain_name;
        }
        $obj->content = $request->content;
        $obj->password = $request->password;
        // return response()->json($obj, 200);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://apiwebconcam.thegioiwebs.net/add_contact",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>json_encode($obj),
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        
        $result = json_decode($response);

        // echo "<pre>"; print_r($result);echo "</pre>";die; 
        
        if($result->code == 300){
            $data['code'] = 300;
            $data['value'] = $result->value;
            return response()->json($data, 200);
        }
        if($request->status_check == 3 ){
            $domain_name = $request->domain_name.'.busotech.site';
        }
        else {
            $domain_name = $request->domain_name;
        }
        DB::table('contacts')->insert([
            'name' => $request->fullname,
            'email' => $request->email,
            'tel' => $request->phone,
            'address' => $request->address,
            'domain_name' => $domain_name,
            'domain_active' => @$request->domain_active,
            'id_price' => $request->id_price,
            'status' => 1,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        $objmail = new \stdClass();
        $objmail->sender = 'buso.asia';
        $objmail->name = $request->fullname;
        $objmail->email = $request->email;
        $objmail->phone = $request->phone;
        $objmail->address = $request->address;
        if($request->status_check == 3 ){
            $objmail->domain_name = $request->domain_name.'.busotech.site';
        }
        else{
            $objmail->domain_name = $request->domain_name;
        }
        // $objmail->domain_name = $request->domain_name.'.busotech.site';
        $objmail->password = $request->password;
        // Mail::to('phancaohuan1989@gmail.com')->send(new ActiveEmail($objmail));
        // Mail::to($request->email)->send(new ActiveEmail1($objmail));

        
        $data['code'] = 200;
        $data['value'] = $result->value;
        $data['email'] = $request->email;
        if($request->status_check == 3 ){
            $data['domain_name'] = $request->domain_name.'.busotech.site';
        }
        else{
            $data['domain_name'] = $request->domain_name;
        }
        // $data['domain_name'] = $request->domain_name.'.busotech.site';
        return response()->json($data, 200);
    }
    public function check_email(Request $request)
    {
        $data = [];
       
        $obj = new \stdClass();
        $obj->email = $request->email;

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://apiwebconcam.thegioiwebs.net/check_email",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>json_encode($obj),
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        
        $result = json_decode($response);

        // echo "<pre>"; print_r($result);die; echo "</pre>";
        
        if($result->code == 300){
            $data['code'] = 300;
            $data['value'] = $result->value;
            return response()->json($data, 200);
        }

        $data['code'] = 200;
        return response()->json($data, 200);
    }
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}