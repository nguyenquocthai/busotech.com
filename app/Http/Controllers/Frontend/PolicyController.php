<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class PolicyController extends BaseFrontendController
{
    public function quydinhsudung($slug)
    {   
        $quydinhsudung = DB::table('policies')
            ->select($this->array_select('policies'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 13]
            ])
            ->first();
        $seopage->meta_title = @$quydinhsudung->meta_title;
        $seopage->meta_description = @$quydinhsudung->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>@$quydinhsudung->avatar,
            'data'=>'policies',
            'time'=>@$quydinhsudung->updated_at
        ]);

        return view('frontend.policy.quydinhsudung')->with(compact('seopage','quydinhsudung'));
    }
    public function huongdanthanhtoan()
    {   
        
        $bank_accs = DB::table('bank_accs')
            ->select($this->array_select('bank_accs'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])->orderBy('position', 'desc')
            ->get();

        $payment_guides = DB::table('payment_guides')
            ->select($this->array_select('payment_guides'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])->orderBy('id', 'asc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 14]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_pages',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.policy.huongdanthanhtoan')->with(compact('seopage','payment_guides','bank_accs'));
    }
    public function dailyvadoitac()
    {   
        $partner_intros = DB::table('partner_intros')
            ->select($this->array_select('partner_intros'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])->orderBy('position', 'desc')
            ->get();
        $partner_advantages = DB::table('partner_advantages')
            ->select($this->array_select('partner_advantages'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])->orderBy('position', 'desc')
            ->get();
        $partner_steps = DB::table('partner_steps')
            ->select($this->array_select('partner_steps'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])->orderBy('position', 'desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 15]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_pages',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.policy.partner')->with(compact('seopage','partner_intros','partner_advantages','partner_steps'));
    }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}