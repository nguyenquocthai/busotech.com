<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class SupportController extends BaseFrontendController
{
    public function index()
    {
        $support_cats = DB::table('support_cats')
        ->select($this->array_select('support_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $supports = DB::table('supports')
        ->select($this->array_select('supports'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $supportfaqs = DB::table('supportfaqs')
        ->select($this->array_select('supportfaqs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
                ['hight_flg', '=', 1]
            ])->orderBy('position','desc')
            ->get();
        $videos = DB::table('videos')
            ->select($this->array_select('videos'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        foreach ($videos as $key => $video) {

            $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
             $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

            if (preg_match($longUrlRegex, $video->link_youtube, $matches)) {
                $youtube_id = $matches[count($matches) - 1];
            }

            if (preg_match($shortUrlRegex, $video->link_youtube, $matches)) {
                $youtube_id = $matches[count($matches) - 1];
            }
            $embed = 'https://www.youtube.com/embed/' . $youtube_id ;
            $img_youtube = 'https://img.youtube.com/vi/'.$youtube_id.'/hqdefault.jpg';
            $video->linkyoutube = $embed;
            $video->img_youtube = $img_youtube;
        }
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 12]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.support.index')->with(compact('seopage','supports','support_cats','videos','supportfaqs'));
    }
    public function guide_cat($slug)
    {
        $support_catdt = DB::table('support_cats')
        ->select($this->array_select('support_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
                ['slug', '=', $slug]
            ])
            ->first();
        $supports = DB::table('supports')
        ->select($this->array_select('supports'))
            ->where([
                ['status', '=', 0],
                ['id_support_cat', '=', $support_catdt->id],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $supportfaqs = DB::table('supportfaqs')
        ->select($this->array_select('supportfaqs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
            ])->orderBy('position','desc')
            ->get();
        $videos = DB::table('videos')
            ->select($this->array_select('videos'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        foreach ($videos as $key => $video) {

            $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
             $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

            if (preg_match($longUrlRegex, $video->link_youtube, $matches)) {
                $youtube_id = $matches[count($matches) - 1];
            }

            if (preg_match($shortUrlRegex, $video->link_youtube, $matches)) {
                $youtube_id = $matches[count($matches) - 1];
            }
            $embed = 'https://www.youtube.com/embed/' . $youtube_id ;
            $img_youtube = 'https://img.youtube.com/vi/'.$youtube_id.'/hqdefault.jpg';
            $video->linkyoutube = $embed;
            $video->img_youtube = $img_youtube;
        }
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 12]
            ])
            ->first();
        $seopage->meta_title = @$support_catdt->meta_title;
        $seopage->meta_description = @$support_catdt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.support.guide_cat')->with(compact('seopage','supports','support_catdt','videos','supportfaqs'));
    }
    public function guide()
    {
        $support_cats = DB::table('support_cats')
        ->select($this->array_select('support_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
            ])->orderBy('position','desc')
            ->get();
        $supports = DB::table('supports')
        ->select($this->array_select('supports'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $supportfaqs = DB::table('supportfaqs')
        ->select($this->array_select('supportfaqs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $videos = DB::table('videos')
            ->select($this->array_select('videos'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        foreach ($videos as $key => $video) {

            $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
             $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

            if (preg_match($longUrlRegex, $video->link_youtube, $matches)) {
                $youtube_id = $matches[count($matches) - 1];
            }

            if (preg_match($shortUrlRegex, $video->link_youtube, $matches)) {
                $youtube_id = $matches[count($matches) - 1];
            }
            $embed = 'https://www.youtube.com/embed/' . $youtube_id ;
            $img_youtube = 'https://img.youtube.com/vi/'.$youtube_id.'/hqdefault.jpg';
            $video->linkyoutube = $embed;
            $video->img_youtube = $img_youtube;
        }
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 12]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.support.guide')->with(compact('seopage','supports','support_cats','videos','supportfaqs'));
    }
    public function guide_detail($slug)
    {
        $supportdt = DB::table('supports')
        ->select($this->array_select('supports'))
            ->where([
                ['status', '=', 0],
                ['slug', '=', $slug],
                ['del_flg', '=', 0]
            ])
            ->first();
        $support_catdt = DB::table('support_cats')
        ->select($this->array_select('support_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
                ['id', '=', $supportdt->id_support_cat],
            ])
            ->first();
        $supports = DB::table('supports')
        ->select($this->array_select('supports'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
                ['id_support_cat', '=', $supportdt->id_support_cat],
                ['id', '<>', $supportdt->id],
            ])->orderBy('position','desc')
            ->get();
        $supportfaqs = DB::table('supportfaqs')
        ->select($this->array_select('supportfaqs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $videos = DB::table('videos')
            ->select($this->array_select('videos'))
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('id', 'desc')
            ->get();
        foreach ($videos as $key => $video) {

            $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
             $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

            if (preg_match($longUrlRegex, $video->link_youtube, $matches)) {
                $youtube_id = $matches[count($matches) - 1];
            }

            if (preg_match($shortUrlRegex, $video->link_youtube, $matches)) {
                $youtube_id = $matches[count($matches) - 1];
            }
            $embed = 'https://www.youtube.com/embed/' . $youtube_id ;
            $img_youtube = 'https://img.youtube.com/vi/'.$youtube_id.'/hqdefault.jpg';
            $video->linkyoutube = $embed;
            $video->img_youtube = $img_youtube;
        }
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 12]
            ])
            ->first();
        $seopage->meta_title = @$supportdt->meta_title;
        $seopage->meta_description = @$supportdt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.support.guide_detail')->with(compact('seopage','supportdt','supports','support_catdt','videos','supportfaqs'));
    }
    public function faq()
    {
        $supportfaq_cats = DB::table('supportfaq_cats')
        ->select($this->array_select('supportfaq_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
            ])->orderBy('position','desc')
            ->get();
        
        $supportfaqs = DB::table('supportfaqs')
        ->select($this->array_select('supportfaqs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 12]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.support.faq')->with(compact('seopage','supportfaq_cats','supportfaqs'));
    }
    public function faqcat($slug)
    {
        $supportfaq_catdt = DB::table('supportfaq_cats')
        ->select($this->array_select('supportfaq_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
            ])->orderBy('position','desc')
            ->first();
        
        $supportfaqs = DB::table('supportfaqs')
        ->select($this->array_select('supportfaqs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
                ['id_supportfaq_cat', '=', $supportfaq_catdt->id],
            ])->orderBy('position','desc')
            ->paginate(15);
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 12]
            ])
            ->first();
        $seopage->meta_title = @$supportfaq_catdt->meta_title;
        $seopage->meta_description = @$supportfaq_catdt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.support.faqcat')->with(compact('seopage','supportfaq_catdt','supportfaqs'));
    }
    public function faqdetail($slug)
    {
        $supportfaqdt = DB::table('supportfaqs')
        ->select($this->array_select('supportfaqs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        $supportfaq_catdt = DB::table('supportfaq_cats')
        ->select($this->array_select('supportfaq_cats'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
                ['id', '=', $supportfaqdt->id_supportfaq_cat],
            ])
            ->first();
        
        $supportfaqs = DB::table('supportfaqs')
        ->select($this->array_select('supportfaqs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0],
                ['id_supportfaq_cat', '=', @$supportfaq_catdt->id],
                ['id', '<>', $supportfaqdt->id],
            ])->orderBy('position','desc')->limit(5)
            ->get();
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 12]
            ])
            ->first();
        $seopage->meta_title = @$supportfaqdt->meta_title;
        $seopage->meta_description = @$supportfaqdt->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.support.faqdetail')->with(compact('seopage','supportfaq_catdt','supportfaqs','supportfaqdt'));
    }
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}