<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Model\ShopCategory;
use App\Model\ShopProduct;
use App\Model\ShopBlog;
use App\Model\ShopRegulation;
use App\Model\Custompage;
use App\Model\ShopInformationCategory;
use App\Model\ShopInformation;
use App\Model\cmprivates;
use App\Model\ShopChainstore;
use App\Model\ShopBlogCategory;

class SitemapController extends BaseFrontendController
{
    public function index()
    {
        $shop_categorie = ShopCategory::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->orderBy('updated_at', 'desc')->first();

        $ShopProduct = ShopProduct::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->orderBy('updated_at', 'desc')->first();

        $ShopBlog = ShopBlog::where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['shop_blog_category_id', '!=', 33]
            ])->orderBy('updated_at', 'desc')->first();

        $support = ShopBlog::where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['shop_blog_category_id', '=', 33]
            ])->orderBy('updated_at', 'desc')->first();

        $ShopRegulation = ShopRegulation::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->orderBy('updated_at', 'desc')->first();

        $Custompage = Custompage::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->orderBy('updated_at', 'desc')->first();

        $ShopInformationCategory = ShopInformationCategory::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->orderBy('updated_at', 'desc')->first();

        $ShopInformation = ShopInformation::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->orderBy('updated_at', 'desc')->first();

        $ShopBlogCategory = ShopBlogCategory::where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', 33]
            ])->orderBy('updated_at', 'desc')->first();

        return response()->view('frontend.sitemap.index', [
            'shop_categorie' => $shop_categorie,
            'ShopProduct' => $ShopProduct,
            'ShopBlog' => $ShopBlog,
            'ShopBlogCategory' => $ShopBlogCategory,
            'support' => $support,
            'ShopRegulation' => $ShopRegulation,
            'Custompage' => $Custompage,
            'ShopInformationCategory' => $ShopInformationCategory,
            'ShopInformation' => $ShopInformation

        ])->header('Content-Type', 'text/xml');
    }

    public function shop_categorie()
    {
        $shop_categories = ShopCategory::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->get();

        return response()->view('frontend.sitemap.shop_categories', [
            'shop_categories' => $shop_categories,
        ])->header('Content-Type', 'text/xml');
    }

    public function ShopProduct()
    {
        $ShopProducts = ShopProduct::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->get();

        return response()->view('frontend.sitemap.ShopProducts', [
            'ShopProducts' => $ShopProducts,
        ])->header('Content-Type', 'text/xml');
    }

    public function ShopBlog()
    {
        $ShopBlogs = ShopBlog::where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['shop_blog_category_id', '!=', 33]
            ])->get();

        return response()->view('frontend.sitemap.ShopBlogs', [
            'ShopBlogs' => $ShopBlogs,
        ])->header('Content-Type', 'text/xml');
    }

    public function support()
    {
        $supports = ShopBlog::where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['shop_blog_category_id', '=', 33]
            ])->get();

        return response()->view('frontend.sitemap.supports', [
            'supports' => $supports,
        ])->header('Content-Type', 'text/xml');
    }

    public function ShopRegulation()
    {
        $ShopRegulations = ShopRegulation::where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])->get();

        return response()->view('frontend.sitemap.ShopRegulations', [
            'ShopRegulations' => $ShopRegulations,
        ])->header('Content-Type', 'text/xml');
    }

    public function Custompage()
    {
        $Custompages = Custompage::where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])->get();

        return response()->view('frontend.sitemap.Custompages', [
            'Custompages' => $Custompages,
        ])->header('Content-Type', 'text/xml');
    }

    public function ShopInformationCategory()
    {
        $ShopInformationCategorys = ShopInformationCategory::where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])->get();

        return response()->view('frontend.sitemap.ShopInformationCategorys', [
            'ShopInformationCategorys' => $ShopInformationCategorys,
        ])->header('Content-Type', 'text/xml');
    }

    public function ShopInformation()
    {
        $ShopInformations = ShopInformation::where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
            ])->get();

        return response()->view('frontend.sitemap.ShopInformations', [
            'ShopInformations' => $ShopInformations,
        ])->header('Content-Type', 'text/xml');
    }

    public function ShopBlogCategory()
    {
        $ShopBlogCategorys = ShopBlogCategory::where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', 33]
            ])->get();

        return response()->view('frontend.sitemap.ShopBlogCategorys', [
            'ShopBlogCategorys' => $ShopBlogCategorys,
        ])->header('Content-Type', 'text/xml');
    }

    public function public_page()
    {
        $cmprivate = cmprivates::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->orderBy('updated_at', 'desc')->first();

        $ShopChainstore = ShopChainstore::where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])->orderBy('updated_at', 'desc')->first();

        return response()->view('frontend.sitemap.public_pages', [
            'cmprivate' => $cmprivate,
            'ShopChainstore' => $ShopChainstore,
        ])->header('Content-Type', 'text/xml');
    }
}