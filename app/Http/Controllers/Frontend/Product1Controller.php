<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Model\contact;

class Product1Controller extends BaseFrontendController
{
    public function index()
    {   
        
        $product1s = DB::table('product1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        $intro = DB::table('intros')
            ->select('*')
            ->where([
                ['id', '=', 15],
            ])
            ->orderBy('position', 'desc')
            ->first();
        $slider_prods = DB::table('sliders')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['type', '=', 7],
            ])
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.product1.index')->with(compact('seopage','product1s','slider_prods','intro'));
    }
    
    public function detail($slug)
    {   
        $product1dt = DB::table('product1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['slug', '=', $slug],
            ])
            ->first();
        
         $product1s = DB::table('product1s')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '!=', $product1dt->id],
            ])
            ->orderBy('position', 'desc')->limit(4)
            ->get();
        $item_albums = DB::table('item_albums')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['type', '=', 2],
                ['item_id', '=', $product1dt->id],
            ])
            ->orderBy('position', 'desc')
            ->get();
        $doitacs = DB::table('doitacs')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>$seo_page->updated_at
        ]);

        return view('frontend.product1.detail')->with(compact('seopage','doitacs','product1dt','product1s','item_albums'));
    }
    
    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}