<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\ErrorCodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Model\setting;
use Illuminate\Support\Facades\DB;
use App\Model\activitie;
use App\Model\plan_child;
use App\Model\plan;

class BaseFrontendController extends Controller
{

    public function __construct()
    {
        $this->boot();
    }

    //-------------------------------------------------------------------------------
    public function boot()
    {
        date_default_timezone_set("Asia/Bangkok");

        

        // Get info web
        $ShopSetting_data = setting::where('key','=','info_website')->first();
        $ShopSetting = json_decode($ShopSetting_data, true);
        $info_web = json_decode($ShopSetting['value'], true);
        View::share('info_web', $info_web);

        if (session('cart')) {
            $carts = session('cart');
        } else {
            $carts = [];
        }

        View::share('count_carts', count($carts));

        $this->middleware(function ($request, $next) {
            $lang = session('lang');
            if ($lang == null) {
                session(['lang' => '']);
                $lang = session('lang');
            } else {
                $lang = session('lang');
            }
            View::share('lang', $lang);

            $db_langs = DB::table('db_langs')
                ->select('*')
                ->where('del_flg', '=', 0)
                ->get();
                
            $langs = [];
            foreach ($db_langs as $row_langs) {
                $langs[$row_langs->slug] = $row_langs->{'title' . $lang};
            }
            View::share('langs', $langs);

            $service_footers = DB::table('services')
                ->select($this->array_select('services'))
                ->where([
                    ['status', '=', 0],
                    ['del_flg', '=', 0]
                ])->limit(6)->orderBy('id','desc')
                ->get();
            View::share('service_footers', $service_footers);

            $policies_footers = DB::table('policies')
                ->select($this->array_select('policies'))
                ->where([
                    ['status', '=', 0],
                    ['del_flg', '=', 0]
                ])->orderBy('id','desc')
                ->get();
            View::share('policies_footers', $policies_footers);

            return $next($request);
        });

        // if (strpos($_SERVER['REQUEST_URI'], '/demo') === false) {
        //     if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
        //         $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        //         header('HTTP/1.1 301 Moved Permanently');
        //         header('Location: ' . $location);
        //         exit;
        //     }
        // } else {
        //     if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === "on") {
        //         $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        //         header('Location: ' . $url, true, 301);
        //         exit();
        //     }
        // }

    }
}
