<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\BaseFrontendController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class HomeController extends BaseFrontendController
{
    public function index()
    {
        
        $intro = DB::table('intros')
            ->select($this->array_select('intros'))
            ->where([
                ['del_flg', '=', 0],
                ['id', '=', 10]
            ])
            ->first();

        $buso_steps = DB::table('buso_steps')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0]
            ])
            ->orderBy('position', 'desc')
            ->get();

        $services = DB::table('services')
        ->select($this->array_select('services'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(6)->orderBy('position','desc')
            ->get();
        $doitacs = DB::table('doitacs')
        ->select($this->array_select('doitacs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->limit(15)->orderBy('position','desc')
            ->get();
        // echo "<pre>"; print_r($intro);die; echo "</pre";
        $blogs = DB::table('blogs')
        ->select($this->array_select('blogs'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        $librarys = DB::table('librarys')
        ->select($this->array_select('librarys'))
            ->where([
                ['status', '=', 0],
                ['del_flg', '=', 0]
            ])->orderBy('position','desc')
            ->get();
        
        // SEO
        $seopage = new \stdClass();
        $seo_page = DB::table('seo_pages')
            ->select('*')
            ->where([
                ['del_flg', '=', 0],
                ['status', '=', 0],
                ['id', '=', 1]
            ])
            ->first();
        $seopage->meta_title = @$seo_page->meta_title;
        $seopage->meta_description = @$seo_page->meta_description;
        $seopage->linkpage = $this->fullBaseUrl();
        $seopage->image = $this->GetImg_seo([
            'avatar'=>@$seo_page->avatar,
            'data'=>'seo_page',
            'time'=>@$seo_page->updated_at
        ]);

        return view('frontend.home.index')->with(compact('intro','seopage','blogs','services','doitacs','buso_steps','librarys'));
    }
    //-------------------------------------------------------------------------------
    public function chang_lang(Request $request)
    {
        try {
            session(['lang' => $request->lang]);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function back_link(Request $request)
    {
        try {
            session()->put('go_page', $request->link);
            $data['code'] = 200;
            return response()->json($data, 200);
    
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}