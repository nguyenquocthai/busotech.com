<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'list_rule'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //-------------------------------------------------------------------------------
    public static function validate() {
        return [
            'pattern' => [
                'name' =>'required',
                'email' =>'required',
                'status' => 'required|numeric'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'numeric'=>':attribute phải là số'
            ],

            'customName' => [
                'name'=>'Tên',
                'email'=>'Email',
                'status'=>'Trạng thái',
            ]
        ];
    }
}
