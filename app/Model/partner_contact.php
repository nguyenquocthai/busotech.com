<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class partner_contact extends Model
{
    protected $table = 'partner_contacts';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id=0) {
        return [
            'pattern' => [
                
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                
            ]
        ];
    }
}
