<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class price_domain extends Model
{
    protected $table = 'price_domains';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id=0) {
        return [
            'pattern' => [
                'title' =>'required',
                'slug' => 'required|unique:price_domains,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                'title'=>'Tiêu đề',
                'slug'=>'Đường dẫn',
            ]
        ];
    }
}
