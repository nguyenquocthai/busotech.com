<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class website_feature_cat extends Model
{
    protected $table = 'website_feature_cats';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id) {
        return [
            'pattern' => [
                'title' =>'required',
                'slug' => 'required|unique:website_feature_cats,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                'title'=>'Tiêu đề',
                'slug'=>'Đường dẫn',
            ]
        ];
    }
}
