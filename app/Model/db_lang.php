<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class db_lang extends Model
{
    protected $table = 'db_langs';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id) {
        return [
            'pattern' => [
                'title' =>'required',
                'slug' => 'required|unique:db_langs,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute đã tồn tại'
            ],

            'customName' => [
                'title'=>'Tiêu đề',
                'slug'=>'Tiêu đề',
            ]
        ];
    }
}
