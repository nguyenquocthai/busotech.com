<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class contact extends Model
{
    protected $table = 'contacts';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id=0) {
        return [
            'pattern' => [
                
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                
            ]
        ];
    }
}
