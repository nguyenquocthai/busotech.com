<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class subscribe extends Model
{
    protected $table = 'subscribes';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id=0) {
        return [
            'pattern' => [
                
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                
            ]
        ];
    }
}
