<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class intro extends Model
{
    protected $table = 'intros';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id=0) {
        return [
            'pattern' => [
                'title' =>'required',
                'slug' => 'unique:blog_cats,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                'title'=>'Tiêu đề',
                'slug'=>'Đường dẫn',
            ]
        ];
    }
}
