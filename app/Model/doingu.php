<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class doingu extends Model
{
    protected $table = 'doingus';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id=0) {
        return [
            'pattern' => [
                'title' =>'required',
                'slug' => 'required|unique:doingus,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                'title'=>'Tiêu đề',
                'slug'=>'Đường dẫn',
            ]
        ];
    }
}
