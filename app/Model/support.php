<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class support extends Model
{
    protected $table = 'supports';
    //public $timestamps = false;

    //-------------------------------------------------------------------------------
    public static function validate($id) {
        return [
            'pattern' => [
                'title' =>'required',
                'slug' => 'required|unique:supports,slug,' . $id . ',id,del_flg,0'
            ],

            'messenger' => [
                'required'=>':attribute không được để trống',
                'unique' => ':attribute không được trùng'
            ],

            'customName' => [
                'title'=>'Tiêu đề',
                'slug'=>'Đường dẫn',
            ]
        ];
    }
}
