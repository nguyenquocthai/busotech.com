@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- 404-->
  <section class="section section-404">
    <div class="container">
      <figure class="img"><img src="/public/theme/img/404.png" alt=""/></figure>
      <div class="title-block text-center">
        <h2 class="title-main">Không tìm thấy trang</h2><a class="rs-btn btn-gradient" href="/">Trở về Trang chủ</a>
      </div>
      <div class="section-404__nav">
        <h3 class="item">Buso Web&nbsp;<a class="link" href="/busoweb">www.buso.asia/busoweb</a></h3>
        <h3 class="item">Buso Brand&nbsp;<a class="link" href="/brand">www.buso.asia/busobrand</a></h3>
        <h3 class="item">Giao diện&nbsp;<a class="link" href="/giao-dien">www.buso.asia/giaodien</a></h3>
      </div>
    </div>
  </section>
</main>
@endsection