@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- BANNER-->
      <section class="section banner-page support-banner bg-gradient">
        <div class="container text-center">
          <p class="rs-list support-center"><span class="item d-inline-block">Free support call center</span><span class="item d-inline-block"><i class="fa fa-phone"></i><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a></span></p>
          <form class="form-search form-search--sm" action="#!">
            <input class="input" type="text" placeholder="What help do you need…?"/>
            <button class="rs-btn button"><svg xmlns="http://www.w3.org/2000/svg" width="35.143" height="39.06" viewBox="0 0 35.143 39.06"><g transform="translate(-736.513 -477.665)" opacity="0.501"><path d="M766.91,516.725a.929.929,0,0,0,.589-.21l3.81-3.094a.932.932,0,0,0,.341-.63.944.944,0,0,0-.2-.688l-9.568-11.779a14.124,14.124,0,1,0-12.7,5.5,14.3,14.3,0,0,0,1.481.077,13.972,13.972,0,0,0,5.938-1.32l9.584,11.8A.935.935,0,0,0,766.91,516.725Zm-17.537-12.767a12.244,12.244,0,1,1,8.982-2.675A12.166,12.166,0,0,1,749.374,503.958Zm10.164-1.22a14.392,14.392,0,0,0,1.089-.983l8.774,10.8-2.355,1.913-8.783-10.812A14.25,14.25,0,0,0,759.537,502.738Z" transform="translate(0)" fill="#696969"/><path d="M758.026,484.63a10.33,10.33,0,1,0-1.506,14.539A10.275,10.275,0,0,0,758.026,484.63Zm-2.689,13.085a8.462,8.462,0,1,1,1.233-11.9A8.471,8.471,0,0,1,755.337,497.715Z" transform="translate(0.633 0.632)" fill="#696969"/></g></svg></button>
          </form>
          <div class="title-block">
            <h2 class="title-main"><span class="d-inline-block">Support</span></h2>
          </div>
        </div>
      </section>
      <!-- BREADCRUMB-->
      <section class="breadcrumbk">
        <div class="container">
          <ul class="rs-list breadcrumbk-list">
            <li class="item"><a class="link" href="/ho-tro">Support page</a></li>
            <li class="item active">Frequently asked questions</li>
          </ul>
        </div>
      </section>
      <!-- SUPPORT FAQ-->
      <section class="section support-faq">
        <div class="container container-2">
          <div class="row">
            <div class="col-lg-8">
              <div class="row">
                <?php foreach ($supportfaq_cats as $supportfaq_cat): ?>
                  <div class="col-md-6 support-faq-block">
                    <h2 class="title animate-fadeUp">{{$supportfaq_cat->title}}</h2>
                    <div class="inner">

                      <?php foreach ($supportfaqs as $key => $supportfaq): ?>
                        @if($key < 3 && $supportfaq->id_supportfaq_cat == $supportfaq_cat->id)
                        <div class="support-faq__item animate-fadeUp">
                          <h3 class="title"><a class="link collapsed" href="/chi-tiet-cau-hoi/{{$supportfaq->slug}}" data-number="{{$key+1}}">{{$supportfaq->title}}</a></h3>
                        </div>
                        @endif
                      <?php endforeach ?>
                      
                    </div>
                    <div class="text-right pt-2"><a class="link-blue" href="/cau-hoi/{{$supportfaq_cat->slug}}">See more &nbsp;<i class="fa fa-angle-right"></i></a></div>
                  </div>
                <?php endforeach ?>
              </div>
            </div>
            <aside class="col-lg-4 support-aside style1">
              <div class="support-aside__inner">
                <div class="support-faq-block">
                  <h2 class="title animate-fadeUp">Latest</h2>
                  <div class="inner">
                    <?php foreach ($supportfaqs as $key => $supportfaq): ?>
                      @if($key < 6)
                      <div class="support-faq__item animate-fadeUp">
                        <h3 class="title"><a class="link collapsed" href="/chi-tiet-cau-hoi/{{$supportfaq->slug}}">{{$supportfaq->title}}</a></h3>
                      </div>
                      @endif
                    <?php endforeach ?>
                  </div>
                </div>
                <div class="support-faq-block">
                  <h2 class="title animate-fadeUp">Most popular</h2>
                  <div class="inner">
                    <?php foreach ($supportfaqs as $key => $supportfaq): ?>
                      @if($key < 6 && $supportfaq->hight_flg == 1)
                      <div class="support-faq__item animate-fadeUp">
                        <h3 class="title"><a class="link collapsed" href="/chi-tiet-cau-hoi/{{$supportfaq->slug}}">{{$supportfaq->title}}</a></h3>
                      </div>
                      @endif
                    <?php endforeach ?>
                  </div>
                </div>
              </div>
            </aside>
          </div>
        </div>
      </section>
      <!-- SECTION SIX-->
      <section class="section section-six"><span class="circle circle--striped"></span>
        <div class="container"><span class="triangle triangle--light"></span>
          <div class="subscribe-box">
            <div class="title-block text-center">
              <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
              <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
            </div>
            <form class="subscribe-form" id="subscribe" action="">
              <input class="input" name="email" placeholder="Your email"/>
              <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
            </form>
            <div class="contact-box">
              <div class="item">
                <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
              </div>
              <div class="item">
                <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
@endsection