@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
      <!-- BANNER PAGE-->
      <section class="banner banner-page embed-responsive" style="background-image: url(/public/img/upload/banners/{{$banner->avatar}});">
        <div class="caption">Tâm linh</div>
      </section>
      <!-- NEWS-->
      <section class="section news">
        <div class="container">
          <div class="row">
            <div class="left">
              <!-- DETAIL-->
              <article class="news-detail">
                <figure class="img"><img src="/public/img/upload/tamlinhs/{{@$tamlinhdt->avatar}}" alt=""></figure>
                <div class="top">
                  <time class="time">{{date('d-m-Y', strtotime(@$tamlinhdt->created_at))}}</time>
                </div>
                <h1 class="title">{{@$tamlinhdt->title}}</h1>
                <div class="content">
                  <p><i>{{@$tamlinhdt->summary}}</i></p>
                  {!!@$tamlinhdt->content!!}
                </div>
                <!-- <div class="bottom">
                  <ul class="rs-list list">
                    <li class="item"><a class="link" href="#comment"><i class="fa fa-comment-o mr-1"></i><span>1</span></a></li>
                    <li class="item"><a class="link" href="#!"><i class="fa fa-heart-o mr-1"></i><span>2</span></a></li>
                    <li class="item"><i class="fa fa-tag mr-1"></i><a class="link" href="#!">Americano</a>&nbsp;,&nbsp;<a class="link" href="#!">Latte</a>&nbsp;,&nbsp;<a class="link" href="#!">Macchiato</a></li>
                  </ul>
                  <ul class="rs-list list">
                    <li class="item"><a href="#!"><i class="fa fa-facebook"></i></a></li>
                    <li class="item"><a href="#!"><i class="fa fa-twitter"></i></a></li>
                    <li class="item"><a href="#!"><i class="fa fa-linkedin"></i></a></li>
                    <li class="item"><a href="#!"><i class="fa fa-tumblr"></i></a></li>
                  </ul>
                </div> -->
              </article>
              
              <!-- RELATED NEWS-->
              <div class="news-related">
                <h4 class="title">Bài viết liên quan</h4>
                <div class="news-related__slider js-sliderNewRelated">
                  <?php foreach ($tamlinhlqs as $key => $tamlinhlq): ?>
                    <div class="item">
                      <figure class="img embed-responsive hasLink"><img src="/public/img/upload/tamlinhs/{{@$tamlinhlq->avatar}}" alt=""><a class="link" href="/tam-linh/{{@$tamlinhlq->slug}}"></a></figure>
                      <div class="content">
                        <h5 class="name"><a class="link" href="/tam-linh/{{@$tamlinhlq->slug}}">{{@$tamlinhlq->title}}</a></h5>
                        <div class="bot">
                          <time class="d-inline-block">{{date('d-m-Y', strtotime(@$tamlinhlq->created_at))}}</time><br><a class="link d-inline-block"  href="/tam-linh/{{@$tamlinhlq->slug}}">{{ Str::limit(@$tamlinhlq->summary, 80) }}</a>
                        </div>
                      </div>
                    </div>
                  <?php endforeach ?>
                </div>
              </div>
              <!-- COMMENTS-->
              <div class="comment-box">
								<div class="fb-comments" data-href="<?=$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";?>/tam-linh/{{$tamlinhdt->slug}}" data-width="100%" data-numposts="5"></div>
							</div>
            </div>
            <aside class="right">
              <div class="aside-block"><span class="title">Bài viết mới</span>
                <?php foreach ($tamlinh_news as $key => $tamlinh_new): ?>
                  <?php if ($key <5): ?>
                  <div class="news-item__aside hasLink">
                    <figure class="img mb-0"><img src="/public/img/upload/tamlinhs/{{@$tamlinh_new->avatar}}" alt=""></figure>
                    <div class="content"><span class="name">{{@$tamlinh_new->title}}</span>
                      <time>{{date('d-m-Y', strtotime(@$tamlinh_new->created_at))}}</time>
                    </div><a class="link" href="/tam-linh/{{@$tamlinh_new->slug}}"></a>
                  </div>
                  <?php endif ?>
                <?php endforeach ?>
              </div>
              <div class="aside-block"><span class="title">Danh mục bài viết</span>
                <ul class="rs-list list">
                  <?php foreach ($tamlinh_cats as $key => $tamlinh_cat): ?>
                    <li class="item"><a class="link" href="/danh-muc-tam-linh/{{@$tamlinh_cat->slug}}">{{@$tamlinh_cat->title}}</a></li>
                  <?php endforeach ?>
                </ul>
              </div>
              <!-- <div class="aside-block"><span class="title">Tags</span>
                <div class="tags"><a class="link" href="#!">Americano</a>,&nbsp;<a class="link" href="#!">Cappucino</a>,&nbsp;<a class="link" href="#!">Espresso</a>,&nbsp;<a class="link" href="#!">Latte</a>,&nbsp;<a class="link" href="#!">Macchiato</a>,&nbsp;<a class="link" href="#!">Mocha</a></div>
              </div> -->
            </aside>
          </div>
        </div>
      </section>
    </main>
@endsection