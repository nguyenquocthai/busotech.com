@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER PAGE-->
  <section class="banner banner-page embed-responsive" style="background-image: url(/public/img/upload/banners/{{$banner->avatar}});">
    <div class="caption">Tâm linh</div>
  </section>
  <!-- NEWS-->
  <section class="section news">
    <div class="container">
      <div class="row">
        <div class="left">
          <?php foreach ($blogs as $key => $blog): ?>
            <article class="news-item">
              <figure class="img hasLink"><img src="/public/img/upload/tamlinhs/{{$blog->avatar}}" alt=""><a class="link" href="/tam-linh/{{@$blog->slug}}"></a></figure>
              <div class="content">
                <div class="top">
                  <time class="time">{{date('d-m-Y', strtotime(@$blog->created_at))}}</time>
                  <?php foreach ($blog_cats as $key => $blog_cat1): ?>
                    <?php if ($blog_cat1->id == $blog->id_tamlinh_cat): ?>
                      <a class="cate link" href="/danh-muc-tam-linh/{{@$blog_cat1->slug}}">{{@$blog_cat1->title}}</a>
                    <?php endif ?>
                  <?php endforeach ?>
                </div>
                <h3 class="title"><a class="link" href="/tam-linh/{{@$blog->slug}}">{{$blog->title}}</a></h3>
                <p>{{Str::limit(@$blog->summary, 250)}}</p><a class="more link" href="/tam-linh/{{@$blog->slug}}">Xem thêm</a>
              </div>
            </article>
          <?php endforeach ?>
          <div class="paginationk center">
            {{$blogs->links()}}
          </div>
          
        </div>
        <aside class="right">
          <div class="aside-block"><span class="title">Bài viết mới</span>
            <?php foreach ($blogs as $key => $blog): ?>
              <?php if ($key < 4): ?>
                <div class="news-item__aside hasLink">
                  <figure class="img mb-0"><img src="/public/img/upload/tamlinhs/{{$blog->avatar}}" alt=""></figure>
                  <div class="content"><span class="name">{{$blog->title}}</span>
                    <time>{{date('d-m-Y', strtotime(@$blog->created_at))}}</time>
                  </div><a class="link" href="/tam-linh/{{$blog->slug}}"></a>
                </div>
              <?php endif ?>
            <?php endforeach ?>
          </div>
          <div class="aside-block"><span class="title">Danh mục bài viết</span>
            <ul class="rs-list list">
               <?php foreach ($blog_cats as $key => $blog_cat): ?>
                  <li class="item"><a class="link" href="/danh-muc-tam-linh/{{$blog_cat->slug}}">{{$blog_cat->title}}</a></li>
               <?php endforeach ?>
            </ul>
          </div>
          <!-- <div class="aside-block"><span class="title">Tags</span>
            <div class="tags"><a class="link" href="#!">Americano</a>,&nbsp;<a class="link" href="#!">Cappucino</a>,&nbsp;<a class="link" href="#!">Espresso</a>,&nbsp;<a class="link" href="#!">Latte</a>,&nbsp;<a class="link" href="#!">Macchiato</a>,&nbsp;<a class="link" href="#!">Mocha</a></div>
          </div> -->
        </aside>
      </div>
    </div>
  </section>
</main>
@endsection