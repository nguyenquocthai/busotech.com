<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<p><b>Chào bạn {{ $obj->name }},</b></p>
	<p>Chào mừng bạn đã đến website {{ $obj->sender }} của chúng tôi</p>
	<p>Đường dẫn website của bạn là: <a href="{{$obj->domain_name}}" title="">{{$obj->domain_name}}</a></p>
	<p>Trang quản trị website của bạn là: <a href="{{$obj->domain_name}}/admin" title="">{{$obj->domain_name}}/admin</p>
	<p>Mật khẩu quản trị website của bạn là: {{$obj->password}} </p>
</body>
</html>