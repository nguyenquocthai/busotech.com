Hello <i>{{ $demo->receiver }}</i>,
<p>Click on the link below to retrieve your password.</p>
 
<div>
<p><b>Link format password:</b>&nbsp;{{BladeGeneral::fullBaseUrl()}}/forget/{{ $demo->token }}</p>
</div>
 
Thank You,
<br/>
{{ $demo->receiver }}