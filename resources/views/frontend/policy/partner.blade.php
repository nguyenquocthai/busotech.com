@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER-->
  <section class="section banner-page partner-banner" style="background-image: url(/public/theme/img/bg-partner.jpg)">
    <div class="container">
      <div class="title-block">
        <h2 class="title-main"><span class="d-block">Comprehensive solution for&nbsp;</span><span class="d-block">all business needs </span></h2><a class="rs-btn btn-animated btn-gradient" href="#modalPartnerForm" data-toggle="modal">Register now</a>
      </div>
    </div>
  </section>
  <!-- BUSSINESS POLICY-->
  <section class="section partner-policy">
    <div class="container container-2">
      <div class="title-block text-center">
        <h2 class="title-main"><span class="d-inline-block">Business Policy&nbsp;</span><span class="d-inline-block">of Buso</span></h2>
        <h3 class="title-sub animate-fadeUp"><i>Buso offers comprehensive online to offline business opportunities for partners through its long-term business policies.</i></h3>
      </div>
      <div class="row">
        <?php foreach ($partner_intros as $key => $partner_intro): ?>
          <div class="col-lg-4 partner-policy__item animate-fadeUp">
            <div class="inner">
              <h4 class="title" style="text-align: center">{{$partner_intro->title}}</h4>
              <figure class="img" style="text-align: center"><img src="/public/img/upload/partner_intros/{{$partner_intro->avatar}}" alt="{{$partner_intro->title}}"/></figure>
              <p class="mb-0">{{$partner_intro->summary}}</p>
            </div>
          </div>
        <?php endforeach ?>
        
      </div>
      <!-- <div class="partner-policy__img">
        <div class="item animate-fadeUp">
          <div class="inner">
            <figure class="img"><img src="/public/theme/img/chinh-sach-kinh-doanh-1.svg" alt=""/></figure>
            <h4 class="title">Social Commerce</h4>
          </div>
        </div>
        <div class="item animate-fadeUp">
          <div class="inner">
            <figure class="img"><img src="/public/theme/img/chinh-sach-kinh-doanh-2.svg" alt=""/></figure>
            <h4 class="title">CRM</h4>
          </div>
        </div>
        <div class="item animate-fadeUp">
          <div class="inner">
            <figure class="img"><img src="/public/theme/img/chinh-sach-kinh-doanh-3.svg" alt=""/></figure>
            <h4 class="title">FB Chatbox Marketing</h4>
          </div>
        </div>
        <div class="item animate-fadeUp">
          <div class="inner">
            <figure class="img"><img src="/public/theme/img/chinh-sach-kinh-doanh-4.svg" alt=""/></figure>
            <h4 class="title">Loyalty Platform</h4>
          </div>
        </div>
        <div class="item animate-fadeUp">
          <div class="inner">
            <figure class="img"><img src="/public/theme/img/chinh-sach-kinh-doanh-5.svg" alt=""/></figure>
            <h4 class="title">Data Analyst Platform</h4>
          </div>
        </div>
      </div> -->
    </div>
  </section>
  <!-- SECTION-->
  <section class="section partner-program">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main"><span class="d-inline-block">BUSO's partnership program is attractive because&nbsp;</span><span class="d-inline-block">is attractive because</span></h2>
        <h3 class="title-sub animate-fadeUp"><i>High discount, permanent income</i></h3>
      </div>
      <div class="row">
        <?php foreach ($partner_advantages as $key => $partner_advantage): ?>
          <div class="col-6 col-md-4 col-lg-3 partner-program__item animate-fadeUp">
            <div class="inner">
              <figure class="img"><img src="/public/img/upload/partner_advantages/{{$partner_advantage->avatar}}" alt="{{$partner_advantage->title}}"/></figure>
              <h4 class="title">{{$partner_advantage->title}}</h4>
            </div>
          </div>
        <?php endforeach ?>
      </div>
      <div class="partner-discount__panel">
        <h3 class="title animate-fadeUp">Discount amount</h3>
        <div class="partner-discount">
          <div class="item animate-fadeUp"><span class="value"><span class="js-contract">30</span></span><span class="text">Contract</span></div>
          <div class="item animate-fadeUp"><span class="value"><span class="js-discountFirst">20</span><span>%</span></span><span class="text">First time discount</span></div>
          <div class="item animate-fadeUp"><span class="value"><span class="js-discountAll">15</span><span>%</span></span><span class="text">Permanent discount</span></div>
          <div class="partner-discount__slider animate-fadeUp">
            <div class="slider js-discountSliderCustom"></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- STEPS-->
  <section class="section section-four partner-step" style="background-image: url(/public/theme/img/backgrounds/bg-wave.svg)">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main"><span>3 steps to become&nbsp;</span><span class="d-inline-block">a partner of Buso </span></h2>
      </div>
      <div class="row partner-step__list">
        <?php foreach ($partner_steps as $key => $partner_step): ?>
          <div class="col-6 col-md-4 partner-step__item animate-fadeUp">
            <div class="inner">
              <figure class="img"><img src="/public/img/upload/partner_steps/{{$partner_step->avatar}}" alt="{{$partner_step->title}}"/></figure>
              <div class="content">
                <h3 class="title">{{$partner_step->title}}</h3>
                <h4 class="title-sub">{{$partner_step->summary}}</h4>
              </div>
            </div>
          </div>
        <?php endforeach ?>
        
      </div>
      <div class="text-center pt-5"><a class="rs-btn btn-gradient mt-1 animate-fadeUp" href="#modalPartnerForm" data-toggle="modal">Sign up for the policy</a></div>
    </div>
  </section>
  <!-- SUBCRIBE-->
  <section class="section section-six"><span class="circle circle--striped"></span>
    <div class="container"><span class="triangle triangle--light"></span>
      <div class="subscribe-box">
        <div class="title-block text-center">
          <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
          <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
        </div>
        <form class="subscribe-form" id="subscribe" action="">
          <input class="input" name="email" placeholder="Your email"/>
          <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
        </form>
        <div class="contact-box">
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
          </div>
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- DOMAINS PRICE MODAL-->
  <div class="modal modal-custom modal-custom--sm fade" id="modalPartnerForm" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content js-tabParent">
        <div class="modal-header">
          <button class="rs-btn modal-close" data-dismiss="modal" arial-label="Close"></button>
          <div class="modal-title">
            <p class="title-sm">Sign up for the program</p>
            <p class="title-lg">Sales agent with buso</p>
          </div>
        </div>
        <div class="modal-body">
          <form class="login-form form-partner" action="#!">
            @csrf
            <p class="login-form__row">
              <input class="input" type="text" name="name" placeholder="Full name"/>
            </p>
            <p class="login-form__row">
              <input class="input" type="number" name="tel" placeholder="Phone number"/>
            </p>
            <p class="login-form__row">
              <input class="input" type="email" name="email" placeholder="Email"/>
            </p>
            <p class="login-form__row">
              <input class="input" type="text" name="name_shop" placeholder="Name of the store"/>
            </p>
            <p class="login-form__row">
              <input class="input" type="text" name="address_shop" placeholder="Store address"/>
            </p>
            <!-- <div class="captcha">
              <p>
                <input class="input" type="text" placeholder="Mã an toàn"/>
              </p>
              <p><span class="captcha-code">1234</span></p>
              <p>
                <button class="rs-btn button" type="button">
                  <svg xmlns="http://www.w3.org/2000/svg" width="38.685" height="31.365" viewBox="0 0 38.685 31.365">
                    <path d="M326.907,223.91l-2.92-3.146-2.948-3.118a1.326,1.326,0,0,0-1.984,0l-2.948,3.118-2.948,3.146c-.539.568-.34,1.049.453,1.049h2.1v.312a11.989,11.989,0,0,1-5.073,9.751,11.831,11.831,0,0,1-14.089-.284c8.986,10.29,27.95,3.572,27.95-9.779h1.955c.794,0,1.021-.481.454-1.049Zm-27.241,3.43,2.948-3.118c.538-.6.34-1.077-.482-1.077h-2.07v-.284a11.945,11.945,0,0,1,19.164-9.5c-8.987-10.262-27.951-3.543-27.951,9.78h-1.956c-.821,0-1.02.482-.481,1.077l2.948,3.118,2.947,3.118a1.325,1.325,0,0,0,1.985,0Z" transform="translate(-288.537 -208.379)" fill="#5a73b8" fill-rule="evenodd"></path>
                  </svg>
                </button>
              </p>
            </div> -->
            <p class="login-form__row">
              <button type="submit" class="rs-btn btn-gradient">Register now</button>
            </p>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
<script>
  jQuery(document).ready(function($) {
    if ($('.js-discountSliderCustom').length) {
    let $this = $(this),
      discountFirst = $this.find('.js-discountFirst'),
      contract = $this.find('.js-contract');

    $('.js-discountSliderCustom').slider({
      range: "min",
      min: 0,
      max: 150,
      value: 30,
      slide: function(event, ui) {
        let discount = 0;

        if (ui.value >= 2 && ui.value < 30) {
          discount = 15
        }
        if (ui.value >= 30 && ui.value < 50) {
          discount = 20
        }
        if (ui.value >= 50 && ui.value < 100) {
          discount = 25
        }
        if (ui.value >= 100 && ui.value < 150) {
          discount = 30
        }
        if (ui.value >= 150) {
          discount = 40
        }
        
        contract.text(ui.value);
        discountFirst.text(discount);
      }
    });
  }
  });

  jQuery(document).ready(function($) {
    
    var formpartner = $('.form-partner').validate({
      highlight: function(element, errorClass, validClass) {
          $(element).removeClass(errorClass);
      },
      rules: {
        email:{
            required: true,
            email:true
        },
        name:{
            required: true
        },
        tel:{
            required: true,
            number: true
        },
        name_shop:{
            required: true,
        },
        address_shop:{
            required: true,
        }
      },
      messages: {
        email:{
            required: 'Please enter your email.',
            email: 'Your email must be in the correct email format',
        },
        name:{
            required: 'Please enter your Name'
        },
        tel:{
            required: 'Please enter your phone number',
            number: 'phone number is incorrect',
        },
        name_shop:{
            required: "Please enter your store name",
        },
        address_shop:{
            required: "Please enter your store address",
        }
      },
      submitHandler: function (form) {
        var data = {};
        $(".form-partner").serializeArray().map(function(x){data[x.name] = x.value;});
        
        $.ajax({
            type: 'POST',
            url: '/add_contact_partner',
            data: data,
            dataType: 'json',
            error: function(){
                
                toastr.error('Error');
            },
            success: function(result) {
                
                switch (result.code) {
                    case 200:
                        $('.form-partner').find("input, textarea").val("");
                        toastr.success('Contact successfully sent.');
                        break;
                    default:
                        toastr.error('Error');
                        break;
                }
            }
        });
        return false;
    }
    });
  });
</script>
@endsection