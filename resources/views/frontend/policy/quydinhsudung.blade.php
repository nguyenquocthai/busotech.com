@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- BANNER-->
      <section class="section banner-page detail-banner bg-gradient">
        <div class="container">
          <div class="title-block">
            <h1 class="title-main"><span class="d-inline-block">{{$quydinhsudung->title}}</span></h1>
            <h2 class="title-sub animate-fadeUp"><span class="d-inline-block">{{$quydinhsudung->summary}}</span></h2>
          </div>
        </div>
      </section>
      <!-- DETAIL PAGE-->
      <section class="section detail-section">
        <div class="container">
          
          <article class="detail-content animate-fadeUp">
            {!!$quydinhsudung->content!!}
          </article>
        </div>
      </section>
      <!-- SECTION SIX-->
      <section class="section section-six"><span class="circle circle--striped"></span>
        <div class="container"><span class="triangle triangle--light"></span>
          <div class="subscribe-box">
            <div class="title-block text-center">
              <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
          		<h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
            </div>
            <form class="subscribe-form" id="subscribe" action="">
              <input class="input" name="email" placeholder="Your email"/>
              <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
            </form>
            <div class="contact-box">
              <div class="item">
                <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
              </div>
              <div class="item">
                <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
@endsection