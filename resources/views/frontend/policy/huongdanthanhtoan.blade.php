@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER-->
  <section class="section banner-page human-resource-banner" style="background-image: url(/public/theme/img/bg-thanh-toan.jpg)">
    <div class="container">
      <div class="title-block">
        <h1 class="title-main"><span class="d-inline-block">Payment Guide</span></h1>
      </div>
    </div>
  </section>
  <!-- PAYMENT GUIDE-->
  <section class="section payment-guide">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main">There are&nbsp;<span class="number">3</span>&nbsp;forms&nbsp;<span class="d-inline-block">of payment assistance </span></h2>
      </div>
    </div>
    <div class="payment-guide__bg" style="background-image: url(/public/theme/img/bg-thanh-toan.png);">
      <div class="container">
        <div class="payment-guide__list">
          <?php foreach ($payment_guides as $key => $payment_guide): ?>
            <?php if ($key == 0): ?>
            <div class="payment-guide__item animate-fadeUp">
              <div class="inner">
                <h3 class="title">1. {{$payment_guide->title}}</h3>
                {!!$payment_guide->content!!}
              </div>
            </div>
            <?php endif;break; ?>
          <?php endforeach ?>
          <?php foreach ($payment_guides as $key => $payment_guide): ?>
          <?php if ($key == 1): ?>
          <div class="payment-guide__item animate-fadeUp">
            <div class="inner">
              <h3 class="title">2. {{$payment_guide->title}}</h3>
              {!!$payment_guide->content!!}
              <ul class="rs-list list">
                <?php foreach ($bank_accs as $key => $bank_acc): ?>
                <li class="item">
                  <figure class="img"><img src="/public/img/upload/bank_accs/{{$bank_acc->avatar}}" alt="{{$bank_acc->title}}"/></figure>
                  <p><strong>{{$key+1}}. {{$bank_acc->title}}</strong><br/><strong>Account name:&nbsp;</strong><span>{{$bank_acc->name_acc}}</span><br/><strong>Account number:&nbsp;</strong><span>{{$bank_acc->num_acc}}</span><br/><strong>Branch:&nbsp;</strong><span>{{$bank_acc->branch_acc}}</span></p>
                </li>
                <?php endforeach ?>
              </ul>
            </div>
          </div>
          <?php endif; ?>
          <?php endforeach ?>
          <?php foreach ($payment_guides as $key => $payment_guide): ?>
          <?php if ($key > 1): ?>
          <div class="payment-guide__item animate-fadeUp">
            <div class="inner">
              <h3 class="title">{{$key+1}}. {{$payment_guide->title}}</h3>
              {!!$payment_guide->content!!}
            </div>
          </div>
          <?php endif; ?>
          <?php endforeach ?>
        </div>
      </div>
    </div>
  </section>
  <!-- SECTION SIX-->
  <section class="section section-six"><span class="circle circle--striped"></span>
    <div class="container"><span class="triangle triangle--light"></span>
      <div class="subscribe-box">
        <div class="title-block text-center">
          <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
          <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
        </div>
        <form class="subscribe-form" id="subscribe" action="">
          <input class="input" name="email" placeholder="Your email"/>
          <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
        </form>
        <div class="contact-box">
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
          </div>
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
@endsection