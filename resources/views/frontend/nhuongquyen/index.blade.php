@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- BANNER INDEX-->
      <section class="banner banner-page embed-responsive" style="background-image: url(/public/img/upload/banners/{{$banner->avatar}});">
        <div class="caption">Nhượng quyền</div>
      </section>
      <!-- FRANCHISE-->
      <section class="section franchise">
        <h1 class="title">Nhượng quyền thương hiệu</h1>
        <h2 class="title-des">Vạn Tường chào đón các đối tác muốn nhượng quyền</h2>
        <div class="container">
          <?php foreach ($nhuongquyens as $key => $nhuongquyen): ?>
            <article class="franchise-item">
              <figure class="img"><img src="/public/img/upload/nhuongquyens/{{$nhuongquyen->avatar}}" alt=""></figure>
              <div class="content">
                <h3 class="name">{{$nhuongquyen->title}}</h3>
                {!!$nhuongquyen->content!!}
              </div>
            </article>
          <?php endforeach ?>
        </div>
        <div class="paginationk center">
          {{@$nhuongquyens->links()}}
        </div>
      </section>
      <!-- CONTACT BLOCK-->
      <section class="contact-block">
        <div class="row">
          <div class="column">
            <div class="text">Bạn đang có nhu cầu kinh doanh nhà hàng thực dưỡng chay</div>
          </div>
          <div class="column"><a class="rs-btn btn-read-more" href="/lien-he">Liên hệ ngay với chúng tôi</a></div>
        </div>
      </section>
    </main>
@endsection