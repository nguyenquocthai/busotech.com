@extends('frontend.layouts.main')
@section('content')
<main class="main">
      <!-- BANNER SLIDER-->
      <section class="banner-main banner-title js-sliderBanner">
        @foreach(@$slider_dvs as $slider_dv)
        <div class="item embed-responsive" style="background-image: url(/public/img/upload/sliders/{{$slider_dv->avatar}});">
          <div class="caption">
            <p class="title-page">Service</p>
            <h3 class="title">{{$slider_dv->title}}</h3>
            <p>{{$slider_dv->link}}</p>
          </div>
        </div>
        @endforeach
      </section>
      <!-- INTRODUCE-->
      <section class="section about-info">
        <div class="container">
          <div class="row pb-4">
            <div class="col-lg-6">
              <h2 class="title-md">{{$servicedt->title}}</h2>
            </div>
            <div class="col-lg-6">
              {!!$servicedt->content_en!!}
            </div>
          </div>
          <div class="service-slider js-sliderService">
            @foreach($item_albums as $item_album)
            <div class="item">
              <figure class="img mb-0"><img src="/public/img/upload/item_albums/{{$item_album->name}}" alt=""></figure>
            </div>
            @endforeach
            
          </div>
        </div>
      </section>
      <!-- SERVICE DETAIL CONTENT-->
      <section class="section bg_fa service-detail__content">
        <div class="container">
          <h2 class="title-md title-line">Service detail</h2>
          <div class="row">
            <div class="col-xl-4 order-xl-1">
              <h3 class="title-md">{{$servicedt->summary}}</h3>
            </div>
            <div class="col-md-12 col-xl-8">
              <div class="cl_80">
                {!!$servicedt->content!!}
              </div>
            </div>
            
          </div>
        </div>
      </section>
      <!-- HOTLINE-->
      <section class="banner-group banner-group--3" style="background-image: url(/public/theme/img/bg-banner-3.png);">
        <div class="container">
          <div class="inner">
            <h2 class="title-lg mb-0">Do you need advice?</h2>
            <h3 class="title-md mb-3"><a class="rs-btn link" href="tel:{{$info_web['phone2']}}">{{$info_web['phone2']}}</a></h3><a class="rs-btn btn-golden" href="/lien-he"><span>Leave your information, we will contact you soon</span><span> we will contact you soon</span></a>
          </div>
        </div>
      </section>
      <!-- OTHER SERVICES-->
      <section class="section service-other">
        <div class="container">
          <h2 class="title-md">Other services</h2>
          <div class="service-other__slider js-sliderServiceOther">
            @foreach($services as $service)
            <div class="item service-item">
              <div class="inner">
                <figure class="img hasLink embed-responsive mb-0"><img src="/public/img/upload/services/{{$service->avatar}}" alt=""><a class="link" href="/dich-vu/{{$service->slug}}"></a></figure>
                <div class="content"><a class="name" href="/dich-vu/{{$service->slug}}" title="{{$service->title}}">{{$service->title}}</a></div>
              </div>
            </div>
            @endforeach
            
          </div>
        </div>
      </section>
      <!-- PARTNERS-->
      <section class="section partner bg_fa">
        <div class="container">
          <h2 class="title">Buso's clients</h2>
          <div class="partner-slider js-sliderPartner">
            @foreach($doitacs as $doitac)
            <div class="item">
              <figure class="img"><img src="/public/img/upload/doitacs/{{$doitac->avatar}}" alt=""></figure>
            </div>
            @endforeach
          </div>
        </div>
      </section>
    </main>
@endsection