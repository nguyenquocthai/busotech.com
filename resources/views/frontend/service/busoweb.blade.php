@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER-->
  <section class="section banner-page bg-gradient">
    <div class="container">
      <div class="title-block">
        <h2 class="title-main title-main--lg"><span class="d-block">1000+ INTERFACE</span><span class="d-block">ARE COMPLETING</span></h2><a class="rs-btn btn-animated btn-white" href="/giao-dien">Try to user</a>
      </div>
    </div>
  </section>
  <!-- SEARCH DOMAIN-->
  <section class="section domain-search">
    <div class="container domain-search-container">
      <div class="domain-search__box">
        <h2 class="title">The best domain name</h2>
        <form class="subscribe-form check_domain" action="#!">
          @csrf
          <input class="input" name="name_domain_search" placeholder="Enter the domain name you want"/>
          <button type="submit" class="rs-btn btn-gradient">Check</button>
        </form>
      </div>
      <div class="domain-search__result">
        <ul class="rs-list list">
          <?php foreach ($price_domains as $key => $price_domain): ?>
            <?php if ($price_domain->noibat == 1): ?>
            <li class="item">
              <h3 class="title"><span>{{$price_domain->title}}</span> {{$price_domain->gia/1000}}k</h3>
            </li> 
            <?php endif ?>
          <?php endforeach ?>
          
          
        </ul><a class="link link-blue" href="#modalDomains" data-toggle="modal">See more&nbsp;<i class="fa fa-angle-right"></i></a>
      </div>
    </div>
  </section>
  <!-- BUSO WEB-->
  <section class="section introduce-section">
    <div class="container">
      <div class="row">
        <?php foreach ($busowebs as $key => $busoweb): ?>
          @if($key == 0)
          <div class="col-lg-6">
            <div class="title-block">
              <h2 class="title-main title-main--lg"><span>{{$busoweb->title}}</span></h2>
              <div class="content">
                {!!$busoweb->content!!}
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <figure class="img"><img src="/public/img/upload/intros/{{$busoweb->avatar}}" alt=""/></figure>
          </div>
          @endif
          @break
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <!-- OKELA-->
  <section class="section okela">
    <div class="container okela-container">
      <?php foreach ($busowebs as $key => $busoweb): ?>
      @if($key == 1)
      <div class="title-block">
        <h2 class="title-main title-main--lg"><span>{{$busoweb->title}}</span></h2>
        <div class="content">
          {!!$busoweb->content!!}
        </div>
        <a href="/giao-dien"><button class="rs-btn btn-animated btn-gradient">View</button></a>
      </div>
      <figure class="img"><img src="/public/img/upload/intros/{{$busoweb->avatar}}" alt=""/></figure>
      @endif
      <?php endforeach ?>
    </div>
  </section>
  <!-- IMPLEMENTED PROJECTS-->
  <section class="section project-implemented">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main"><span>Project &nbsp;</span><span class="d-inline-block">was implemented</span></h2>
      </div>
    </div>
    <div class="project-implemented__container">
      <div class="project-implemented__slider js-sliderProjectImplemented">
        <?php foreach ($duans as $key => $duan): ?>
          <div class="project-item">
            <div class="inner hasLink">
              <figure class="img embed-responsive"><img src="/public/img/upload/duans/{{$duan->avatar}}" alt=""/>
                <div class="ctrl">
                  <a class="rs-btn btn-blue" href="{{$duan->link}}" target="_blank">View template</a>
                  <!-- <a class="rs-btn btn-green" href="#!">Sử dụng thử</a> -->
                </div>
              </figure>
              <div class="content">
                <a class="title" target="_blank" href="{{$duan->link}}" title="{{$duan->title}}">{{$duan->title}}</a>
              </div>
              <a class="link" target="_blank" href="{{$duan->link}}" title="{{$duan->title}}"></a>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <!-- THEMES-->
  <section class="section theme-section">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main"><span>Website &nbsp;</span><span class="d-inline-block">interface warehouse</span></h2>
      </div>
      <div class="theme-filter js-tabParent">
            <div class="theme-filter__type">
              <div class="list js-tabNav">
                <a class="rs-btn btn-animated item js-tabLink active" data-target="#all" href="#!">All</a>
                <?php foreach ($theme_cates as $key => $theme_cate): ?>
                  <a class="rs-btn btn-animated item js-tabLink" href="#!" data-id="{{$theme_cate->id}}" data-target="#{{$theme_cate->slug}}">{{$theme_cate->title}}</a>
                <?php endforeach ?>
              </div>
              <form class="form-search form-search--sm" action="#!">
                <input class="input" type="text" placeholder="Search template"/>
                <button class="rs-btn button"><svg xmlns="http://www.w3.org/2000/svg" width="23" height="25.564" viewBox="0 0 23 25.564"><g transform="translate(-736.513 -477.665)"><path d="M756.407,503.229a.608.608,0,0,0,.386-.138l2.494-2.025a.609.609,0,0,0,.223-.413.617.617,0,0,0-.134-.45l-6.262-7.709a9.244,9.244,0,1,0-8.311,3.6,9.342,9.342,0,0,0,.969.05,9.144,9.144,0,0,0,3.886-.864L755.931,503A.611.611,0,0,0,756.407,503.229Zm-11.477-8.356a8.013,8.013,0,1,1,5.879-1.751A7.962,7.962,0,0,1,744.93,494.873Zm6.652-.8a9.432,9.432,0,0,0,.713-.644l5.743,7.07-1.541,1.252-5.748-7.076A9.335,9.335,0,0,0,751.582,494.075Z" fill="#272525"/><path d="M751.681,483.31a6.761,6.761,0,1,0-.986,9.516A6.725,6.725,0,0,0,751.681,483.31Zm-1.76,8.564a5.538,5.538,0,1,1,.807-7.791A5.544,5.544,0,0,1,749.922,491.874Z" transform="translate(-0.675 -0.673)" fill="#272525"/></g></svg></button>
              </form>
            </div>
            <div class="theme-filter__category js-tabContent">
              <div class="tab-panel fade active" id="all">
                <div class="inner" data-id='{{$theme_cate->id}}'>
                  <?php foreach ($theme_cate1s as $key => $theme_cate1): ?>
                    
                      <a class="rs-btn btn-animated item" href="/danh-muc-giao-dien/{{$theme_cate1->slug}}"><h3 class="title">{{$theme_cate1->title}}</h3></a>
                    
                  <?php endforeach ?>
                </div>
                </div>
              <?php foreach ($theme_cates as $key => $theme_cate): ?>
                <div class="tab-panel fade" id="{{$theme_cate->slug}}">
                <div class="inner" data-id='{{$theme_cate->id}}'>
                  <?php foreach ($theme_cate1s as $key => $theme_cate1): ?>
                    <?php if ($theme_cate->id == $theme_cate1->id_theme_cat): ?>
                      <a class="rs-btn btn-animated item" href="/danh-muc-giao-dien/{{$theme_cate1->slug}}"><h3 class="title">{{$theme_cate1->title}}</h3></a>
                    <?php endif ?>
                  <?php endforeach ?>
                </div>
                </div>
              <?php endforeach ?>
              
            </div>
          </div>
    </div>
    <div class="container-fluid project-list">
      <div class="row">
        <?php foreach ($themes as $key => $theme): ?>
          <div class="col-6 col-lg-4 col-xl-3 project-item">
            <div class="inner hasLink">
              <figure class="img embed-responsive"><img src="/public/img/upload/themes/{{$theme->avatar}}" alt=""/>
                <div class="ctrl"><a class="rs-btn btn-blue" href="/giao-dien/{{$theme->slug}}">View template</a><a class="rs-btn btn-green" href="#!">Try to user</a></div>
              </figure>
              <div class="content"><a class="title" href="/giao-dien/{{$theme->slug}}" title="{{$theme->title}}">{{$theme->title}}</a></div><a class="link" href="#!" title="{{$theme->title}}"></a>
            </div>
          </div>
        <?php endforeach ?>
        
      </div>
      <div class="text-center"><a class="rs-btn btn-animated btn-gradient" href="/giao-dien">Find more interface templates</a></div>
    </div>
  </section>
  <!-- CUSTOMER-->
  <section class="section section-four" style="background-image: url(/public/theme/img/backgrounds/bg-wave.svg)">
    <div class="container"><span class="circle circle--light"></span>
      <div class="title-block text-center">
        <h2 class="title-main"><span>Customers&nbsp;</span><span class="d-inline-block">& Partners</span></h2>
      </div>
      <ul class="rs-list customer-list">
        <?php foreach ($doitacs as $key => $doitac): ?>
          <li class="item">
            <figure class="img hasLink"><img src="/public/img/upload/doitacs/{{$doitac->avatar}}" alt=""/><a class="link" href="#!"></a></figure>
          </li>
        <?php endforeach ?>
      </ul>
    </div>
  </section>
  <!-- SUBCRIBE-->
  <section class="section section-six"><span class="circle circle--striped"></span>
    <div class="container"><span class="triangle triangle--light"></span>
      <div class="subscribe-box">
        <div class="title-block text-center">
          <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
              <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
        </div>
        <form class="subscribe-form" id="subscribe" action="#!">
          <input class="input" name="email" placeholder="Your email"/>
          <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
        </form>
        <div class="contact-box">
      <div class="item">
        <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
      </div>
      <div class="item">
        <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
      </div>
    </div>
      </div>
    </div>
  </section>
  <!-- DOMAINS PRICE MODAL-->
  <div class="modal modal-custom fade" id="modalDomains" data-backdrop="static" tabindex="-1" role="dialog">
    <button class="rs-btn modal-close" data-dismiss="modal" arial-label="Close"></button>
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content js-tabParent">
        <div class="modal-header">
          <div class="modal-title">Domain price list</div>
          <div class="tab-nav js-tabNav">
            <?php foreach ($price_domain_cats as $key => $price_domain_cat): ?>
                  <button class="rs-btn item js-tabLink <?php if ($key == 0) echo 'active'; ?>" data-target="#tabDomain{{$price_domain_cat->id}}" type="button">{{$price_domain_cat->title}}</button>
                <?php endforeach ?>
          </div>
        </div>
        <div class="modal-body tab-content js-tabContent">
          <?php foreach ($price_domain_cats as $key => $price_domain_cat): ?>
          <div class="tab-panel fade <?php if ($key == 0) echo 'active'; ?>" id="tabDomain{{$price_domain_cat->id}}">
            <ul class="rs-list list list-title">
              <li class="item">Domain</li>
              <li class="item">price registration</li>
              <li class="item">extend</li>
              <li class="item">Switch to Buso</li>
            </ul>
            <div class="content">
              <?php foreach ($price_domains as $key => $price_domain): ?>
                <?php if ($price_domain->id_price_domain_cat == $price_domain_cat->id): ?>
                <ul class="rs-list list">
                  <li class="item">{{$price_domain->title}}</li>
                  <li class="item">{{number_format($price_domain->gia)}} VNĐ</li>
                  <li class="item">{{number_format($price_domain->giahan)}} VNĐ</li>
                  <li class="item">{{number_format($price_domain->chuyensangbuso)}} VNĐ</li>
                </ul>
                <?php endif ?>
              <?php endforeach ?>
            </div>
          </div>
          <?php endforeach ?>
          
        </div>
      </div>
    </div>
  </div>
</main>
<script>
  var data_form1 = [];
  jQuery(document).ready(function($) {
    var check_domain = $('.check_domain').validate({
              highlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass);
              },
              rules: {
                  
                  name_domain_search:{
                      required: true,

                  }
              },
              messages: {
                  
                  name_domain_search:{
                      required: 'Please enter your website address',

                  }
              },
              submitHandler: function (form) {
                  var data = {};
                  
                  $(".check_domain").serializeArray().map(function(x){data[x.name] = x.value;});
                  
                  data: {
                    _token : "{{ csrf_token() }}"
                    }
                  $.ajax({
                      type: 'POST',
                      url: '/add_contact_theme',
                      data: data,
                      dataType: 'json',
                      error: function(){
                          
                          toastr.error('Error');
                      },
                      success: function(result) {
                          console.log(result);
                          switch (result.code) {
                              case 200:
                                  $('.domainchuadangky').val(result.domain_check); 
                                  toastr.success(result.value);
                                  break;
                                case 300:
                                  toastr.error(result.value);
                                  break;
                                default:
                                  toastr.error('Error');
                                  break;
                          }
                      }
                  });
                  return false;
              }
          });
  });
</script>
@endsection