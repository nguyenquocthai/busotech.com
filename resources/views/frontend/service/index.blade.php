@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- BANNER SLIDER-->
      <section class="banner-main banner-title js-sliderBanner">
        @foreach(@$slider_dvs as $slider_dv)
        <div class="item embed-responsive" style="background-image: url(/public/img/upload/sliders/{{$slider_dv->avatar}});">
          <div class="caption">
            <p class="title-page">Service</p>
            <h3 class="title">{{$slider_dv->title}}</h3>
            <p>{{$slider_dv->link}}</p>
          </div>
        </div>
        @endforeach
      </section>
      <!-- INTRODUCE-->
      <section class="section about-info">
        <div class="container">
          <div class="row pb-4">
            <div class="col-lg-6">
              <h2 class="title-md">{{@$intro_dv->title}}</h2>
            </div>
            <div class="col-lg-6">
              {!!@$intro_dv->content!!}
            </div>
          </div>
          <div class="row service-list">
            @foreach($services as $key => $service)
            <div class="col-6 col-md-4 col-xl-3 service-item">
              <div class="inner">
                <figure class="img hasLink embed-responsive mb-0"><img src="/public/img/upload/services/{{$service->avatar}}" alt="{{$service->title}}"><a class="link" href="/dich-vu/{{$service->slug}}"></a></figure>
                <div class="content"><a class="name" href="/dich-vu/{{$service->slug}}" title="{{$service->title}}">{{$service->title}}</a></div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </section>
      <!-- PARTNERS-->
      <section class="section partner bg_fa">
        <div class="container">
          <h2 class="title">Buso's clients</h2>
          <div class="partner-slider js-sliderPartner">
            @foreach($doitacs as $doitac)
            <div class="item">
              <figure class="img"><img src="/public/img/upload/doitacs/{{$doitac->avatar}}" alt=""></figure>
            </div>
            @endforeach
            
          </div>
        </div>
      </section>
    </main>
@endsection