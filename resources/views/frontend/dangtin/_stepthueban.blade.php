@if($item_project->step == 1)
<div class="container-fluid">
    <br /><br />
    <ul class="list-unstyled multi-steps">
        <li class="{{@$b1}} ready"><a href="/create-rental-step1/{{$item_project->id}}" title="">Create</a></li>
        <li class="{{@$b2}}"><a href="/create-rental-step2/{{$item_project->id}}" title="">Detail</a></li>
        <li class="{{@$b3}}"><a href="/create-rental-step3/{{$item_project->id}}" title="">Finish</a></li>
    </ul>
</div>
@endif

@if($item_project->step == 2)
<div class="container-fluid">
    <br /><br />
    <ul class="list-unstyled multi-steps">
        <li class="{{@$b1}} ready"><a href="/create-rental-step1/{{$item_project->id}}" title="">Create</a></li>
        <li class="{{@$b2}} ready"><a href="/create-rental-step2/{{$item_project->id}}" title="">Detail</a></li>
        <li class="{{@$b13}}"><a href="/create-rental-step3/{{$item_project->id}}" title="">Finish</a></li>
    </ul>
</div>
@endif

@if($item_project->step == 3)
<div class="container-fluid">
    <br /><br />
    <ul class="list-unstyled multi-steps">
        <li class="{{@$b1}} ready"><a href="/create-rental-step1/{{$item_project->id}}" title="">Create</a></li>
        <li class="{{@$b2}} ready"><a href="/create-rental-step2/{{$item_project->id}}" title="">Detail</a></li>
        <li class="{{@$b3}} ready"><a href="/create-rental-step3/{{$item_project->id}}" title="">Finish</a></li>
    </ul>
</div>
@endif