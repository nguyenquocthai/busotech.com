<!-- Modal add -->
<div id="modal_tienich" class="modal list_store fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Utilities information</h4>
            </div>
            <div class="modal-body custom-list-data1 load_thue_ban">
                <table id="tbl-dataz2" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
                    <thead>
                        <tr>
                            <th><label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" value="1" name="test" id="all_id_player2"><span></span></label></th>
                            <th>Id</th>
                            <th>Icon</th>
                            <th>Title</th>
                        </tr>
                    </thead>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button onclick="addstore2()" type="button" class="btn btn-primary">Add</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#all_id_player').change(function(){
        var check = this.checked ? '1' : '0';
        if (check == 1) {
            $('.check_player').prop('checked', true);
        } else {
            $('.check_player').prop('checked', false);
        }
    });

    $('#all_id_player2').change(function(){
        var check = this.checked ? '1' : '0';
        if (check == 1) {
            $('.check_player2').prop('checked', true);
        } else {
            $('.check_player2').prop('checked', false);
        }
    });
</script> 