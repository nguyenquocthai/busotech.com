@extends('frontend.layouts.main')
@section('content')
@include('frontend.nguoidung._plugin')

<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="."><i class="zmdi zmdi-home mr-2"></i>Trang chủ</a></li>
                <li class="item active">Sửa tài khoản</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <div class="profile-block">
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&libraries=geometry,places" async></script>
        <script src="/public/assets/global/plugins/tinymce/tinymce.min.js"></script>
        <!--/head-block-->
        <div class="content-index bg-gray store-dang-tin">
            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content">
                        <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                        <h2 class="title text-uppercase">Đăng tin thuê <small class="d-block">Quý khách nhập thông tin nhà đất cần bán hoặc cho thuê vào các mục dưới đây</small></h2>

                        <form id="create_item" autocomplete="off" class="panel-form">
                            <input type="hidden" id="recaptcha" name="recaptcha">
                            <input type="hidden" name="type" value="0">
                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fas fa-book"></i> Thông tin cho thuê</span></h4>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Tiêu đề</label>
                                    <div class="input">
                                        <input type="text" name="title" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Danh mục</label>
                                    <div class="input">
                                        <select class="form-control" name="category_id" id="category_id">
                                            <option value="">-- Chọn danh mục --</option>
                                            @foreach($productcats as $productcat)
                                            <option value="{{ $productcat->id }}">{{ $productcat->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Tỉnh / Thành phố</label>
                                    <div class="input">
                                        <select id="tinhthanh" name="tinhthanh" class="form-control">
                                            <option value="">-- Chọn tỉnh thành --</option>
                                            @if (count($provinces) != 0)
                                                @foreach ($provinces as $province)
                                                <option value="{{$province->id}}">{{$province->m_province_nm}}</option>
                                                @endforeach
                                            @else
                                            
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Quận / Huyện</label>
                                    <div class="input load_quanhuyen">
                                        <span class="form-control">Chọn tỉnh thành trước</span>
                                    </div>
                                </div>

                                <div class="panel-form__row toadobando_box">
                                    <label class="text mb-md-0">Bản đồ </label>
                                    <div class="input">
                                        <div class="input-group"> <input data-toggle="modal" data-target="#modalMap" readonly name="map" id="map" type="text" class="form-control toadobando"> <span class="input-group-btn"></span> </div>
                                    </div>
                                </div>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Giá thuê</label>
                                    <div class="input">
                                        <div class="price_box style2">
                                            <input value="" type="text" name="fromprice" class="form-control">
                                             Đến 
                                            <input value="" type="text" name="toprice" class="form-control">
                                            <span class="donvi_mod">/tháng</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-form__row d-none">
                                    <label class="text mb-md-0">Ngày bắt đầu</label>
                                    <div class="input">
                                        <input type="text" value="01/03/2019" name="start_day" class="form-control">
                                    </div>
                                </div>

                            </div>

                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fa fa-info-circle"></i>Mô tả ngắn</span></h4>

                                <div class="input">
                                    <textarea name="short_content" id="short_content" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Liên hệ</span></h4>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Tên liên hệ</label>
                                    <div class="input">
                                        <input type="text" value="{{$nguoidung->callname}}" name="name_contact" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Địa chỉ</label>
                                    <div class="input">
                                        <input type="text" value="{{$nguoidung->address}}" name="address_contact" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Di động</label>
                                    <div class="input">
                                        <input type="text" value="{{$nguoidung->phone}}" name="phone_contact" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-form__row">
                                    <label class="text mb-md-0">Emai</label>
                                    <div class="input">
                                        <input type="text" value="{{$nguoidung->email}}" name="email_contact" class="form-control">
                                    </div>
                                </div>

                                <div class="panel-form__row">
                                    <label class="text mb-md-0"></label>
                                    <div class="input">
                                        <label class="mt-2"><input name="email_flg" value="1" type="checkbox"> Nhận email phản hồi</label>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="panel-form--footer">
                                <button id="submit-project" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Tiếp tục</span></button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- Modal -->
        <div id="modalMap" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Chọn vị trí bất động sản</h4>
                    </div>

                    <div class="modal-body">
                        <input id="pac-input" class="controls" type="text" placeholder="Nhập địa chỉ cần tìm...">
                        <div id="googleMap" style="width: 100%; height: 500px;"></div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-close width90" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                            <span>Đóng</span>
                        </button>

                        <button type="button" class="btn btn-primary width90 apply_geolocation">
                            <i class="fa fa-check"></i>
                            <span>Chọn</span>
                        </button>
                    </div>
                </div>

            </div>
        </div>
<script>
    var $scope = {};

    tinymce.init({
        selector: '#short_content'
    });

    //-------------------------------------------------------------------------------
    $('#modalMap').on('shown.bs.modal', function() {
        // GOOGLE MAP
        var map = "10.8230989,106.6296638"
        $scope.pointer = $scope.pointer || {};

        var geo = map.split(',');
        if (geo.length == 2) {
            $scope.pointer.latitude = geo[0];
            $scope.pointer.longitude = geo[1];
        }

        init_google_map($scope, {
            marker: $scope.marker,
            pointer: $scope.pointer,
        });
    });

    //----------------------------------------------------------------------------
    function init_google_map($scope, options) {
        if (!$scope.map) {
            var p = { lat: parseFloat(options.pointer.latitude), lng: parseFloat(options.pointer.longitude) };

            if (isNaN(options.pointer.latitude) || isNaN(options.pointer.longitude))
                p = { lat: 10.8230989, lng: 106.6296638 };

            $scope.map = new google.maps.Map($('#googleMap')[0], {
                zoom: 17,
                center: p,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            placeMarker(p);

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');

            var searchBox = new google.maps.places.SearchBox(input);

            $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        return;
                    }

                    placeMarker(place.geometry.location);

                    options.pointer.latitude = place.geometry.location.lat();
                    options.pointer.longitude = place.geometry.location.lng();
                    options.pointer.description = place.formatted_address;

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                $scope.map.fitBounds(bounds);
            });
        }
        google.maps.event.addListener($scope.map, 'click', function(event) {
            placeMarker(event.latLng);

            options.pointer.latitude = event.latLng.lat();
            options.pointer.longitude = event.latLng.lng();
        });

        function placeMarker(location) {
            if (options.marker == null) {
            options.marker = new google.maps.Marker({
                position: location,
                map: $scope.map
            });
            } else {
                options.marker.setPosition(location);
            }
        }
    };

    $('.apply_geolocation').click(function(event) {
        $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude)
        $("#modalMap").modal('toggle');
    });

</script>   

<script>
    $('#tinhthanh, #category_id').select2({
        theme: "bootstrap",
        width: '100%'
    });

    $('#tinhthanh, #category_id').change(function(event) {
        if ($(this).val() != "") {
            $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
        } else {
            $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
        }
    });

    $('#tinhthanh').change(function(event) {
        var data = {};
        data['status_code'] = "quanhuyen";
        data['id'] = $(this).val();

        $('.block-page-all').addClass('active');
        $.ajax({
            type: 'POST',
            url: '/api_dangtin',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                $('.load_quanhuyen').html(result.value);
                $('#quanhuyen').select2({
                    theme: "bootstrap",
                    width: '100%'
                });
                $('.block-page-all').removeClass('active');
            }
        });
    });

    var create_item = $('#create_item').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
            $(element).addClass("error_input");
            $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("error_input");
            $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
        },
        rules: {
            title:{
                required: true
            },
            address:{
                required: true
            },
            tinhthanh:{
                required: true
            },
            category_id:{
                required: true
            },
            map:{
                required: true
            },
            short_content:{
                required: true
            },
            name_contact:{
                required: true
            },
            address_contact:{
                required: true
            },
            phone_contact:{
                required: true
            },
            email_contact:{
                required: true
            },
            fromprice:{
                number: true,
                //required: true
            },
            toprice:{
                number: true,
                //required: true
            },
            
        },
        messages: {
            title:{
                required: 'Tên dự án'
            },
            address:{
                required: 'Địa chỉ dự án'
            },
            tinhthanh:{
                required: 'Tỉnh thành'
            },
            map:{
                required: 'Bản đồ'
            },
            category_id:{
                required: 'Danh mục'
            },
            short_content:{
                required: 'Mô tả ngắn'
            },
            name_contact:{
                required: 'Tên liên hệ'
            },
            address_contact:{
                required: 'Địa chỉ liên hệ'
            },
            phone_contact:{
                required: 'Số điện thoại liên hệ'
            },
            email_contact:{
                required: 'Email liên hệ'
            },
            fromprice:{
                digits: 'true',
                //required: true
            },
            toprice:{
                digits: 'true',
                //required: true
            },
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#create_item").serializeArray().map(function(x){data[x.name] = x.value;});
            data['status_code'] = "add";
            data['slug'] = ChangeToSlug(data['title']);
            data['short_content'] = tinymce.get('short_content').getContent();

            $('.block-page-all').addClass('active');
            $.ajax({
                type: 'POST',
                url: '/api_crate_item',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    toastr.success(result.message);
                    document.location.href = '/tao-tin-thue-b2/'+result.id;
                }
            });
            return false;
        }
    });
</script>  
@endsection