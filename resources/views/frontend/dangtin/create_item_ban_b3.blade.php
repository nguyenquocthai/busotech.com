@extends('frontend.layouts.application')
@section('content')
@include('frontend.dangtin._head')
<div class="content-index bg-gray store-dang-tin">
    <div class="container-fluid t-video-rp item_project-video without-video project-video"></div>

    <div class="container">
        <div class="bg-white panel-wrap my-3 my-md-5">
            @include('frontend.elements.sidebar_dangtin')

            <section class="panel-content dangtin_b3">
                <h2 class="title text-uppercase">Đăng tin bán <small class="d-block"><b>Chú ý: </b>Hoàn thành những mục được cảnh báo (<i class="fas fa-exclamation-triangle"></i>) bên dưới để bài đăng của bạn tốt nhất. Xin cám ơn</small></h2>
                @include('frontend.dangtin._stepban')
                @if($item_project->step == 3)
                <div class="alert alert-success" role="alert">
                    Bài đăng của bạn đã gửi đến admin. Vui lòng chờ duyệt.
                </div>
                @endif
                <div class="block">
                    <h4 class="title-line"><span class="text"><i class="fas fa-database"></i> Thông tin khởi tạo</span></h4>
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Mục tiêu</th>
                                <th scope="col">Nội dung</th>
                                <th scope="col">Tình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tên bất động sản</td>
                                <td>{{$item_project->title}}</td>
                                <td>
                                    @if($item_project->title != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Loại bất động sản</td>
                                <td>{{$item_project->type}}</td>
                                <td>
                                    <i class="fas fa-check"></i>
                                </td>
                            </tr>

                            <tr>
                                <td>Danh mục</td>
                                <td>{{$item_project->catname}}</td>
                                <td>
                                    @if($item_project->category_id != 0) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Tỉnh / Thành phố</td>
                                <td>{{$item_project->tinhthanh}}</td>
                                <td>@if($item_project->tinhthanh != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Quận / Huyện</td>
                                <td>{{$item_project->quanhuyen}}</td>
                                <td>@if($item_project->quanhuyen != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Giá bán</td>
                                <td>{!!$item_project->price!!}</td>
                                <td>
                                    @if($item_project->price != 'null') 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Bản đồ</td>
                                <td>{{$item_project->map}}</td>
                                <td>@if($item_project->map != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Mô tả ngắn</td>
                                <td>{{$item_project->short_content}}</td>
                                <td>@if($item_project->short_content != "") 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>

                <div class="block">
                    <h4 class="title-line"><span class="text"><i class="fas fa-user"></i> Thông tin liên hệ</span></h4>
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Mục tiêu</th>
                                <th scope="col">Nội dung</th>
                                <th scope="col">Tình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tên liên hệ</td>
                                <td>{{$item_project->name_contact}}</td>
                                <td>
                                    @if($item_project->name_contact != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Địa chỉ</td>
                                <td>{{$item_project->address_contact}}</td>
                                <td>
                                    @if($item_project->address_contact != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Di động</td>
                                <td>{{$item_project->phone_contact}}</td>
                                <td>@if($item_project->phone_contact != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Emai</td>
                                <td>{{$item_project->email_contact}}</td>
                                <td>@if($item_project->email_contact != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Nhận email phản hồi</td>
                                <td>{{$item_project->email_flg}}</td>
                                <td><i class="fas fa-check"></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="block">
                    <h4 class="title-line"><span class="text"><i class="fas fa-image"></i> Hình ảnh dự án</span></h4>
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Mục tiêu</th>
                                <th scope="col">Nội dung</th>
                                <th scope="col">Tình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Hình đại diện</td>
                                <td><a target="_blank" href="{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item', 'time' => $item_project->updated_at])}}" title="">
                                    <img width="100" class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $item_project->avatar,'data' => 'item', 'time' => $item_project->updated_at])}}" alt="...">
                                </a></td>
                                <td>
                                    @if($item_project->avatar != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Album dự án</td>
                                <td><a target="_blank" href="/tao-dang-tin-b2/{{$item_project->id}}" title="">Có {{$item_project->album_count}} hình trong album</a></td>
                                <td>
                                    @if($item_project->album_count != 0) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>

                            <tr>
                                <td>Emai</td>
                                <td>{{$item_project->email_contact}}</td>
                                <td>@if($item_project->email_contact != null) 
                                    <i class="fas fa-check"></i>
                                    @else <i class="fas fa-exclamation-triangle"></i>@endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                @if($item_project->step != 3)
                <div class="panel-form--footer">
                    <button data-id="{{$item_project->id}}" type="button" id="submit-item_project" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Hoàn thành</span></button>
                </div>
                @endif

            </section>
        </div>
    </div>
</div>


<script>
    $('#submit-item_project').click(function(event) {
        var data = {};
        $('.block-page-all').addClass('active');
        data['status_code'] = 'submit_item_project';
        data['id'] = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: '/api_crate_item',
            data: data,
            dataType: 'json',
            error: function(){
                $('.block-page-all').removeClass('active');
                toastr.error(result.error);
            },
            success: function(result) {
                if (result.code == 300) {
                    toastr.error(result.error);
                    $('.block-page-all').removeClass('active');
                    return false
                }
                document.location.href = '/tao-tin-ban-b3/'+result.id;
            }
        });
    });
</script>
@endsection