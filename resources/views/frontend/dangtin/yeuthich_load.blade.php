<table id="tbl-dataz" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
    <thead>
        <tr>
            <th class="text-uppercase">Tin đăng</th>
            <th class="text-uppercase">Hình</th>
            <th class="text-uppercase">Giá bán</th>
            <th class="text-uppercase">Giá thuê</th>
            <th class="text-uppercase">Ngày tạo</th>
            <th class="text-uppercase">Thao tác</th>
        </tr>
    </thead>
    <tbody class="load_seach">
        @foreach (@$projects as $project)
        <tr>
            <td>
                <p class="mb-0"><a href="/du-an/{{@$project->slug}}" title=""><b>{{@$project->title}}</b></a></p>
                <p class="mb-0">Ngày cập nhật: {{date('d/m/Y', strtotime(@$project->updated_at))}}</p>
            </td>

            <td><figure class="img"><img src="/public/img/upload/projects/{{$project->avatar}}" alt=""></figure></td>

            <td>
                <span class="price">
                    {{$project->fromprice}}
                    @if($project->fromprice != null && $project->toprice != null)
                    -
                    @endif
                    {{$project->toprice}}
                </span>
                <span class="date"> tr/m<sup>2</sup></span>
            </td>

            <td>
                <span class="price">
                    {{$project->thue_fromprice}}
                    @if($project->thue_fromprice != null && $project->thue_toprice != null)
                    -
                    @endif
                    {{$project->thue_toprice}}
                </span>
                <span class="date"> tr/tháng</span>
            </td>

            <td>
                <span class="d-inline-block"><span class="hidden">{{$project->created_at}}</span>{{date('d/m/Y', strtotime(@$project->created_at))}}</span>
            </td>

            <td>
                <a href="javascript:;" class="reset-btn btn-ctrl remove_favorite" data-group="0" data-id="{{@$project->id}}"><i class="far fa-trash-alt"></i>Bỏ thích</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    var datatable = $('#tbl-dataz').DataTable({
        order: [[ 4, 'desc' ]],
        columnDefs: [
            { sortable: false, searchable: false, targets: [ 0,1,5 ] },
        ],
        displayStart: 0,
        displayLength: 5,
        "autoWidth": false
    });
    datatable.search('').draw();
</script>