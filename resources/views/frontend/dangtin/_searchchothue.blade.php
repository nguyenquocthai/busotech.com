<table id="tbl-dataz" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
    <thead>
        <tr>
            <th class="text-uppercase">Postings</th>
            <th class="text-uppercase">Type</th>
            <th class="text-uppercase">Image</th>
            <th class="text-uppercase">Price</th>
            <th class="text-uppercase">Date created</th>
            <th class="text-uppercase">Manipulation</th>
        </tr>
    </thead>
    <tbody class="load_seach">
        @foreach (@$item_projects as $item_project)
        <tr>
            <td>
                <p class="mb-0"><b>{{@$item_project->title}}</b></p>
                <p class="mb-0">Update day: {{BladeGeneral::dateTime($item_project->updated_at)}}</p>
            </td>
            <td>
                <span class="price_table">
                    @if(@$item_project->type == 0)
                        Rental
                    @else
                        In grafting
                    @endif
                </span>
            </td>

            <td><figure class="img"><img src="{{BladeGeneral::GetImg(['avatar' => @$item_project->album->name,'data' => 'item_album', 'time' => @$item_project->album->updated_at])}}" alt=""></figure></td>

            <td>
                <span class="price_table">
                    {{BladeGeneral::priceFormat(@$item_project->price)}} / Month
                </span>
            </td>

            <td>
                <span class="d-inline-block"><span class="hidden">{{$item_project->created_at}}</span>{{date('d/m/Y', strtotime(@$item_project->created_at))}}</span>
            </td>

            @if($item_project->active == 4)
            <td>
                <a href="javascript:;" class="reset-btn btn-ctrl delete_thue_ban2"  data-id="{{@$item_project->id}}"><i class="far fa-trash-alt"></i> Delete</a>
                <a href="javascript:;" class="reset-btn btn-ctrl resum_thue_ban"  data-id="{{@$item_project->id}}"><i class="fas fa-undo-alt"></i> Restore</a>
            </td>
            @elseif($item_project->active == 0)
            <td>
                <a target="_blank" href="/rent/{{$item_project->slug}}" class="reset-btn btn-ctrl xuatban">Running</a>
                <a href="javascript:;" class="reset-btn btn-ctrl delete_thue_ban"  data-id="{{@$item_project->id}}"><i class="far fa-trash-alt"></i> Delete</a>
            </td>
            @else
            <td>
                <a href="/create-rental-step1/{{$item_project->id}}" class="reset-btn btn-ctrl"><i class="far fa-edit"></i>Edit</a>
                <a href="javascript:;" class="reset-btn btn-ctrl delete_thue_ban"  data-id="{{@$item_project->id}}"><i class="far fa-trash-alt"></i> Delete</a>
            </td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    var datatable = $('#tbl-dataz').DataTable({
        order: [[ 3, 'desc' ]],
        columnDefs: [
            { sortable: false, searchable: false, targets: [ 0,1,2,4 ] },
        ],
        displayStart: 0,
        displayLength: 5,
        "autoWidth": false
    });
    datatable.search('').draw();


</script>