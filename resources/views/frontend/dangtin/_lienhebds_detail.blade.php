<table class="table table-bordered">
    <tbody>
        <tr>
            <td>Name</td>
            <td>{{$customer->name_contact}}</td>
        </tr>
        <tr>
            <td>Property</td>
            <td>{{$customer->title}}</td>
        </tr>
        <tr>
            <td>Phone</td>
            <td>{{$customer->phone_contact}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$customer->email_contact}}</td>
        </tr>
        @if($customer->status == 2)
        <tr>
            <td>Thương lượng giá</td>
            <td>{{$customer->negotiable_price}}</td>
        </tr>
        @endif
        <tr>
            <td>Ask for information</td>
            <td>{{$customer->comment}}</td>
        </tr>
    </tbody>
</table>