<table id="tbl-dataz" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
    <thead>
        <tr>
            <th class="text-uppercase">Name</th>
            <th class="text-uppercase">Property</th>
            <th class="text-uppercase">Phone</th>
            <th class="text-uppercase">Date</th>
            <th class="text-uppercase">Messages</th>
        </tr>
    </thead>
    <tbody class="load_seach">
        @if (count($customers) != 0)
            @foreach ($customers as $customer)
            <tr>

                <td>{{$customer->name_contact}}</td>
                <td>
                    <a href="/cho-thue/{{$customer->slug}}" title="">{{$customer->title}}</a>
                </td>
                <td>{{$customer->phone_contact}}</td>
                
                <td>
                    <span class="d-inline-block"><span class="hidden">{{$customer->created_at}}</span>{{date('d/m/Y', strtotime($customer->created_at))}}</span>
                </td>

                <td>
                    <a data-id="{{@$customer->id}}" href="javascript:;" class="reset-btn btn-ctrl xem_lienhe">View</a>
                    <a href="javascript:;" class="reset-btn btn-ctrl delete_lienhe" data-id="{{@$customer->id}}"><i class="far fa-trash-alt"></i> Delete</a>
                </td>
            </tr>
            @endforeach
        @else
        
        @endif
    </tbody>
</table>

<script>
    var datatable = $('#tbl-dataz').DataTable({
        order: [[ 4, 'desc' ]],
        columnDefs: [
            { sortable: false, searchable: false, targets: [ 5 ] },
        ],
        displayStart: 0,
        displayLength: 5,
        "autoWidth": false
    });
    datatable.search('').draw();
</script>