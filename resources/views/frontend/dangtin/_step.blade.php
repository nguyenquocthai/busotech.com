@if($project->step == 1)
<div class="container-fluid">
    <br /><br />
    <ul class="list-unstyled multi-steps">
        <li class="{{@$b1}} ready"><a href="/tao-dang-tin-b1/{{$project->id}}" title="">Khởi tạo</a></li>
        <li class="{{@$b2}}"><a href="/tao-dang-tin-b2/{{$project->id}}" title="">Chi tiết</a></li>
        <li class="{{@$b3}}"><a href="/tao-dang-tin-b3/{{$project->id}}" title="">Hoàn thành</a></li>
    </ul>
</div>
@endif

@if($project->step == 2)
<div class="container-fluid">
    <br /><br />
    <ul class="list-unstyled multi-steps">
        <li class="{{@$b1}} ready"><a href="/tao-dang-tin-b1/{{$project->id}}" title="">Khởi tạo</a></li>
        <li class="{{@$b2}} ready"><a href="/tao-dang-tin-b2/{{$project->id}}" title="">Chi tiết</a></li>
        <li class="{{@$b13}}"><a href="/tao-dang-tin-b3/{{$project->id}}" title="">Hoàn thành</a></li>
    </ul>
</div>
@endif

@if($project->step == 3)
<div class="container-fluid">
    <br /><br />
    <ul class="list-unstyled multi-steps">
        <li class="{{@$b1}} ready"><a href="/tao-dang-tin-b1/{{$project->id}}" title="">Khởi tạo</a></li>
        <li class="{{@$b2}} ready"><a href="/tao-dang-tin-b2/{{$project->id}}" title="">Chi tiết</a></li>
        <li class="{{@$b3}} ready"><a href="/tao-dang-tin-b3/{{$project->id}}" title="">Hoàn thành</a></li>
    </ul>
</div>
@endif