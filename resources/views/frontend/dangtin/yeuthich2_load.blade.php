<table id="tbl-dataz" class="table table-striped table-bordered table-hover table-checkable order-column profile_advertise">
    <thead>
        <tr>
            <th class="text-uppercase">Tin đăng</th>
            <th class="text-uppercase">Hình</th>
            <th class="text-uppercase">Giá</th>
            <th class="text-uppercase">Ngày tạo</th>
            <th class="text-uppercase">Thao tác</th>
        </tr>
    </thead>
    <tbody class="load_seach">
        @foreach (@$item_projects as $project)
        <tr>
            <td>
                <p class="mb-0"><a href="/cho-thue/{{$project->slug}}" title=""><b>{{@$project->title}}</b></a></p>
                <p class="mb-0">Ngày cập nhật: {{date('d/m/Y', strtotime(@$project->updated_at))}}</p>
                @if($project->type == 0)
                <p class="mb-0">Loại BĐS: Cho thuê</p>
                @else
                <p class="mb-0">Loại BĐS: Bán</p>
                @endif
            </td>

            <td><figure class="img"><img src="/public/img/upload/items/{{$project->avatar}}" alt=""></figure></td>

            <td>
                @if($project->type == 0)
                <span class="price">
                    {{$project->fromprice}}
                    @if($project->fromprice != null && $project->toprice != null)
                    -
                    @endif
                    {{$project->toprice}}
                </span>
                <span class="date"> tr/tháng</span>
                @else
                <span class="price">
                    {{$project->fromprice}}
                    @if($project->fromprice != null && $project->toprice != null)
                    -
                    @endif
                    {{$project->toprice}}
                </span>
                <span class="date"> tr/m<sup>2</sup></span>
                @endif
            </td>

            <td>
                <span class="d-inline-block"><span class="hidden">{{$project->created_at}}</span>{{date('d/m/Y', strtotime(@$project->created_at))}}</span>
            </td>

            <td>
                <a href="javascript:;" class="reset-btn btn-ctrl remove_favorite" data-group="1" data-id="{{@$project->id}}"><i class="far fa-trash-alt"></i>Bỏ thích</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    var datatable = $('#tbl-dataz').DataTable({
        order: [[ 3, 'desc' ]],
        columnDefs: [
            { sortable: false, searchable: false, targets: [ 0,1,4 ] },
        ],
        displayStart: 0,
        displayLength: 5,
        "autoWidth": false
    });
    datatable.search('').draw();
</script>