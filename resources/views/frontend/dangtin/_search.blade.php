@foreach (@$projects as $project)
<tr>
    <td>
        <p class="mb-1"><b>Tên:</b> <span class="cl_cyan">{{@$project->title}}</span></p>
        <div class="post">
            <figure class="img"><img src="/public/img/upload/projects/{{$project->avatar}}" alt=""><span class="label">{{@$project->id}}</span></figure>
            <div class="text">
                <p class="mb-0">Ngày tạo: {{date('d/m/Y', strtotime(@$project->created_at))}}</p>
                <p class="mb-0">Ngày cập nhật: {{date('d/m/Y', strtotime(@$project->updated_at))}}</p>
            </div>
        </div>
        <p class="mb-1"><b>Địa chỉ:</b> <span class="cl_cyan">{{@$project->address}}</span></p>
        <p class="mb-1"><b>Loại BĐS:</b> <span>{{$project->catname}}</span></p>
    </td>

    <td>
        {{(@$project->price)}}
    </td>

    <td><b>{{@$project->project_scale}}</b> m<sup>2</sup></td>

    <td>
        <span class="d-inline-block">Từ: {{@$project->start_day}}</span>
        <span class="d-inline-block">Đến: {{@$project->end_day}}</span>
    </td>

    <td>
        <a href="/tao-dang-tin-b1/{{$project->id}}" class="reset-btn btn-ctrl"><i class="far fa-edit"></i>Sửa tin</a>
        <a href="javascript:;" class="reset-btn btn-ctrl xoadangtin"  xoa-id="{{@$project->id}}"><i class="far fa-trash-alt"></i>Xóa tin</a>
    </td>
</tr>
@endforeach