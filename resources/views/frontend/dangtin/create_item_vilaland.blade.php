@extends('frontend.layouts.main')
@section('content')
@include('frontend.nguoidung._plugin')
<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="."><i class="zmdi zmdi-home mr-2"></i>Trang chủ</a></li>
                <li class="item active">Đăng tin thuê</li>
            </ul>
        </div>
    </nav>
    <!-- NEWS-->
    <div class="profile-block">
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCQiaMU8DTCCDi90sT1ApqUYM737YYXymU&libraries=geometry,places" async></script>
        <script src="/public/assets/global/plugins/tinymce/tinymce.min.js"></script>
        <!--/head-block-->

        <div class="content-index bg-gray store-dang-tin">

            <div class="container">
                <div class="bg-white panel-wrap my-3 my-md-5">
                    @include('frontend.nguoidung._widget')

                    <section class="panel-content">
                        <button class="reset-btn panel-aside__open d-lg-none js-panelAsideTrigger"><i class="fa fa-user"></i>Bảng thông tin</button>

                        <h2 class="title text-uppercase">Đăng tin thuê <small class="d-block">Quý khách nhập thông tin nhà đất cần cho thuê vào các mục dưới đây</small></h2>

                        <form id="create_item" autocomplete="off" class="panel-form">
                            <input type="hidden" id="recaptcha" name="recaptcha">
                            <input type="hidden" name="type" value="0">
                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fas fa-book"></i> Info of your property</span></h4>

                                <div class="form-group">
                                    <label class="text mb-md-0">Title</label>
                                    <div class="input">
                                        <input type="text" name="title" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Street address</label>
                                            <input class="form-control local" type="text" name="duong" id="duong">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Poster Code</label>
                                            <input class="form-control local" type="text" id="phuongxa" name="phuongxa">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Province</label>
                                            <input class="form-control local" type="text" id="tinhthanh" name="tinhthanh">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">City</label>
                                            <input class="form-control local" type="text" id="quanhuyen" name="quanhuyen">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="text mb-md-0">address</label>
                                    <input class="form-control" id="address" type="text" name="address">
                                    <input type="hidden" name="map" class="toadobando">
                                </div>

                                <div class="form-group toadobando_box">
                                    <div class="input">
                                        <input id="pac-input" class="controls" type="text" placeholder="Nhập địa chỉ cần tìm...">
                                        <div id="googleMap" style="width: 100%; height: 300px;"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Type of property </label>
                                            <div class="input">
                                                <select class="form-control" name="category_id" id="category_id">
                                                    <option value="">-- choose --</option>
                                                    @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">For</label>
                                            <select class="form-control" name="doituong" id="doituong">
                                                <option value="">-- choose --</option>
                                                <option value="0">Men or Women</option>
                                                <option value="1">Only Men</option>
                                                <option value="2">Only Women</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">

                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Area</label>
                                                    <div class="input-group mb-3">
                                                        <input type="number" class="form-control" name="area" id="area">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2">feet<sup>2</sup></span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="text mb-md-0">Parking</label>
                                                    <select class="form-control" name="baixe" id="baixe">
                                                        <option value="">-- choose --</option>
                                                        <option value="0">No</option>
                                                        <option value="1">Yes</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Capacity</label>
                                            <div class="input">
                                                <div class="input-group mb-3">
                                                    <input type="number" class="form-control" name="succhua" id="succhua">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">person</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Price per month</label>
                                            <div class="input">
                                                <input id="price" value="" type="number" name="price" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Deposit</label>
                                            <input type="number" name="datcoc" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Availability</label>
                                            <select class="form-control" name="sangco" id="sangco">
                                                <option value="">-- choose --</option>
                                                <option value="0">Beginning of...</option>
                                                <option value="1">Anytime</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text mb-md-0">Beginning of...</label>
                                            <input class="form-control" type="text" name="sangco_time">
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="block">
                                <h4 class="title-line"><span class="text"><i class="fa fa-user"></i>Ower Info</span></h4>

                                @include('frontend.dangtin._lienhetin')

                                <div class="form-group">
                                    <label class="text mb-md-0">Describe feature of your property</label>
                                    <textarea name="short_content" id="short_content" class="form-control"></textarea>
                                </div>
                                
                            </div>

                            <div class="panel-form--footer">
                                <button id="submit-project" class="reset-btn btn-save"><span class="icon"><i class="fa fa-pencil-alt"></i></span><span class="text text-uppercase">Next Step</span></button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var $scope = {};

    tinymce.init({
        selector: '#short_content'
    });

    jQuery(document).ready(function($) {
        // GOOGLE MAP
        var map = "10.8230989,106.6296638"
        $scope.pointer = $scope.pointer || {};

        var geo = map.split(',');
        if (geo.length == 2) {
            $scope.pointer.latitude = geo[0];
            $scope.pointer.longitude = geo[1];
        }

        init_google_map($scope, {
            marker: $scope.marker,
            pointer: $scope.pointer,
        });
    });

    //----------------------------------------------------------------------------
    function init_google_map($scope, options) {
        if (typeof google === "undefined") {
            location.reload();
        }

        if (!$scope.map) {
            var p = { lat: parseFloat(options.pointer.latitude), lng: parseFloat(options.pointer.longitude) };

            if (isNaN(options.pointer.latitude) || isNaN(options.pointer.longitude))
                p = { lat: 10.8230989, lng: 106.6296638 };

            $scope.map = new google.maps.Map($('#googleMap')[0], {
                zoom: 17,
                center: p,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            placeMarker(p);

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');

            var searchBox = new google.maps.places.SearchBox(input);

            $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        return;
                    }

                    placeMarker(place.geometry.location);

                    options.pointer.latitude = place.geometry.location.lat();
                    options.pointer.longitude = place.geometry.location.lng();
                    options.pointer.description = place.formatted_address;

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                $scope.map.fitBounds(bounds);
                $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
            });
        }
        google.maps.event.addListener($scope.map, 'click', function(event) {
            placeMarker(event.latLng);

            options.pointer.latitude = event.latLng.lat();
            options.pointer.longitude = event.latLng.lng();
            $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
        });

        function placeMarker(location) {
            if (options.marker == null) {
            options.marker = new google.maps.Marker({
                position: location,
                map: $scope.map
            });
            } else {
                options.marker.setPosition(location);
            }
            $('.toadobando').val($scope.pointer.latitude + ',' +$scope.pointer.longitude);
        }
    };

    $('#create_item').on('blur', '.local', function(event) {
        var tinhthanh = $('#tinhthanh').val();

        var quanhuyen = $('#quanhuyen').val();
        if (quanhuyen == '') {
            quanhuyen = '';
        } else {
            quanhuyen = quanhuyen + ', ';
        }

        var phuongxa = $('#phuongxa').val();
        if (phuongxa == '') {
            phuongxa = '';
        } else {
            phuongxa = phuongxa + ', ';
        }

        var duong = $('#duong').val();
        if (duong == '') {
            duong = '';
        } else {
            duong = duong + ', ';
        }

        var address = duong + phuongxa + quanhuyen + tinhthanh;
        $('#address').val(address);

        var input = document.getElementById('pac-input');
        $('#pac-input').val($('#address').val());
        google.maps.event.trigger(input, 'focus', {});
        google.maps.event.trigger(input, 'keydown', { keyCode: 13 });
        google.maps.event.trigger(this, 'focus', {});
    });

    $("#address").blur(function(){
        var input = document.getElementById('pac-input');
        $('#pac-input').val($(this).val());
        google.maps.event.trigger(input, 'focus', {});
        google.maps.event.trigger(input, 'keydown', { keyCode: 13 });
        google.maps.event.trigger(this, 'focus', {});
    });

</script>   

<script>
    $('#category_id, #id_project, #donvi').select2({
        theme: "bootstrap",
        width: '100%'
    });

    $('#category_id, #id_project').change(function(event) {
        if ($(this).val() != "") {
            $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
        } else {
            $(this).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
        }
    });

    var create_item = $('#create_item').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
            $(element).addClass("error_input");
            $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').addClass('error_select');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("error_input");
            $(element).closest('.input').find('.select2-container--bootstrap .select2-selection').removeClass('error_select');
        },
        rules: {
            title:{
                required: true
            },
            address:{
                required: true
            },
            tinhthanh:{
                required: true
            },
            quanhuyen:{
                required: true
            },
            phuongxa:{
                required: true
            },
            duong:{
                required: true
            },
            category_id:{
                required: true
            },
            map:{
                required: true
            },
            short_content:{
                required: true
            },
            doituong:{
                required: true
            },
            area:{
                required: true
            },
            baixe:{
                required: true
            },
            succhua:{
                required: true
            },
            price:{
                required: true
            },
            datcoc:{
                required: true
            },
            sangco:{
                required: true
            },
            lh_phone:{
                required: true
            },
            lh_email:{
                required: true
            }
        },
        messages: {
            title:{
                required: 'Tên dự án'
            },
            address:{
                required: 'Địa chỉ dự án'
            },
            tinhthanh:{
                required: 'Tỉnh thành'
            },
            quanhuyen:{
                required: 'Quận huyện'
            },
            phuongxa:{
                required: 'Phường xã'
            },
            duong:{
                required: 'Đường'
            },
            map:{
                required: 'Bản đồ'
            },
            category_id:{
                required: 'Danh mục'
            },
            short_content:{
                required: 'Mô tả ngắn'
            },
            doituong:{
                required: 'Đối tượng'
            },
            area:{
                area: 'area'
            },
            baixe:{
                baixe: 'baixe'
            },
            succhua:{
                succhua: 'succhua'
            },
            price:{
                price: 'price'
            },
            datcoc:{
                datcoc: 'datcoc'
            },
            sangco:{
                sangco: 'sangco'
            },
            lh_phone:{
                lh_phone: 'lh_phone'
            },
            lh_email:{
                lh_email: 'lh_email'
            }
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#create_item").serializeArray().map(function(x){data[x.name] = x.value;});
            data['status_code'] = "add";
            data['slug'] = ChangeToSlug(data['title']);
            data['short_content'] = tinymce.get('short_content').getContent();

            var search_val = {};
            search_val[0] = $('#category_id option:selected').html();
            search_val[1] = $('#tinhthanh').val();
            search_val[2] = $('#quanhuyen').val();
            search_val[3] = $('#phuongxa').val();
            search_val[4] = $('#duong').val();
            search_val[5] = data['title'];
            data['search_val'] = search_val;

            console.log(data);
            return false;

            $('.block-page-all').addClass('active');
            $.ajax({
                type: 'POST',
                url: '/api_crate_item',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    toastr.success(result.message);
                    document.location.href = '/tao-tin-thue-b2/'+result.id;
                }
            });
            return false;
        }
    });
</script>  
@endsection