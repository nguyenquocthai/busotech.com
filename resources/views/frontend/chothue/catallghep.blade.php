@extends('frontend.layouts.main')
<style>

    .project-list{
        position: relative;
    }

    .filter-box{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        z-index: 10;
        background: #fff;
        display: none;
    }

    .filter-box.active{
        display: block;
    }

    .filter-collapse {
        z-index: 20 !important;
    }
    .filter-box ul{
        list-style: none;
        padding-left: 0;
        margin-bottom: 0;
        padding: 10px;
    }

    .filter-box ul li{
        padding: 2px 5px;
        background: #f7346f;
        color: #fff;
        display: none;
        margin-bottom: 5px;
    }

    .filter-box ul li.active{
        display: inline-block;
    }

    .btn_del{
        cursor: pointer;
        padding-left: 3px;
        padding-right: 3px;
    }

    .btn_bocx{
        text-align: right;
        border-top: 1px solid #f7346f;
        padding: 10px;
    }

    .btn_bocx .btn{
        border-radius: 0;
    }
</style>
@section('content')
<div class="col-lg-12">
    <!-- HOUSES FOR RENT-->
    <section class="block" style="margin-top: 250px;">
        <div class="block-title-wrap">
            <h3 class="block-title">New roommate</h3>
            <a class="reset-btn btn-more" href="/rent-category">See all</a>
        </div>

        @if (count($item_projects) != 0)
            @foreach ($item_projects as $key => $item_project)
            <article class="project-item hor hasLink">
                <div class="img embed-responsive albums">
                    
                    @if (count(@$item_project->albums) != 0)
                        @foreach (@$item_project->albums as $item_album)
                        <div class="item_albums">
                            <div class="img_mod">
                                <img src="{{BladeGeneral::GetImg(['avatar' => $item_album->name,'data' => 'item_album', 'time' => $item_album->updated_at])}}" alt="">
                            </div>
                            
                        </div>

                        @endforeach
                    @else
                    
                    @endif
                </div>
                <div class="content">
                    <a class="name link" href="/rent/{{$item_project->slug}}" title="{{$item_project->title}}">{{$item_project->title}}</a>
                    <div class="content-block">
                        <div class="left">
                            <p class="mb-2"><i class="icon icon-nav-home"></i>
                                @if($item_project->type == 0)
                                rent
                                @endif
                                @if($item_project->type == 1)
                                in grafting
                                @endif
                            </p>
                            <p class="mb-2">
                                <span class="mr-4">
                                    <i class="icon icon-nav-roomate"></i>
                                    @if($item_project->doituong == 0)
                                    Men or Women
                                    @endif
                                    @if($item_project->doituong == 1)
                                    Only Men
                                    @endif
                                    @if($item_project->doituong == 2)
                                    Only Women
                                    @endif
                                </span>
                                <span><i class="icon icon-squaremeter"></i>{{$item_project->area}} sqft</span></p>
                            <p class="mb-2">
                                <span class="mr-4"><a href="#"><i class="icon icon-bed-fill"></i>{{$item_project->phongngu}}</a></span>
                                <span><a href="#"><i class="icon icon-toilet-fill"></i>{{$item_project->phongtam}}</a></span></p>
                            <p class="mb-2"><i class="icon icon-location"></i>{{$item_project->address}}
                            </p>
                        </div>
                        <div class="right">
                            <div class="price"><span class="price-val">${{BladeGeneral::priceFormat($item_project->price)}}</span><span class="price-unit">/ month</span></div>
                        </div>
                    </div>
                </div>
            </article>
            @if($key == 10) @break @endif
            @endforeach
        @else
        
        @endif
    </section>
</div>

<script>
    $('.albums').slick();
</script>
@endsection