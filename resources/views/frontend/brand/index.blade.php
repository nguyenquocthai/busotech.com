@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER-->
  <section class="section banner-page bg-gradient">
    <div class="container">
      <div class="title-block">
        <h2 class="title-main title-main--lg"><span class="d-block">Consulting branding&nbsp;<span class="d-inline-block">strategy</span></span><span class="d-block">for&nbsp;<span class="d-inline-block">businesses</span></span></h2><a class="rs-btn btn-animated btn-white" href="/branding-form">Consult now</a>
      </div>
    </div>
  </section>
  <!-- SECTION TWO-->
  <section class="section branding">
    <div class="container branding-container">
      <div class="title-block text-center">
        <h2 class="title-main">Buso Brand perform&nbsp;<span class="d-block d-lg-inline-block"><span class="js-typing" data-value="Brand strategy consulting; Consulting on branding; Building brand identity system; Development of communication products; Media advertisement"></span><span>_</span></span></h2>
      </div>
      <div class="row">
        <?php foreach ($brand_items as $key => $brand_item): ?>
          <div class="col-sm-6 col-lg-4 branding-item animate-fadeUp">
            <div class="inner">
              <figure class="img"><img src="/public/img/upload/brand_items/{{$brand_item->avatar}}" alt=""/></figure>
              <div class="content">
                <h3 class="title">{{$brand_item->title}}</h3>
                <ul class="rs-list list">
                  {!!$brand_item->content!!}
                </ul>
              </div>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <!-- WORK FLOW-->
  <section class="section workflow" style="background-image: url(/public/theme/img/bg-quy-trinh-lam-viec.jpg);">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main"><span>Working&nbsp;</span><span class="d-inline-block">process</span></h2>
      </div>
      <div class="roadmap-slider js-sliderRoadmap">
        <?php foreach ($brand_steps as $key => $brand_step): ?>
          <div class="item">
            <div class="icon-box">
              <figure class="icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="104" height="135.243" viewBox="0 0 104 135.243">
                  <defs>
                    <filter id="a" x="0" y="0" width="104" height="135.243" filterUnits="userSpaceOnUse">
                      <feoffset dy="3" input="SourceAlpha"></feoffset>
                      <fegaussianblur stdDeviation="3" result="b"></fegaussianblur>
                      <feflood flood-opacity="0.161"></feflood>
                      <fecomposite operator="in" in2="b"></fecomposite>
                      <fecomposite in="SourceGraphic"></fecomposite>
                    </filter>
                  </defs>
                  <g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#a)">
                    <g transform="translate(-388 -351)" fill="" stroke-miterlimit="10">
                      <path d="M 440 472.121337890625 L 438.9393310546875 471.0606689453125 C 438.8385009765625 470.9598388671875 428.740966796875 460.8191223144531 418.7868957519531 447.1322631835938 C 412.9253540039063 439.0726318359375 408.2507019042969 431.3680114746094 404.8927612304688 424.2324523925781 C 400.6508483886719 415.2183532714844 398.5 407.0653686523438 398.5 400 C 398.5 394.3982849121094 399.5974731445313 388.9633178710938 401.761962890625 383.8460083007813 C 403.8522644042969 378.9040832519531 406.8443908691406 374.4660339355469 410.6552124023438 370.6552124023438 C 414.4660339355469 366.8443908691406 418.9040832519531 363.8522644042969 423.8460083007813 361.761962890625 C 428.9633178710938 359.5974731445313 434.3982849121094 358.5 440 358.5 C 445.6017150878906 358.5 451.0366821289063 359.5974731445313 456.1539916992188 361.761962890625 C 461.0959167480469 363.8522644042969 465.5339660644531 366.8443908691406 469.3447875976563 370.6552124023438 C 473.1556091308594 374.4660339355469 476.1477355957031 378.9040832519531 478.238037109375 383.8460083007813 C 480.4025268554688 388.9633178710938 481.5 394.3982849121094 481.5 400 C 481.5 407.0653686523438 479.3491516113281 415.2183532714844 475.1072387695313 424.2324523925781 C 471.7492980957031 431.3680114746094 467.0746459960938 439.0726318359375 461.2131042480469 447.1322631835938 C 451.259033203125 460.8191223144531 441.1614990234375 470.9598388671875 441.0606689453125 471.0606689453125 L 440 472.121337890625 Z" stroke="none"></path>
                      <path d="M 440 360 C 417.9089965820313 360 400 377.9089965820313 400 400 C 400 430 440 470 440 470 C 440 470 480 430 480 400 C 480 377.9089965820313 462.0910034179688 360 440 360 M 440 357 C 445.8035278320313 357 451.4351196289063 358.1373596191406 456.7383117675781 360.3804321289063 C 461.8590698242188 362.54638671875 466.4573364257813 365.6464538574219 470.4054260253906 369.5945739746094 C 474.3535461425781 373.5426635742188 477.45361328125 378.1409301757813 479.6195678710938 383.2616882324219 C 481.8626403808594 388.5648803710938 483 394.1964721679688 483 400 C 483 407.287841796875 480.8011169433594 415.6557312011719 476.4644470214844 424.8711547851563 C 473.0663757324219 432.0920715332031 468.3432312011719 439.8786010742188 462.4262084960938 448.0145263671875 C 452.4030151367188 461.7964172363281 442.5364379882813 471.7062072753906 442.1213073730469 472.1213073730469 L 440 474.2426452636719 L 437.8786926269531 472.1213073730469 C 437.4635620117188 471.7062072753906 427.5969848632813 461.7964172363281 417.5737915039063 448.0145263671875 C 411.6567687988281 439.8786010742188 406.9336242675781 432.0920715332031 403.5355529785156 424.8711547851563 C 399.1988830566406 415.6557312011719 397 407.287841796875 397 400 C 397 394.1964721679688 398.1373596191406 388.5648803710938 400.3804321289063 383.2616882324219 C 402.54638671875 378.1409301757813 405.6464538574219 373.5426635742188 409.5945739746094 369.5945739746094 C 413.5426635742188 365.6464538574219 418.1409301757813 362.54638671875 423.2616882324219 360.3804321289063 C 428.5648803710938 358.1373596191406 434.1964721679688 357 440 357 Z" stroke="none" fill="#fff"></path>
                    </g>
                  </g>
                </svg>
                <figcaption class="caption">{{$brand_step->title}}</figcaption>
              </figure>
            </div>
            <time class="time"></time>
            <div class="content">
              <div class="inner">
                {!!$brand_step->content!!}
              </div>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <!-- IMPLEMENTED PROJECTS-->
  <section class="section project-implemented style2">
    <div class="container-fluid container-2">
      <div class="title-block text-center">
        <h2 class="title-main"><span>Project&nbsp;</span><span class="d-inline-block">was implemented</span></h2>
        <h3 class="title-sub animate-fadeUp">With many years of operation in the field of consultancy, brand strategy development, advertising, we have implemented many projects of different sizes, large and small. Regardless of the scale, we take great care and pride in the products and projects that we have implemented. </h3>
      </div>
      <div class="row">
        <?php foreach ($brand_duans as $key => $brand_duan): ?>
          <div class="col-sm-6 col-lg-4 project-implemented__item animate-fadeUp">
            <div class="inner hasLink">
              <figure class="img embed-responsive"><img src="/public/img/upload/brand_duans/{{$brand_duan->avatar}}" alt="{{$brand_duan->title}}"/></figure>
              <div class="content">
                <h4 class="title">{{$brand_duan->title}}</h4>
              </div><a class="link" href="{{$brand_duan->link}}" target="_blank" title="{{$brand_duan->title}}"></a>
            </div>
          </div>
        <?php endforeach ?>
        
      </div>
      <div class="text-center"><a class="rs-btn btn-gradient" href="/branding-projects">See more</a></div>
    </div>
  </section>
  <!-- SECTION FIVE-->
  <section class="section section-five pt-0">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main"><span>Research&nbsp;</span><span class="d-inline-block">Articles</span></h2>
      </div>
      <div class="news-slider--index js-sliderNewsIndex">
        <?php foreach ($blogs as $key => $blog): ?>
          <div class="item news-item">
            <div class="inner hasLink">
              <figure class="img embed-responsive"><img src="/public/img/upload/blogs/{{$blog->avatar}}" alt=""/></figure>
              <div class="content">
                <h4 class="title">{{$blog->title}}</h4><span class="more">See more&#62;</span>
              </div><a class="link" href="/tin-tuc/{{$blog->slug}}" title="{{$blog->title}}"></a>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <!-- CUSTOMER-->
  <section class="section section-four" style="background-image: url(/public/theme/img/backgrounds/bg-wave.svg)">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main"><span>Customers&nbsp;</span><span class="d-inline-block">& Partners</span></h2>
      </div>
      <ul class="rs-list customer-list">
        <?php foreach ($doitacs as $key => $doitac): ?>
          <li class="item">
            <figure class="img hasLink"><img src="/public/img/upload/doitacs/{{$doitac->avatar}}" alt=""/><a class="link" href="#!"></a></figure>
          </li>
        <?php endforeach ?>
        </li>
      </ul>
    </div>
  </section>
  <!-- SUBCRIBE-->
  <section class="section section-six"><span class="circle circle--striped"></span>
    <div class="container"><span class="triangle triangle--light"></span>
      <div class="subscribe-box">
        <div class="title-block text-center">
          <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
          <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
        </div>
        <form class="subscribe-form" id="subscribe" action="">
          <input class="input" name="email" placeholder="Your email"/>
          <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
        </form>
        <div class="contact-box">
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
          </div>
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
@endsection