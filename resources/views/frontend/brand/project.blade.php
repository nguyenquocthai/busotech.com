@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER-->
  <section class="section banner-page bg-gradient">
    <div class="container">
      <div class="title-block">
        <h2 class="title-main title-main--lg"><span class="d-block">Consulting branding&nbsp;<span class="d-inline-block">strategy</span></span><span class="d-block">for&nbsp;<span class="d-inline-block">businesses</span></span></h2><a class="rs-btn btn-animated btn-white" href="/branding-form">Tư vấn ngay</a>
      </div>
    </div>
  </section>
  <!-- IMPLEMENTED PROJECTS-->
  <section class="section project-implemented style2">
    <div class="container container-2">
      <div class="title-block text-center">
        <h2 class="title-main"><span>Project&nbsp;</span><span class="d-inline-block">was implemented</span></h2>
        <h3 class="title-sub animate-fadeUp">With many years of operation in the field of consultancy, brand strategy development, advertising, we have implemented many projects of different sizes, large and small. Regardless of the scale, we take great care and pride in the products and projects that we have implemented. </h3>
      </div>
      <div class="row">
        <?php foreach ($brand_duans as $key => $brand_duan): ?>
          <div class="col-sm-6 col-lg-4 project-implemented__item animate-fadeUp">
            <div class="inner hasLink">
              <figure class="img embed-responsive"><img src="/public/img/upload/brand_duans/{{$brand_duan->avatar}}" alt="{{$brand_duan->title}}"/></figure>
              <div class="content">
                <h4 class="title">{{$brand_duan->title}}</h4>
              </div><a class="link" href="{{$brand_duan->link}}" target="_blank" title="{{$brand_duan->title}}"></a>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <!-- SUBCRIBE-->
  <section class="section section-six"><span class="circle circle--striped"></span>
    <div class="container"><span class="triangle triangle--light"></span>
      <div class="subscribe-box">
        <div class="title-block text-center">
          <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
          <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
        </div>
        <form class="subscribe-form" id="subscribe" action="">
          <input class="input" name="email" placeholder="Your email"/>
          <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
        </form>
        <div class="contact-box">
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
          </div>
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
@endsection