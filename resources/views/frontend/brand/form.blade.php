@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<style>
  header,footer{
    display: none;
  }
</style>
<main class="main">
  <section class="login-section branding-form bg-gradient">
    <figure class="img"><img src="/public/theme/img/login-img.svg" alt=""/></figure>
    <div class="content">
      <div class="inner actived">
        <div class="title-block text-left">
          <div class="title">Build your own brand &nbsp;<span class="d-inline-block">with Buso</span></div>
          <p class="title-sub">Customers choose YOU, not just your product or service is good, but your brand's reputation. Buso can provide the branding solutions and tools needed to add efficiency to your work. <br/> Let Buso advise and strategize your branding.</p>
        </div>
        <form class="login-form form-brand" action="#!">
          <p class="login-form__row">
            <input class="input" type="text" name="name" placeholder="Full name"/>
          </p>
          <p class="login-form__row">
            <input class="input" type="email" name="email" placeholder="Email"/>
          </p>
          <p class="login-form__row">
            <input class="input" type="number" name="tel" placeholder="Phone number"/>
          </p>
          <p class="login-form__row text-left">
            <?php foreach ($brand_items as $key => $brand_item): ?>
              <label class="checkbox radio w-100">
                <input class="checkbox-input" type="radio" name="branding_type" value="{{$brand_item->title}}" <?php if($key == 0) echo 'checked' ?>/><span class="checkbox-icon"></span><span class="checkbox-text">{{$brand_item->title}}</span>
              </label>
            <?php endforeach ?>
            
          </p>
          <p class="login-form__row text-left">
            <button class="rs-btn btn-gradient my-0" type="submit">Send</button>
          </p>
        </form>
        <div class="branding-form__panel">
          <div class="title text-center">Quick Contact</div>
          <div class="contact-box">
            <div class="item">
              <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
            </div>
            <div class="item">
              <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
<script>
  jQuery(document).ready(function($) {
    
    var formbrand = $('.form-brand').validate({
      highlight: function(element, errorClass, validClass) {
          $(element).removeClass(errorClass);
      },
      rules: {
        email:{
            required: true,
            email:true
        },
        name:{
            required: true
        },
        tel:{
            required: true,
            number: true
        }
      },
      messages: {
        email:{
            required: 'Please enter your email',
            email: 'Your email must be a valid email.',
        },
        name:{
            required: 'Please enter your name.'
        },
        tel:{
            required: 'Please enter your phone number',
            number: 'Phone number must be number.',
        }
      },
      submitHandler: function (form) {
        var data = {};
        $(".form-brand").serializeArray().map(function(x){data[x.name] = x.value;});
        
        $.ajax({
            type: 'POST',
            url: '/add_contact_brand',
            data: data,
            dataType: 'json',
            error: function(){
                
                toastr.error('Error');
            },
            success: function(result) {
                
                switch (result.code) {
                    case 200:
                        $('.form-brand').find("input, textarea").val("");
                        toastr.success('Contact successfully sent.');
                        break;
                    default:
                        toastr.error('Error');
                        break;
                }
            }
        });
        return false;
    }
    });
  });
</script>
@endsection