@extends('frontend.layouts.main')
@section('content')
<div class="row banner">
  <div class="slide">
    <div class="inner"><img src="/public/img/upload/program_cats/{{@$program_cat->avatar}}" width="1160" height="494" /></div>
    <div class="title">
        <h2>{{@$program_cat->title}}</h2>
        <h4>{{@$program_cat->summary}}</h4>
    </div>
  </div>
</div>
<div class="row">
        <div class="content small-12 medium-9 columns">
        <?php foreach ($programs as $key => $program): ?>
          
            <h3><a href="/program/{{$program->slug}}" target="_blank">{{$program->title}}<br /></a></h3>
            <p>{{$program->summary}}</p>

          
        <?php endforeach ?>
        </div>
        <div class="small-12 medium-3 columns sidebar">

            <div class="small-12 columns featbox">
               <h4>{{@$program_cat->title}}</h4>
               <ul class="eventside">
                  <?php foreach ($program_cats as $key => $program_cat): ?>
                    <li>
                     <strong><a href="/program-category/{{$program_cat->slug}}">{{$program_cat->title}}</a></strong><br>
                  </li>
                  <?php endforeach ?>
                  
               </ul>
            </div>
            <div class="small-12 columns">
                <h4>{{@$langs['su-kien-sap-toi']}}</h4>
                <ul class="eventside">
                  <?php foreach ($events as $key => $event): ?>
                      <li>
                          <strong><a href="/event/{{$event->slug}}">{{$event->title}}</a></strong><br />
                      </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="small-12 columns">
                <h4>{{@$langs['tin-moi-nhat']}}</h4>
                <ul class="">
                    <?php foreach ($blogs as $key => $blog): ?>
                      <li>
                          <strong><a href="/news/{{$blog->slug}}">{{$blog->title}}</a></strong><br />
                      </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>
@endsection