@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- BANNER-->
      <section class="section banner-page human-resource-banner" style="background-image: url(/public/theme/img/bg-nhan-su.jpg)">
        <div class="container">
          <div class="title-block">
            <h1 class="title-main"><span class="d-inline-block">About personnel</span></h1>
            <h2 class="title-sub animate-fadeUp"><span class="d-inline-block">Buso owns a team of experienced programmers working in the field of consulting, building marketing strategies; designing application programming on website platform. Buso always listens and supports so that employees can fulfill their full potential </span></h2>
          </div>
        </div>
      </section>
      <!-- CORE VALUES-->
      <section class="section core-value">
        <div class="container container-2">
          <figure class="core-value__img"><img src="/public/theme/img/gia-tri-cot-loi.png" alt=""/></figure>
          <div class="title-block text-center">
            <h2 class="title-main mb-0"><span class="number">{{$personnel_count}}</span>&nbsp;<span class="d-inline-block">Core values</span></h2>
          </div>
          <div class="row core-value__list">
            <?php foreach ($personnel_values as $key => $personnel_value): ?>
              <div class="col-sm-6 col-lg-4 core-value__item animate-fadeUp">
                <div class="inner">
                  <figure class="img"><img src="/public/img/upload/personnel_values/{{$personnel_value->avatar}}" alt=""/></figure>
                  <div class="content">
                    <h3 class="title">{{$personnel_value->title}}</h3>
                    <p>{{$personnel_value->summary}}</p>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
            
          </div>
          <div class="row justify-content-center">
            <div class="col-lg-10">
              <div class="text-center">We strive to put human development above business interests. Buso values the employee's contribution to the development of the department and the company very much. Managers always listen and support so that employees can maximize their potential, contributing to further development of Buso..</div>
            </div>
          </div>
        </div>
      </section>
      <!-- HUMAN RESOURCES-->
      <section class="section human-resource">
        <div class="container">
          <h2 class="d-none">Personnel</h2>
          <div class="news-slider--index js-sliderNewsIndex">
            <?php foreach ($personnels as $key => $personnel): ?>
            <div class="item human-resource__item">
              <div class="inner text-center">
                <figure class="img"><img src="/public/img/upload/personnels/{{$personnel->avatar}}" alt="{{$personnel->title}}"/></figure>
                <div class="content">
                  <h3 class="title mb-0">{{$personnel->title}}</h3>
                  <p class="position mb-0">{{$personnel->summary}}</p>
                </div>
              </div>
            </div> 
            <?php endforeach ?>
          </div>
        </div>
      </section>
      <!-- SECTION SIX-->
      <section class="section section-six"><span class="circle circle--striped"></span>
        <div class="container"><span class="triangle triangle--light"></span>
          <div class="subscribe-box">
            <div class="title-block text-center">
              <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
              <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
            </div>
            <form class="subscribe-form" id="subscribe" action="">
              <input class="input" name="email" placeholder="Your email"/>
              <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
            </form>
            <div class="contact-box">
              <div class="item">
                <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
              </div>
              <div class="item">
                <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
@endsection