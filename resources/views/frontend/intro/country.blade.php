@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<div class="row banner">
  <div class="slide">
    <div class="inner"><img src="/public/img/upload/banners/{{$banner->avatar}}" width="1160" height="494" /></div>
    <div class="title">
        <h2>{{$banner->title}}</h2>
        <h4>{{$banner->summary}}</h4>
    </div>
  </div>
</div>
<div class="row">
        <div class="content small-12 medium-9 columns">
            <ul class="eventlist">
              <?php foreach ($countrys as $key => $country): ?>
                <div class="row">
                    <div class="small-12 columns">
                        <a href="/countries/{{$country->slug}}"><img
                                src="/public/img/upload/intros/{{$country->avatar}}"
                                alt="{{$country->title}}" /></a>
                    </div>

                    <div class="post small-12 columns">
                        <h3 class="title"><a href="/countries/{{$country->slug}}">{{$country->title}}</a></h3>
                    </div>
                </div>
              <?php endforeach ?>
            </ul>

        </div>
        <div class="small-12 medium-3 columns sidebar">

            <div class="small-12 columns featbox">
                <h4>{{@$langs['cac-quoc-gia']}}</h4>
                <ul class="eventside">
                  <?php foreach ($countrys as $key => $country): ?>
                    <li>
                        <strong><a href="/countries/{{$country->slug}}">{{$country->title}}</a></strong><br />
                    </li>
                    <?php endforeach ?>
                    
                </ul>
            </div>
            <div class="small-12 columns">
                <h4>{{@$langs['su-kien-sap-toi']}}</h4>
                <ul class="eventside">
                  <?php foreach ($events as $key => $event): ?>
                      <li>
                          <strong><a href="/event/{{$event->slug}}">{{$event->title}}</a></strong><br />
                      </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="small-12 columns">
                <h4>{{@$langs['tin-moi-nhat']}}</h4>
                <ul class="">
                    <?php foreach ($blogs as $key => $blog): ?>
                      <li>
                          <strong><a href="/news/{{$blog->slug}}">{{$blog->title}}</a></strong><br />
                      </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>
@endsection