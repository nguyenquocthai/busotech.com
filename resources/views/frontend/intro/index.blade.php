@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <?php foreach ($intros as $key => $intro): ?>
        <?php if ($key == 0): ?>
        <section class="section banner-page about-banner" style="background-image: url(/public/img/upload/intros/{{$intro->avatar}});">
          <div class="container">
            <div class="title-block text-center">
              <h2 class="title-main"><span class="d-inline-block">{{$intro->title}}</span></h2>
              {!!$intro->content!!}
            </div>
          </div>
        </section>
        <?php break; endif ?>
      <?php endforeach ?>
      <!-- BANNER-->
      
      <!-- ABOUT BUSO-->
      <?php foreach ($intros as $key => $intro): ?>
        <?php if ($key == 1): ?>
        <section class="section about-section-1">
          <div class="container">
            <div class="title-block text-center">
              <h2 class="title-main">{{$intro->title}}</h2>
            </div>
          </div>
          <div class="container">
            <div class="content animate-fadeUp">
              <p><span class="text-large">{{$intro->summary[0]}}</span>{{substr($intro->summary, 1)}}</p><img src="/public/img/upload/intros/{{$intro->avatar}}" alt="{{$intro->title}}"/>
              {!!$intro->content!!}
            </div>
          </div>
        </section>
        <?php break; endif ?>
      <?php endforeach ?>
      <!-- PRODUCT ORIENTATION-->
      <?php foreach ($intros as $key => $intro): ?>
        <?php if ($key == 2): ?>
        <section class="section about-section-2" style="background-image: url(/public/theme/img/backgrounds/bg-wave--blue.jpg);">
          <div class="container">

            <div class="title-block text-center">
              <h2 class="title-main">Định hướng sản phẩm</h2>
              <h3 class="title-sub animate-fadeUp">{{$intro->summary}}</h3>
            </div>
            <div class="about-product__grid">
              <?php foreach ($dinhhuongs as $key => $dinhhuong): ?>
                <div class="item animate-fadeUp">
                  <figure class="icon"><img src="/public/img/upload/dinhhuongs/{{$dinhhuong->avatar}}" alt="{{$dinhhuong->title}}"/></figure>
                  <h3 class="title">{{$dinhhuong->title}}</h3>
                  <h4 class="sum">{{$dinhhuong->summary}}</h4>
                </div>
              <?php endforeach ?>
              <div class="item img animate-fadeUp"><img src="/public/img/upload/intros/{{$intro->avatar}}" alt=""/></div>
            </div>
            <div class="description">{!!$intro->content!!}</div>
          </div>
        </section>
        <?php break; endif ?>
      <?php endforeach ?>
      <!-- BRAND VALUE-->
      <?php foreach ($intros as $key => $intro): ?>
      <?php if ($key == 3): ?>
      <section class="section about-section-3">
        <div class="container">
          <div class="title-block text-center">
            <h2 class="title-main text-uppercase">{{$intro->title}}</h2>
            {!!$intro->content!!}
          </div>
        </div>
        <div class="container container-2">
          <div class="row justify-content-center">
            <div class="col-md-4 about-brand__item animate-fadeUp">
              <div class="inner">
                <figure class="img"><img src="/public/theme/img/icons/bar-chart--circle.png" alt=""/></figure>
                <div class="content">
                  <h4 class="title">Buso đồng hành cùng doanh nghiệp và cá nhân đạt mục tiêu gia tăng doanh số bán hàng trong thương mại điện tử</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4 about-brand__item animate-fadeUp">
              <div class="inner">
                <figure class="img"><img src="/public/theme/img/icons/shopping-cart--circle.png" alt=""/></figure>
                <div class="content">
                  <h4 class="title">Buso mang đến cho các doanh nghiệp bán lẻ một nền tảng quản lý và bán hàng tổng thể từ online đến offline</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4 about-brand__item animate-fadeUp">
              <div class="inner">
                <figure class="img"><img src="/public/theme/img/icons/arrow--circle.png" alt=""/></figure>
                <div class="content">
                  <h4 class="title">Buso cung cấp các giải pháp, xây dựng chiến thược quảng bá thương hiệu hiệu quả</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php break; endif ?>
      <?php endforeach ?>
      
      <!-- ABOUT SECTION 4-->
      <section class="section about-section-4">
        <div class="container">
          <div class="about-list">
            <?php foreach ($intros as $key => $intro): ?>
            <?php if ($key >= 4 && $key <=6): ?>
            <div class="item animate-fadeUp">
              <div class="inner">
                <figure class="img"><img src="/public/img/upload/intros/{{$intro->avatar}}" alt="{{$intro->title}}"/></figure>
                <div class="content">
                  <h2 class="title-main">{{$intro->title}}</h2>
                  <div class="sum">{!!$intro->content!!}</div>
                </div>
              </div>
            </div>
            <?php endif ?>
            <?php endforeach ?>
            
          </div>
        </div>
      </section>
      
      <!-- DOMINANCE-->
      <?php foreach ($intros as $key => $intro): ?>
      <?php if ($key == 7 ): ?>
      <section class="section about-section-5">
        <div class="container container-2">
          <div class="row align-items-center">
            <div class="col-lg-6">
              <figure class="img text-center animate-fadeUp"><img src="/public/img/upload/intros/{{$intro->avatar}}" alt="{{$intro->title}}"/></figure>
            </div>
            <div class="col-lg-6">
              <div class="title-block">
                <h2 class="title-main">{{$intro->title}}</h2>
              </div>
              {!!$intro->content!!}
            </div>
          </div>
        </div>
      </section>
      <?php break; endif ?>
      <?php endforeach ?>
      <!-- PARTNER-->
      <section class="section about-section-6">
        <div class="container">
          <div class="title-block">
            <h2 class="title-main text-center">Đối tác chiến lược</h2>
          </div>
          <ul class="rs-list about-partner__list">
            <li class="item animate-fadeUp">
              <figure class="img"><img src="/public/theme/img/facebook-marketing.png" alt=""/></figure>
            </li>
            <li class="item animate-fadeUp">
              <figure class="img"><img src="/public/theme/img/google-developer.png" alt=""/></figure>
            </li>
          </ul>
        </div>
      </section>
      <!-- SECTION SIX-->
      <section class="section section-six"><span class="circle circle--striped"></span>
        <div class="container"><span class="triangle triangle--light"></span>
          <div class="subscribe-box">
            <div class="title-block text-center">
              <h2 class="title-main"><span>Buso luôn sẵn sàng để&nbsp;</span><span class="d-inline-block">tư vấn bạn</span></h2>
              <h3 class="title-des">Buso luôn đồng hành cùng bạn nhằm mang đến các giải pháp tổng thể<br class="d-none d-lg-block"/>thúc đẩy gia tăng doanh số cho doanh nghiệp của bạn</h3>
            </div>
            <form class="subscribe-form" id="subscribe" action="">
              <input class="input" name="email" placeholder="Your email"/>
              <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
            </form>
            <div class="contact-box">
              <div class="item">
                <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
              </div>
              <div class="item">
                <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
@endsection