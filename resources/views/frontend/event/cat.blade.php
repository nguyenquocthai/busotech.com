@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
      <!-- BANNER PAGE-->
      <section class="banner banner-page embed-responsive" style="background-image: url(/public/img/upload/banners/{{$banner->avatar}});">
        <h1 class="caption">Nhà hàng</h1>
      </section>
      <!-- FOODS-->
      <section class="section food js-filter">
        <div class="filter-list" id="filter-list">
          <a href="/nha-hang" title="" class=" filter-item"><button class="rs-btn  js-filterTrigger active" type="button" data-target="">Tất cả</button></a>
          <?php foreach ($nhahang_cats as $key => $nhahang_cat): ?>
            <a href="/danh-muc-nha-hang/{{$nhahang_cat->slug}}" class="<?php if($nhahang_cat->id == $nhahang_cat_active->id) echo 'active' ?> filter-item"><button class="rs-btn js-filterTrigger" type="button" data-target="" >{{$nhahang_cat->title}}</button></a>
          <?php endforeach ?>
        </div>
        <div class="container food-list">
          <div class="row" id="list-prod">
              <?php foreach (@$nhahangs as $key => $nhahang): ?>
              <div class="col-sm-6 col-lg-4 food-item filterDiv {{@$nhahang->id_nhahang_cat}}">
                <div class="inner">
                  <figure class="img hasLink"><img src="/public/img/upload/nhahangs/{{@$nhahang->avatar}}" alt=""><a class="link" href="/nha-hang/{{@$nhahang->slug}}"></a></figure>
                  <div class="content"><a class="name link" href="/nha-hang/{{@$nhahang->slug}}">{{@$nhahang->title}}</a>
                    <div class="des"><span class="text">
                      <?php foreach ($nhahang_cats as $key => $nhahang_cat1): ?>
                      @if($nhahang->id_nhahang_cat == $nhahang_cat1->id)
                        {{$nhahang_cat1->title}}
                      @endif
                      <?php endforeach ?>
                    </span><span class="price">{{(@$nhahang->price)}} VNĐ</span></div>
                  </div>
                </div>
              </div>
              <?php endforeach ?>
          </div>
          @if($next_page != 0)
            <div class="text-center">
              <a class="rs-btn btn-read-more" data-id-cat="{{$nhahang_cat_active->id}}" data-page="{{@$page_current}}" id="loadMoreprodcat" href="javascript:;">Xem thêm</a>
            </div>
          @endif
        </div>
      </section>
    </main>
@endsection