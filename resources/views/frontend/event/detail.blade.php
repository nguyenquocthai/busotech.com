@extends('frontend.layouts.main')
@section('content')
<div class="row banner">
  <div class="slide">
    <div class="inner"><img src="/public/img/upload/events/{{$eventdt->avatar}}" width="1160" height="494" /></div>
    <div class="title">
        <h2>{{$eventdt->title}}</h2>
        <h4>{{$eventdt->summary}}</h4>
    </div>
  </div>
</div>
<div class="row">
        <div class="content small-12 medium-9 columns">
            <!-- <h2>{{$eventdt->title}}</h2>
            <p><i>{{$eventdt->summary}}</i></p> -->
            {!!$eventdt->content!!}
            <p><a href="/event" class="button secondary" style="margin-top:3rem;">
                    <<&nbsp;&nbsp;&nbsp;{{@$langs['tro-ve-trang-su-kien']}}</a>
            </p>
        </div>
        <div class="small-12 medium-3 columns sidebar">
            <div class="small-12 columns">
                <h4>{{@$langs['su-kien-sap-toi']}}</h4>
                <ul class="eventside">
                  <?php foreach ($events as $key => $event): ?>
                      <li>
                          <strong><a href="/event/{{$event->slug}}">{{$event->title}}</a></strong><br />
                      </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="small-12 columns featbox">
                <h4>{{@$langs['tin-moi-nhat']}}</h4>
                <ul class="eventide">
                    <?php foreach ($blogs as $key => $blog): ?>
                      
                        <li>
                          <strong><a href="/news/{{$blog->slug}}">{{$blog->title}}</a></strong><br />
                      </li>
                      
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>
@endsection