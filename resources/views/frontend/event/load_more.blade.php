<?php foreach (@$nhahangs as $key => $nhahang): ?>
  <div class="col-sm-6 col-lg-4 food-item filterDiv {{@$nhahang->id_nhahang_cat}} show">
    <div class="inner">
      <figure class="img hasLink"><img src="/public/img/upload/nhahangs/{{@$nhahang->avatar}}" alt=""><a class="link" href="/nha-hang/{{@$nhahang->slug}}"></a></figure>
      <div class="content"><a class="name link" href="/san-pham/{{@$nhahang->slug}}">{{@$nhahang->title}}</a>
        <div class="des"><span class="text">
          <?php foreach ($nhahang_cats as $key => $nhahang_cat1): ?>
          @if($nhahang->id_nhahang_cat == $nhahang_cat1->id)
            {{$nhahang_cat1->title}}
          @endif
          <?php endforeach ?>
        </span><span class="price">{{@$nhahang->price}} VNĐ</span></div>
      </div>
    </div>
  </div>
  <?php endforeach ?>