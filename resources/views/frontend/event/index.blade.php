@extends('frontend.layouts.main')
@section('content')
<div class="row banner">
  <div class="slide">
    <div class="inner"><img src="/public/img/upload/banners/{{$banner->avatar}}" width="1160" height="494" /></div>
    <div class="title">
        <h2>{{$banner->title}}</h2>
        <h4>{{$banner->summary}}</h4>
    </div>
  </div>
</div>
<!-- MAIN CONTENT-->
<div class="row">
        <div class="content small-12 medium-9 columns">
          <?php foreach ($events as $key => $event): ?>
            <div class="post">
                <h2 class="title"><a href="/event/{{$event->slug}}">{{$event->title}}</a></h2>
                <div class="entry">
                    <p>
                        <p>{{$event->summary}}</p>
                    </p>
                </div>
                <p class="postmeta">
                    <span class="links">
                        <a href="/event/{{$event->slug}}" class="readmore">{{@$langs['xem-them']}}</a>

                        | <span class="date">{{date('M d, Y', strtotime($event->updated_at))}}</span>
                    </span>
                </p>
            </div>
          <?php endforeach ?>
            
            <div class='paging pagination'>
                {{ $events->links() }}
            </div>


        </div>
        <div class="small-12 medium-3 columns sidebar">
            <div class="small-12 columns">
                <h4>{{@$langs['su-kien-sap-toi']}}</h4>
                <ul class="eventside">
                  <?php foreach ($events as $key => $event): ?>
                    <?php if ($key <5): ?>
                      <li>
                          <strong><a href="/event/{{$event->slug}}">{{$event->title}}</a></strong><br />
                      </li>
                    <?php endif ?>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="small-12 columns featbox">
                <h4>{{@$langs['tin-moi-nhat']}}</h4>
                <ul class="eventide">
                    <?php foreach ($blogs as $key => $blog): ?>
                      
                        <li>
                          <strong><a href="/news/{{$blog->slug}}">{{$blog->title}}</a></strong><br />
                      </li>
                      
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>
@endsection