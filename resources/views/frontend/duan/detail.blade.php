@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER SLIDER-->
  <section class="banner-main banner-title js-sliderBanner">
    @foreach (@$sliders as $slider)
    <div class="item embed-responsive" style="background-image: url(/public/img/upload/sliders/{{$slider->avatar}});">
          <div class="caption">
            <p class="title-page">Dự án</p>
            <h3 class="title">{{$slider->title}}</h3>
            <p>{{$slider->link}}</p>
          </div>
        </div>
    @endforeach
  </section>
  <!-- PROJECTS-->
  <section class="section project-index js-filterParent">
    <div class="container">
      <h2 class="title-lg">{!!@$duandt->title!!}</h2>
      <div class="">

        {!!@$duandt->content!!}
      </div>
  </section>
  <!-- RELATED PROJECTS-->
  <section class="section project-related partner bg_fa">
    <div class="container">
      <h2 class="title" style="text-align: center;">DỰ ÁN KHÁC</h2>
      <div class="row" style="justify-content: center;">
        @foreach (@$duans as $duankhac)
        <div class="col-6 col-md-4 col-xl-3 project-related__item">
          <div class="inner">
            <figure class="img hasLink embed-responsive"><img src="/public/img/upload/duans/{{$duankhac->avatar}}" alt=""><a class="link" href="/du-an/{{$duankhac->slug}}" title="{{$duankhac->title}}"></a></figure>
            <div class="content"><a class="name link" href="/du-an/{{$duankhac->slug}}" title="{{$duankhac->title}}">{{$duankhac->title}}</a></div>
          </div>
        </div>
        @endforeach
        
      </div>
    </div>
  </section>
</main>
@endsection