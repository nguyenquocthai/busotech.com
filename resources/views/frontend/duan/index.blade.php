@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER SLIDER-->
  <section class="banner-main banner-title js-sliderBanner">
    @foreach (@$sliders as $slider)
    <div class="item embed-responsive" style="background-image: url(/public/img/upload/sliders/{{$slider->avatar}});">
          <div class="caption">
            <p class="title-page">Projects</p>
            <h3 class="title">{{$slider->title}}</h3>
            <p>{{$slider->link}}</p>
          </div>
        </div>
    @endforeach
  </section>
  <!-- PROJECTS-->
  <section class="section project-index js-filterParent">
    <div class="container">
      <h2 class="title-main">{{$info_web["title_duan"]}}</h2>
      <p class="title-description">{{$info_web["summary_duan"]}}</p>
      <nav class="portfolio-nav js-filterNav">
        <ul class="rs-list list">
          <li class="item">
            <button class="rs-btn link js-filterTrigger active" type="button" data-filter="*">All</button>
          </li>
          @foreach (@$duan_cats as $duan_cat)
          <li class="item">
            <button class="rs-btn link js-filterTrigger" type="button" data-filter=".{{$duan_cat->id}}">{{$duan_cat->title}}</button>
          </li>
          @endforeach
          
        </ul>
      </nav>
      <div class="portfolio-list js-filterContent">
        @foreach(@$duans as $duan)
        <div class="item active {{$duan->id_duan_cat}}">
          <div class="inner hasLink"><a class="link" href="/du-an/{{$duan->slug}}" title="{{$duan->title}}"></a>
            <figure class="img embedresponsive mb-0"><img src="/public/img/upload/duans/{{$duan->avatar}}" alt="{{$duan->title}}"></figure>
            <h3 class="title">{{$duan->title}}</h3>
          </div>
        </div>
        @endforeach
        
      </div>
    </div>
  </section>
</main>
<style>
  .portfolio-list .item .img,.portfolio-list .item .inner {
    height: 100%;
  }
  .portfolio-list .item .img img{
    object-fit: cover;
    height: 100%;
    width: 100%;
  }
</style>
@endsection