@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER-->
  <section class="section banner-page style3 bg-gradient">
    <div class="container" style="align-items: center">
      <div class="title-block text-center">
        <h2 class="title-main"><span class="d-inline-block">Website service price list</span></h2>
      </div>
    </div>
  </section>
  <!-- HOSTING PRICE-->
  <section class="section hosting-price js-tabParent">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main">Price list</h2>
      </div>
    </div>
    <div class="tab-nav js-tabNav">
      <?php foreach ($giatenmien_cats as $key => $giatenmien_cat): ?>
        <button class="rs-btn item <?php if($key == 0) echo 'active' ?> js-tabLink" type="button" data-target="#tabHostingPrice{{$giatenmien_cat->id}}">{{$giatenmien_cat->title}}</button>
      <?php endforeach ?>
      
      
    </div>
    <div class="tab-content js-tabContent">
      <?php foreach ($giatenmien_cats as $key => $giatenmien_cat): ?>
      <div class="tab-panel fade <?php if($key == 0) echo 'active' ?>" id="tabHostingPrice{{$giatenmien_cat->id}}">
        <div class="container">
          <div class="row hosting-price__list">
            
            <?php $i=0; foreach ($giatenmiens as $key => $giatenmien): ?>
              <?php  if ($giatenmien_cat->id == $giatenmien->id_giatenmien_cat):$i++ ?>
              <div class="hosting-price__item animate-fadeUp <?php if($i == 2) echo 'active' ?>">
                <div class="inner">
                  <h3 class="title"><span class="text">{{$giatenmien->title}}&nbsp;</span><span class="price"><span>{{number_format($giatenmien->gia)}} VNĐ&nbsp;/&nbsp;</span><span>tháng</span></span><span class="note"><?php if($i == 2) echo '(Khuyên dùng)' ?></span>
                </h3>
                  <div class="content">
                    {!!$giatenmien->content!!}
                    <div class="ctrl"><a class="link-blue" href="#modalCompare{{$giatenmien_cat->id}}" data-toggle="modal">See more&nbsp;<i class="fa fa-angle-right"></i></a><br/><a class="rs-btn btn-gradient" href="/trial/website---sku046?priceID={{$giatenmien->id}}">Register</a></div>
                  </div>
                </div>
              </div>
              <?php endif ?>
            <?php endforeach ?>
            
          </div>
        </div>
      </div>
      <?php endforeach ?>
      
    </div>
  </section>
  <!-- SECTION SIX-->
  <section class="section section-six"><span class="circle circle--striped"></span>
    <div class="container"><span class="triangle triangle--light"></span>
      <div class="subscribe-box">
        <div class="title-block text-center">
          <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
          <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
        </div>
        <form class="subscribe-form" id="subscribe" action="">
          <input class="input" name="email" placeholder="Your email"/>
          <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
        </form>
        <div class="contact-box">
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
          </div>
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php foreach ($giatenmien_cats as $key => $giatenmien_cat): ?>
  <div class="modal modal-large fade" id="modalCompare{{$giatenmien_cat->id}}" role="dialog" tab-index="-1" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title">compare features of packages&nbsp;<span class="d-inline-block">of packages {{$giatenmien_cat->title}}</span></div>
          <div class="price-table__head">
            <?php $i=0; foreach ($giatenmiens as $key => $giatenmien): ?>
              <?php if ($giatenmien->id_giatenmien_cat == $giatenmien_cat->id): $i++; ?>
                <div class="item <?php if($i == 2) echo 'active' ?>"><span>{{$giatenmien->title}}</span><span class="price">{{number_format($giatenmien->gia)}}</span><span>VNĐ / tháng</span></div>
              <?php endif ?>
            <?php endforeach ?>
            
          </div>
          <button class="rs-btn modal-close" data-dismiss="modal" arial-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="price-table__body">
            <?php foreach ($website_feature_cats as $key => $website_feature_cat): ?>
            
            <div class="price-table__row title">
              <div class="item">{{$website_feature_cat->title}}</div>
            </div>
            
            <?php foreach ($website_features as $key => $website_feature): ?>
            <?php if ($website_feature->id_website_feature_cat == $website_feature_cat->id): ?>
            <div class="price-table__row">
              <div class="item">{{$website_feature->title}}<br>@if($website_feature->summary != null) ({{$website_feature->summary}}) @endif</div>
              <?php foreach ($giatenmiens as $key => $giatenmien): ?>
              <?php if ($giatenmien->id_giatenmien_cat == $giatenmien_cat->id): ?>
                
                <?php if ( in_array($website_feature->id, json_decode($giatenmien->id_feature))): ?>
                <div class="item" style="color: green;font-weight: 600">&#10003;</div>
                <?php else: ?>
                <div class="item" style="color: red;font-weight: 600">x</div>
                <?php endif ?>

              <?php endif ?>
              <?php endforeach ?>
            </div>
            <?php endif ?>
            <?php endforeach ?>
            <?php endforeach ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php endforeach ?>
  
</main>
@endsection