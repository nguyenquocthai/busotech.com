@extends('frontend.layouts.main')
@section('content')
<main class="main">
      <!-- BANNER SLIDER-->
      <section class="banner-main banner-title js-sliderBanner">
        @foreach (@$sliders as $slider)
        <div class="item embed-responsive" style="background-image: url(/public/img/upload/sliders/{{$slider->avatar}});">
          <div class="caption">
            <p class="title-page">Customers</p>
            <h3 class="title">{{$slider->title}}</h3>
          </div>
        </div>
        @endforeach
      </section>
      <!-- PARTNERS-->
      <section class="section partner">
        <div class="container">
          <div class="row" style="justify-content: center">
            @foreach($doitacs as $doitac)
            <div class="col-6 col-sm-4 col-lg-3 col-xl-2">
              <figure class="img hasLink embed-responsive"><img src="/public/img/upload/doitacs/{{$doitac->avatar}}" alt=""><a class="link" href="#!" title=""></a></figure>
            </div>
            @endforeach
            
          </div>
        </div>
      </section>
    </main>
@endsection