@extends('frontend.layouts.main')
@section('content')
<main class="main">
    <!-- BREADCRUMB-->
    <nav class="breadcrumbk">
        <div class="container">
            <ul class="reset-list breadcrumbk-list">
                <li class="item"><a class="link" href="/"><i class="zmdi zmdi-home mr-2"></i>Home</a></li>
                <li class="item active">Forgot password</li>
            </ul>
        </div>
    </nav>
    <!-- ACCOUNT-->
    <section class="section-spacing">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 col-xl-6 mb-3">
                    <div class="block">
                        <h3 class="block-title mb-3">Change Password</h3>
                        <form class="bg-white p-3 p-lg-4 form-login" id="formdoimatkhau" autocomplete="off">   
                            <input type="hidden" name="token_forget" value="{{$nguoidung->token_forget}}">
                            <div class="form-hor__row mb-3">
                                <div class="text"><span>Password </span><span class="cl_red">*</span></div>
                                <div class="input">
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                            </div>

                            <div class="form-hor__row mb-3">
                                <div class="text"><span>Re password </span><span class="cl_red">*</span></div>
                                <div class="input">
                                    <input type="password" name="repassword" class="form-control" id="repassword">
                                </div>
                            </div>

                            <div class="form-hor__row mb-3">
                                <div class="text d-none d-md-block">&nbsp;</div>
                                <div class="input">
                                    <button class="reset-btn btn-main w-100 fs_14" type="submit">Change password</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script>
    var formdoimatkhau = $('#formdoimatkhau').validate({
        highlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            
            password: {
                required: true,
                minlength : 3,
                maxlength: 15
            },
            repassword: {
              equalTo: "#password"
            }
        },
        messages: {
           
            password: {
                required: 'Password cannot be empty.',
                minlength: 'Please enter 3 - 15 characters',
                maxlength: 'Please enter 3 - 15 characters'

            },
            repassword: {
                equalTo: 'Password incorrect, please try again',
               

            },
        },
        submitHandler: function (form) {
            var data = {};
            $("#formdoimatkhau").serializeArray().map(function(x){data[x.name] = x.value;});
            $.ajax({
                type: "POST",
                url: '/set_pass',
                data: data,
                dataType: 'json',
                success: function(result) {
                    switch (result.code) {
                        case 200:
                            $('#formdoimatkhau').find("input, textarea").val("");
                            toastr.success("Change password successfully");
                            window.location.href = "/create-account";
                            break;
                        default:
                            toastr.error("Data error.");
                            break;
                    }
                }
            });
            return false;
        }
    });
</script>
@endsection