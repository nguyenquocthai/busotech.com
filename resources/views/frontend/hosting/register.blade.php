@extends('frontend.layouts.main') 
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <section class="login-section trial-section bg-gradient">
    <figure class="img"><img src="/public/theme/img/login-img.svg" alt=""/></figure>
    <div class="content hosting-register__content">
      <div class="progressbar js-progressBar"><span class="step active" data-value="0"></span></div>
      <div class="inner actived" id="step1">
        <div class="title-block text-center">
          <h1 class="title">Sign up for Web Hosting</h1>
        </div>
        <div class="hosting-register__info text-center">
          <p class="title">{{$hostingdt->title}}</p>
          <ul>
            {!!$hostingdt->content!!}
          </ul>
        </div>
        <form class="login-form pb-4 form-hosting-step1" action="#!">
          <p class="login-form__row mb-3 text-left"><span class="label">Time</span>
            <label class="selectbox input">
              <select name="time_host">
                <option value="1" selected>By 1 Years - {{number_format($hostingdt->gia)}} VNĐ</option>
                <option value="2">By 2 Years - {{number_format($hostingdt->gia*2)}} VNĐ</option>
                <option value="3">By 3 Years - {{number_format($hostingdt->gia*3)}} VNĐ</option>
                <option value="5">By 5 Years - {{number_format($hostingdt->gia*5)}} VNĐ</option>
                <option value="10">By 10 Years - {{number_format($hostingdt->gia*10)}} VNĐ</option>
              </select>
            </label>
          </p>
          <p class="login-form__row mb-3 text-left"><span class="label">Tên máy chủ</span>
            <input name="name_host" class="input" type="text" placeholder=""/>
            <input type="hidden" name="id_host" value="{{$hostingdt->id}}">
            <input type="hidden" name="price_host" value="{{$hostingdt->gia}}">
            <input type="hidden" name="ten_goi_host" value="{{$hostingdt->title}}">
          </p>
          <p class="login-form__row">
            <button class="rs-btn btn-gradient btn-next mt-3 " type="submit" data-target="">Countinue</button>
          </p>
        </form>
      </div>
      <div class="inner" id="step2">
        <div class="title-block text-center">
          <div class="title">Sign up for Web Hosting</div>
        </div>
        <p class="hosting-register__title">Order Information</p>
        <div class="hosting-register__table">
          <ul class="rs-list list list-3 list-head">
            <li class="item">Sumary</li>
            <li class="item">Expiry date</li>
            <li class="item">Price</li>
          </ul>
          <ul class="rs-list list list-3">
            <li class="item name-host">{{$hostingdt->title}}</li>
            <li class="item time-host">6 months</li>
            <li class="item price-host">204.000&nbsp;<span class="d-inline-block">VNĐ</span></li>
          </ul>
          <ul class="rs-list list list-2">
            <li class="item">VAT (10%)</li>
            <li class="item vat-host">20.400&nbsp;<span class="d-inline-block">VNĐ</span></li>
          </ul>
          <ul class="rs-list list list-2">
            <li class="item">Total price</li>
            <li class="item total-price-host">224.400&nbsp;<span class="d-inline-block">VNĐ</span></li>
          </ul>
        </div>
        <p class="hosting-register__title">Order Information</p>
        <form class="login-form hosting-register__form form-hosting-step2" action="#!">
          <div class="left">
            <p class="login-form__row">
              <input class="input" type="text" name="name" placeholder="Full name"/>
            </p>
            <p class="login-form__row">
              <input class="input" type="email" name="email" placeholder="Email"/>
            </p>
            <p class="login-form__row">
              <input class="input" type="number" name="tel" placeholder="Phone number"/>
            </p>
            <p class="login-form__row">
              <input class="input" type="password" name="password" id="password" placeholder="Password"/>
            </p>
            <p class="login-form__row">
              <input class="input" type="password" name="repassword" placeholder="Confirm password"/>
            </p>
          </div>
          <div class="right">
            <p class="login-form__row">
              <input class="input" type="text" name="address" placeholder="Address"/>
            </p>
            <p class="login-form__row">
              <label class="selectbox input">
                <select name="city">
                  <option value="">Province / City</option>
                  <?php foreach ($za_provinces as $key => $za_province): ?>
                    <option value="{{$za_province->name}}">{{$za_province->name}}</option>
                  <?php endforeach ?>
                </select>
              </label>
            </p>
            <p class="login-form__row">
              <input class="input" type="text" name="zip_code" placeholder="Zip code"/>
            </p>
            <p class="login-form__row">
              <input class="input" type="text" name="country" placeholder="Nation"/>
            </p>
            <p class="login-form__row">
              <input class="input" type="text" name="user" placeholder="Account"/>
            </p>
          </div>
          <div class="left mb-0">
            
            <input type="checkbox" name="check_invoice" id="Check_invoice" onclick="Function_invoice()">
            <label for="Check_invoice"> Invoice</label> 
            <script>
              function Function_invoice() {
                var checkBox = document.getElementById("Check_invoice");
                var text = document.getElementById("collapseHosting");
                if (checkBox.checked == true){
                  text.style.display = "block";
                  $('#Check_invoice').val(1);
                } else {
                   text.style.display = "none";
                   $('#Check_invoice').val(0);
                }
              }
              </script>
            <div class="collapse" id="collapseHosting">
              <p class="login-form__row">
                <input class="input" type="text" name="name_co" placeholder="Company name"/>
              </p>
              <p class="login-form__row">
                <input class="input" type="text" name="address_co" placeholder="Company address"/>
              </p>
              <p class="login-form__row">
                <input class="input" type="number" name="tax_code" placeholder="Tax code"/>
              </p>
            </div>
          </div>
          
          <div class="text-center pb-4" style="width: 100%">
            <button class="rs-btn btn-gradient btn-next mt-2 " type="submit" data-target="">Domain name registration</button>
          </div>
        </form>
        
      </div>
      
      <div class="inner" id="step3">
        <div class="title-block text-center">
          <div class="title">Congratulations on your successful registration</div>
        </div>
        <div class="hosting-register__table py-4">
          <ul class="rs-list list list-2">
            <li class="item justify-content-start tengoihost">Personal hosting - Host one</li>
            <li class="item giagoihost">204.000&nbsp;<span class="d-inline-block">VNĐ</span></li>
          </ul>
          <ul class="rs-list list list-2">
            <li class="item">VAT (10%)</li>
            <li class="item giagoihostvat">20.400&nbsp;<span class="d-inline-block">VNĐ</span></li>
          </ul>
          <ul class="rs-list list list-2">
            <li class="item">Total price</li>
            <li class="item thanhtiengoihost">224.400&nbsp;<span class="d-inline-block">VNĐ</span></li>
          </ul>
        </div>
        <div class="text-center sticky pt-4 pb-4">
          <button class="rs-btn btn-gradient btn-next js-tabLogin" type="button" data-target="#step3" onclick="location.href='/hosting'">Completed</button>
        </div>
      </div>
    </div>
  </section>
  
</main>
<script>
  var data_host = [];
  jQuery(document).ready(function($) {
    var formhostingstep1 = $('.form-hosting-step1').validate({
              highlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass);
              },
              rules: {
                  name_host:{
                      required: true,
                  }
              },
              messages: {
                  name_host:{
                      required: 'Please enter the server name.',
                  }
              },
              submitHandler: function (form) {
                  var data = {};
                  
                  $(".form-hosting-step1").serializeArray().map(function(x){data[x.name] = x.value;});
                  
                  data: {
                    _token : "{{ csrf_token() }}"
                    }
                  // console.log(data);
                 
                  data_host = data;

                  console.log(data_host);
                  
                  $('.form-hosting-step1').closest('.trial-section').find('#step2').addClass('actived');
                  $('.form-hosting-step1').closest('.trial-section').find('#step1').removeClass('actived');
                  $('.form-hosting-step1').closest('.trial-section').find('.progressbar').find('.step').attr('style', 'left: 33.333%');
                  $('.form-hosting-step1').closest('.trial-section').find('#step2').find('.time-host').html(data_host.time_host+' năm');
                  $('.form-hosting-step1').closest('.trial-section').find('#step2').find('.price-host').html(new Intl.NumberFormat().format(data_host.time_host*data_host.price_host)+' đ');
                  $('.form-hosting-step1').closest('.trial-section').find('#step2').find('.vat-host').html(new Intl.NumberFormat().format(data_host.time_host*data_host.price_host*0.1)+' đ');
                  $('.form-hosting-step1').closest('.trial-section').find('#step2').find('.total-price-host').html(new Intl.NumberFormat().format(data_host.time_host*data_host.price_host*0.1+data_host.time_host*data_host.price_host)+' đ');
                  // $.ajax({
                  //     type: 'POST',
                  //     url: '/add_contact_theme',
                  //     data: data,
                  //     dataType: 'json',
                  //     error: function(){
                          
                  //         toastr.error('Lỗi');
                  //     },
                  //     success: function(result) {
                  //         console.log(result);
                  //         switch (result.code) {
                  //             case 200:
                  //                 $('.form-hosting-step1').find('.cle').val("");
                  //                 $('.form-hosting-step1').closest('.trial-section').find('#step3').find('.tendangnhap').html(result.email);
                  //                 $('.form-hosting-step1').closest('.trial-section').find('#step3').find('.diachiweb').html(result.domain_name);
                  //                 toastr.success('Đăng ký thành công');
                  //                 $('.form-hosting-step1').closest('.trial-section').find('#step3').addClass('actived');
                  //                 $('.form-hosting-step1').closest('.trial-section').find('#step2').removeClass('actived');
                  //                 $('.form-hosting-step1').closest('.trial-section').find('.progressbar').find('.step').attr('style', 'left: 66.666%');
                  //                 break;
                  //               case 300:
                  //                 toastr.error(result.value);
                  //                 break;
                  //               default:
                  //                 toastr.error('Lỗi');
                  //                 break;
                  //         }
                  //     }
                  // });
                  return false;
              }
          });
    var formhostingstep2 = $('.form-hosting-step2').validate({
              highlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass);
              },
              rules: {

                  name: {
                     required: true,
                  },
                  address: {
                     required: true,
                  },
                  email: {
                     required: true,
                     email: true,
                  },
                  city: {
                     required: true,
                  },
                  tel: {
                     required: true,
                  },
                  zip_code: {
                     required: true,
                  },
                  country: {
                     required: true,
                  },
                  user: {
                     required: true,
                  },
                  password: {
                     required: true,
                     minlength : 8,
                  },
                  repassword: {
                    minlength : 8,
                    equalTo : "#password"
                  }
              },
              messages: {
                 
                  name: {
                      required: "Please enter full name",
                  },
                  address: {
                      required: "Please enter address",
                  },
                  email: {
                     required: "Please enter email",
                     email: "Your email is not in the correct format ",
                  },
                  city: {
                     required: "Please select Province / City",
                  },
                  tel: {
                     required: "Please enter phone number",
                  },
                  zip_code: {
                     required: "Please enter zip code",
                  },
                  country: {
                     required: "Please enter nation",
                  },
                  user: {
                     required: "Please enter user",
                  },
                  password: {
                     required: "Please enter password",
                     minlength : "Password must be at least 8 characters",
                  },
                  repassword: {
                    minlength : "Password must be at least 8 characters",
                    equalTo : "Import passwords do not match"
                  }
              },
              submitHandler: function (form) {
                  var data = {};
                  
                  $(".form-hosting-step2").serializeArray().map(function(x){data[x.name] = x.value;});
                  
                  data: {
                    _token : "{{ csrf_token() }}"
                    }

                  data = Object.assign(data, data_host);
                  // console.log(data);return false;
                  
                  $.ajax({
                      type: 'POST',
                      url: '/add_contact_hosting',
                      data: data,
                      dataType: 'json',
                      error: function(){
                          
                          toastr.error('Error');
                      },
                      success: function(result) {
                          console.log(result);
                          switch (result.code) {
                              case 200:
                                  $('.form-hosting-step2').find('.cle').val("");
                                  $('.form-hosting-step2').closest('.trial-section').find('#step3').find('.tengoihost').html(result.ten_goi_host);
                                  $('.form-hosting-step2').closest('.trial-section').find('#step3').find('.giagoihost').html(new Intl.NumberFormat().format(result.price_host*result.time_host)+' VNĐ');
                                  $('.form-hosting-step2').closest('.trial-section').find('#step3').find('.giagoihostvat').html(new Intl.NumberFormat().format(result.price_host*result.time_host*0.1)+' VNĐ');
                                  $('.form-hosting-step2').closest('.trial-section').find('#step3').find('.thanhtiengoihost').html(new Intl.NumberFormat().format(result.price_host*result.time_host+result.price_host*result.time_host*0.1)+' VNĐ');
                                  toastr.success('Đăng ký thành công');
                                  $('.form-hosting-step2').closest('.trial-section').find('#step3').addClass('actived');
                                  $('.form-hosting-step2').closest('.trial-section').find('#step2').removeClass('actived');
                                  $('.form-hosting-step2').closest('.trial-section').find('.progressbar').find('.step').attr('style', 'left: 66.666%');
                                  break;
                                case 300:
                                  toastr.error('Registration failed');
                                  break;
                                default:
                                  toastr.error('Error');
                                  break;
                          }
                      }
                  });
                  return false;
              }
          });
  });
</script>
@endsection
