@extends('frontend.layouts.main') 
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER-->
  <section class="section banner-page style3 bg-gradient">
    <div class="container" style="align-items: center;">
      <div class="title-block text-center">
        <h2 class="title-main"><span class="d-inline-block">Website Hosting</span></h2>
      </div>
    </div>
  </section>
  <!-- HOSTING INTRO-->
  <section class="section hosting-intro">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main">Why choose Buso?</h2>
      </div>
      <div class="row">
        <?php foreach ($hosting_advantages as $key => $hosting_advantage): ?>
          <div class="col-md-6">
            <h3 class="hosting-intro__item animate-fadeUp">{{$hosting_advantage->title}}</h3>
          </div>
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <!-- HOSTING PRICE-->
  <section class="section hosting-price js-tabParent">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main">Price list</h2>
      </div>
    </div>
    <div class="tab-nav js-tabNav">
      <?php foreach ($hosting_cats as $key => $hosting_cat): ?>
        <button class="rs-btn item <?php if($key == 0) echo 'active' ?> js-tabLink" type="button" data-target="#tabHostingPrice{{$hosting_cat->id}}">{{$hosting_cat->title}}</button>
      <?php endforeach ?>
      
      
    </div>
    <div class="tab-content js-tabContent">
      <?php foreach ($hosting_cats as $key => $hosting_cat): ?>
      <div class="tab-panel fade <?php if($key == 0) echo 'active' ?>" id="tabHostingPrice{{$hosting_cat->id}}">
        <div class="container">
          <div class="row hosting-price__list">
            
            <?php $i=0; foreach ($hostings as $key => $hosting): ?>
              <?php  if ($hosting_cat->id == $hosting->id_hosting_cat):$i++ ?>
              <div class="hosting-price__item animate-fadeUp <?php if($i == 2) echo 'active' ?>">
                <div class="inner">
                  <h3 class="title"><span class="text">{{$hosting->title}}&nbsp;</span><span class="price"><span>{{number_format($hosting->gia)}} VNĐ&nbsp;/&nbsp;</span><span>năm</span></span><span class="note"><?php if($i == 2) echo '(Khuyên dùng)' ?></span>
                </h3>
                  <div class="content">
                    {!!$hosting->content!!}
                    <div class="ctrl"><a class="link-blue" href="#modalCompare{{$hosting_cat->id}}" data-toggle="modal">See more&nbsp;<i class="fa fa-angle-right"></i></a><br/><a class="rs-btn btn-gradient" href="/hosting-register?hostingID={{$hosting->id}}">Register</a></div>
                  </div>
                </div>
              </div>
              <?php endif ?>
            <?php endforeach ?>
            
          </div>
        </div>
      </div>
      <?php endforeach ?>
      
    </div>
  </section>
  <!-- HOSTING FEATURES-->
  <section class="section hosting-feature">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main">Features</h2>
      </div>
      <div class="row hosting-feature__list">
        <?php foreach ($hosting_features as $key => $hosting_feature): ?>
          <?php if ($key < 10): ?>
          <div class="hosting-feature__item animate-fadeUp">
            <div class="inner">
              <figure class="img"><img src="/public/img/upload/hosting_features/{{$hosting_feature->avatar}}" alt="{{$hosting_feature->title}}"/></figure>
              <h3 class="title">{{$hosting_feature->title}}</h3>
            </div>
          </div>
          <?php endif; ?>
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <!-- SECTION SIX-->
  <section class="section section-six"><span class="circle circle--striped"></span>
    <div class="container"><span class="triangle triangle--light"></span>
      <div class="subscribe-box">
        <div class="title-block text-center">
          <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
          <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
        </div>
        <form class="subscribe-form" id="subscribe" action="">
          <input class="input" name="email" placeholder="Your email"/>
          <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
        </form>
        <div class="contact-box">
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
          </div>
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php foreach ($hosting_cats as $key => $hosting_cat): ?>
  <div class="modal modal-large fade" id="modalCompare{{$hosting_cat->id}}" role="dialog" tab-index="-1" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title">Compare features&nbsp;<span class="d-inline-block">of packages {{$hosting_cat->title}}</span></div>
          <div class="price-table__head">
            <?php $i=0; foreach ($hostings as $key => $hosting): ?>
              <?php if ($hosting->id_hosting_cat == $hosting_cat->id): $i++; ?>
                <div class="item <?php if($i == 2) echo 'active' ?>"><span>{{$hosting->title}}</span><span class="price">{{number_format($hosting->gia)}}</span><span>VNĐ / tháng</span></div>
              <?php endif ?>
            <?php endforeach ?>
            
          </div>
          <button class="rs-btn modal-close" data-dismiss="modal" arial-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="price-table__body">
            <?php foreach ($hosting_feature_cats as $key => $hosting_feature_cat): ?>
            
            <div class="price-table__row title">
              <div class="item">{{$hosting_feature_cat->title}}</div>
            </div>
            
            <?php foreach ($hosting_features as $key => $hosting_feature): ?>
            <?php if ($hosting_feature->id_hosting_feature_cat == $hosting_feature_cat->id): ?>
            <div class="price-table__row">
              <div class="item">{{$hosting_feature->title}}<br>@if($hosting_feature->summary != null) ({{$hosting_feature->summary}}) @endif</div>
              <?php foreach ($hostings as $key => $hosting): ?>
              <?php if ($hosting->id_hosting_cat == $hosting_cat->id): ?>
                
                <?php if ( in_array($hosting_feature->id, json_decode($hosting->id_feature_hosting))): ?>
                <div class="item" style="color: green;font-weight: 600">&#10003;</div>
                <?php else: ?>
                <div class="item" style="color: red;font-weight: 600">x</div>
                <?php endif ?>

              <?php endif ?>
              <?php endforeach ?>
            </div>
            <?php endif ?>
            <?php endforeach ?>
            <?php endforeach ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php endforeach ?>
</main>

@endsection
