
<!-- Modal -->
<div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Go to Cart</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button onClick="window.location.reload()" type="button" class="btn btn-secondary" data-dismiss="modal">Countinue Shopping</button>
        <a href="/gio-hang"><button type="button" class="btn btn-primary" style="border-color: #744d27;background-color: #744d27">OK</button></a>
      </div>
    </div>
  </div>
</div>
<script>
    jQuery(document).ready(function($) {
        $('.count_cart').html('{{$count_carts}}')
    });

    // add_cart
	$(document).on('click', '.add_cart', function(event) {
	    var id = $(this).attr('data-id');
	    var soluong = $(this).closest('.form-muahang').find('.quantity-val').val();
	    var data = {};
	    data['id'] = id;
	    data['soluong'] = soluong;
	    //$('.block-page-all').addClass('active');
	    $.ajax({
	        type: 'POST',
	        url: '/add_cart',
	        data: data,
	        dataType: 'json',
	        error: function(){
	            //$('.block-page-all').removeClass('active');
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                //$('.block-page-all').removeClass('active');
	                return false
	            }
	            $('#modalProduct .product-table').html(result.viewcart);
	            $('.count_cart').html(result.count);
	            $('#modalProduct').modal('show');
	            //$('.block-page-all').removeClass('active');
	        }
	    });
	});

	// add_cart2
	$(document).on('click', '.add_cart2', function(event) {
	    var id = $(this).attr('data-id');
	    var soluong = $(this).closest('.content').find('.quantity-val').val();
	    var data = {};
	    data['id'] = id;
	    data['soluong'] = soluong;
	    //$('.block-page-all').addClass('active');
	    $.ajax({
	        type: 'POST',
	        url: '/add_cart',
	        data: data,
	        dataType: 'json',
	        error: function(){
	            //$('.block-page-all').removeClass('active');
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                //$('.block-page-all').removeClass('active');
	                return false
	            }
	            $('#modalProduct .product-table').html(result.viewcart);
	            $('.count_cart').html(result.count);
	            $('#modalProduct').modal('show');
	            //$('.block-page-all').removeClass('active');
	        }
	    });
	});
	function addCommas(nStr)
	{
	    nStr += '';
	    x = nStr.split('.');
	    x1 = x[0];
	    x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + ',' + '$2');
	    }
	    return x1 + x2;
	}
	// update_cart
	$(document).on('click', '.update_cart', function(event) {
	    var id = $(this).attr('data-id');
	    var soluong = $(this).closest('.quantity').find('.quantity-val').val();
	    var price = $(this).closest('.content').find('.price-sp').val();
	    var thanhtien = addCommas(soluong * price);
		$(this).closest('.content').find('.thanhtien').html(thanhtien);
	    var data = {};
	    data['id'] = id;
	    data['soluong'] = soluong;
	    data['price'] = price;
	    console.log(data);
	    $.ajax({
	        type: 'POST',
	        url: '/update_cart',
	        data: data,
	        dataType: 'json',
	        error: function(){
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                return false
	            }
	            //location.reload();
	        }
	    });
	});

	

	// remove_cart
	$(document).on('click', '.remove_cart', function(event) {
	    var here = $(this).closest('.product-table-parent');
	    var id = $(this).attr('data-id');
	    var data = {};
	    data['id'] = id;
	    //$('.block-page-all').addClass('active');
	    $.ajax({
	        type: 'POST',
	        url: '/remove_cart',
	        data: data,
	        dataType: 'json',
	        error: function(){
	            //$('.block-page-all').removeClass('active');
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                //$('.block-page-all').removeClass('active');
	                return false
	            }
	            here.remove();
	            $('.count_cart').html(result.count);
	            //$('.block-page-all').removeClass('active');
	        }
	    });
	});

	// remove_cart_all
	$(document).on('click', '.remove_cart_all', function(event) {
	    var here = $('.product-table .product-table__row');
	    var data = {};
	    data['status_code'] = 'all';
	    $('.block-page-all').addClass('active');
	    $.ajax({
	        type: 'POST',
	        url: '/remove_cart',
	        data: data,
	        dataType: 'json',
	        error: function(){
	            $('.block-page-all').removeClass('active');
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                $('.block-page-all').removeClass('active');
	                return false
	            }
	            here.remove();
	            $('.count_cart').html(result.count);
	            $('.block-page-all').removeClass('active');
	        }
	    });
	});

	// show_cart
	$(document).on('click', '.show_cart', function(event) {
	    $('.block-page-all').addClass('active');
	    $.ajax({
	        type: 'GET',
	        url: '/show_cart',
	        error: function(){
	            $('.block-page-all').removeClass('active');
	            toastr.error(result.error);
	        },
	        success: function(result) {
	            if (result.code == 300) {
	                toastr.error(result.error);
	                $('.block-page-all').removeClass('active');
	                return false
	            }
	            $('#modalProduct .product-table').html(result.viewcart);
	            $('.count_cart').html(result.count);
	            $('#modalProduct').modal('show');
	            $('.block-page-all').removeClass('active');
	        }
	    });
	});

    var send_cart = $('#send_cart').validate({
        highlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass);
        },
        rules: {
            email:{
                required: true,
                email: true
            },
            name:{
                required: true
            },
            phone:{
                required: true,
                number: true
            }
        },
        messages: {
            email:{
                required: "Please enter your email.",
                email: "Your email must be a valid email."
            },
            name:{
                required: "Please enter your name."
            },
            phone:{
                required: "Please enter your phone number.",
                number: "Phone number must be number."
            }
        },
        submitHandler: function (form) {
    
            var data = {};
            $("#send_cart").serializeArray().map(function(x){data[x.name] = x.value;});
           
            $('.block-page-all').addClass('active');
    
            $.ajax({
                type: 'POST',
                url: '/send_cart',
                data: data,
                dataType: 'json',
                error: function(){
                    $('.block-page-all').removeClass('active');
                    toastr.error(result.error);
                },
                success: function(result) {
                    if (result.code == 300) {
                        toastr.error(result.error);
                        $('.block-page-all').removeClass('active');
                        return false
                    }
                    $('#modalProduct').modal('hide');
                    toastr.success(result.message);
                    $('.count_cart').html('0');
                    $('.block-page-all').removeClass('active');
                }
            });
            return false;
        }
    });
</script>