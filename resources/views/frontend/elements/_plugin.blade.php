<!-- main -->
<link rel="stylesheet" href="/public/theme/vendor/bootstrap-4/bootstrap.min.css"/>
<link rel="stylesheet" href="/public/theme/vendor/font-awesome-4.7.0/css/font-awesome.min.css"/>
<link rel="stylesheet" href="/public/theme/vendor/js/jquery-ui.min.css"/>
<link rel="stylesheet" href="/public/theme/vendor/slick-1.8.1/slick.css"/>
<link id="main-css" rel="stylesheet" href="/public/theme/css/main.css?v=1.0.8"/>

<script src="/public/theme/vendor/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>

<!-- validate -->
<script src="/public/vendor/jquery-validation-1.16.0/jquery.validate.min.js"></script>
<script src="/public/vendor/jquery-validation-1.16.0/additional-methods.min.js"></script>
<!-- /validate -->

<!-- toastr -->
<link href="/public/assets/global/plugins/toastr/toastr.min.css" rel="stylesheet">
<script src="/public/assets/global/plugins/toastr/toastr.min.js"></script>
<!-- /toastr -->

<!-- datepicker -->
<link href="/public/vendor/air-datepicker-master/dist/css/datepicker.min.css" rel="stylesheet">
<script src="/public/vendor/air-datepicker-master/dist/js/datepicker.min.js"></script>
<script src="/public/vendor/air-datepicker-master/dist/js/i18n/datepicker.en.js"></script>
<!-- /datepicker -->

<!-- DataTables -->
<link href="/public/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" />
<link href="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" />
<script src="/public/assets/global/scripts/datatable.js"></script>
<script src="/public/assets/global/plugins/datatables/datatables.min.js" ></script>
<script src="/public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
<!-- /DataTables -->

<!-- select2 -->
<link href="/public/assets/global/plugins/select2-4.0.3/css/select2.min.css" rel="stylesheet">
<link href="/public/assets/global/plugins/select2-4.0.3/css/select2-bootstrap.css" rel="stylesheet">
<script src="/public/assets/global/plugins/select2-4.0.3/js/select2.full.min.js"></script>
<script src="/public/assets/global/plugins/select2-4.0.3/js/i18n/en.js"></script>
<!-- /select2 -->

<!-- profile -->
<link rel="stylesheet" href="/public/css/profile/style.css">
<link rel="stylesheet" href="/public/css/profile/custom.css">



	

	
