<!-- meta seo -->
<title>{{isset($seopage->meta_title) ? $seopage->meta_title : @$info_web['name']}}</title>
<meta name="description" content="{{@$seopage->meta_description}}">
<meta property="og:url" content="{{@$seopage->linkpage}}" />
<meta property="og:type" content="website"/>
<meta property="og:title" content="{{@$seopage->meta_title}}" />
<meta property="og:description" content="{{@$seopage->meta_description}}" />
<meta property="og:image" content="{{@$seopage->image}}" />
<meta property="og:image:type" content="image/jpeg/png" />
<meta property="og:image:width" content="400" />
<meta property="og:image:height" content="300" />
<meta property="og:image:alt" content="{{@$seopage->meta_title}}" />
<meta property="fb:app_id" content="2810046865917540" />
<!-- /meta seo -->
