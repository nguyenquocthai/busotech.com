<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,viewport-fit=cover">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="icon" type="image/png" sizes="92x92" href="/public/img/upload/settings/{{@$info_web['favicon']}}">
<link rel="canonical" href="http://buso.asia"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="revisit-after" content="1 day"/>
<meta name="HandheldFriendly" content="true"/>
<link rel="apple-touch-icon-precomposed" href="/public/img/upload/settings/{{@$info_web['favicon']}}" type="image/x-icon" sizes="92x92"/>
<meta name="robots" content="noodp,index,follow"/>
<meta http-equiv="content-language" content="vi"/>

@include('frontend.elements._seo')
@include('frontend.elements._plugin')
@include('frontend.elements._loading')
{!!@$info_web['google_analytics']!!}
{!!@$info_web['facebook_pixel']!!}
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=1808055386163320&autoLogAppEvents=1"></script>
<!-- <div class="zalo-chat-widget" data-oaid="1570087441318660269" data-welcome-message="Buso Branding Desing thúc đẩy thương hiệu của bạn chạm nhanh đến khách hàng" data-autopopup="0" data-width="350" data-height="420"></div>

<script src="https://sp.zalo.me/plugins/sdk.js"></script> -->