<!-- FOOTER-->
<footer class="footer">
  <div class="footer-top container">
    <div class="row">
      <div class="col-xl-3 footer-top__item">
        <figure class="footer-logo hasLink"><img src="/public/img/upload/settings/{{@$info_web['logo_white']}}" alt=""/><a class="link" href="/"></a></figure>
      </div>
      <div class="col-xl-3 footer-top__item">
        <h2 class="title">{{@$info_web['name']}}</h2>
      </div>
      <div class="col-xl-3 footer-top__item">
        <address class="address">{{@$info_web['address']}}</address>
      </div>
      <div class="col-xl-3 footer-top__item"><span class="social-label">Connect with Buso</span>
        <ul class="rs-list social">
          <li class="item"><a class="link" href="{{@$info_web['facebook']}}" title="Fanpage Facebook"><i class="fa fa-facebook"></i></a></li>
          <li class="item"><a class="link" href="{{@$info_web['instagram']}}" title="Instagram"><i class="fa fa-instagram"></i></a></li>
          <li class="item"><a class="link" href="{{@$info_web['skype']}}" title="Skype"><i class="fa fa-skype"></i></a></li>
          <li class="item"><a class="link" href="{{@$info_web['dribbble']}}" title="Tribbble"><i class="fa fa-dribbble"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-bot container">
    <div class="row">
      <div class="col-lg-3">
        <h3 class="footer-title">
          <button class="rs-btn collapse-button collapsed" data-toggle="collapse" data-target="#collapseFooterMenu1">Policy</button>
        </h3>
        <div class="footer-menu collapse" id="collapseFooterMenu1">
          <ul class="rs-list list">
            <?php foreach ($policies_footers as $key => $policies_footer): ?>
              <li class="item"><a class="link" href="/chinh-sach/{{$policies_footer->slug}}">{{$policies_footer->title}}</a></li>
            <?php endforeach ?>
            <li class="item"><a class="link" href="/dai-ly-va-doi-tac">Policy for Agents & Partners</a></li>
            <li class="item"><a class="link" href="/huong-dan-thanh-toan">Payment Guide</a></li>
            <!-- <li class="item"><a class="link" href="/chinh-sach/chinh-sach-bao-mat">Chính sách bảo mật</a></li>
            <li class="item"><a class="link" href="#!">Chính sách cho đại lý và đối tác</a></li>
            <li class="item"><a class="link" href="#!">Chính sách cho khách hàng</a></li>
            <li class="item"><a class="link" href="#!">Hướng dẫn thanh toán</a></li> -->
          </ul>
        </div>
      </div>
      <div class="col-lg-3">
        <h3 class="footer-title">
          <button class="rs-btn collapse-button collapsed" data-toggle="collapse" data-target="#collapseFooterMenu2">Service</button>
        </h3>
        <div class="footer-menu collapse" id="collapseFooterMenu2">
          <ul class="rs-list list">
            <?php foreach ($service_footers as $key => $service_footer): ?>
              <li class="item"><a class="link" href="{{$service_footer->link}}">{{$service_footer->title}}</a></li>
            <?php endforeach ?>
          </ul>
        </div>
      </div>
      <div class="col-lg-3">
        <h3 class="footer-title">
          <button class="rs-btn collapse-button collapsed" data-toggle="collapse" data-target="#collapseFooterMenu3">Infomation</button>
        </h3>
        <div class="footer-menu collapse" id="collapseFooterMenu3">
          <ul class="rs-list list">
            <li class="item"><a class="link" href="/tin-tuc">Information About Services & Products</a></li>
            <li class="item"><a class="link" href="/busoweb">Buso's Library</a></li>
            <li class="item"><a class="link" href="/ho-tro">Service Guide</a></li>
            <li class="item"><a class="link" href="/tuyen-dung">Employment Information</a></li>
            <li class="item"><a class="link" href="/ho-tro">Support Information</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-3">
        <h3 class="footer-title">
          <button class="rs-btn collapse-button collapsed" data-toggle="collapse" data-target="#collapseFooterMenu4">About Busotech.com</button>
        </h3>
        <div class="footer-menu collapse" id="collapseFooterMenu4">
          <ul class="rs-list list">
            <li class="item"><a class="link" href="/gioi-thieu">Projects</a></li>
            <li class="item"><a class="link" href="/nhan-su">Personnel</a></li>
            <li class="item"><a class="link" href="/lien-he">Contact Information</a></li>
          </ul>
        </div><a class="link bct" href="#!"><img src="/public/theme/img/da-dang-ky-bo-cong-thuong.png" alt=""/></a>
      </div>
    </div>
  </div>
  <div class="copyright container">&nbsp;{{@$info_web['copyright']}} |</span><span class="d-inline-block">&nbsp;{{@$info_web['name']}} |</span><span class="d-inline-block">&nbsp;MST: {{@$info_web['mst']}}</span></div>
</footer>
<script>
  jQuery(document).ready(function($) {
    var subscribeform = $('#subscribe').validate({
      highlight: function(element, errorClass, validClass) {
          $(element).removeClass(errorClass);
      },
      rules: {
        email:{
            required: true,
            email:true
        }
      },
      messages: {
        email:{
            required: 'Please enter your email.',
            email: 'Your email must be a valid email.',
        }
      },
      submitHandler: function (form) {
        var data = {};
        $("#subscribe").serializeArray().map(function(x){data[x.name] = x.value;});
        
        $.ajax({
            type: 'POST',
            url: '/add_subscribe',
            data: data,
            dataType: 'json',
            error: function(){
                
                toastr.error('Error');
            },
            success: function(result) {
                
                switch (result.code) {
                    case 200:
                        $('#subscribe').find("input, textarea").val("");
                        toastr.success('Contact sent successfully, we will connect with you soon!');
                        break;
                    default:
                        toastr.error('Error');
                        break;
                }
            }
        });
        return false;
    }
    });
  });
</script>

