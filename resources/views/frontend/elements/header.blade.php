<!-- HEADER-->
<header class="header">
  <div class="container header-container">
    <h1 class="header-logo img hasLink"><img src="/public/img/upload/settings/{{@$info_web['logo']}}" alt=""/><a class="link" href="/" title="{{@$info_web['name']}}">{{@$info_web['name']}}</a></h1>
    <nav class="navigation js-nav">
      <button class="rs-btn navigation-close" type="button">MENU</button>
      <div class="navigation-wrap js-blurOff">
        <ul class="rs-list navigation-list">
          <li class="item"><a class="link" href="/giao-dien">template</a></li>
          <li class="item"><a class="link" href="/busoweb">service</a></li>
          <!-- <li class="item drop"><a class="link collapsed" href="#!" data-toggle="collapse" data-target="#collapseNavDrop1">Dịch vụ</a>
            <ul class="rs-list drop-list collapse" id="collapseNavDrop1">
              <?php foreach ($service_footers as $key => $service_footer): ?>
              <li class="item"><a class="link" href="{{$service_footer->link}}">{{$service_footer->title}}</a></li>
              <?php endforeach ?>
            </ul>
          </li> -->
          
          <li class="item drop"><a class="link collapsed" href="#!" data-toggle="collapse" data-target="#collapseNavDrop1">explore</a>
            <ul class="rs-list drop-list collapse" id="collapseNavDrop1">

              <li class="item"><a class="link" href="/gioi-thieu">about us</a></li>
              <li class="item"><a class="link" href="/bang-gia">pricing</a></li>
              <!-- <li class="item"><a class="link" href="/hosting">Bảng giá hosting</a></li> -->
              <li class="item"><a class="link" href="/tuyen-dung">recruitment</a></li>
              <!-- <li class="item"><a class="link" href="/tin-tuc">Tin tức</a></li> -->
              <li class="item"><a class="link" href="/thu-vien">library</a></li>
            </ul>
          </li>
          <!-- <li class="item"><a class="link" href="/ho-tro">Hỗ trợ</a></li> -->
        </ul>
      </div>
    </nav><a class="rs-btn btn-gradient" href="/giao-dien">DEMO</a>
    <button class="rs-btn navigation-open js-navOpen" type="button"><i class="fa fa-bars"></i></button>
  </div>
</header>