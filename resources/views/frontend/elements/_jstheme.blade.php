<!-- js theme -->
<script src="/public/theme/vendor/js/popper.min.js"></script>
<script src="/public/theme/vendor/bootstrap-4/bootstrap.min.js"></script>
<script src="/public/theme/vendor/slick-1.8.1/slick.min.js"></script>
<script src="/public/theme/vendor/js/typed.js"></script>
<script id="main-js" src="/public/theme/js/main.min.js?v=1.0.8"></script>
<script src="/public/theme/vendor/js/jquery-ui.min.js"></script>