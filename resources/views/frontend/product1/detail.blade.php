@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- -->
      <section class="section">
        <div class="container">
          <nav class="breadcrumbk">
            <ul class="rs-list breadcrumbk-list">
              <li class="item"><a class="link" href="/">Trang chủ</a></li>
              <li class="item"><a class="link" href="/san-pham-chong-chay">Sản phẩm chống cháy</a></li>
              <li class="item active">{{$product1dt->title}}</li>
            </ul>
          </nav>
          <style>
            .product-sync-lg img{
              margin: auto;
            }
            .sp-lienquan{
              justify-content: center;
            }
            .product-sync-thumb .img:before {
			    padding-top: 100%;
			}
          </style>
          <div class="row justify-content-between">
            <div class="col-lg-6 col-xl-5 mb-4">
              <div class="product-sync-lg js-sliderSync1">
                <div class="item">
                  <figure class="img embed-responsive"><img src="/public/img/upload/product1s/{{$product1dt->avatar}}" alt=""></figure>
                </div>
                @foreach($item_albums as $item_album)
                <div class="item">
                  <figure class="img embed-responsive"><img src="/public/img/upload/item_albums/{{$item_album->name}}" alt=""></figure>
                </div>
                @endforeach
                
              </div>
              <div class="product-sync-thumb js-sliderSync2">
                <div class="item">
                  <figure class="img embed-responsive"  style="margin: 0;"><img src="/public/img/upload/product1s/{{$product1dt->avatar}}" alt=""></figure>
                </div>
                @foreach($item_albums as $item_album)
                <div class="item">
                  <figure class="img embed-responsive" style="margin: 0;"><img src="/public/img/upload/item_albums/{{$item_album->name}}" alt=""></figure>
                </div>
                @endforeach
                
              </div>
            </div>
            <div class="col-lg-6">
              <div class="product-detail">
                <h3 class="title-sm">{{$product1dt->title}}</h3>
                {!!$product1dt->summary!!}
                <div class="row">
                  <div class="col-md-6">
                    <h4 class="title">Kích thước</h4>
                    {!!$product1dt->kichthuoc!!}
                  </div>
                  <div class="col-md-6">
                    <h4 class="title">Chất liệu</h4>
                    {!!$product1dt->chatlieu!!}
                  </div>
                  <div class="col-md-6">
                    <h4 class="title">Liên hệ đặt hàng vui lòng gọi:</h4>
                    <p>Điện thoại:&nbsp;<a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a></p>
                    <p>HOTLINE:&nbsp;<a class="link" href="tel:{{$info_web['phone2']}}">{{$info_web['phone2']}}</a></p>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="">
        <div class="container">
          {!!$product1dt->content!!}
        </div>
      </section>
      <!-- RELATED PRODUCTS-->
      <section class="section product-related">
        <div class="container">
          <h2 class="title-md text-center">SẢN PHẨM LIÊN QUAN</h2>
          <div class="portfolio-list sp-lienquan style2">
            @foreach($product1s as $prod)
            <div class="item active decoration">
              <div class="inner hasLink"><a class="link" href="/san-pham-chong-chay/{{$prod->slug}}" title="{{$prod->title}}"></a>
                <figure class="img embed-responsive"><img src="/public/img/upload/product1s/{{$prod->avatar}}" alt="{{$prod->title}}"></figure>
                <h3 class="title fb">{{$prod->title}}</h3>
                <div class="rating without-caption text-center">
                  <input class="d-none kv-fa js-ratingStars rating-loading" type="text" readonly="readonly" value="{{$prod->sosao}}">
                </div>
              </div>
            </div>
            @endforeach
            
          </div>
        </div>
      </section>
    </main>
@endsection