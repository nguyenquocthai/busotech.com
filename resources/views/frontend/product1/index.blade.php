@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER SLIDER-->
  <section class="banner-main banner-title js-sliderBanner">
    @foreach (@$slider_prods as $slider_prod)
    <div class="item embed-responsive" style="background-image: url(/public/img/upload/sliders/{{$slider_prod->avatar}});">
      <div class="caption">
        <p class="title-page">Sản phẩm chống cháy</p>
        <h3 class="title">{{$slider_prod->title}}</h3>
        <p>{{$slider_prod->link}}</p>
      </div>
    </div>
    @endforeach
  </section>
  <!-- PROJECTS-->
  <section class="section js-filterParent">
    <div class="container">
      <h3 class="title-main" style="font-weight: 600;margin-bottom: 5px;">{{$intro->title}}</h3>
      <p class="title-description" style="margin-top: 1px;margin-bottom: 1px;">{{$intro->summary}}</p>
      <div style="text-align: center">{!!$intro->content!!}</div>
      
      <h3 class="title-main" style="font-weight: 600">CÁC SẢN PHẨM CHỐNG CHÁY</h3>
      
      
      <div class="portfolio-list js-filterContent style2">
        @foreach($product1s as $product)
        <div class="item active {{$product->id_product_cat}}">
          <div class="inner hasLink"><a class="link" href="/san-pham-chong-chay/{{$product->slug}}" title="{{$product->title}}"></a>
            <figure class="img embed-responsive"><img src="/public/img/upload/product1s/{{$product->avatar}}" alt="{{$product->title}}"></figure>
            <h3 class="title fb">{{$product->title}}</h3>
            <div class="rating without-caption text-center">
              <input class="d-none kv-fa js-ratingStars rating-loading" type="text" readonly="readonly" value="{{$product->sosao}}">
            </div>
          </div>
        </div>
        @endforeach
        
      </div>
    </div>
  </section>
</main>
@endsection