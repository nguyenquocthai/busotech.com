@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER SLIDER-->
  <section class="banner-main banner-title js-sliderBanner">
    <div class="item embed-responsive" style="background-image: url(/public/theme/img/bg-banner-service.png);">
      <div class="caption">
        <p class="title-page">Nội thất rời</p>
        <h3 class="title">Các sản phẩm nội thất của AMAVI</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi fuga molestias laborum dignissimos unde alias voluptatibus amet, dolorem magni sed laudantium, ut magnam aut illo, earum assumenda optio animi.</p>
      </div>
    </div>
  </section>
  <!-- PROJECTS-->
  <section class="section js-filterParent">
    <div class="container">
      <h2 class="title-main">CÁC SẢN PHẨM NỘI THẤT</h2>
      <p class="title-description">Các thiết kế đã được tối ưu hóa cho bạn dễ sử dụng</p>
      
      <div class="portfolio-list js-filterContent style2">
        @foreach($products as $product)
        <div class="item active {{$product->id_product_cat}}">
          <div class="inner hasLink"><a class="link" href="/san-pham/{{$product->slug}}" title="{{$product->title}}"></a>
            <figure class="img embed-responsive"><img src="/public/img/upload/products/{{$product->avatar}}" alt="{{$product->title}}"></figure>
            <h3 class="title fb">{{$product->title}}</h3>
            <div class="rating without-caption text-center">
              <input class="d-none kv-fa js-ratingStars rating-loading" type="text" readonly="readonly" value="{{$product->sosao}}">
            </div>
          </div>
        </div>
        @endforeach
        
      </div>
    </div>
  </section>
</main>
@endsection