@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- BANNER PAGE SLIDER-->
  <section class="section banner-page-slider">
    <div class="js-sliderBannerPageSlider">
      <?php foreach ($sliders as $key => $slider): ?>
        <div class="item"><img src="/public/img/upload/sliders/{{$slider->avatar}}" alt=""/>
          <div class="container">
            <div class="inner">
              <h2 class="title"><span class="d-block">{{$slider->title}}</span></h2><a class="rs-btn btn-gradient" href="{{$slider->link}}">View</a>
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>
  </section>
  <!-- NEWS CONTAINER-->
  <section class="section news-library">
    <div class="container">
      <div class="title-block text-center">
        <h2 class="title-main"><span class="d-inline-block">Information pages&nbsp;</span><span class="d-inline-block">& Libraries</span></h2>
      </div>
    </div>
    <div class="container news-container">
      <div class="row">
        <div class="col-lg-8">
          <div class="news-item-lg">
            <figure class="img embed-responsive hasLink"><img src="/public/img/upload/librarys/{{$librarynb->avatar}}" alt=""/><a class="link" href="/thu-vien/{{$librarynb->slug}}" title="{{$librarynb->title}}"></a></figure>
            <div class="content">
              <h3 class="title"><a class="link" href="/thu-vien/{{$librarynb->slug}}" title="{{$librarynb->title}}">{{$librarynb->title}}</a></h3>
              <ul class="rs-list cate">
                <?php foreach ($library_cats as $key => $library_cat): ?>
                  @if($library_cat->id == $librarynb->id_library_cat)
                  <li class="item"><a class="link" href="/danh-muc-thu-vien/{{$library_cat->slug}}">{{$library_cat->title}}</a></li>
                  @endif
                <?php endforeach ?>
                
                
              </ul>
              <div class="summary">
                <p>{{$librarynb->summary}}</p>
              </div><a class="rs-btn btn-animated btn-gradient" href="/thu-vien/{{$librarynb->slug}}">Read more</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 news-aside">
          <div class="inner">
            <div class="inner-block">
              <form class="form-search form-search--sm" action="#!">
                <input class="input" type="text" placeholder="Search"/>
                <button class="rs-btn button"><svg xmlns="http://www.w3.org/2000/svg" width="23" height="25.564" viewBox="0 0 23 25.564"><g transform="translate(-736.513 -477.665)"><path d="M756.407,503.229a.608.608,0,0,0,.386-.138l2.494-2.025a.609.609,0,0,0,.223-.413.617.617,0,0,0-.134-.45l-6.262-7.709a9.244,9.244,0,1,0-8.311,3.6,9.342,9.342,0,0,0,.969.05,9.144,9.144,0,0,0,3.886-.864L755.931,503A.611.611,0,0,0,756.407,503.229Zm-11.477-8.356a8.013,8.013,0,1,1,5.879-1.751A7.962,7.962,0,0,1,744.93,494.873Zm6.652-.8a9.432,9.432,0,0,0,.713-.644l5.743,7.07-1.541,1.252-5.748-7.076A9.335,9.335,0,0,0,751.582,494.075Z" fill="#272525"/><path d="M751.681,483.31a6.761,6.761,0,1,0-.986,9.516A6.725,6.725,0,0,0,751.681,483.31Zm-1.76,8.564a5.538,5.538,0,1,1,.807-7.791A5.544,5.544,0,0,1,749.922,491.874Z" transform="translate(-0.675 -0.673)" fill="#272525"/></g></svg></button>
              </form>
            </div>
            <div class="inner-block">
              <h3 class="inner-block__title">Category</h3>
              <div class="cate">
                <a class="rs-btn btn-animated item" href="/tat-ca-thu-vien">All</a>
                <?php foreach ($library_cats as $key => $library_cat): ?>
                  <a class="rs-btn btn-animated item" href="/danh-muc-thu-vien/{{$library_cat->slug}}">{{$library_cat->title}}</a>
                <?php endforeach ?>
              </div>
            </div>
            <div class="inner-block">
              <h3 class="inner-block__title">Recent Blogs</h3>
              <div class="list">
                <?php foreach ($librarys as $key => $library): ?>
                  @if($key < 3)
                  <div class="news-aside__item hasLink animate-fadeUp">
                    <figure class="img"><img src="/public/img/upload/librarys/{{$library->avatar}}" alt=""/></figure>
                    <h4 class="title">{{$library->title}}</h4><a class="link" href="/thu-vien/{{$library->slug}}" title="{{$library->title}}"></a>
                  </div>
                  @endif
                <?php endforeach ?>
              </div>
            </div>
            <div class="inner-block">
              <ul class="rs-list social">
                <li class="item"><a class="link-blue" href="{{@$info_web['facebook']}}" title="Fanpage Facebook"><i class="fa fa-facebook"></i></a></li>
                <li class="item"><a class="link-blue" href="{{@$info_web['instagram']}}" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                <li class="item"><a class="link-blue" href="{{@$info_web['skype']}}" title="Skype"><i class="fa fa-skype"></i></a></li>
                <li class="item"><a class="link-blue" href="{{@$info_web['dribbble']}}" title="Tribbble"><i class="fa fa-dribbble"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- SECTION FOUR-->
  <section class="section section-four news" style="background-image: url(/public/theme/img/backgrounds/bg-wave.svg)">
    <?php foreach ($library_cats as $key => $library_cat): ?>
      <div class="container news-container">
        <div class="title-block style2">
          <h2 class="title-main"><span>{{$library_cat->title}}</span></h2><a class="link link-blue" href="/danh-muc-thu-vien/{{$library_cat->slug}}">See more&nbsp;<i class="fa fa-angle-right"></i></a>
        </div>
        <div class="row news-list__mobile">
			<?php $i = 0 ?>
          <?php foreach ($librarys as $key => $library): ?>
            @if($library_cat->id == $library->id_library_cat)
            <?php $i++; ?>
            @if($i < 4)
            <div class="col-sm-6 col-md-4 news-item">
              <div class="inner hasLink">
                <figure class="img embed-responsive"><img src="/public/img/upload/librarys/{{$library->avatar}}" alt=""/></figure>
                <div class="content">
                  <h4 class="title">{{$library->title}}</h4><span class="more">Read more&nbsp;&#62;</span>
                </div><a class="link" href="/thu-vien/{{$library->slug}}" title="{{$library->title}}"></a>
              </div>
            </div>
            @endif
            @endif
          <?php endforeach ?>
          
        </div>
      </div>
    <?php endforeach ?>
  </section>
  <!-- SECTION SIX-->
  <section class="section section-six"><span class="circle circle--striped"></span>
    <div class="container"><span class="triangle triangle--light"></span>
      <div class="subscribe-box">
        <div class="title-block text-center">
          <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
          <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
        </div>
        <form class="subscribe-form" id="subscribe" action="">
          <input class="input" name="email" placeholder="Your email"/>
          <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
        </form>
        <div class="contact-box">
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
          </div>
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
@endsection