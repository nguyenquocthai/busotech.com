@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- CONTACT-->
  <section class="section contact-section" style="background-image: url(/public/theme/img/bg-contact-page.png);">
    <div class="content">
      <div class="inner">
        <h2 class="title-main">{{$info_web['name']}}</h2>
        <ul class="rs-list contact-info">
          <li class="item"><span class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="37.562" height="37.563" viewBox="0 0 37.562 37.563">
                <path id="Path_727" data-name="Path 727" d="M139.963,136.636l-1.778,2.917s-6.756,9.264,7.72,23.841l0,0c.023.023.046.043.069.066s.043.046.066.069l0,0c14.576,14.475,23.84,7.72,23.84,7.72l2.917-1.778a3.439,3.439,0,0,0,.368-5.183l-3.479-3.48a3.439,3.439,0,0,0-4.5-.319l-3.7,2.367s-2.489,1.469-9.448-5.463c-6.932-6.959-5.463-9.448-5.463-9.448l2.367-3.7a3.44,3.44,0,0,0-.319-4.5l-3.479-3.479A3.439,3.439,0,0,0,139.963,136.636Z" transform="translate(-136.61 -135.261)" fill="#5a73b8"></path>
              </svg></span><span class="text"><span class="title">Hotline</span><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a></span></li>
          <li class="item"><span class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="39.713" height="27.451" viewBox="0 0 39.713 27.451">
                <g id="Group_581" data-name="Group 581" transform="translate(-135.535 -391.508)">
                  <rect id="Rectangle_103" data-name="Rectangle 103" width="37.713" height="25.451" transform="translate(136.535 392.508)" stroke-width="2" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" fill="#5a73b8"></rect>
                  <path id="Path_478" data-name="Path 478" d="M160.2,405.233l4.617,4.126,9.428,8.426H136.535l9.428-8.426,4.617-4.126" fill="#5a73b8" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                  <path id="Path_477" data-name="Path 477" d="M158.243,406.811a4.28,4.28,0,0,1-5.7,0l-6.577-5.878-9.428-8.425h37.713l-9.428,8.425-2.984,2.667Z" fill="#5a73b8" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                </g>
              </svg></span><span class="text"><span class="title">Mail</span><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a></span></li>
          <li class="item"><span class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="28.899" height="42.221" viewBox="0 0 28.899 42.221">
                <path id="Path_726" data-name="Path 726" d="M171.349,214.5a14.435,14.435,0,1,0-28.871,0s-1.009,14.125,14.435,27.785a48.74,48.74,0,0,0,9.105-10.634C171.911,222.364,171.349,214.5,171.349,214.5ZM156.913,220.6a6.092,6.092,0,1,1,6.093-6.093A6.093,6.093,0,0,1,156.913,220.6Z" transform="translate(-142.465 -200.067)" fill="#5a73b8"></path>
              </svg></span>
            <address class="text"><span class="title">Address</span><span>{{$info_web['address']}}</span></address>
          </li>
        </ul>
        <div class="social-wrap">
          <h3 class="title d-inline-block">Connect with Buso</h3>
          <ul class="rs-list social">
            <li class="item"><a class="link" href="{{@$info_web['facebook']}}" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li class="item"><a class="link" href="{{@$info_web['instagram']}}" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
            <li class="item"><a class="link" href="{{@$info_web['skype']}}" target="_blank" title="Skype"><i class="fa fa-skype"></i></a></li>
            <li class="item"><a class="link" href="{{@$info_web['dribbble']}}" target="_blank" title="Dribbble"><i class="fa fa-dribbble"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
</main>
@endsection