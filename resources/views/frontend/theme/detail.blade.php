@extends('frontend.layouts.main') 
@section('content')
<!-- MAIN CONTENT-->
<main class="main">
  <!-- THEME DETAIL-->
  <section class="section theme-detail">
    <div class="container theme-detail__container">
      <div class="content">
        <div class="title-block">
          <h1 class="title-main"><span>{{@$themedt->title}}</span></h1>
          <h2 class="title-sub"><a class="link link-blue" href="/danh-muc-giao-dien/{{@$theme_catedt->slug}}">{{@$theme_catedt->title}}</a></h2>
        </div>
        <div class="summary">{!!@$themedt->content!!}</div>
        <div class="ctrl"><a class="rs-btn btn-animated btn-blue" href="/demo/{{@$themedt->slug}}">Demo</a><a class="rs-btn btn-animated btn-green" href="/trial/{{@$themedt->slug}}">Try to user</a></div>
      </div>
      <figure class="img embed-responsive"><img src="/public/img/upload/themes/{{@$themedt->avatar}}" alt=""/></figure>
    </div>
  </section>
  <!-- SAMED THEMES-->
  <section class="section theme-section style2">
    <div class="container-fluid project-list">
      <div class="title-block text-center">
        <h2 class="title-main"><span>Related&nbsp;</span><span class="d-inline-block">Template</span></h2>
      </div>
      <div class="row">
        <?php foreach ($theme_same_types as $key => $theme_same_type): ?>
          <div class="col-6 col-lg-3 project-item style2">
            <div class="inner hasLink">
              <figure class="img embed-responsive"><img src="/public/img/upload/themes/{{$theme_same_type->avatar}}" alt="{{$theme_same_type->title}}"/>
                <div class="ctrl"><a class="rs-btn btn-blue" href="/giao-dien/{{$theme_same_type->slug}}">View</a><a class="rs-btn btn-green" href="#!">Try to user</a></div>
              </figure>
              <div class="content"><a class="title" href="/giao-dien/{{$theme_same_type->slug}}" title="{{$theme_same_type->title}}">{{$theme_same_type->title}}</a></div><a class="link" href="/giao-dien/{{$theme_same_type->slug}}" title="{{$theme_same_type->title}}"></a>
            </div>
          </div>
        <?php endforeach ?>
        
      </div>
      <div class="text-center"><a class="rs-btn btn-animated btn-gradient" href="/danh-muc-giao-dien/{{@$theme_catedt->slug}}">See more</a></div>
    </div>
  </section>
  <!-- PROPOSED THEMES-->
  <section class="section theme-section style2">
    <div class="container-fluid project-list">
      <div class="title-block text-center">
        <h2 class="title-main"><span>Recommended &nbsp;</span><span class="d-inline-block">interface template</span></h2>
      </div>
      <div class="project-implemented__container">
        <div class="project-implemented__slider js-sliderProjectImplemented">
          <?php foreach ($theme_noibats as $key => $theme_noibat): ?>
            <div class="project-item">
              <div class="inner hasLink">
                <figure class="img embed-responsive"><img src="/public/img/upload/themes/{{$theme_noibat->avatar}}" alt="{{$theme_noibat->title}}"/>
                  <div class="ctrl"><a class="rs-btn btn-blue" href="/giao-dien/{{$theme_noibat->slug}}">View</a><a class="rs-btn btn-green" href="#!">Try to user</a></div>
                </figure>
                <div class="content"><a class="title" href="/giao-dien/{{$theme_noibat->slug}}" title="{{$theme_noibat->title}}">{{$theme_noibat->title}}</a></div><a class="link" href="/giao-dien/{{$theme_noibat->slug}}" title="{{$theme_noibat->title}}"></a>
              </div>
            </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>
  </section>
  <!-- SUBCRIBE-->
  <section class="section section-six"><span class="circle circle--striped"></span>
        <div class="container"><span class="triangle triangle--light"></span>
          <div class="subscribe-box">
            <div class="title-block text-center">
              <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
              <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
            </div>
            <form class="subscribe-form" id="subscribe" action="#!">
              <input class="input" name="email" placeholder="Your email"/>
              <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
            </form>
            <div class="contact-box">
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
          </div>
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
          </div>
        </div>
          </div>
        </div>
      </section>
</main>

@endsection
