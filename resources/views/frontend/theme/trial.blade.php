@extends('frontend.layouts.main') 
@section('content')
<style>
  .btn-domain-submit {
    display:none;
  }
  .btn-domain-submit.active {
    display:inline-block;
  }
</style>
<!-- MAIN CONTENT-->
    <main class="main">
      <section class="login-section trial-section bg-gradient">
        <figure class="img"><img src="/public/theme/img/login-img.svg" alt=""/></figure>
        <div class="content">
          <div class="progressbar js-progressBar"><span class="step active" data-value="0"></span></div>
          <div class="inner actived" id="step1">
            <div class="title-block text-center">
              <h1 class="title">Try the Buso Web sales website</h1>
              <h2 class="title-sub">Free for 15 days with full features</h2>
            </div>
            <form class="login-form form-trial" action="#!" >
              @csrf
              <p class="login-form__row">
                <input class="input cle" type="text" id="name" name="name" placeholder="Name store"/>
              </p>
              <div class="flex-2">
                <p class="login-form__row">
                  <input class="input cle" type="text" id="fullname" name="fullname" placeholder="Full name"/>
                </p>
                <p class="login-form__row">
                  <input class="input cle" name="phone" id="phone" type="number" placeholder="Phone number"/>
                </p>
              </div>
              <p class="login-form__row">
                <input class="input cle" name="email" id="email" type="email" placeholder="Email"/>
              </p>
              <p class="login-form__row">
                <input class="input cle" type="password" id="password" name="password" placeholder="Password"/>
                <input type="hidden" name="domain_old" value="{{$themedt->summary}}">
                <input type="hidden" name="id_price" value="{{$id_price}}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
              </p>
              <p class="login-form__row">
                <button class="rs-btn btn-gradient btn-next dangkydungthu" type="submit" data-target="">Sign up for a trial</button>
              </p>
              <p class="login-form__row"><span>Or register quickly with your account</span></p>
              <p class="login-form__row"><a class="rs-btn social-icon facebook" href="#!" title="Đăng ký bằng tài khoản Facebook"><i class="fa fa-facebook"></i></a><a class="rs-btn social-icon google" href="#!" title="Đăng ký bằng tài khoản Google"><i class="fa fa-google-plus"></i></a><a class="rs-btn social-icon linkedin" href="#!" title="Đăng ký bằng tài khoản Linkedin"><i class="fa fa-linkedin"></i></a></p>
            </form>
          </div>
          <div class="inner" id="step2">
            <div class="title-block text-center">
              <div class="title">Choose a domain name</div>
              <p class="title-sub">Free for 15 days with full features</p>
              <div class="domain-search-container">
                <div class="domain-search__box">
                  <form class="subscribe-form check_domain" action="">
                    @csrf
                    <input class="input cle name_domain_search" name="name_domain_search" placeholder="Điền tên miền bạn muốn"/>
                    <input type="hidden" name="status_code" value="search_domain">
                    
                    <button type="submit" class="rs-btn btn-gradient">Check</button>
                  </form>

                  <div class="list-domain">
                  </div>
                </div>
                <div class="domain-search__result">
                  <ul class="rs-list list">
                    <?php foreach ($price_domains as $key => $price_domain): ?>
                      <?php if ($price_domain->noibat == 1): ?>
                        <li class="item">
                          <h3 class="title"><span>{{$price_domain->title}}</span> {{($price_domain->gia)/1000}}k</h3>
                        </li>
                      <?php endif ?>
                    <?php endforeach ?>
                    
                  </ul><a class="link" href="#modalDomains" data-toggle="modal">See more&nbsp;<i class="fa fa-angle-right"></i></a>
                </div>
              </div>
            </div>
            <div class="domain-list ">
              <!-- <ul class="rs-list list">
                <li class="item ">.com</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item"><span>Đã được đăng ký</span>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.net</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item">
                  <button class="rs-btn btn-gradient">Chọn</button>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.vn</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item"><span>Đã được đăng ký</span>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.org</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item">
                  <button class="rs-btn btn-gradient">Chọn</button>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.com.vn</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item">
                  <button class="rs-btn btn-gradient">Chọn</button>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.info</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item"><span>Đã được đăng ký</span>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.edu.vn</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item"><span>Đã được đăng ký</span>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.net.vn</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item">
                  <button class="rs-btn btn-gradient">Chọn</button>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.asia</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item"><span>Đã được đăng ký</span>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.me</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item">
                  <button class="rs-btn btn-gradient">Chọn</button>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.co</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item"><span>Đã được đăng ký</span>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.us</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item">
                  <button class="rs-btn btn-gradient">Chọn</button>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.biz</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item">
                  <button class="rs-btn btn-gradient">Chọn</button>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.xyz</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item"><span>Đã được đăng ký</span>
                </li>
              </ul>
              <ul class="rs-list list">
                <li class="item">.tv</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item"><span>Đã được đăng ký</span>
                </li>
              </ul> -->
            </div>
            <div class="text-center" style="margin-top: 50px;">
              
              <form action="" class="form-trial1">
                <button class="rs-btn btn-gradient btn-next js-progressBtn js-tabLogin quaylai" type="button" data-target="#step1">Back</button>
                <input type="hidden" name="domain_name" class="domainchuadangky">
                <input type="hidden" name="status_check" value="1">
                <input type="hidden" name="domain_active" value="0">
                <button class="rs-btn btn-gradient btn-next btn-domain-submit" type="submit" data-target="">Domain name registration</button> 
              </form>
              
            </div>
            <div class="domain-exist" id="collapseDomain">
              <button class="rs-btn domain-exist__btn link collapsed mr-3" data-toggle="collapse" data-target="#collapseDomainExist">Already have a domain name</button>
              <button class="rs-btn domain-exist__btn link collapsed" data-toggle="collapse" data-target="#collapseDomainFree">Use a free domain name</button>
              <div class="collapse" id="collapseDomainExist" data-parent="#collapseDomain">
                <form class="login-form form-trial2">
                  <input class="input domain_name" type="text" name="domain_name" placeholder="Enter the domain name you have" required />
                  <input type="hidden" name="status_check" value="2">
                  <button class="rs-btn btn-gradient btn-next js-progressBtn js-tabLogin quaylai" type="button" data-target="#step1">Back</button>
                  <button class="rs-btn btn-gradient btn-next"  type="submit" data-target="">Countinue</button>
                </form>
              </div>
              <div class="collapse" id="collapseDomainFree" data-parent="#collapseDomain">
                <form class="login-form form-trial3">
                  <div class="input-wrap">
                    <input class="input cle domain_name" name="domain_name" id="domain_free" type="text" placeholder="Enter the domain name you want" required /><span class="input-label">.busotech.com</span>
                    <input type="hidden" name="status_check" value="3">

                  </div>
                  <button class="rs-btn btn-gradient btn-next js-progressBtn js-tabLogin quaylai" type="button" data-target="#step1">Back</button>
                  <button class="rs-btn btn-gradient btn-next"  type="submit" data-target="">Countinue</button>
                </form>
              </div>
            </div>
          </div>
          <div class="inner" id="step3">
            <div class="title-block text-center">
              <div class="title">Welcome to Buso!</div>
              <p class="title-sub">Your account has been successfully registered</p>
            </div>
            <div class="trial-sucessful">
              <p class="description text-center">With the best technology solutions for e-commerce, Omnichannel retail and Marketing, Buso will be the foundation to help you grow your business.</p>
              <p class="trial-sucessful__row">Your website registration information is as follows:</p>
              <p class="trial-sucessful__row"><span>User name</span><span class="tendangnhap">Vy Lê</span></p>
              <p class="trial-sucessful__row"><span>Link website</span><span class="diachiweb">www.abc.com</span></p>
              <p class="trial-sucessful__row"><span>Password has been sent via email</span><span>******</span></p>
              <!-- <p class="trial-sucessful__row"><span>Tên miền đã chọn mua</span><span>.com</span></p> -->
            </div>
            <div class="text-center">
              <a href="/giao-dien"><button class="rs-btn btn-gradient btn-next js-tabLogin" type="button" data-target="#step3">Completed</button></a>
            </div>
          </div>
        </div>
      </section>
      <!-- DOMAINS PRICE MODAL-->
      <div class="modal modal-custom fade" id="modalDomains" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
          <div class="modal-content js-tabParent">
            <div class="modal-header">
              <button class="rs-btn modal-close" data-dismiss="modal" arial-label="Close"></button>
              <div class="modal-title">Domain price list</div>
              <div class="tab-nav js-tabNav">
                <?php foreach ($price_domain_cats as $key => $price_domain_cat): ?>
                  <button class="rs-btn item js-tabLink <?php if ($key == 0) echo 'active'; ?>" data-target="#tabDomain{{$price_domain_cat->id}}" type="button">{{$price_domain_cat->title}}</button>
                <?php endforeach ?>
                
              </div>
            </div>
            <div class="modal-body tab-content js-tabContent">
              <?php foreach ($price_domain_cats as $key => $price_domain_cat): ?>
                <div class="tab-panel fade <?php if ($key == 0) echo 'active'; ?>" id="tabDomain{{$price_domain_cat->id}}">
                  <ul class="rs-list list list-title">
                    <li class="item">Domain name</li>
                    <li class="item">Price registration</li>
                    <li class="item">Renewal</li>
                    <li class="item">Switch to Buso</li>
                  </ul>
                  <div class="content">
                    <?php foreach ($price_domains as $key => $price_domain): ?>
                      <?php if ($price_domain->id_price_domain_cat == $price_domain_cat->id): ?>
                      <ul class="rs-list list">
                        <li class="item">{{$price_domain->title}}</li>
                        <li class="item">{{number_format($price_domain->gia)}} VNĐ</li>
                        <li class="item">{{number_format($price_domain->giahan)}} VNĐ</li>
                        <li class="item">{{number_format($price_domain->chuyensangbuso)}} VNĐ</li>
                      </ul>
                      <?php endif ?>
                    <?php endforeach ?>
                  </div>
                </div>
              <?php endforeach ?>
              
            </div>
          </div>
        </div>
      </div>
    </main>
    <script>
       var data_form1 = [];
       var loadding = false;                 
      jQuery(document).ready(function($) {
        // $('.quaylai').click(function(event) {
        //   $('.form-trial').find('.domain_name').val("");
        // });
        
        var formtrial = $('.form-trial').validate({
              highlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass);
              },
              rules: {
                  fullname:{
                      required: true,
                  },
                  email:{
                      required: true,
                      email:true
                  },
                  phone:{
                      required: true,
                      number: true,
                      
                  },
                  address:{
                      required: true,

                  },
                  
                  password: {
                     required: true,
                  }
              },
              messages: {
                  fullname:{
                      required: 'Please enter your name.',
                  },
                  
                  email:{
                      required: 'Please enter your email.',
                      email:'Your email is not in the correct email format'
                  },
                  
                  phone:{
                      required: 'Please enter your phone.',
                      number: 'Please enter the correct phone number.',
                      
                  },
                  address:{
                      required: 'Please enter your address.',
                      
                  },
                  
                  password: {
                      required: "Please enter your password",
                  }
              },
              submitHandler: function (form) {
                  var data = {};
                  
                  $(".form-trial").serializeArray().map(function(x){data[x.name] = x.value;});
                  
                  // data: {
                  //   _token : "{{ csrf_token() }}",
                  //   }
                  // console.log(data);
                 
                  data_form1 = data;

                  // console.log(data_form1);
                  
                  $.ajax({
                      type: 'POST',
                      url: '/check_email',
                      data: data,
                      dataType: 'json',
                      error: function(){
                          
                          toastr.error('Error');
                      },
                      success: function(result) {
                          console.log(result);
                          switch (result.code) {
                              case 200:
                                $('.form-trial').closest('.trial-section').find('#step2').addClass('actived');
                                $('.form-trial').closest('.trial-section').find('#step1').removeClass('actived');
                                $('.form-trial').closest('.trial-section').find('.progressbar').find('.step').attr('style', 'left: 33.333%');
                                  break;
                                case 300:
                                  toastr.error(result.value);
                                  break;
                                default:
                                  toastr.error('Error');
                                  break;
                          }
                      }
                  });
                  return false;
              }
          });
          
        var formtrial1 = $('.form-trial1').validate({
              highlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass);
              },
              rules: {

                  domain_name: {
                     required: true,
                  }
              },
              messages: {
                 
                  domain_name: {
                      required: "Please enter the domain name",
                  }
              },
              submitHandler: function (form) {
                  var data = {};
                  
                  $(".form-trial1").serializeArray().map(function(x){data[x.name] = x.value;});
                  
                  data: {
                    _token : "{{ csrf_token() }}"
                    }

                  data = Object.assign(data, data_form1);
                  // console.log(data);return false;
                  
                  $.ajax({
                      type: 'POST',
                      url: '/add_contact_theme',
                      data: data,
                      dataType: 'json',
                      error: function(){
                          
                          toastr.error('Error entering domain name');
                      },
                      success: function(result) {
                          console.log(result);
                          switch (result.code) {
                              case 200:
                                  $('.form-trial1').find('.cle').val("");
                                  $('.form-trial1').closest('.trial-section').find('#step3').find('.tendangnhap').html(result.email);
                                  $('.form-trial1').closest('.trial-section').find('#step3').find('.diachiweb').html(result.domain_name);
                                  toastr.success('Sign Up Success');
                                  $('.form-trial1').closest('.trial-section').find('#step3').addClass('actived');
                                  $('.form-trial1').closest('.trial-section').find('#step2').removeClass('actived');
                                  $('.form-trial1').closest('.trial-section').find('.progressbar').find('.step').attr('style', 'left: 66.666%');
                                  break;
                                case 300:
                                  toastr.error(result.value);
                                  break;
                                default:
                                  toastr.error('Error');
                                  break;
                          }
                      }
                  });
                  return false;
              }
          });
        var formtrial2 = $('.form-trial2').validate({
              highlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass);
              },
              rules: {

                  domain_name: {
                     required: true,
                  }
              },
              messages: {
                 
                  domain_name: {
                      required: "Please enter the domain name",
                  }
              },
              submitHandler: function (form) {
                  var data = {};
                  
                  $(".form-trial2").serializeArray().map(function(x){data[x.name] = x.value;});
                  
                  data: {
                    _token : "{{ csrf_token() }}"
                    }

                  data = Object.assign(data, data_form1);
                  // console.log(data);return false;
                  
                  $.ajax({
                      type: 'POST',
                      url: '/add_contact_theme',
                      data: data,
                      dataType: 'json',
                      error: function(){
                          
                          toastr.error('Error');
                      },
                      success: function(result) {
                          console.log(result);
                          switch (result.code) {
                              case 200:
                                  $('.form-trial2').find('.cle').val("");
                                  $('.form-trial2').closest('.trial-section').find('#step3').find('.tendangnhap').html(result.email);
                                  $('.form-trial2').closest('.trial-section').find('#step3').find('.diachiweb').html(result.domain_name);
                                  toastr.success('Đăng ký thành công');
                                  $('.form-trial2').closest('.trial-section').find('#step3').addClass('actived');
                                  $('.form-trial2').closest('.trial-section').find('#step2').removeClass('actived');
                                  $('.form-trial2').closest('.trial-section').find('.progressbar').find('.step').attr('style', 'left: 66.666%');
                                  break;
                                case 300:
                                  toastr.error(result.value);
                                  break;
                                default:
                                  toastr.error('Error');
                                  break;
                          }
                      }
                  });
                  return false;
              }
          });
        var formtrial3 = $('.form-trial3').validate({
              highlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass);
              },
              rules: {

                  domain_name: {
                     required: true,
                  }
              },
              messages: {
                 
                  domain_name: {
                      required: "Please enter the domain name",
                  }
              },
              submitHandler: function (form) {
                  var data = {};
                  
                  $(".form-trial3").serializeArray().map(function(x){data[x.name] = x.value;});
                  
                  data: {
                    _token : "{{ csrf_token() }}"
                    }

                  data = Object.assign(data, data_form1);
                  // console.log(data);return false;
                  
                  $.ajax({
                      type: 'POST',
                      url: '/add_contact_theme',
                      data: data,
                      dataType: 'json',
                      error: function(){
                          
                          toastr.error('Error');
                      },
                      success: function(result) {
                          console.log(result);
                          switch (result.code) {
                              case 200:
                                  $('.form-trial3').find('.cle').val("");
                                  $('.form-trial3').closest('.trial-section').find('#step3').find('.tendangnhap').html(result.email);
                                  $('.form-trial3').closest('.trial-section').find('#step3').find('.diachiweb').html(result.domain_name);
                                  toastr.success('Sign Up Success');
                                  $('.form-trial3').closest('.trial-section').find('#step3').addClass('actived');
                                  $('.form-trial3').closest('.trial-section').find('#step2').removeClass('actived');
                                  $('.form-trial3').closest('.trial-section').find('.progressbar').find('.step').attr('style', 'left: 66.666%');
                                  break;
                                case 300:
                                  toastr.error(result.value);
                                  break;
                                default:
                                  toastr.error('Error');
                                  break;
                          }
                      }
                  });
                  return false;
              }
          });
        var arr_domain = [
          {
            name: '.vn',
            status: 0,
            rs: ''
          },
          {
            name: '.com',
            status: 0,
            rs: ''
          },
          {
            name: '.asia',
            status: 0,
            rs: ''
          },
          {
            name: '.com.vn',
            status: 0,
            rs: ''
          },
          {
            name: '.org',
            status: 0,
            rs: ''
          },
          {
            name: '.info',
            status: 0,
            rs: ''
          },
          {
            name: '.online',
            status: 0,
            rs: ''
          },
          {
            name: '.xyz',
            status: 0,
            rs: ''
          },
          {
            name: '.top',
            status: 0,
            rs: ''
          },
          {
            name: '.vip',
            status: 0,
            rs: ''
          },
          {
            name: '.site',
            status: 0,
            rs: ''
          },
          {
            name: '.pro',
            status: 0,
            rs: ''
          },
          {
            name: '.club',
            status: 0,
            rs: ''
          }
        ]

        function views(name) {
          var htmlDomain = ''
          arr_domain.map(function(val, index1) {
              // var chon = ''
              // if (val.rs.code == 200) {
              //   chon = `<li class="item btn-chon-domain" data-value="${name}${val.name}">
              //     <button class="rs-btn btn-gradient" >Chọn</button>
              //   </li>`
              // } else {
              //   chon = `<li class="item"><span>${val.rs.value || 'Waiting...'}</span></li>`
              // }

              htmlDomain = htmlDomain + `<ul data-index="${index1}" class="rs-list list">
                <li class="item ">${name}${val.name}</li>
                <li class="item">300.000 <span class="d-inline-block">VNĐ / năm</span></li>
                <li class="item chon">Waiting...</li>
              </ul>`
            
          })
          $('.domain-list').html(htmlDomain)
        }

        // $('.btn-chon-domain').on('click', function () {
        //     $('.btn-chon-domain').html(`<button class="rs-btn btn-gradient" >Chọn</button>`);
        //     $(this).html(`<button class="rs-btn btn-gradient"><i class="fa fa-check" aria-hidden="true"></i></button>`);
        // });
        $( ".domain-list" ).on( "click", ".btn-chon-domain", function() {
          $('.btn-chon-domain').html(`<button class="rs-btn btn-gradient" >Chọn</button>`);
          $(this).html(`<button class="rs-btn btn-gradient"><i class="fa fa-check" aria-hidden="true"></i></button>`);
          
          var domain_search = $(this).data('value');
          // console.log(domain_search)
          $('.domainchuadangky').val(domain_search)
          $('.btn-domain-submit').addClass('active');
          // console.log($('.domainchuadangky').val())
          // $('.domainchuadangky').val(result.domain_check) 
          
        });

        function whois() {

          arr_domain.map(function(val, index) {
            var data = {};
            $(".check_domain").serializeArray().map(function(x){data[x.name] = x.value;});

            var name_domain = data.name_domain_search
            var res = name_domain.split(".")
            data.name_domain_search = res[0] + arr_domain[index].name
            
            data: {
              _token : "{{ csrf_token() }}"
            }
            $.ajax({
                type: 'POST',
                url: '/add_contact_theme',
                data: data,
                dataType: 'json',
                error: function(){
                  toastr.error('Error');
                  loading = false
                },
                success: function(result) {
                  // console.log(result)
                  var chonhtml = ''
                  if (result.code == 200) {
                    chonhtml = `<div class="btn-chon-domain" data-value="${result.domain_check}"><button class="rs-btn btn-gradient" >Chọn</button></div>`
                  } else {
                    chonhtml = `<span>${result.value || 'Waiting...'}</span>`
                  }
                  $('.domain-list ul[data-index="'+index+'"] .chon').html(chonhtml)
                  if (index < arr_domain.length - 1) {
                    return true
                  } else {
                    loadding = false
                    return true
                  }
                }
            });
          })
          
        }
        var check_domain = $('.check_domain').validate({
          highlight: function(element, errorClass, validClass) {
              $(element).removeClass(errorClass);
          },
          rules: {
              
              name_domain_search:{
                  required: true,

              }
          },
          messages: {
              
              name_domain_search:{
                  required: 'Please enter your website address',

              }
          },
          submitHandler: function (form) {
            if (loadding == true) {
              return false
            }
            loadding = true
            var name_domain = $('.check_domain .name_domain_search').val()
            var res = name_domain.split(".")
            views(res[0])
            whois()
            return false;
          }
        });
      });
      
    </script>

@endsection
