@extends('frontend.layouts.main') 
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      
      <!-- THEMES-->
      <section class="section theme-section">
        <div class="container">
          <div class="title-block text-center">
            <h2 class="title-main"><span>Website&nbsp;</span><span class="d-inline-block">Interface warehouse</span></h2>
            <h2 class="title-main"><span style="color:#5a73b8;">{{$theme_catedt->title}}</span></h2>
          </div>
          
        </div>
        <div class="container-fluid project-list">
          <div class="row">
            <?php foreach ($themes as $key => $theme): ?>
              <div class="col-6 col-lg-4 col-xl-3 project-item">
                <div class="inner hasLink">
                  <figure class="img embed-responsive"><img src="/public/img/upload/themes/{{$theme->avatar}}" alt=""/>
                    <div class="ctrl"><a class="rs-btn btn-blue" href="/giao-dien/{{$theme->slug}}">View</a><a class="rs-btn btn-green" href="/trial/{{$theme->slug}}">Try to user</a></div>
                  </figure>
                  <div class="content"><a class="title" href="/giao-dien/{{$theme->slug}}" title="{{$theme->title}}">{{$theme->title}}</a></div><a class="link" href="/giao-dien/{{$theme->slug}}" title="{{$theme->title}}"></a>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <div class="paginationk center">
            {{$themes->links()}}
            <div class="paginationk-move">
              <label class="label">Go to page</label>
              <form class="form" action="/giao-dien" method="put">
                <input class="input" name="page" type="number"/>
                <button class="rs-btn button" type="submit">Go&nbsp;<i class="fa fa-chevron-right"></i></button>
              </form>
            </div>
          </div>
        </div>
      </section>
      <!-- TECHNOLOGY-->
      <section class="section section-four" style="background-image: url(/public/theme/img/backgrounds/bg-wave.svg)">
        <div class="container"><span class="circle circle--light"></span>
          <div class="title-block text-center">
            <h2 class="title-main"><span>Technology</span></h2>
          </div>
          <ul class="rs-list customer-list">
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-1.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-2.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-3.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-4.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-5.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-6.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-7.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-8.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-9.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-10.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
          </ul>
        </div>
      </section>
      <!-- SUBCRIBE-->
      <section class="section section-six"><span class="circle circle--striped"></span>
        <div class="container"><span class="triangle triangle--light"></span>
          <div class="subscribe-box">
            <div class="title-block text-center">
              <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
              <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
            </div>
            <form class="subscribe-form" id="subscribe" action="#!">
              <input class="input" name="email" placeholder="Your email"/>
              <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
            </form>
            <div class="contact-box">
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
          </div>
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
          </div>
        </div>
          </div>
        </div>
      </section>
    </main>

@endsection
