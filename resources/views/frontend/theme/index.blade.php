@extends('frontend.layouts.main') 
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- IMPLEMENTED PROJECTS-->
      <section class="section project-implemented">
        <div class="container">
          <div class="title-block text-center">
            <h2 class="title-main"><span>Proposed &nbsp;</span><span class="d-inline-block">interface template</span></h2>
          </div>
        </div>
        <div class="project-implemented__container">
          <div class="project-implemented__slider js-sliderProjectImplemented">
            <?php foreach ($theme_noibats as $key => $theme_noibat): ?>
            <div class="project-item">
              <div class="inner hasLink">
                <figure class="img embed-responsive"><img src="/public/img/upload/themes/{{$theme_noibat->avatar}}" alt=""/>
                  <div class="ctrl"><a class="rs-btn btn-blue" href="/giao-dien/{{$theme_noibat->slug}}">View</a><a class="rs-btn btn-green" href="/trial/{{$theme_noibat->slug}}">Try to use</a></div>
                </figure>
                <div class="content"><a class="title" href="/giao-dien/{{$theme_noibat->slug}}" title="{{$theme_noibat->title}}">{{$theme_noibat->title}}</a></div><a class="link" href="/giao-dien/{{$theme_noibat->slug}}" title="{{$theme_noibat->title}}"></a>
              </div>
            </div>
          <?php endforeach ?>
          </div>
        </div>
      </section>
      <!-- THEMES-->
      <section class="section theme-section">
        <div class="container">
          <div class="title-block text-center">
            <h2 class="title-main"><span>Website &nbsp;</span><span class="d-inline-block">Interface Warehouse</span></h2>
          </div>
          <div class="theme-filter js-tabParent">
            <div class="theme-filter__type">
              <div class="list js-tabNav">
                <a class="rs-btn btn-animated item js-tabLink active" data-target="#all" href="#!">All</a>
                <?php foreach ($theme_cates as $key => $theme_cate): ?>
                  <a class="rs-btn btn-animated item js-tabLink" href="#!" data-id="{{$theme_cate->id}}" data-target="#{{$theme_cate->slug}}">{{$theme_cate->title}}</a>
                <?php endforeach ?>
              </div>
              <form class="form-search form-search--sm" action="#!">
                <input class="input" type="text" placeholder="Tìm giao diện"/>
                <button class="rs-btn button"><svg xmlns="http://www.w3.org/2000/svg" width="23" height="25.564" viewBox="0 0 23 25.564"><g transform="translate(-736.513 -477.665)"><path d="M756.407,503.229a.608.608,0,0,0,.386-.138l2.494-2.025a.609.609,0,0,0,.223-.413.617.617,0,0,0-.134-.45l-6.262-7.709a9.244,9.244,0,1,0-8.311,3.6,9.342,9.342,0,0,0,.969.05,9.144,9.144,0,0,0,3.886-.864L755.931,503A.611.611,0,0,0,756.407,503.229Zm-11.477-8.356a8.013,8.013,0,1,1,5.879-1.751A7.962,7.962,0,0,1,744.93,494.873Zm6.652-.8a9.432,9.432,0,0,0,.713-.644l5.743,7.07-1.541,1.252-5.748-7.076A9.335,9.335,0,0,0,751.582,494.075Z" fill="#272525"/><path d="M751.681,483.31a6.761,6.761,0,1,0-.986,9.516A6.725,6.725,0,0,0,751.681,483.31Zm-1.76,8.564a5.538,5.538,0,1,1,.807-7.791A5.544,5.544,0,0,1,749.922,491.874Z" transform="translate(-0.675 -0.673)" fill="#272525"/></g></svg></button>
              </form>
            </div>
            <div class="theme-filter__category js-tabContent">
              <div class="tab-panel fade active" id="all">
                <div class="inner" data-id='{{$theme_cate->id}}'>
                  <?php foreach ($theme_cate1s as $key => $theme_cate1): ?>
                    
                      <a class="rs-btn btn-animated item" href="/danh-muc-giao-dien/{{$theme_cate1->slug}}"><h3 class="title">{{$theme_cate1->title}}</h3></a>
                    
                  <?php endforeach ?>
                </div>
                </div>
              <?php foreach ($theme_cates as $key => $theme_cate): ?>
                <div class="tab-panel fade" id="{{$theme_cate->slug}}">
                <div class="inner" data-id='{{$theme_cate->id}}'>
                  <?php foreach ($theme_cate1s as $key => $theme_cate1): ?>
                    <?php if ($theme_cate->id == $theme_cate1->id_theme_cat): ?>
                      <a class="rs-btn btn-animated item" href="/danh-muc-giao-dien/{{$theme_cate1->slug}}"><h3 class="title">{{$theme_cate1->title}}</h3></a>
                    <?php endif ?>
                  <?php endforeach ?>
                </div>
                </div>
              <?php endforeach ?>
              
            </div>
          </div>
        </div>
        <div class="container-fluid project-list">
          <div class="row">
            <?php foreach ($themes as $key => $theme): ?>
              <div class="col-6 col-lg-4 col-xl-3 project-item">
                <div class="inner hasLink">
                  <figure class="img embed-responsive"><img src="/public/img/upload/themes/{{$theme->avatar}}" alt=""/>
                    <div class="ctrl"><a class="rs-btn btn-blue" href="/giao-dien/{{$theme->slug}}">View</a><a class="rs-btn btn-green" href="/trial/{{$theme->slug}}">Try to user</a></div>
                  </figure>
                  <div class="content"><a class="title" href="/giao-dien/{{$theme->slug}}" title="{{$theme->title}}">{{$theme->title}}</a></div><a class="link" href="/giao-dien/{{$theme->slug}}" title="{{$theme->title}}"></a>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <div class="paginationk center">
            {{$themes->links()}}
            <div class="paginationk-move">
              <label class="label">Go to page</label>
              <form class="form" action="/giao-dien" method="put">
                <input class="input" name="page" type="number"/>
                <button class="rs-btn button" type="submit">Go&nbsp;<i class="fa fa-chevron-right"></i></button>
              </form>
            </div>
          </div>
        </div>
      </section>
      <!-- TECHNOLOGY-->
      <section class="section section-four" style="background-image: url(/public/theme/img/backgrounds/bg-wave.svg)">
        <div class="container"><span class="circle circle--light"></span>
          <div class="title-block text-center">
            <h2 class="title-main"><span>Technology</span></h2>
          </div>
          <ul class="rs-list customer-list">
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-1.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-2.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-3.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-4.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-5.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-6.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-7.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-8.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-9.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
            <li class="item">
              <figure class="img hasLink"><img src="/public/theme/img/cong-nghe-10.svg" alt=""/><a class="link" href="#!"></a></figure>
            </li>
          </ul>
        </div>
      </section>
      <!-- SUBCRIBE-->
      <section class="section section-six"><span class="circle circle--striped"></span>
        <div class="container"><span class="triangle triangle--light"></span>
          <div class="subscribe-box">
            <div class="title-block text-center">
              <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
              <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
            </div>
            <form class="subscribe-form" id="subscribe" action="#!">
              <input class="input" name="email" placeholder="Your email"/>
              <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
            </form>
            <div class="contact-box">
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
          </div>
          <div class="item">
            <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
          </div>
        </div>
          </div>
        </div>
      </section>
    </main>

@endsection
