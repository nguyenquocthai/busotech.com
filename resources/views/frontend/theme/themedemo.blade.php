@extends('frontend.layouts.main') 
@section('content')
<style>
  header, footer{
    display: none;
  }
</style>
<!-- MAIN CONTENT-->
<main class="main">
  <!-- TOP BAR-->
  <div class="theme-demo__top">
    <div class="container">
      <div class="left"><a class="link back" href="javascript:history.back()"><i class="fa fa-angle-left"></i>&nbsp; Go back to see details</a></div>
      <div class="ctrl">
        <button class="rs-btn button js-deviceTrigger active" data-type="desktop" title="Desktop version"><img src="/public/theme/img/icons/desktop.png" alt=""/></button>
        <button class="rs-btn button js-deviceTrigger" data-type="mobile-portrait" title="Mobile version vertical"><img src="/public/theme/img/icons/mobile-portrait.png" alt=""/></button>
        <button class="rs-btn button js-deviceTrigger" data-type="mobile-landscape" title="Mobile version horizontal"><img src="/public/theme/img/icons/mobile-landscape.png" alt=""/></button>
        <button class="rs-btn button js-deviceTrigger" data-type="tablet-portrait" title="Tablet version vertical"><img src="/public/theme/img/icons/tablet-portrait.png" alt=""/></button>
        <button class="rs-btn button js-deviceTrigger" data-type="tablet-landscape" title=" Tablet version horizontal"><img src="/public/theme/img/icons/tablet-landscape.png" alt=""/></button>
      </div>
      <div class="right">
        <button class="rs-btn btn-white" data-target="#modalThemeUsing" data-toggle="modal">Use this interface</button>
        <button class="rs-btn modal-close js-removeBar"></button>
      </div>
    </div>
  </div>
  <!-- CONTENT-->
  <div class="theme-demo__body js-device" type="desktop">
    <div class="inner">
      <div class="box">
        <?php if(substr($themedt->summary, 0, 5) == 'https') {
          $result_demo = $themedt->summary;
        }else{
          $result_demo = str_replace('http', 'https', $themedt->summary);
        } ?>
        
        <iframe src="{{$result_demo}}" frameborder="0"></iframe>
      </div>
    </div>
  </div>
  <!-- DOMAINS PRICE MODAL-->
  <div class="modal modal-custom modal-custom--sm theme-demo__modal fade" id="modalThemeUsing" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content js-tabParent">
        <div class="modal-header">
          <button class="rs-btn modal-close" data-dismiss="modal" arial-label="Close"></button>
        </div>
        <div class="modal-body">
          <!-- <div class="block">
            <div class="title text-center">You already have a website at Buso Web</div>
            <form class="login-form" action="#!">
              <div class="login-flex">
                <p class="item mb-0">
                  <input class="input" type="text" placeholder="Nhập tên website của bạn"/>
                </p>
                <p class="item mb-0">
                  <button class="rs-btn btn-gradient">Log in</button>
                </p>
              </div>
            </form>
          </div> -->
          <div class="block text-center">
            <div class="title">You do not have a website?</div>
            <div class="title-sm mb-3">The system will install the default interface for your trial website. Premium theme will be available when you use Buso and download the theme.</div><a class="rs-btn btn-gradient lg" href="/trial/{{@$themedt->slug}}">Sign up for a free trial</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@endsection
