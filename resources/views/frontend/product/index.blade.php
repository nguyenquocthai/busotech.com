@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- BANNER PAGE-->
      <section class="banner banner-page embed-responsive" style="background-image: url(/public/img/upload/banners/{{$banner->avatar}});">
        <div class="caption">Product</div>
      </section>
      <!-- PRODUCT-->
      <section class="section">
        <div class="container product-list">
          <div class="row">
            <div class="left">
              <div class="row">
                <?php foreach (@$products as $key => $product): ?>
                <div class="col-sm-6 col-md-4 product-item form-muahang">
                  <div class="inner">
                    <figure class="img embed-responsive hasLink">
                      <img src="/public/img/upload/products/{{$product->avatar}}" alt="">
                      <a class="link" href="/san-pham/{{$product->slug}}"></a>
                      <?php if ($product->noibat == 0): ?>
                        <span class="label label-green">New</span>
                      <?php elseif($product->noibat == 2): ?>
                        <span class="label label-red">Sale</span>
                      <?php endif ?>
                      <a data-id="{{$product->id}}" class="rs-btn add-cart add_cart" href="#!">Add to cart</a>
                      <input class="quantity-value js-quantityVal quantity-val" type="hidden" value="1">
                    </figure>
                    <div class="content"><a class="name" href="/san-pham/{{$product->slug}}">{{$product->title}}</a>
                      <p class="price">{{number_format($product->price)}} VNĐ</p>
                    </div>
                  </div>
                </div>
                <?php endforeach ?>
              </div>
              <div class="paginationk center">
                {{@$products->links()}}
              </div>
            </div>
            <aside class="right">
              <div class="aside-block"><span class="title">Category product</span>
                <ul class="rs-list list">
                  <?php foreach (@$product_cats as $key => $product_cat): ?>
                    <li class="item"><a class="link" href="/danh-muc-san-pham/{{$product_cat->slug}}">{{$product_cat->title}}</a></li>
                  <?php endforeach ?>
                </ul>
              </div>
              <!-- <div class="aside-block"><span class="title">Tag sản phẩm</span>
                <div class="tags"><a class="link" href="#!">Sale</a>,&nbsp;<a class="link" href="#!">Hàng mới</a>,&nbsp;<a class="link" href="#!">Bán chạy</a></div>
              </div> -->
              <div class="aside-block"><span class="title">Hot product</span>
                <?php foreach (@$productnbs as $key => $productnb): ?>
                <div class="product-item__sale hasLink">
                  <figure class="img"><img src="/public/img/upload/products/{{$productnb->avatar}}" alt=""></figure>
                  <div class="content"><span class="name">{{$productnb->title}}</span><span class="price">{{number_format($productnb->price)}} VNĐ</span></div><a class="link" href="/san-pham/{{$productnb->slug}}"></a>
                </div>
                <?php endforeach ?>
              </div>
              <div class="aside-block d-none d-lg-block">
                <figure class="img img-ads hasLink"><img src="/public/img/upload/albums/{{@$album->avatar}}" alt=""><a class="link" href="/hinh-anh"></a></figure>
              </div>
            </aside>
          </div>
        </div>
      </section>
    </main>
@endsection