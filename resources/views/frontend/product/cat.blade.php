@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- BANNER PAGE-->
      <section class="banner banner-page embed-responsive" style="background-image: url(/public/img/upload/banners/{{$banner->avatar}});">
        <div class="caption">Product</div>
      </section>
      <!-- PRODUCT-->
      <section class="section">
        <div class="container product-list">
          <div class="row">
            <div class="left">
              <div class="row">
                <?php foreach (@$products as $key => $product): ?>
                <div class="col-sm-6 col-md-4 product-item">
                  <div class="inner">
                    <figure class="img embed-responsive hasLink"><img src="/public/img/upload/products/{{$product->avatar}}" alt=""><a class="link" href="/san-pham/{{$product->slug}}"></a>
                    </figure>
                    <div class="content"><a class="name" href="/san-pham/{{$product->slug}}">{{$product->title}}</a>
                      <p class="price">{{number_format($product->price)}} VNĐ</p>
                    </div>
                  </div>
                </div>
                <?php endforeach ?>
              </div>
              <div class="paginationk center">
                {{@$products->links()}}
              </div>
            </div>
            <aside class="right">
              <div class="aside-block"><span class="title">Category</span>
                <ul class="rs-list list dmsp">
                  <?php foreach (@$product_cats as $key => $product_cat1): ?>
                    <li class="item">
                      <a class="link <?php if($product_cat1->id == $product_cat->id) echo 'active' ?>" href="/danh-muc-san-pham/{{$product_cat1->slug}}">{{$product_cat1->title}}</a>
                    </li>
                  <?php endforeach ?>
                </ul>
              </div>
              <!-- <div class="aside-block"><span class="title">Tag sản phẩm</span>
                <div class="tags"><a class="link" href="#!">Sale</a>,&nbsp;<a class="link" href="#!">Hàng mới</a>,&nbsp;<a class="link" href="#!">Bán chạy</a></div>
              </div> -->
              <div class="aside-block"><span class="title">Hot products</span>
                <?php foreach (@$productnbs as $key => $productnb): ?>
                <div class="product-item__sale hasLink">
                  <figure class="img"><img src="/public/img/upload/products/{{$productnb->avatar}}" alt=""></figure>
                  <div class="content"><span class="name">{{$productnb->title}}</span><span class="price">{{number_format($productnb->price)}} VNĐ</span></div><a class="link" href="/san-pham/{{$productnb->slug}}"></a>
                </div>
                <?php endforeach ?>
              </div>
              <div class="aside-block d-none d-lg-block">
                <figure class="img img-ads hasLink"><img src="/public/img/upload/albums/{{@$album->avatar}}" alt=""><a class="link" href="/hinh-anh/{{@$album->slug}}"></a></figure>
              </div>
            </aside>
          </div>
        </div>
      </section>
    </main>
@endsection