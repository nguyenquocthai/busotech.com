@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <section class="order-section">
        <div class="container">
          <h1 class="title-link">
            <span class="text">Ordered</span>
            <a class="link" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i>Back</a>
            
          </h1>
          <form class="row cart-form" action="/send_cart" method="post">
            @csrf
            <div class="col-md-6">
              <div class="order-panel">
                <div class="inner">
                  <h3 class="title">PAYMENT METHODS</h3>
                  <ul class="rs-list payment-list">
                    <li class="item">
                      <label class="checkbox radio d-flex align-items-start">
                        <input class="checkbox-input" type="radio" name="payment" value="cod" checked><span class="checkbox-icon"></span><span class="checkbox-text">Cash on delivery (COD) payment</span>
                      </label>
                    </li>
                    <!-- <li class="item">
                      <label class="checkbox radio d-flex align-items-start">
                        <input value="payment" class="checkbox-input" type="radio" name="payment"><span class="checkbox-icon"></span><span class="checkbox-text">Cổng thanh toán Ngân lượng</span>
                      </label>
                    </li> -->
                  </ul>
                </div>
              </div>
              <div class="order-panel">
                <div class="inner">
                  <h3 class="title">SHIPMENT DETAILS</h3>
                  <div class="order-row">
                    <input class="input" name="name" type="text" placeholder="Name">
                  </div>
                  <div class="order-row">
                    <input class="input" name="address" type="text" placeholder="Delivery Address">
                  </div>
                  <div class="order-row row">
                    <div class="col-6">
                      <div class="selectbox">
                        <select name="tinh" class="tinh">
                          <option value="">Province / City</option>
                          @foreach($za_provinces as $za_province)
                          <option data-id="{{$za_province->id}}" value="{{$za_province->name}}">{{$za_province->name}}</option>
                          @endforeach
                        </select><i class="fa fa-angle-down"></i>
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="selectbox">
                        <select name="huyen" class="huyen">
                          <option value="">District</option>
                        </select><i class="fa fa-angle-down"></i>
                      </div>
                    </div>
                  </div>
                  <div class="order-row">
                    <input class="input" name="email" type="email" placeholder="Example@email.com.vn">
                  </div>
                  <div class="order-row">
                    <input class="input" name="phone" type="number" placeholder="Phone number">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="order-panel">
                <div class="inner">
                  <h3 class="title">INFORMATION LINE<span class="small">({{count($carts)}} product)</span></h3>
                  <ul class="rs-list order-list">
                    @if (count($carts) != 0)
                    <?php $total = 0; $sanpham = ''; ?>
                    @foreach ($carts as $cart)
                    <li class="item">
                      <div class="left"><strong>{{$cart['soluong']}} x&nbsp;</strong><span>{{$cart['product']->title}}</span></div>
                      <div class="right">{{number_format($cart['product']->price * $cart['soluong'] )}} đ</div>
                    </li>
                    <?php $total = $total + $cart['product']->price * $cart['soluong'];
                     $sanpham = $sanpham .', '. $cart['product']->title.' - SL: '.$cart['soluong']; 

                     ?>
                    @endforeach
                    @endif
                    
                    <input type="hidden" name="sanpham" value="{{@$sanpham}}">
                    <input type="hidden" name="thanhtien" value="{{@$total}}">
                    <li class="item">
                      <div class="left"><strong>Shipping cost</strong></div>
                      <div class="right">0 đ</div>
                    </li>
                    <li class="item">
                      <div class="left"><strong>Total</strong></div>
                      <div class="right"><span class="total">{{number_format(@$total)}} đ</span></div>
                    </li>
                  </ul>
                </div>
                <button type="submit" class="rs-btn btn-main btn-order">ORDER</button>
                <p>(Please check your order before placing your order)</p>
              </div>
            </div>
          </form>
        </div>
      </section>
    </main>
    <script>
      jQuery(document).ready(function($) {
        $('.tinh').change(function(event) {
            var name = $(this).val();
            console.log(name);
            $.ajax({
                type: 'POST',
                url: '/country',
                data: {
                    'name': name
                },
                success: function (data) {
                    console.log(data);
                    $('.huyen').html(data);
                    
                }
            });
        });
      });

  jQuery(document).ready(function($) {
    
    var contactform = $('.cart-form').validate({
      highlight: function(element, errorClass, validClass) {
          $(element).removeClass(errorClass);
      },
      rules: {
        name:{
            required: true
        },
        address:{
            required: true
        },
        tinh:{
            required: true
        },
        phone:{
            required: true,
            number:true,
            
        },
        email:{
            required: true,
            email:true
        },
        content:{
            required: true,
        }
      },
      messages: {
        name:{
            required: 'Please enter your name.'
        },
         address:{
            required: 'Please enter your address.'
        },
        tinh:{
            required: 'Please select District'
        },
        phone:{
            required: 'Please enter your phone number',
            number: 'Phone number must be number.',
            
        },
        email:{
            required: 'Please enter your email.',
            email: 'Email is not correct.',
        },
        content:{
            required: 'You have not entered your message.',
        }
      },
    //   submitHandler: function (form) {
    //     var data = {};
    //     $(".cart-form").serializeArray().map(function(x){data[x.name] = x.value;});
    //     $('.block-page-all').addClass('active');
        
    //     $.ajax({
    //         type: 'POST',
    //         url: '/send_cart',
    //         data: data,
    //         dataType: 'json',
    //         error: function(){
    //             $('.block-page-all').removeClass('active');
    //             toastr.error('Error');
    //         },
    //         success: function(result) {
    //             $('.block-page-all').removeClass('active');
    //             switch (result.code) {
    //                 case 200:
    //                     $('.cart-form').find("input, textarea").val("");
    //                     toastr.success('Gửi liên hệ thành công.');
    //                     break;
    //                 default:
    //                     toastr.error('Error');
    //                     break;
    //             }
    //         }
    //     });
    //     return false;
    // }
    });

    
  });
</script>
@endsection