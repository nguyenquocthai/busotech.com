@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main product-detail">
        
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center">
                        <div class="t-dktc">
                            <figure class="img"><img src="/public/theme/img/icon/icon-dktc.png" alt=""></figure>
                            <p class="t-title-dktc" style="color: #744d27">Order Success</p>
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p> -->
                            <!-- <p>Tên tài khoản:ZuumViet</p>
                            <p>Số tài khoản: 1000456788</p>
                            <p>Tại Ngân hàng: Vietcombank Tân Định, TPHCM</p> -->
                            <p>Lưu ý: Staff will confirm and deliver in 1-2 days. You can call the number {{@$info_web['phone2']}} ( 10h - 21h30 ) for faster order confirmation</p>
                        </div>
                    </div>
                    <p><a style="color:#744d27; font-weight: 600;" href="/"><< Back to Home</a></p>
                </div>

            </div>

        </section>
        
        
    </main>
@endsection