<?php foreach (@$products as $key => $product): ?>
  <div class="col-sm-6 col-lg-4 food-item filterDiv {{@$product->id_product_cat}} show">
    <div class="inner">
      <figure class="img hasLink"><img src="/public/img/upload/products/{{@$product->avatar}}" alt=""><a class="link" href="/san-pham/{{@$product->slug}}"></a></figure>
      <div class="content"><a class="name link" href="/san-pham/{{@$product->slug}}">{{@$product->title}}</a>
        <div class="des"><span class="text">
          <?php foreach ($product_cats as $key => $product_cat1): ?>
          @if($product->id_product_cat == $product_cat1->id)
            {{$product_cat1->title}}
          @endif
          <?php endforeach ?>
        </span><span class="price">{{@$product->price}} VNĐ</span></div>
      </div>
    </div>
  </div>
  <?php endforeach ?>