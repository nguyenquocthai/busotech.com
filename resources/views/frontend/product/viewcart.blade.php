@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main product-detail" style="padding-bottom: 0;">
        
        <section class="section community t-all-products t-giohang-all">
            <div class="container">
  
                <h2 class="title-main text-left t-all-product t-title-giohang">Cart</h2>
                <span><?php if(isset($carts)) echo count(@$carts);else echo '0' ?> product</span>
    
            </div>

        </section>
        
        <section>

            <div class="container t-btn-trolai t-giohang-ctr">

                <div class="row">

                    <div class="col-md-12 text-right">
                      <script>
                          document.write('<a href="' + document.referrer + '"><< Trở lại</a>');
                      </script>
                        <!-- <a href="javascript:history.back()"><span><< Trở lại</span></a> -->
                    </div>
                
                </div>

            </div>
            
        </section>

        <section class="t-view-cart"  style="margin-bottom: 0;">
          <div class="container">
            <div class="row">
              @if (count(@$carts) != 0)
              <?php $total=0; ?>
                @foreach (@$carts as $cart)
              <div class="col-xs-12 col-sm-6 col-md-4 product-table-parent">
                <div class="inner product-table__row">
                    <figure class="img"><img src="/public/img/upload/products/{{$cart['product']->avatar}}" alt=""></figure>
                    <div class="content">
                      <h3 class="test-name-top-sp"><a href="/chi-tiet-san-pham/{{$cart['product']->slug}}">{{$cart['product']->title}}</a></h3>
                      
                      <p class="mb-0-1 "><span class="thanhtien">{{number_format($cart['product']->price * $cart['soluong'] )}}</span> đ</p>
                      <input type="hidden" class="price-sp" value="{{$cart['product']->price}}">
                      <div class="quantity js-quantity control-btn">   
                        <button data-id="{{$cart['product']->id}}" type="button" class="rs-btn quantity-btn js-quantityTrigger update_cart" data-type="minus"><i class="fa fa-minus" aria-hidden="true"></i></button>

                        <input class="quantity-val js-quantityVal " type="text" value="{{$cart['soluong']}}">
                        
                        <button data-id="{{$cart['product']->id}}" type="button" class="rs-btn quantity-btn js-quantityTrigger update_cart" data-type="plus"><i class="fa fa-plus" aria-hidden="true"></i></button>    
                      </div>
                      <span style="cursor: pointer;" class="remove_cart" data-id="{{$cart['product']->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i><span>Delete</span></span>
                    </div>
                </div>
              </div>

              <?php $total=$total+$cart['product']->price * $cart['soluong']; ?>
              @endforeach
            

            @endif

              

            </div>
          </div>
        </section>

        <!-- COMMUNITY DEVELOPMENT-->
              
        <section class="cart-shoping" style="margin-bottom: 0;background: white;padding: 0;">
            <div class="container">
              <div class=" align-items-center">
                  <!-- <div class="col-md-4">
                    <div class="t-text-cart">
                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                        <span>GIỎ HÀNG (<?php if(isset($carts)) echo count(@$carts);else echo '0' ?>)</span>
                    </div>
                  </div>
                  <div class="col-md-4 text-center">
                      <p style="margin-bottom: 0;" >Tổng cộng <span class="tongcongtien">{{number_format(@$total)}}</span> đồng</p>
                      <span class="t-text-vat">(Đã bao gồm thuế VAT)</span>
                  </div> -->
                  <div class=" text-right">
                    @if(count(@$carts) > 0)
                    <a href="/show_cart" class="test-name-top t-btn-top muangay" style="color:#601f81">BUY NOW</a>
                    @endif
                  </div>
              </div>
            </div>
        </section>
        
    </main>
    <style>
      .muangay:hover{
        color: white !important;
      }
    </style>
@endsection