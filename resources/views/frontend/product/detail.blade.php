@extends('frontend.layouts.main')
@section('content')
<main class="main">
      <!-- BANNER PAGE-->
      <section class="banner banner-page embed-responsive" style="background-image: url(/public/img/upload/banners/{{$banner->avatar}});">
        <div class="caption">Product Detail</div>
      </section>
      <!-- PRODUCT DETAIL-->
      <section class="section product-detail">
        <div class="container">
          <div class="row">
            <div class="left">
              <div class="slider-large js-sliderSync1">
                <div class="item">
                  <div class="product-item">
                    <figure class="img embed-responsive"><img src="/public/img/upload/products/{{$productdt->avatar}}" alt=""><span class="label label-green">New</span></figure>
                  </div>
                </div>
                <?php foreach (@$item_albums as $key => $item_album): ?>
                  <div class="item">
                    <div class="product-item">
                      <figure class="img embed-responsive"><img src="/public/img/upload/item_albums/{{$item_album->name}}" alt=""><span class="label label-green">New</span></figure>
                    </div>
                  </div>
                <?php endforeach ?>
              </div>
              <div class="slider-nav js-sliderSync2">
                <div class="item">
                  <figure class="img embed-responsive"><img src="/public/img/upload/products/{{$productdt->avatar}}" alt=""></figure>
                </div>
                <?php foreach (@$item_albums as $key => $item_album): ?>
                  <div class="item">
                  <figure class="img embed-responsive"><img src="/public/img/upload/item_albums/{{$item_album->name}}" alt=""></figure>
                </div>
                <?php endforeach ?>
              </div>
            </div>
            <div class="right">
              <h1 class="name">{{$productdt->title}}</h1>
              <p class="price">{{number_format($productdt->price)}} VNĐ</p>
              <p class="content">{{$productdt->summary}}</p>
              <form class="product-detail__form form-muahang" action="#!" target="_blank">
                <div class="quantity js-quantity">
                  <button class="rs-btn quantity-button js-quantityTrigger" type="button" data-type="plus">+</button>
                  <button class="rs-btn quantity-button js-quantityTrigger" type="button" data-type="minus">-</button>
                  <input class="quantity-value js-quantityVal quantity-val" type="number" value="1">
                </div>
                <button class="rs-btn btn-read-more add_cart" type="button" data-id="{{@$productdt->id}}">ADD TO CART</button>
              </form>
              <p><strong>CLASSIFY:&nbsp;</strong><span>{{@$product_cat->title}}</span></p>
              <p><strong>PHONE NUMBER:&nbsp;</strong><span class="d-inline-block">{{@$info_web['phone']}}</span></p>
              <p class="support"><strong>DIRECT SUPPORT:&nbsp;</strong><a class="link" href="{{@$info_web['facebook']}}"><i class="fa fa-facebook"></i></a><a class="link" href="{{@$info_web['zalo']}}"><i class="fa fa-zalo"></i></a></p>
              <div class="js-tabParent">
                <ul class="rs-list list js-tabNav">
                  <li class="item">
                    <button class="rs-btn button js-tabLink active" data-target="#mo-ta">SUMARY</button>
                  </li>
                  <li class="item">
                    <button class="rs-btn button js-tabLink" data-target="#thong-tin-san-pham">PRODUCT INFORMATION</button>
                  </li>
                  <!-- <li class="item">
                    <button class="rs-btn button js-tabLink" data-target="#review">REVIEW</button>
                  </li> -->
                </ul>
                <div class="list-content js-tabContent">
                  <div class="tab-panel fade active" id="mo-ta">{!!@$productdt->content!!}</div>
                  <div class="tab-panel fade" id="thong-tin-san-pham">{!!@$productdt->thongtinsp!!}</div>
                  <!-- <div class="tab-panel fade" id="review">Đang cập nhật...</div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- RELATED PRODUCTS-->
      <section class="section product-related">
        <div class="container">
          <h2 class="title">Other products</h2>
          <div class="product-related__slider js-sliderProductRelated">
            <?php foreach (@$products as $key => $product): ?>
              <div class="item product-item">
                <div class="inner">
                  <figure class="img embed-responsive hasLink"><img src="/public/img/upload/products/{{@$product->avatar}}" alt=""><a class="link" href="/san-pham/{{$product->slug}}"></a>
                  </figure>
                  <div class="content">
                    <h3 class="mb-0"><a class="name" href="/san-pham/{{$product->slug}}">{{$product->title}}</a></h3>
                    <p class="price">{{number_format($product->price)}} VNĐ</p>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </div>
        </div>
      </section>
    </main>
@endsection