@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- BANNER INDEX-->
      <section class="banner banner-page embed-responsive" style="background-image: url(/public/img/upload/banners/{{$banner->avatar}});">
        <div class="caption">Picture</div>
      </section>
      <!-- ALBUMS-->
      <section class="section album album-page">
        <div class="container album-container js-filter">
          <div class="filter-list">
            <a href="/hinh-anh" class="danh-muc-hinh" title=""><button class="rs-btn filter-item js-filterTrigger" type="button" data-target="">All</button></a>
            <?php foreach ($album_cats as $key => $album_cat): ?>
              <a href="/danh-muc-hinh-anh/{{$album_cat->slug}}" class="danh-muc-hinh" title=""><button class="rs-btn filter-item js-filterTrigger  <?php if($album_cat->id == $album_catac->id) echo 'active'; ?>" type="button" data-target="">{{$album_cat->title}}</button></a>
            <?php endforeach ?>
          </div>
          <div class="row">
            <?php foreach ($albums as $key => $album): ?>
              <div class="col-6 col-sm-4 album-item">
                <div class="inner hasLink">
                  <figure class="img"><img src="/public/img/upload/albums/{{$album->avatar}}" alt=""></figure>
                  <ul class="rs-list list">
                    <li class="item">{{$album->title}}</li>
                    <li class="item">{{$album->summary}}</li>
                  </ul><a class="link" href="/hinh-anh/{{$album->slug}}"></a>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <div class="row">
            {{$albums->links()}}
          </div>
        </div>
      </section>
    </main>
@endsection