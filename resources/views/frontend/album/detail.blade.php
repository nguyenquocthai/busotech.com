@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main"> 
      <!-- BANNER INDEX-->
      <section class="banner banner-page embed-responsive" style="background-image: url(/public/img/upload/banners/{{$banner->avatar}});">
        <div class="caption">Image details</div>
      </section>
      <!-- ALBUMS-->
      <section class="section album album-page">
        <div class="container album-container">
          <div class="filter-list">
            <a href="/hinh-anh" class="danh-muc-hinh" title=""><button class="rs-btn filter-item js-filterTrigger" type="button" data-target="">All</button></a>
            <?php foreach ($album_cats as $key => $album_cat): ?>
              <a href="/danh-muc-hinh-anh/{{$album_cat->slug}}" class="danh-muc-hinh" title=""><button class="rs-btn filter-item js-filterTrigger <?php if($album_cat->id == $albumdt->id_album_cat) echo 'active'; ?>" type="button" data-target="">{{$album_cat->title}}</button></a>
            <?php endforeach ?>
          </div>
          <div class="row zoom-gallery js-zoomGallery">
            <?php foreach ($item_albums as $key => $item_album): ?>
              <div class="col-6 col-sm-4 album-item">
                <div class="inner hasLink"><a class="img embed-responsive" href="/public/img/upload/item_albums/{{$item_album->name}}" data-source="/public/img/upload/item_albums/{{$item_album->name}}" title=""><img src="/public/img/upload/item_albums/{{$item_album->name}}" alt=""></a></div>
              </div>
            <?php endforeach ?>
          </div>
        </div>
      </section>
    </main>
@endsection