@extends('frontend.layouts.main') 
@section('content')
<!-- MAIN CONTENT-->
    <!-- MAIN CONTENT-->
    <main class="main">
      <!-- BANNER-->
      <section class="section banner-page recruitment-banner" style="background-image: url(/public/theme/img/bg-tuyen-dung.jpg);">
        <div class="container">
          <div class="title-block text-center">
            <h2 class="title-main"><span class="d-inline-block">Buso recruits</span></h2>
            <h3 class="title-sub animate-fadeUp">You love the youthful working environment and the Startup spirit?&nbsp;<span class="d-none d-md-block"></span>Join Buso and explore now!</h3><a class="rs-btn btn-white animate-fadeUp js-smoothScroll" href="#recruitment-contact">Recruitment</a>
          </div>
        </div>
      </section>
      <!-- BREADCRUMB-->
      <section class="breadcrumbk">
        <div class="container">
          <ul class="rs-list breadcrumbk-list">
            <li class="item"><a class="link" href="/tuyen-dung">Recruitment page</a></li>
            <li class="item active">{{$recruitmentdt->title}}</li>
          </ul>
        </div>
      </section>
      <section class="recruitment-detail">
        <article class="container container-2">
          <div class="title-block">
            <h1 class="title-main">{{$recruitmentdt->title}}</h1>
          </div>
          <div class="row">
            <div class="col-lg-8 recruitment-detail__content">
              {!!$recruitmentdt->content!!}
            </div>
            <aside class="col-lg-4 recruitment-aside">
              <div class="inner" style="white-space: pre-line;">{!!$recruitmentdt->summary!!}</div>
            </aside>
          </div>
        </article>
      </section>
      <!-- RECRUITING-->
      <section class="section recruiting recruiting-2">
        <div class="container">
          <h2 class="recruiting__title">Other recruitment news</h2>
          <div class="recruiting__list">
            <?php foreach ($recruitments as $key => $recruitment): ?>
            <?php if ($recruitment->id <> $recruitmentdt->id): ?>
            <div class="recruiting__item animate-fadeUp">
              <div class="item title"><a class="link" href="/tuyen-dung/{{$recruitment->slug}}">{{$recruitment->title}}</a></div>
              <div class="item type">{{$recruitment->type}}</div>
              <div class="item ctrl"><a class="rs-btn btn-white js-smoothScroll" href="#recruitment-contact">Recruitment</a></div>
            </div>
            <?php endif ?>
            <?php endforeach ?>
          </div>
        </div>
      </section>
      <!-- CONTACT-->
      <section class="section recruitment-contact" id="recruitment-contact">
        <div class="recruitment-contact__img animate-fadeUp"><img src="/public/theme/img/recruitment-form.jpg" alt=""/></div>
        <div class="recruitment-contact__content">
          <div class="inner">
            <div class="title-block text-center">
              <h2 class="title-main"><span class="d-inline-block">Do you want to accompany&nbsp;<span class="d-none d-xl-block"></span>Buso?</span></h2>
            </div>
            <form class="login-form form-tuyen-dung" action="#!">
              @csrf
              <p class="login-form__row animate-fadeUp">
                <input class="input" name="name" type="text" placeholder="Full name"/>
              </p>
              <p class="login-form__row animate-fadeUp">
                <input class="input" name="email" type="email" placeholder="Email"/>
              </p>
              <p class="login-form__row animate-fadeUp">
                <input class="input" name="tel" type="number" placeholder="Phone number"/>
              </p>
              <p class="login-form__row animate-fadeUp">
                <select class="input" name="vitri" id="" placeholder="Select a position to apply">
                  <option disabled selected value>Select a position to apply</option>
                  <?php foreach ($recruitments as $key => $recruitment): ?>
                  <option value="{{$recruitment->title}}">{{$recruitment->title}}</option>
                  <?php endforeach ?>
                </select>
              </p>
              <p class="login-form__row animate-fadeUp">
                <p style="margin-bottom: 0;text-align: left;font-weight: 600;">Select your CV: </p>
                  <input type="file" name="file" id="file_tuyendung" placeholder="Gửi CV" />
                
              </p>
              <p class="login-form__row text-center animate-fadeUp">
                <button type="submit" class="rs-btn btn-gradient">Send</button>
              </p>
            </form>
          </div>
        </div>
      </section>
      <!-- SECTION SIX-->
      <section class="section section-six"><span class="circle circle--striped"></span>
        <div class="container"><span class="triangle triangle--light"></span>
          <div class="subscribe-box">
            <div class="title-block text-center">
              <h2 class="title-main"><span>Buso is always ready to &nbsp;</span><span class="d-inline-block">advise you</span></h2>
              <h3 class="title-des">Buso always accompanies you to bring total solutions to<br class="d-none d-lg-block"/>drive more sales for your business</h3>
            </div>
            <form class="subscribe-form" id="subscribe" action="">
              <input class="input" name="email" placeholder="Your email"/>
              <button type="submit" class="rs-btn btn-gradient">Subscribe</button>
            </form>
            <div class="contact-box">
              <div class="item">
                <figure class="img"><img src="/public/theme/img/icons/phone.png" alt=""/></figure><a class="link" href="tel:{{$info_web['phone']}}">{{$info_web['phone']}}</a>
              </div>
              <div class="item">
                <figure class="img"><img src="/public/theme/img/icons/envelope.png" alt=""/></figure><a class="link" href="mailto:{{$info_web['email']}}">{{$info_web['email']}}</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <script>
  jQuery(document).ready(function($) {
    
    var formtuyendung = $('.form-tuyen-dung').validate({
      highlight: function(element, errorClass, validClass) {
          $(element).removeClass(errorClass);
      },
      rules: {
        email:{
            required: true,
            email:true
        },
        name:{
            required: true
        },
        tel:{
            required: true,
            number: true
        },
        vitri:{
            required: true
        },
        file:{
            required: true
        }
      },
      messages: {
        email:{
            required: 'Please enter your email.',
            email: 'Your email must be in the correct email format.',
        },
        name:{
            required: 'Please enter your Full name'
        },
        tel:{
            required: 'Please enter your phone number',
            number: 'Phone number must be number'
        },
        vitri:{
            required: 'You have not selected a position to apply'
        },
        file:{
            required: 'You have not selected attachments'
        }
      },
      submitHandler: function (form) {
        var data = {};
        var fd = new FormData();

        // var files = [];
        //         if ($("#file_tuyendung").length != 0) {
        //             if ($("#file_tuyendung").get(0).files[0])
        //                 files = files.concat($("#file_tuyendung").get(0).files[0]);
        //         }
        // Attach file
        var file = $("#file_tuyendung").get(0).files[0];
        fd.append('file_tuyendung', file);

        $(".form-tuyen-dung").serializeArray().map(function(x){data[x.name] = x.value;});
        fd.append('params', JSON.stringify(data));
        $.ajax({
            type: 'POST',
            url: '/add_tuyendung',
            data: fd,
            dataType: 'json',
            contentType: false, 
            processData: false,
            error: function(){
                
                toastr.error('Error');
            },
            success: function(result) {
                
                switch (result.code) {
                    case 200:
                        $('.form-tuyen-dung').find("input, textarea").val("");
                        toastr.success('Gửi liên hệ thành công.');
                        break;
                    default:
                        toastr.error('Error');
                        break;
                }
            }
        });
        return false;
    }
    });
    
    
  });
</script>
@endsection
