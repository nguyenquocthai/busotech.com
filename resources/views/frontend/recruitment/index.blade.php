@extends('frontend.layouts.main') 
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- BANNER-->
      <section class="section banner-page recruitment-banner" style="background-image: url(/public/theme/img/bg-tuyen-dung.jpg);">
        <div class="container">
          <div class="title-block text-center">
            <h2 class="title-main"><span class="d-inline-block">Buso recruits</span></h2>
            <h3 class="title-sub animate-fadeUp">You love the youthful working environment and the Startup spirit?&nbsp;<span class="d-none d-md-block"></span>Join Buso and explore now!</h3><a class="rs-btn btn-white animate-fadeUp js-smoothScroll" href="#recruitment-contact">Recruitment</a>
          </div>
        </div>
      </section>
      <!-- WELCOME-->
      <section class="section recruitment-welcome">
        <div class="container">
          <div class="title-block text-center">
            <h2 class="title-main">Welcome to Buso</h2>
            <h3 class="title-sub animate-fadeUp">Buso is a company specializing in providing effective business solutions for SMES businesses to&nbsp;<span class="d-none d-xl-block"></span>boost business growth and promote successful brands.</h3>
          </div>
        </div>
        <div class="container container-2">
          <div class="row justify-content-center">
            <?php foreach ($recruitments as $key => $recruitment): ?>
              <?php if ($recruitment->hight_flg == 1): ?>
              <div class="col-6 col-md-4 recruitment-welcome__item animate-fadeUp">
                <div class="inner">
                  <a href="/tuyen-dung/{{$recruitment->slug}}"><figure class="img mb-0"><img src="/public/img/upload/recruitments/{{$recruitment->avatar}}" alt="{{$recruitment->title}}"/></figure></a>
                </div>
              </div>
              <?php endif ?>
            <?php endforeach ?>
          </div>
        </div>
      </section>
      <!-- RECRUITING-->
      <section class="section recruiting" style="background-image: url(/public/theme/img/bg-tuyen-dung-2.jpg);">
        <div class="container">
          <div class="title-block text-center">
            <h2 class="title-main">Buso is recruiting</h2>
          </div>
          <div class="recruiting__list">
            <?php foreach ($departments as $key => $department): ?>
              <?php foreach ($recruitments as $key => $recruitment): ?>
                <?php if ($department->id == $recruitment->id_department): ?>
                
                <div class="recruiting__item animate-fadeUp">
                  <div class="item title"><a class="link" href="/tuyen-dung/{{$recruitment->slug}}">{{$recruitment->title}}</a></div>
                  <div class="item type">{{$department->title}}</div>
                  <div class="item ctrl"><a class="rs-btn btn-white js-smoothScroll" href="#recruitment-contact">Recruitment</a></div>
                </div>
                
                <?php endif ?>
              <?php endforeach ?>
            <?php endforeach ?>
            
          </div>
        </div>
      </section>
      <!-- BENEFIT-->
      <section class="section recruitment-benefit">
        <div class="container">
          <div class="title-block text-center">
            <h2 class="title-main">Benefit</h2>
          </div>
        </div>
        <div class="container container-2">
          <div class="row">
            <?php foreach ($recruitment_benefits as $key => $recruitment_benefit): ?>
            <div class="col-md-6 recruitment-benefit__item animate-fadeUp">
              <div class="inner">
                <figure class="img"><img src="/public/img/upload/recruitment_benefits/{{$recruitment_benefit->avatar}}" alt="{{$recruitment_benefit->title}}"/></figure>
                <div class="content">
                  <h3 class="title"><span class="d-inline-block">{{$recruitment_benefit->title}}</span></h3>
                  <div class="summary">{!!$recruitment_benefit->content!!}</div>
                </div>
              </div>
            </div>
            <?php endforeach ?>
          </div>
        </div>
      </section>
      <!-- CONTACT-->
      <section class="section recruitment-contact" id="recruitment-contact">
        <div class="recruitment-contact__img animate-fadeUp"><img src="/public/theme/img/recruitment-form.jpg" alt=""/></div>
        <div class="recruitment-contact__content">
          <div class="inner">
            <div class="title-block text-center">
              <h2 class="title-main"><span class="d-inline-block">Do you want to accompany &nbsp;<span class="d-none d-xl-block"></span>Buso?</span></h2>
            </div>
            <form class="login-form form-tuyen-dung" action="#!">
              @csrf
              <p class="login-form__row animate-fadeUp">
                <input class="input" name="name" type="text" placeholder="Full name"/>
              </p>
              <p class="login-form__row animate-fadeUp">
                <input class="input" name="email" type="email" placeholder="Email"/>
              </p>
              <p class="login-form__row animate-fadeUp">
                <input class="input" name="tel" type="number" placeholder="Phone number"/>
              </p>
              <p class="login-form__row animate-fadeUp">
                <select class="input" name="vitri" id="" placeholder="Select a position to apply">
                  <option disabled selected value>Select a position to apply</option>
                  <?php foreach ($recruitments as $key => $recruitment): ?>
                  <option value="{{$recruitment->title}}">{{$recruitment->title}}</option>
                  <?php endforeach ?>
                </select>
              </p>
              <p class="login-form__row animate-fadeUp">
                <p style="margin-bottom: 0;text-align: left;font-weight: 600;">Select your CV: </p>
                  <input type="file" name="file" id="file_tuyendung" placeholder="Send CV" />
                
              </p>
              <p class="login-form__row text-center animate-fadeUp">
                <button type="submit" class="rs-btn btn-gradient">Send</button>
              </p>
            </form>
          </div>
        </div>
      </section>
    </main>
    <script>
  jQuery(document).ready(function($) {
    
    var formtuyendung = $('.form-tuyen-dung').validate({
      highlight: function(element, errorClass, validClass) {
          $(element).removeClass(errorClass);
      },
      rules: {
        email:{
            required: true,
            email:true
        },
        name:{
            required: true
        },
        tel:{
            required: true,
            number: true
        },
        vitri:{
            required: true
        },
        file:{
            required: true
        }
      },
      messages: {
        email:{
            required: 'Please enter your email.',
            email: 'Your email must be in the correct email format.',
        },
        name:{
            required: 'Please enter your Full name'
        },
        tel:{
            required: 'Please enter your phone number',
            number: 'Phone number must be number'
        },
        vitri:{
            required: 'You have not selected a position to apply'
        },
        file:{
            required: 'You have not selected attachments'
        }
      },
      submitHandler: function (form) {
        var data = {};
        var fd = new FormData();

        // var files = [];
        //         if ($("#file_tuyendung").length != 0) {
        //             if ($("#file_tuyendung").get(0).files[0])
        //                 files = files.concat($("#file_tuyendung").get(0).files[0]);
        //         }
        // Attach file
        var file = $("#file_tuyendung").get(0).files[0];
        fd.append('file_tuyendung', file);

        $(".form-tuyen-dung").serializeArray().map(function(x){data[x.name] = x.value;});
        fd.append('params', JSON.stringify(data));
        $.ajax({
            type: 'POST',
            url: '/add_tuyendung',
            data: fd,
            dataType: 'json',
            contentType: false, 
            processData: false,
            error: function(){
                
                toastr.error('Error');
            },
            success: function(result) {
                
                switch (result.code) {
                    case 200:
                        $('.form-tuyen-dung').find("input, textarea").val("");
                        toastr.success('Submit successful application information.');
                        break;
                    default:
                        toastr.error('Error');
                        break;
                }
            }
        });
        return false;
    }
    });
    
    
  });
</script>
@endsection
