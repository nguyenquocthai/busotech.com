<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		@include('frontend.elements.head')
    </head>

    <body >
		@include('frontend.elements.header')
        @yield('content')
        @include('frontend.elements.footer')
        @include('frontend.elements._jstheme')
    </body>
</html>