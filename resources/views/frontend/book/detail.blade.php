@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<div class="row banner">
  <div class="slide">
    <div class="inner"><img src="/public/img/upload/banners/{{$banner->avatar}}" width="1160" height="494" /></div>
    <div class="title">
        <h2>{{$bookdt->title}}</h2>
        <h4>{{$bookdt->summary}}</h4>
    </div>
  </div>
</div>
<div class="row">
        <div class="content small-12 medium-9 columns">
            <!-- <h2>{{$bookdt->title}}</h2> -->
            <img src="/public/img/upload/books/{{$bookdt->avatar}}" alt="{{$bookdt->title}}"
                class="small-6 medium-5 large-4 columns" style="float:right;" />
            
            <p>
                <strong>{{@$langs['tac-gia']}}: </strong>{{$bookdt->author}}<br />
                <strong>{{@$langs['danh-gia-boi']}}: </strong>{{$bookdt->reviewby}}
            </p>
            <p><i>{{$bookdt->summary}}</i></p>
            {!!$bookdt->content!!}

            <p><a href="{{$bookdt->link}}"
                    target="_blank" class="button alert">{{@$langs['mua-hang-tren-amazon']}}</a></p>
            <p><a href="/book" class="button secondary" style="margin-top:3rem;">
                    <<&nbsp;&nbsp;&nbsp;{{@$langs['tro-ve-trang-danh-gia-sach']}}</a>
            </p>
        </div>
        <div class="small-12 medium-3 columns sidebar">
            <div class="small-12 columns">
                <h4>{{@$langs['su-kien-sap-toi']}}</h4>
                <ul class="eventside">
                  <?php foreach ($events as $key => $event): ?>
                      <li>
                          <strong><a href="/event/{{$event->slug}}">{{$event->title}}</a></strong><br />
                      </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="small-12 columns">
                <h4>{{@$langs['popular-book-reviews']}}</h4>
                <ul class="row medium-collapse large-uncollapse">
                    <?php foreach ($booknbs as $key => $booknb): ?>
                    <li class="small-4 medium-6 large-6 columns">
                        <a href="/book/{{$booknb->slug}}"><img
                                src="/public/img/upload/books/{{$booknb->avatar}}"
                                alt="{{$booknb->title}}" /></a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>
@endsection