@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
<div class="row banner">
  <div class="slide">
    <div class="inner"><img src="/public/img/upload/banners/{{$banner->avatar}}" width="1160" height="494" /></div>
    <div class="title">
        <h2>{{$banner->title}}</h2>
        <h4>{{$banner->summary}}</h4>
    </div>
  </div>
</div>
<div class="row">
        <div class="content small-12 medium-9 columns">

            <?php foreach ($books as $key => $book): ?>
            <div class="row">
                <div class="small-4 medium-4 large-3 columns">
                    <a href="/book/{{$book->slug}}"><img
                            src="/public/img/upload/books/{{$book->avatar}}"
                            alt="{{$book->title}}" /></a>
                </div>

                <div class="post small-8 medium-8 large-9 columns">
                    <h3 class="title"><a href="/book/{{$book->slug}}">{{$book->title}}</a>
                    </h3>
                    <div class="entry">
                        <p>
                            {{$book->summary}}
                        </p>
                    </div>
                    <p class="postmeta">
                        <span class="links">
                            <a href="/book/{{$book->slug}}" class="read more">{{@$langs['doc-bai-danh-gia']}}</a>
                        </span>
                    </p>
                </div>
            </div>
            <?php endforeach ?>
            <div class='paging pagination'>
                {{$books->links()}}
            </div>
        </div>
        <div class="small-12 medium-3 columns sidebar">
            <div class="small-12 columns">
                <h4>{{@$langs['su-kien-sap-toi']}}</h4>
                <ul class="eventside">
                  <?php foreach ($events as $key => $event): ?>
                      <li>
                          <strong><a href="/event/{{$event->slug}}">{{$event->title}}</a></strong><br />
                      </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="small-12 columns">
                <h4>{{@$langs['popular-book-reviews']}}</h4>
                <ul class="row medium-collapse large-uncollapse">
                    <?php foreach ($booknbs as $key => $booknb): ?>
                    <li class="small-4 medium-6 large-6 columns">
                        <a href="/book/{{$booknb->slug}}"><img
                                src="/public/img/upload/books/{{$booknb->avatar}}"
                                alt="{{$booknb->title}}" /></a>
                    </li>
                    <?php endforeach ?>

                </ul>
            </div>
        </div>
    </div>
@endsection