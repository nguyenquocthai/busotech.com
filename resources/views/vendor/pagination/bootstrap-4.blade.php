@if ($paginator->hasPages())
    <nav>
        <ul class=" rs-list paginationk-list">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class=" item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="page-link paginationk-link" aria-hidden="true"></span>
                </li>
            @else
                <li class=" item">
                    <a class="page-link paginationk-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"></a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class=" item disabled" aria-disabled="true"><span class="page-link paginationk-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class=" item active" aria-current="page"><span class="page-link paginationk-link">{{ $page }}</span></li>
                        @else
                            <li class=" item"><a class="page-link paginationk-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class=" item">
                    <a class="page-link paginationk-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"></a>
                </li>
            @else
                <li class=" item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="page-link paginationk-link" aria-hidden="true"></span>
                </li>
            @endif
        </ul>
    </nav>
@endif
