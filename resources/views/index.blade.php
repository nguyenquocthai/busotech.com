@extends('frontend.layouts.main')
@section('content')
<!-- MAIN CONTENT-->
    <main class="main">
      <!-- SERVICES-->
      <section class="service">
        <div class="container">
          <h2 class="title-main">Các loại dịch vụ</h2>
          <div class="row">
            <div class="col-6 col-lg-4 col-xl-3 service-item">
              <div class="inner">
                <div class="inner-wrapper">
                  <figure class="img img-zuum-bike"><img src="/public/theme/img/zuum-bike.png" alt=""/></figure>
                  <h3 class="title">ZuumBike</h3>
                  <div class="price"><span class="price-text">Giá cước 2km&nbsp;<br/>đầu tiên</span><span class="price-value">11.000 đ</span></div>
                  <div class="price"><span class="price-text">Giá cước mỗi Km&nbsp;<br/>tiếp theo</span><span class="price-value">3.200 đ</span></div>
                </div>
              </div>
            </div>
            <div class="col-6 col-lg-4 col-xl-3 service-item">
              <div class="inner">
                <div class="inner-wrapper">
                  <figure class="img img-zuum-bike-plus"><img src="/public/theme/img/zuum-bike-plus.png" alt=""/></figure>
                  <h3 class="title">ZuumBike+</h3>
                  <div class="price"><span class="price-text">Giá cước 2km&nbsp;<br/>đầu tiên</span><span class="price-value">14.000 đ</span></div>
                  <div class="price"><span class="price-text">Giá cước mỗi Km&nbsp;<br/>tiếp theo</span><span class="price-value">4.200 đ</span></div>
                </div>
              </div>
            </div>
            <div class="col-6 col-lg-4 col-xl-3 service-item">
              <div class="inner">
                <div class="inner-wrapper">
                  <figure class="img img-zuum-car-4-sits"><img src="/public/theme/img/zuum-car-4-sits.png" alt=""/></figure>
                  <h3 class="title">ZuumCar (4 chỗ)</h3>
                  <div class="price"><span class="price-text">Giá cước 2km&nbsp;<br/>đầu tiên</span><span class="price-value">24.000 đ</span></div>
                  <div class="price"><span class="price-text">Giá cước mỗi Km&nbsp;<br/>tiếp theo</span><span class="price-value">8.900 đ</span></div>
                </div>
              </div>
            </div>
            <div class="col-6 col-lg-4 col-xl-3 service-item">
              <div class="inner">
                <div class="inner-wrapper">
                  <figure class="img img-zuum-car-7-sits"><img src="/public/theme/img/zuum-car-7-sits.png" alt=""/></figure>
                  <h3 class="title">ZuumCar (7 chỗ)</h3>
                  <div class="price"><span class="price-text">Giá cước 2km&nbsp;<br/>đầu tiên</span><span class="price-value">24.000 đ</span></div>
                  <div class="price"><span class="price-text">Giá cước mỗi Km&nbsp;<br/>tiếp theo</span><span class="price-value">11.400 đ</span></div>
                </div>
              </div>
            </div>
            <div class="col-6 col-lg-4 col-xl-3 service-item">
              <div class="inner">
                <div class="inner-wrapper">
                  <figure class="img img-zuum-car-lux"><img src="/public/theme/img/zuum-car-lux.png" alt=""/></figure>
                  <h3 class="title">ZuumLux</h3>
                  <div class="price"><span class="price-text">Giá cước 2km&nbsp;<br/>đầu tiên</span><span class="price-value">60.000 đ</span></div>
                  <div class="price"><span class="price-text">Giá cước mỗi Km&nbsp;<br/>tiếp theo</span><span class="price-value">20.400 đ</span></div>
                </div>
              </div>
            </div>
            <div class="col-6 col-lg-4 col-xl-3 service-item">
              <div class="inner">
                <div class="inner-wrapper">
                  <figure class="img img-zuum-car-supper-lux"><img src="/public/theme/img/zuum-car-supper-lux.png" alt=""/></figure>
                  <h3 class="title">ZuumSuplux</h3>
                  <div class="price"><span class="price-text">Giá cước 2km&nbsp;<br/>đầu tiên</span><span class="price-value">150.000 đ</span></div>
                  <div class="price"><span class="price-text">Giá cước mỗi Km&nbsp;<br/>tiếp theo</span><span class="price-value">40.400 đ</span></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- PROMOTIONS-->
      <section class="promotion">
        <div class="container">
          <h2 class="title-main line-right"><span>Chương trình khuyến mãi</span></h2>
          <div class="row">
            <?php foreach (@$khuyenmais as $key => $khuyenmai): ?>
            <div class="col-6 col-lg-4 promotion-item">
              <div class="inner embed-responsive hasLink" style="background-image: url(/public/img/upload/blogs/{{$khuyenmai->avatar}});">
                <h4 class="title">{{$khuyenmai->title}}</h4><a class="link" href="/chi-tiet-tin/{{$khuyenmai->slug}}" title="{{$khuyenmai->title}}"></a>
              </div>
            </div>
            <?php endforeach ?>
            
            <div class="col-6 promotion-item">
              <div class="inner embed-responsive"><a class="rs-btn more" href="/danh-muc-tin-tuc/{{$khuyenmai_cat->slug}}">Xem thêm&nbsp;<span>>></span></a></div>
            </div>
          </div>
        </div>
      </section>
      <!-- COMMUNITY-->
      <section class="community">
        <div class="container">
          <h2 class="title-main line-right"><span>Cộng đồng ZuumViet</span></h2>
          <div class="video-box embed-responsive">
            {!!$about->content!!}
          </div>
        </div>
      </section>
      <!-- BENEFIT-->
      <section class="benefit">
        <div class="container">
          <h2 class="title-main line-right"><span>Vì sao chọn Zuumviet</span></h2>
          <div class="benefit-row">
            <div class="benefit-col">
              <?php foreach ($dichvus as $key => $dichvu): ?>
                <?php if ($key < 2): ?>
                  <div class="item">
                    <figure class="img img-smart-phone"><img src="/public/img/upload/dichvus/{{@$dichvu->avatar}}" alt=""/></figure>
                    <div class="content">{{$dichvu->title}}</div>
                  </div>
                <?php endif ?>
              <?php endforeach ?>
            </div>
            <div class="benefit-col">
              <?php foreach ($dichvus as $key => $dichvu): ?>
                <?php if ($key >= 2 and $key < 4): ?>
                  <div class="item">
                    <figure class="img img-smart-phone"><img src="/public/img/upload/dichvus/{{@$dichvu->avatar}}" alt=""/></figure>
                    <div class="content">{{$dichvu->title}}</div>
                  </div>
                <?php endif ?>
              <?php endforeach ?>
            </div>
            <figure class="benefit-img"><img src="/public/theme/img/service-phone.png" alt=""/></figure>
          </div>
        </div>
      </section>
      <!-- DOWNLOAD APPLICATION-->
      <section class="download">
        <div class="container">
          <h2 class="title-main">Tải ứng dụng ZuumViet ngay</h2>
          <div class="download-row">
            <figure class="download-img download-logo"><img src="/public/theme/img/logo-zuum-app.png" alt=""/></figure>
            <figure class="download-img download-app">
              <a href="{{@$info_web['linkchplay']}}" title=""><img src="/public/theme/img/google-play.png" alt=""/></a>
              <a href="{{@$info_web['linkios']}}" title=""><img src="/public/theme/img/app-store.png" alt=""/></a></figure>
            <figure class="download-img download-qr-code"><img src="/public/theme/img/qr-code.png" alt=""/></figure>
          </div>
        </div>
      </section>
    </main>
@endsection