<?php $data_json = json_encode($data);?>
<div class="row">

    <div class="col-sm-12">

        <div class="portlet light bordered">

            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Thông tin</span>
                </div>
                <div class="actions box_lang_muti">
                    <div class="btn-group">
                        <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> <span class="name_mod">Vietnam</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a data-lang="vn" class="pick_lang_muti" href="javascript:;">Vietnam</a>
                            </li>
                            <li>
                                <a data-lang="en" class="pick_lang_muti" href="javascript:;">English</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="portlet-body form">
                <div class="form-group">
                    <label class="lang_label">
                        <span>Tiêu đề</span>
                        <img class="lang_pick active" data-lang="vn" src="/public/img/vn.png" alt="">
                        <img class="lang_pick" data-lang="en" src="/public/img/en.png" alt="">
                    </label>
                    
                    <input data-lang="vn" type="text" placeholder="Nhập tên" class="form-control value_lang active" name="title">
                    <input name="title_en" data-lang="en" type="text" placeholder="Tiêu đề (English)" class="form-control value_lang" >
                </div>

                
                <div class="form-group">
                    <label class="next-label">Trạng thái</label>
                    <select class="form-control" name="status">
                        <option value="0">Kích hoạt</option>
                        <option value="1">Tạm dừng</option>
                    </select>
                </div>
            </div>

        </div>

    </div>

    <div class="col-sm-4 hidden">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-settings font-green-sharp"></i>
                    <span class="caption-subject bold uppercase">Hình đại diện</span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-group">
                    <div class="file_upload_box">
                        <input type="file" class="file_image">
                        <img class="image_review" src="/public/img/no-image.png" alt="...">
                    </div>
                </div>

            </div>
        </div>

        

    </div>
</div>

<div class="form-group hidden">
    <label class="next-label">Thứ tự</label>
    <div class="load_position">
        <div class="position_box" data-id="">
            @if (count($positions) != 0)
                @foreach ($positions as $key => $position)
                <span style="margin-bottom: 3px" data-value="{{$position->position}}" data-id="{{$position->id}}" class="item-position btn btn-icon-only grey-cascade @if($data->position == $position->position) active @endif">{{$key+1}}</span>
                @endforeach
            @else
            @endif
        </div>
    </div>
</div>

<script>
    // add value to input
    var data_json = {!!$data_json!!};
    $('#form_edit input').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]);
    });

    $('#form_edit select').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).val(data_json[name]).trigger('change');
        $(this).attr('data-select', data_json[name]);
    });

    $('#form_edit textarea').each(function(index, el) {
        var name = $(this).attr('name');
        $(this).html(data_json[name]);
    });

    $('#form_edit .image_review').attr('src',"{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}")
    // add value to input
</script>