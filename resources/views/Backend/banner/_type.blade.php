<?php
    $class_name = '';
    $text = '';

    switch ($type) {
        case 0:
            $class_name = 'green';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Trang chủ';
            break;

        case 1:
            $class_name = 'green';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Trang giới thiệu';
            break;
        case 2:
            $class_name = 'green';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Trang event';
            break;

        case 3:
            $class_name = 'green';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Trang tin tức';
            break;
        case 4:
            $class_name = 'green';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Trang program';
            break;

        case 5:
            $class_name = 'green';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Trang resource';
            break;
        case 6:
            $class_name = 'green';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Trang liên hệ';
            break;
        case 7:
            $class_name = 'green';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Trang book';
            break;
        case 8:
            $class_name = 'green';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Trang video';
            break;
        
    }
?>
<span class="hidden">{{$type}}</span>
<span title="<?= @$title ?>" class="btn_type <?= $class_name ?>">
    <?= @$title; ?>
</span>