WebWorldApp.controller('payment_guides.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới: phương thức thanh toán';

        // Title block
        $scope.detail_block_title = 'Chi tiết phương thức thanh toán';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.payment_guide = {};
            $scope.payment_guide.status = '0';
            tinymce.remove();
            load_tinymce('#content', null);
            load_tinymce('#content_en', null);
        }

        // $('#payment_guide_cat').change(function(event) {
        //     var id_payment_guide_cat = $(this).val();
        //     $('#payment_guide_cat1').html('');
        //     select2s('#payment_guide_cat1',{
        //         commonService: commonService,
        //         name:'payment_guide_cat1s',
        //         have_default: true,
        //         where: ['id_payment_guide_cat,=,' + id_payment_guide_cat]
        //     });
        // });

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.payment_guide.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();
            data['content_en'] = tinymce.get('content_en').getContent();
            if (files.length == 0) {
                request['noimg'] = 1;
            }
            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_payment_guide', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/payment_guides');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
