@if (count($medias) != 0)
    @foreach ($medias as $media)
  	<div data-id="{{$media->id}}" class="col-sm-2 item-load-media">
		<div class="img-mod">
			<a onclick="delete_media({{$media->id}})" class="btn btn-xs red" title="Xóa">
			    <i class="fa fa-trash-o"></i> Xóa
			</a>
			<img src="/public/img/upload/itemalbums/<?= $media->name;?>" alt="">
		</div>
	</div>
    @endforeach
@else
<div class="col-sm-12">Chưa có hình</div>
@endif
