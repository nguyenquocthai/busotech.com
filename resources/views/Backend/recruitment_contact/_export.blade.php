<style>
    .table_export{
        width: 100%
    }
    .table_export tbody{
        opacity: 0;
        visibility: hidden;
        height: 0;
        overflow: hidden;
        display: block;
        position: absolute;
    }
</style>
<table class="table_export">
    <tbody>
        <tr>
            <td>Tên</td>
            <td>Điện thoại</td>
            <td>Email</td>
            <td>Ngày đăng ký</td>
            <td>Vị trí ứng tuyển</td>
        </tr>
        @if (count($recruitment_contacts) != 0)
            @foreach ($recruitment_contacts as $recruitment_contact)
            <tr>
                <td>{{$recruitment_contact['name']}}</td>
                <td>{{$recruitment_contact['tel']}}</td>
                <td>{{$recruitment_contact['email']}}</td>
                <td>{{ date("d/m/Y", strtotime($recruitment_contact['created_at']))}}</td>
                <td>{{$recruitment_contact['vitri']}}</td>
            </tr>
            @endforeach
        @else

        @endif
    </tbody>
</table>