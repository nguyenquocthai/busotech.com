WebWorldApp.controller('albums.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới: bài viết';

        // Title block
        $scope.detail_block_title = 'Chi tiết bài viết';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.album = {};
            $scope.album.status = '0';

            select2s('#album_cat',{
                commonService: commonService,
                name:'album_cats',
                have_default: true,
            });
            
        }
        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.album.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            
            request['albums'] = $scope.album.album;
            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_album', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/albums');
                        break;
                    default:
                        break;
                }
            });
        }
        //-------------------------------------------------------------------------------
        $("#gallery_player").change(function() {
            var list_files = [];
            var files = this.files;

            $('.item-gallery').remove();
            for (var i = 0; i < files.length; i++) {
                var file = this.files[i];
                var fileType = file["type"];
                var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(fileType, ValidImageTypes) > 0) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#list-gallery').append('<div class="item-gallery col-sm-2"><div class="img-mod"><img src="' + e.target.result +'" ></div></div>').show().fadeIn("slow");
                        //$('.item-gallery:last').after('<img class="item-gallery" src="' + e.target.result +'" >').show().fadeIn("slow");
                    }
                    reader.readAsDataURL(file);
                    list_files.push(file);
                }
            }

            var files = [];
            var list_image = $("#gallery_player").get(0).files;

            for (var i = 0; i < list_image.length; i++) {
                files = files.concat($("#gallery_player").get(0).files[i]);
            }
            $scope.album.album = files;
        });

        //-------------------------------------------------------------------------------
        delete_item_album = function(id) {
            confirmPopup('Xóa', 'Bạn muốn xóa hình này' , function() {
                commonService.requestFunction('delete_item_album/' + id, {}, function(e) {
                    switch (e.code) {
                        case 200:
                           $('.item-load-item_album[data-id="'+id+'"]').remove();
                        default:
                            break;
                    }
                });
            });
        };
        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
