<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="next-label">Image</label>
            <div class="file_upload_box">
                <input type="file" class="file_image">
                <img class="image_review" src="{{BladeGeneral::GetImg(['avatar' => $data->avatar,'data' => $table, 'time' => $data->updated_at])}}" alt="...">
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="form-group">
            <label>Name</label>
            <input value="{{$data->id_khach}}" type="text" class="form-control" name="id_khach" >
        </div>

        <div class="form-group">
            <label class="next-label">Status</label>
            <select class="form-control" ng-required="true" name="status">
                <option @if($data->status == 1) selected @endif value="1">Activated</option>
                <option @if($data->status == 0) selected @endif value="0">Unactivated</option>
            </select>
        </div>
        <div class="form-group">
            <label class="next-label">Email</label>
            <input type="text" class="form-control" name="email_contact"  value="{{$data->email_contact}}">
        </div>
        <div class="form-group">
            <label class="next-label">Phone</label>
            <input type="text" class="form-control" name="phone_contact"  value="{{$data->phone_contact}}">
        </div>
        <div class="form-group">
            <label class="next-label">Comment</label>
            <textarea class="form-control" name="comment" rows="5">{{$data->comment}}</textarea>
        </div>

        <div class="form-group">
            <label class="next-label">Note</label>
            <textarea class="form-control" name="note" rows="5">{{$data->note}}</textarea>
        </div>
    </div>
</div>
