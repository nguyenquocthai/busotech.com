<?php
    $class_name = '';
    $text = '';

    switch ($status) {
        case 1:
            $class_name = 'green';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Read';
            break;

        case 0:
            $class_name = 'red';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Unread';
            break;
    }
?>
<span class="hidden">{{$status}}</span>
<span title="<?= $title ?>" class="btn_status <?= $class_name ?>">
    <?= $text; ?>
</span>