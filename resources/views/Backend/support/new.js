WebWorldApp.controller('supports.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới: hướng dẫn sử dụng';

        // Title block
        $scope.detail_block_title = 'Chi tiết hướng dẫn sử dụng';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.support = {};
            $scope.support.status = '0';

            select2s('#support_cat',{
                commonService: commonService,
                name:'support_cats',
                have_default: true,
            });

            tinymce.remove();
            load_tinymce('#content', null);
            load_tinymce('#content_en', null);
        }

        // $('#support_cat').change(function(event) {
        //     var id_support_cat = $(this).val();
        //     $('#support_cat1').html('');
        //     select2s('#support_cat1',{
        //         commonService: commonService,
        //         name:'support_cat1s',
        //         have_default: true,
        //         where: ['id_support_cat,=,' + id_support_cat]
        //     });
        // });

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.support.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();
            data['content_en'] = tinymce.get('content_en').getContent();
            
            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_support', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/supports');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
