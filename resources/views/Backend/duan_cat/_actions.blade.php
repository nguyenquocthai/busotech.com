<a data-id="{{$id}}"  class="btn btn-xs blue btn-edit-row" title="Edit">
	<i class="fa fa-edit"></i>
</a>

<a data-id="{{$id}}" class="btn btn-xs red delete_row" title="Delete"><i class="fa fa-trash-o"></i></a>

<span data-id="{{$id}}" class="up btn btn-xs purple"><i class="fa fa-arrow-up" aria-hidden="true"></i></span>
<span data-id="{{$id}}" class="down btn btn-xs grey-cascade"><i class="fa fa-arrow-down" aria-hidden="true"></i></span>