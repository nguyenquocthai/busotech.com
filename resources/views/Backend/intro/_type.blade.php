<?php
    $class_name = '';
    $text = '';

    switch ($type) {
        case 0:
            $class_name = 'green';
            $text = '<i class="fas fa-check"></i>';
            $title = 'Trang chủ';
            break;

        case 1:
            $class_name = 'green';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Trang giới thiệu';
            break;
        case 2:
            $class_name = 'green';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Footer';
            break;
        case 3:
            $class_name = 'green';
            $text = '<i class="fas fa-times"></i>';
            $title = 'Trang dịch vụ';
            break;
        default:
        break;
        
    }
?>
<span class="hidden">{{$type}}</span>
<span title="<?= $title ?>" class="btn_type <?= $class_name ?>">
    <?= $title; ?>
</span>