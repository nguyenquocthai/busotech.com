WebWorldApp.controller('intros.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới: bài viết';

        // Title block
        $scope.detail_block_title = 'Chi tiết bài viết';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.intro = {};
            $scope.intro.status = '0';

            

            tinymce.remove();
            load_tinymce('#content', null);
        }

        

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.intro.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();

            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_intro', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/intros');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
