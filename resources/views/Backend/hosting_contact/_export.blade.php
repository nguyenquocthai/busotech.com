<style>
    .table_export{
        width: 100%
    }
    .table_export tbody{
        opacity: 0;
        visibility: hidden;
        height: 0;
        overflow: hidden;
        display: block;
        position: absolute;
    }
</style>
<table class="table_export">
    <tbody>
        <tr>
            <td>Tên</td>
            <td>Điện thoại</td>
            <td>Email</td>
            <td>Ngày đăng ký</td>
            <td>Giá gói</td>
        </tr>
        @if (count($hosting_contacts) != 0)
            @foreach ($hosting_contacts as $hosting_contact)
            <tr>
                <td>{{$hosting_contact['name']}}</td>
                <td>{{$hosting_contact['tel']}}</td>
                <td>{{$hosting_contact['email']}}</td>
                <td>{{ date("d/m/Y", strtotime($hosting_contact['created_at']))}}</td>
                <td>{{$hosting_contact['price_host']}}</td>
            </tr>
            @endforeach
        @else

        @endif
    </tbody>
</table>