<?php 
// echo "<pre>"; print_r($data);die; echo "</pre";
 ?>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Họ Tên</label>
            <input value="{{$data->name}}" type="text" class="form-control" name="name" readonly>
        </div>

        <div class="form-group">
            <label class="next-label">Trạng thái</label>
            <select class="form-control" ng-required="true" name="status">
                <option @if($data->status == 0) selected @endif value="0">Đã xem</option>
                <option @if($data->status == 1) selected @endif value="1">Chưa xem</option>
            </select>
        </div>
        <div class="form-group">
            <label class="next-label">Email</label>
            <input type="text" class="form-control" name="email" readonly value="{{$data->email}}">
        </div>
        <div class="form-group">
            <label class="next-label">Số điện thoại</label>
            <input type="text" class="form-control" name="tel" readonly value="{{$data->tel}}">
        </div>
        <div class="form-group">
            <label class="next-label">Địa chỉ</label>
            <input type="text" class="form-control" name="address" readonly value="{{$data->address}}">
        </div>
        <div class="form-group">
            <label class="next-label">Thành phố</label>
            <input type="text" class="form-control" name="city" readonly value="{{$data->city}}">
        </div>
        <div class="form-group">
            <label class="next-label">Mã vùng</label>
            <input type="text" class="form-control" name="zip_code" readonly value="{{$data->zip_code}}">
        </div>
        <div class="form-group">
            <label class="next-label">Quốc gia</label>
            <input type="text" class="form-control" name="country" readonly value="{{$data->country}}">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="next-label">Tên host</label>
            <input type="text" class="form-control" name="name_host" readonly value="{{$data->name_host}}">
        </div>
        <div class="form-group">
            <label class="next-label">Gói host đăng ký</label>
            <select name="id_host" id="id_host"></select>
        </div>
        <div class="form-group">
            <label class="next-label">Thời gian đăng ký(năm)</label>
            <input type="text" class="form-control" name="time_host" readonly value="{{$data->time_host}}">
        </div>
        <div class="form-group">
            <label class="next-label">Giá host</label>
            <input type="text" class="form-control" name="price_host" readonly value="{{number_format($data->price_host*$data->time_host)}}">
        </div>
        <div class="form-group">
            <label class="next-label">Xuất hóa đơn</label>
            <select class="form-control" ng-required="true" name="check_invoice">
                <option @if($data->check_invoice == 0) selected @endif value="0">Không</option>
                <option @if($data->check_invoice == 1) selected @endif value="1">Có</option>
            </select>
        </div>
        <div class="form-group">
            <label class="next-label">Tên công ty</label>
            <input type="text" class="form-control" name="name_co" readonly value="{{$data->name_co}}">
        </div>
        <div class="form-group">
            <label class="next-label">Địa chỉ công ty</label>
            <input type="text" class="form-control" name="address_co" readonly value="{{$data->address_co}}">
        </div>
        <div class="form-group">
            <label class="next-label">Mã số thuế công ty</label>
            <input type="text" class="form-control" name="tax_code" readonly value="{{$data->tax_code}}">
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            <label class="next-label">Ghi chú</label>
            <textarea class="form-control" name="note" rows="3">{{$data->note}}</textarea>
        </div>
    </div>
    
</div>


