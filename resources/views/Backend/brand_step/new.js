WebWorldApp.controller('brand_steps.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới: brand_step';

        // Title block
        $scope.detail_block_title = 'Chi tiết brand_step';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.brand_step = {};
            $scope.brand_step.status = '0';

            tinymce.remove();
            load_tinymce('#content', null);
            load_tinymce('#content_en', null);
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.brand_step.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();
            data['content_en'] = tinymce.get('content_en').getContent();
            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_brand_step', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/brand_steps');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
