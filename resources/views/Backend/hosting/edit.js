WebWorldApp.controller('hostings.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Giá hosting';

        // Title block
        $scope.detail_block_title = 'Giá hosting';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_hosting/' + $routeParams.id, {}, function(e) {
                //$('#form_box').html(e.data);
                $scope.hosting = e.data;
                console.log(e.data);
                // load image avatar
                $('.image_review').attr('src', e.data.avatar);

                select2s('#hosting_cat',{
                    commonService: commonService,
                    name:'hosting_cats',
                    selected: e.data.id_hosting_cat,
                    //title: 'code',
                    //limit: 100,
                    //where: ['type,=,1'],
                });
                select2s('#hosting_feature',{
                    commonService: commonService,
                    name:'hosting_features',
                    multiple: true,
                    disabled: true,
                    selected: e.data.id_feature_hosting,
                    
                });
                tinymce.get('content').setContent(html_entity_decode($scope.hosting.content) || '');
                tinymce.get('content_en').setContent(html_entity_decode($scope.hosting.content_en) || '');

            });
            // Tiny mce
                tinymce.remove();
                load_tinymce('#content', null);
                load_tinymce('#content_en', null);
            
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.hosting.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var id_edit = $routeParams.id;

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['id_feature_hosting'] = JSON.stringify($('#hosting_feature').val());
            data['content'] = tinymce.get('content').getContent();
            data['content_en'] = tinymce.get('content_en').getContent();
            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";

            // console.log(data);return false;

            fileUpload.uploadFileToUrl(files, request, 'update_hosting/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/hostings');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

    }
]);
