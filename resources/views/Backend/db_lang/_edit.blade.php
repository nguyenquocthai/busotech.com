<div class="form-group">
    <label class="next-label">Tiếng việt</label>
    <textarea class="form-control" name="title">{{$data->title}}</textarea>
</div>

<div class="form-group">
    <label class="next-label">Tiếng anh</label>
    <textarea class="form-control" name="title_en">{{$data->title_en}}</textarea>
</div>

<div class="form-group hidden">
    <label class="next-label">Status</label>
    <select class="form-control" ng-required="true" name="status">
        <option @if($data->status == 0) selected @endif value="0">Activated</option>
        <option @if($data->status == 1) selected @endif value="1">Unactivated</option>
    </select>
</div>

<div class="form-group hidden">
    <label class="next-label">Location</label>
    <div class="load_position">
        <div class="position_box" data-id="">
            @if (count($positions) != 0)
                @foreach ($positions as $key => $position)
                <span style="margin-bottom: 3px" data-value="{{$position->position}}" data-id="{{$position->id}}" class="item-position btn btn-icon-only grey-cascade @if($data->position == $position->position) active @endif">{{$key+1}}</span>
                @endforeach
            @else
            @endif
        </div>
    </div>
</div>