WebWorldApp.controller('price_domains.edit', ['$scope','$rootScope', 'commonService', '$routeParams', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $routeParams, $location, fileUpload) {
        $rootScope.app.title = 'Giá tên miền';

        // Title block
        $scope.detail_block_title = 'Giá tên miền';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {

            // LOAD DATA
            commonService.requestFunction('show_price_domain/' + $routeParams.id, {}, function(e) {
                //$('#form_box').html(e.data);
                $scope.price_domain = e.data;
                console.log(e.data);
                // load image avatar
                $('.image_review').attr('src', e.data.avatar);

                select2s('#price_domain_cat',{
                    commonService: commonService,
                    name:'price_domain_cats',
                    selected: e.data.id_price_domain_cat,
                    //title: 'code',
                    //limit: 100,
                    //where: ['type,=,1'],
                });
                
                
            });
            
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.price_domain.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var id_edit = $routeParams.id;

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            
            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";

            // console.log(data);return false;

            fileUpload.uploadFileToUrl(files, request, 'update_price_domain/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/price_domains');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

    }
]);
