<?php

namespace App\Http\Controllers\Backend\api;

use App\Http\Controllers\Backend\BaseAdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Model\ShopPartnerSupport;
use App\Model\FunctionAuthority;
use App\Exceptions\ErrorCodes;
use App\Exceptions\ShopCommon;
use App\Exceptions\ShopUpload;
use Illuminate\Support\Facades\DB;

class AdminpartnersupportsController extends BaseAdminController
{

    public function search_datatable (Request $request) {

        // rule
        if (Auth::user()->list_rule != null)
            $list_rules = json_decode(Auth::user()->list_rule, true);
        else
            $list_rules = [];

        $list_url = [];
        foreach($list_rules as $list_rule) {
            $function = FunctionAuthority::where([
                ['id', '=', $list_rule['id']],
            ])->first();

            if($function) {
                $list_url[] = $function->role_json;
            }
        }
        $role = Auth::user()->role;

        $blogs = DB::table('shop_partner_supports')
            ->select('*')
            ->where([
                ['del_flg', 0],
            ])
            ->orderBy('position', 'desc')
            ->get();

        $RandomString = '?'.$this->RandomString(5);
        $output = [];
        foreach ($blogs as $key => $blog) {

            $vitri = $key+1;

            $row = [];

            $row[] = $blog->id;

            $row[] = $blog->position;

            $row[] = $vitri;

            $row[] = $blog->title;

            $row[] = '<span class="hidden">'.$blog->updated_at.'</span>'.date('d/m/Y', strtotime($blog->updated_at));

            $view = View::make('Backend/Adminpartnersupport/_status', ['status' => $blog->status]);
            $row[] = $view->render();

            $view = View::make('Backend/Adminpartnersupport/_actions', ['id' => $blog->id, 'list_url' => $list_url, 'role' => $role, 'page' => 'partner_support']);
            $row[] = $view->render();
            $output[] = $row;
        }

        $data['code'] = 200;
        $data['data'] = $output;
        return response()->json($data, 200);
    }

    //-------------------------------------------------------------------------------
    public function store(Request $request)
    {
        try {

            // Create folder
            if ( !file_exists(config('general.shop_support_path')) )
                mkdir(config('general.shop_support_path'), config('permission_folder'), true);

            // validate
            $id = 0;
            $error_validate = ShopPartnerSupport::validate($id);
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            // upload avatar
            /*if($request->hasFile('files') != 1) {
                $data['code'] = 300;
                $data['error'] = 'Hãy chọn hình đại diện';
                return response()->json($data, 200);
            }

            $files = $request->file('files');
            $avatar = $files[0];
            $avatar_link['path'] = config('general.shop_support_path');
            $avatar_link['url'] = config('general.shop_support_url');

            $upload = ShopUpload::upload($avatar, $avatar_link);
            if ($upload == 'error') {
                $data['code'] = 300;
                $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                return response()->json($data, 200);
            }

            $file_name = $upload ? $upload['file_name'] : '';
            */

            $where = [
                ['shop_blog_category_id', '==', 33]
            ];
            $position = $this->GetPos('shop_partner_supports');

            // save
            $shop_blog = new ShopPartnerSupport;

            $shop_blog->title = $request->title;
            $shop_blog->title_en = $request->title_en;

            $shop_blog->content = $request->content;
            $shop_blog->content_en = $request->content_en;

            $shop_blog->slug = $request->slug;
            $shop_blog->meta_keyword = $request->meta_keyword;
            $shop_blog->meta_description = $request->meta_description;
            $shop_blog->status = $request->status;
            $shop_blog->position = $position;
            /*$shop_blog->loai = $request->loai;*/

            //$shop_blog->avatar = $file_name;

            $shop_blog->del_flg = 0;

            $shop_blog->save();

            $data['code'] = 200;
            $data['message'] = 'Lưu thành công';
            return response()->json($data, 200);

        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function update(Request $request, $id)
    {
        try {

            if ($request->status_code == "up") {
                $where = [];
                $pos = $this->UpPos('shop_partner_supports',$where, $id);
                return response()->json($pos, 200);
            }

            if ($request->status_code == "down") {
                $where = [];
                $pos = $this->DownPos('shop_partner_supports',$where, $id);
                return response()->json($pos, 200);
            }

            if ($request->status_code == "change") {
                $pos = $this->ChangePos('shop_partner_supports',$id, $request->id_swap);
                return response()->json($pos, 200);
            }

            $error_validate = ShopPartnerSupport::validate($id);
            $validator = \Validator::make($request->all(), $error_validate['pattern'], $error_validate['messenger'], $error_validate['customName']);

            if ($validator->fails()) {
                $data['code'] = 300;
                $data['error'] = $validator->errors();
                return response()->json($data, 200);
            }

            // check $id isset in database
            $blog = ShopPartnerSupport::where([
                ['id', '=', $id],
            ])->first();

            if (!$blog) {
                $data['code'] = 300;
                $data['error'] = 'Lưu thất bại';
                return response()->json($data, 200);
            }

            $file_name = $blog['avatar'];
            // Create folder
            if ( !file_exists(config('general.shop_support_path')) )
                mkdir(config('general.shop_support_path'), config('permission_folder'), true);

            // upload avatar
            /*if($request->hasFile('files') == 1) {

                $files = $request->file('files');
                $avatar = $files[0];
                $avatar_link['path'] = config('general.shop_support_path');
                $avatar_link['url'] = config('general.shop_support_url');
                $options['file_name'] = $file_name;

                $upload = ShopUpload::upload($avatar, $avatar_link, $options);
                if ($upload == 'error') {
                    $data['code'] = 300;
                    $data['error'] = 'Hình vượt quá ' . config('general.notification_image');
                    return response()->json($data, 200);
                }

                $file_name = $upload ? $upload['file_name'] : $blog['avatar'];

            }
            */

            // save
            $shop_blog = ShopPartnerSupport::find($id);

            $shop_blog->title = $request->title;
            $shop_blog->title_en = $request->title_en;
            $shop_blog->content = $request->content;
            $shop_blog->content_en = $request->content_en;
            $shop_blog->slug = $request->slug;
            $shop_blog->meta_keyword = $request->meta_keyword;
            $shop_blog->meta_description = $request->meta_description;
            $shop_blog->status = $request->status;
            $shop_blog->position = $request->position;
            //$shop_blog->loai = $request->loai;

            //$shop_blog->avatar = $file_name;

            $shop_blog->save();

            $data['code'] = 200;
            $data['message'] = 'Cập nhật thành công';
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function show($id)
    {
        try {
            $blog = ShopPartnerSupport::where([
                ['id', $id],
                ['del_flg', 0],
            ])->first();

            if ($blog) {
                $image_path = config('general.shop_support_path').$blog->avatar;

                if (file_exists($image_path)) {
                    $blog->avatar = config('general.shop_support_url') . $blog->avatar;
                } else{
                    $blog->avatar = '/public/img/no-image.png';
                }
            }

            $positions = DB::table('shop_partner_supports')
                ->select('id','position')
                ->where([
                    ['del_flg', '=', 0]
                ])
                ->orderBy('position', 'desc')
                ->get();

            $view = View::make('Backend/Adminpartnersupport/_position', ['positions' => $positions, 'here' => $blog->position]);
            $list_position = $view->render();

            $data['code'] = 200;
            $data['list_position'] = $list_position;
            $data['data'] = $blog;
            return response()->json($data, 200);
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }

    //-------------------------------------------------------------------------------
    public function destroy ($id) {

        try {
            $blog = ShopPartnerSupport::where([
                ['id', $id],
            ])->update(['del_flg' => 1]);
            if ($blog) {
                $data['code'] = 200;
                $data['message'] = 'Xóa thành công';
                return response()->json($data, 200);
            } else {
                $data['code'] = 300;
                $data['error'] = 'Xóa không thành công';
                return response()->json($data, 200);
            }
        } catch (Exception $e) {
            $data['code'] = 300;
            $data['error'] = 'Lỗi kết nối';
            return response()->json($data, 200);
        }
    }
}
