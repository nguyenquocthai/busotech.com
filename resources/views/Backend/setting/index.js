WebWorldApp.controller('settings.index', ['$scope','$rootScope', 'commonService', '$routeParams', '$location',
    function ($scope, $rootScope, commonService, $routeParams, $location) {
        $rootScope.app.title = $rootScope.app.title + ' | Setting';

        // Title block
        $scope.title = 'Thông tin website';
        $scope.title2 = 'Mạng xã hội';
        $scope.title3 = 'Giới thiệu các trang';
        $scope.title_crate = 'Image';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            commonService.requestFunction('index_setting/', {}, function(callback) {
                $scope.setting = callback.data;
                $scope.setting.info_website.chat = html_entity_decode($scope.setting.info_website.chat);

                // load logo
                options = { element: '.image_review.logo' };
                var path_img = '/public/img/upload/settings/' + $scope.setting.info_website.logo;
                testImage(path_img, attr_url_image, null, options);

                // load logo_white
                options = { element: '.image_review.logo_white' };
                var path_img = '/public/img/upload/settings/' + $scope.setting.info_website.logo_white;
                testImage(path_img, attr_url_image, null, options);

                // load favicon
                options = { element: '.image_review.favicon' };
                var path_img = '/public/img/upload/settings/' + $scope.setting.info_website.favicon;
                testImage(path_img, attr_url_image, null, options);

                // tinymce.get('address').setContent(html_entity_decode($scope.setting.info_website.address) || '');
                // tinymce.get('facebook_code').setContent(html_entity_decode($scope.setting.info_website.facebook_code) || '');

            });
            // Tiny mce
                // tinymce.remove();
                // load_tinymce('#address', null);
                // load_tinymce('#facebook_code', null);
        }

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.apply_geolocation = function() {
            $scope.setting.info_website.map = $scope.pointer.latitude + ',' +$scope.pointer.longitude;
            
            $("#modalMap").modal('toggle');
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {
            
            var formData = new FormData(document.getElementById('userForm'));
            // $setting_address = tinymce.get('address').getContent();
            // $intro_footer = tinymce.get('facebook_code').getContent();
            // formData.append('address', $setting_address);
            // formData.append('facebook_code', $intro_footer);
            commonService.requestajaxform('update_setting/' + $scope.setting.id, formData, function(e) {
                
                switch (e.code) {
                    case 200:
                        
                        $scope.setting.id = e.data.id;
                        $scope.setting.info_website.avatar = e.data.avatar;
                        $scope.setting.info_website.favicon = e.data.favicon;
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });

        //----------------------------------------------------------------------------------
        $('#modalMap').on('shown.bs.modal', function() {
            // GOOGLE MAP
            $scope.pointer = $scope.pointer || {};

            var geo = $scope.setting.info_website.map.split(',');
            if (geo.length == 2) {
                $scope.pointer.latitude = geo[0];
                $scope.pointer.longitude = geo[1];
            }

            init_google_map($scope, {
                marker: $scope.marker,
                pointer: $scope.pointer,
            });
        });
    }
]);
