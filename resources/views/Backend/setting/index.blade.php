@extends('frontend.layouts.main')
@section('content')
<div class="container-fluid">
    <div class="container">
        <div class="row row_top_use_all">
            <div class="col-md-12">
                <div class="">
                    <span class="span_head_title">
                        @if(@session('lang') == 'en')
                            Home
                        @else
                         Trang chủ
                        @endif ></span>
                    <span class="span_para_title">
                    @if(@session('lang') == 'en')
                            About
                        @else
                         Giới thiệu
                        @endif </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div class="large_about">
                    <ul class="ul_about">
                        @if(isset($supports))
                            @foreach ($supports as $support)
                            <li class="">
                                <a class="" href="/gioi-thieu/{{$support->slug}}">{{BladeGeneral::lang($support, 'title')}}</a>
                                <i class="right_icon_about fas fa-caret-right"></i>
                            </li>
                            @endforeach
                        @endif
                        
                    </ul>
                </div>
                <div class="fair_information">
                    <div class="sec1_fair_information">
                        <i class="fas fa-user-friends"></i>
                        @if(@session('lang') == 'en')
                            Infomation
                        @else
                         Thông tin hội chợ
                        @endif
                    </div>
                    <div class="box_gay">
                        <div class="head_sec1_fair_information">
                            
                            @if(@session('lang') == 'en')
                                TIME
                            @else
                             THỜI GIAN
                            @endif
                        </div>
                        <div class="txt_sec1_fair_information">
                            {{$info_web['ngaymocua']}}
                        </div>
                        <div class="txt_sec1_fair_information">
                            {{$info_web['giomocua']}}
                        </div>
                        
                        <div class="head_sec1_fair_information">
                            
                            @if(@session('lang') == 'en')
                                ADDRESS
                            @else
                             ĐỊA CHỈ
                            @endif
                        </div>
                        <div class="txt_sec1_fair_information">
                            {{$info_web['diadiem']}}
                        </div>
                        <div class="txt_sec1_fair_information">
                            {{$info_web['address']}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-lg-9">
                <div class="box_top_content">
                    <div class="txt_top_content">
                        {{BladeGeneral::lang($support1, 'title')}}
                    </div>
                </div>
                <div class="border_content">
                    <div class="txt_fair">
                        {!!BladeGeneral::lang($support1, 'content')!!}
                    </div>
                </div>
                
                <!-- <div class="border_content">
                    <div class="txt_fair">
                    	<video width="100%" controls muted autoplay id="myVideo" controls controlsList="nodownload">
                    		<source src="video/STYLE_3mn.mp4" type="video/mp4">
                    	</video>
                    </div>
                    </div>
                    <div class="border_content">
                    <div class="txt_fair">
                    	STYLE Bangkok is an international trade fair that offers all sorts of lifestyle products. Gathering the latest products that shape tomorrow's
                    trends from gifts, home decor, furniture to fashion and more. Held under “Crenovative Origin” that combines and highlights “Creative”,
                    “Innovation”, and Thai “Original” identity to respond to the new lifestyle product trends. The fair is biannually held (April and October)
                    and is a combination of three prestige international trade fairs, namely <strong>Bangkok International Fashion Fair and Bangkok International Leather Fair
                    (BIFF & BIL), Bangkok International Gifts and Bangkok International Houseware Fair (BIG + BIH), and Thailand International Furniture Fair (TIFF)</strong>
                    which were all well received for more than 20 years.
                    </div>
                    </div>
                    <div class="bg_head_fair">
                    Lifestyle Categories
                    </div>
                    <div class="border_content">
                    <div class="row">
                    	<div class="col-md-12">
                    		<div class="head__fair">
                    																BIG + BIH (Gifts, Premiums and Home Decoration)
                    			<span>
                    				<img class="img_big" src="img/big.png" alt="">
                    			</span>
                    			 <div class="">
                    				 ( Gifts, Premiums and Home Decoration )
                    			 </div>
                    
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    			<p>* Gifts, premiums and souvenirs</p>
                    			<p>* Home Decoration Items</p>
                    			<p>* Home textile</p>
                    			<p>* Gifts and textile home decoration</p>
                    			<p>* Handicrafts</p>
                    			<p>* Artificial flowers and plants</p>
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    			<p>* Aroma products</p>
                    			<p>* Christmas decoration and ornaments</p>
                    			<p>* Toys</p>
                    			<p>* Sports gear and travel accessories</p>
                    			<p>* Kitchenware</p>
                    			<p>* Stationeries and office supplies</p>
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    			<p>* Hotel and Spa supplies</p>
                    			<p>* Cosmetics</p>
                    			<p>* OTOP products</p>
                    			<p>* Cartoon characters</p>
                    			<p>* Pet products</p>
                    		</div>
                    	</div>
                    </div>
                    <div class="row">
                    	<div class="col-md-12">
                    		<div class="head__fair">
                    																BIF & BILL (Fashion and Leather)
                    				<span>
                    					<img class="img_big" src="img/biff.png" alt="">
                    				</span>
                    				<div class="">
                    					( Fashion and Leather )
                    				</div>
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    			<p>* Garments </p>
                    			<p>* Costumes and accessories for different occasions</p>
                    
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    			<p>* Textile and silk</p>
                    			<p>* Leather goods such as leather shoes, bags and leatherwear</p>
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    			<p>* Raw materials (leather hides) and leather scraps</p>
                    			<p>* Printing and chemical supplies used in fashion industry</p>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="col-md-12">
                    		<div class="head__fair">
                    		TIFF (Furniture)
                    		<span>
                    			<img class="img_big" src="img/tiff.png" alt="">
                    		</span>
                    		<div class="">
                    			( Furniture )
                    		</div>
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    			<p>* Furniture and its spare parts,</p>
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    			<p>* Interior design decoration items</p>
                    
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    			<p>* Related products</p>
                    
                    		</div>
                    	</div>
                    </div>
                    
                    
                    
                    </div>
                    <div class="bg_head_fair">
                    VISITOR GROUP
                    </div>
                    <div class="border_content">
                    <div class="row">
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    				<p>* Importers</p>
                    				<p>* Manufacturers</p>
                    				<p>* Trading Companies</p>
                    				<p>* Distributors </p>
                    					<p>* Wholesalers</p>
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    
                    				<p>* Retailers</p>
                    				<p>* Boutiques</p>
                    				<p>* Fashion Showrooms</p>
                    				<p>* Department Stores</p>
                    				<p>* Buying Agents</p>
                    		</div>
                    	</div>
                    	<div class="col-md-4">
                    		<div class="contect_list_fir">
                    				<p>* Designers</p>
                    				<p>* Real Estate Buyers</p>
                    				<p>* Interior Designers</p>
                    				<p>* Etc.</p>
                    		</div>
                    	</div>
                    </div>
                    
                    </div>
                    <div class="bg_head_fair">
                    EXHIBITOR ESTIMATES
                    </div>
                    <div class="border_content">
                    <div class="contect_list_fir">
                    																	<p>1000 Companies</p>
                    																	<p>2000 exhibition booths</p>
                    	<img src="img/group-4@2x.png" />
                    	<img src="img/group-2@2x.png" />
                    </div>
                    </div> -->
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9">
            </div>
        </div>
    </div>
</div>
@endsection