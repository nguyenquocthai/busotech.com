<?php

Route::get('/test_payment', 'Frontend\PaymentController@test_payment');
Route::get('/payment_success/{id}', 'Frontend\PaymentController@payment_success');

// Home
Route::get('/', 'Frontend\HomeController@index');

// category
Route::get('/danh-muc/{slug}', 'Frontend\CategoryController@show');
Route::get('/tim-kiem', 'Frontend\CategoryController@find');
Route::get('/gioi-thieu/{slug}', 'Frontend\AboutsController@index');
Route::get('/thong-tin-chung', 'Frontend\AboutsController@thongtinchung');
Route::get('/doanh-nghiep/{slug}', 'Frontend\InfomationController@index');
// blog
Route::get('/tin-tuc', 'Frontend\NewController@index');
Route::get('/doanh-nghiep-tieu-bieu', 'Frontend\NewController@doanhnghieptieubieu');
Route::get('/tin-tuc/{slug}', 'Frontend\NewController@detail');
// nha tai tro
Route::get('/nha-tai-tro', 'Frontend\DonorController@index');
Route::get('/nha-tai-tro/{slug}', 'Frontend\DonorController@detail');
// khach tham quan
Route::get('/khach-tham-quan/{slug}', 'Frontend\RegulationController@detail');

// gallery
Route::get('/gallery', 'Frontend\GalleryController@index');
// tra benh
Route::get('/danh-muc-tra-benh/{id}', 'Frontend\DiagnosisController@category');
Route::get('/tra-benh', 'Frontend\DiagnosisController@index');
Route::get('/tra-benh/{slug}', 'Frontend\DiagnosisController@detail');
// tra thuoc
Route::get('/tra-thuoc/{slug}', 'Frontend\MedicineController@index');
Route::get('/thuoc/{slug}', 'Frontend\MedicineController@detail');

// lien he
Route::get('/lien-he', 'Frontend\ContactController@index');
Route::get('/dang-ky-tham-quan', 'Frontend\ContactController@dangkythamquan');
Route::get('/dang-ky-gian-hang', 'Frontend\ContactController@dangkygianhang');
// lien he phong kham
Route::get('/dang-ky-kham', 'Frontend\ContactController@contact_exam');

// product
Route::get('/san-pham/{slug}', 'Frontend\ProductController@detail');
Route::post('/api/add_comment', 'Frontend\api\ProductController@add_comment');

// user
Route::post('/api/auth/login', 'Frontend\api\UserController@change_api');
Route::post('/api/favorite', 'Frontend\api\UserController@favorite');
Route::post('/api/favorite_delete', 'Frontend\api\UserController@favorite_delete');
Route::get('/doi-mat-khau/{slug}', 'Frontend\ProfileController@changepass');
Route::get('/thong-tin-ca-nhan', 'Frontend\ProfileController@index');
Route::get('/lich-su-don-hang', 'Frontend\ProfileController@order');
Route::get('/lich-su-don-hang/{id}', 'Frontend\ProfileController@order_detail');
Route::get('/yeu-thich', 'Frontend\ProfileController@favorite');

// Cart
Route::get('/thanh-toan', 'Frontend\CartController@index');
Route::get('/gio-hang', 'Frontend\CartController@cart_store');
Route::post('/check_voucher', 'Frontend\CartController@check_voucher');

Route::get('/check-login', 'Frontend\CartController@cart_step_2');
Route::get('/thanh-toan-thanh-cong/{order_no}', 'Frontend\CartController@cart_final');
Route::get('/loi-thanh-toan', 'Frontend\CartController@payment_error');
Route::post('/markaddress', 'Frontend\CartController@markaddress');

// Login
Route::get('/dang-nhap', 'Frontend\LoginController@login');
Route::get('/dang-ky', 'Frontend\LoginController@index');
Route::get('/quen-mat-khau', 'Frontend\LoginController@forget');
Route::get('/dang-xuat', 'Frontend\LoginController@logout');

// api mobie
Route::post('/api/product/store', 'Frontend\api\ProductController@api_mobie_store');
Route::post('/api/product/price_update', 'Frontend\api\ProductController@api_mobie_update');

// ajax
Route::group(['prefix' => 'ajax'], function () {
    // SHOPPING CART
    Route::resource('/shopping_cart', 'Frontend\ajax\ShoppingCartController');

    //NEW
    Route::post('/new/filter', 'Frontend\ajax\NewController@filter');

    //Diagnosis
    Route::post('/diagnosis/filter', 'Frontend\ajax\DiagnosisController@filter');

    // PRODUCT
    Route::post('/product/filter', 'Frontend\ajax\ProductController@filter');
    Route::post('/product/filter_header', 'Frontend\ajax\ProductController@filter_header');
    Route::post('/product/filter_drug', 'Frontend\ajax\ProductController@filter_drug');
});

// API Frontend--------------------------------------------------------
Route::group(['prefix' => 'api/frontend'], function () {

    // onpage
    Route::post('/edit_onpage', 'Frontend\api\OnpageController@edit_onpage');
    Route::post('/edit_img', 'Frontend\api\OnpageController@edit_img');
    Route::get('/show_onpage/{id}', 'Frontend\api\OnpageController@show_onpage');

    // contact
    Route::post('/contact/register', 'Frontend\ContactController@ajax_contact');
    Route::post('/contact/register-dangkygianhang', 'Frontend\ContactController@ajax_dangkygianhang');
    Route::post('/contact/register-dangkythamquan', 'Frontend\ContactController@ajax_dangkythamquan');
    Route::post('/contact/register_footer', 'Frontend\ContactController@ajax_contact_footer');

    // slider
    Route::get('/get_sliders', 'Frontend\api\SliderController@get_sliders');

    // order
    Route::post('/checkout', 'Frontend\api\OrderController@checkout');
    Route::get('/reponse_payment/{id}', 'Frontend\api\OrderController@reponse_payment');

    // product
    Route::post('/get_products', 'Frontend\api\ProductController@get_products');
    Route::post('/fillter_product', 'Frontend\api\ProductController@fillter_product');
    Route::post('/view_more', 'Frontend\api\ProductController@view_more');
    Route::post('/add_comment', 'Frontend\api\ProductController@add_comment');

    // user
    Route::resource('/user', 'Frontend\api\UserController');
    Route::post('/user/login', 'Frontend\api\UserController@login');
    Route::post('/user/forget', 'Frontend\api\UserController@forget');
    Route::post('/user/changepass', 'Frontend\api\UserController@changepass');
    Route::post('/user/login_social', 'Frontend\api\UserController@login_social');
    Route::post('/user/method_delivery', 'Frontend\api\UserController@method_delivery');
    Route::post('/user/user_edit_profile', 'Frontend\api\UserController@user_edit_profile');

    Route::get('/user/user_get_district/{id}', 'Frontend\api\UserController@user_get_district');

    Route::post('/user/user_edit_address', 'Frontend\api\UserController@user_edit_address');

    Route::post('/user/check_order', 'Frontend\api\UserController@check_order');

    // district
    Route::post('/district', 'Frontend\api\DistrictController@list_district');

    // cart
    Route::get('/get_carts', 'Frontend\api\ProductController@get_carts');

    // product
    Route::post('/view_more_menu', 'Frontend\MenuController@view_more');

    // product
    Route::post('/view_more_infor', 'Frontend\NewController@view_more');
    // lang
    Route::post('/lang', 'Frontend\api\SessionController@lang');
});

// END API Frontend----------------------------------------------------

// Backend
Route::get('login', 'Backend\AdminController@login');


Route::group(['prefix' => 'admin', 'middleware' => 'AdminLogin'], function() {

    Route::post('/session', 'Backend\SessionController@index');
    Route::get('/excel', 'Backend\ExcelController@index');
    Route::get('/logout', 'Backend\AdminController@logout');

    Route::get('/', 'Backend\AdminController@index');
    Route::get('{multi}', 'Backend\AdminController@index');

    // ROUTER SLIDER
    Route::get('slider/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER TBlang
    Route::get('TBlang/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER TBabouts
    Route::get('TBabout/new', 'Backend\AdminController@index');
    Route::get('TBabout/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER Delivery Time
    Route::get('deliverytime/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER COOKING SLIDER
    Route::get('cooking_slider/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER partner support
    Route::get('partner_support/new', 'Backend\AdminController@index');
    Route::get('partner_support/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER regulation
    Route::get('regulation/new', 'Backend\AdminController@index');
    Route::get('regulation/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER BLOG
    Route::get('blog/new/{multi}', 'Backend\AdminController@index');
    Route::get('blog/edit/{multi}', 'Backend\AdminController@index');
    // ROUTER donor
    Route::get('donor/new/{multi}', 'Backend\AdminController@index');
    Route::get('donor/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER BLOGCATEGORY
    Route::get('blogcategory/new', 'Backend\AdminController@index');
    Route::get('blogcategory/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER size
    Route::get('size/new', 'Backend\AdminController@index');
    Route::get('size/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER INFORMATIONCATEGORY
    Route::get('informationcategory/new', 'Backend\AdminController@index');
    Route::get('informationcategory/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER INFORMATION
    Route::get('information/new', 'Backend\AdminController@index');
    Route::get('information/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER COUNTRY
    Route::get('country/new', 'Backend\AdminController@index');
    Route::get('country/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER CHAINSTORE
    Route::get('chainstore/new', 'Backend\AdminController@index');
    Route::get('chainstore/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER CATEGORY
    Route::get('category/new', 'Backend\AdminController@index');
    Route::get('category/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER PRODUCT
    Route::get('product/new', 'Backend\AdminController@index');
    Route::get('product/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER MUACHUNG
    Route::get('muachung/new', 'Backend\AdminController@index');
    Route::get('muachung/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER NEW
    Route::get('order/new', 'Backend\AdminController@index');
    Route::get('order/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER DISCOUNT
    Route::get('discount/new', 'Backend\AdminController@index');
    Route::get('discount/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER Function Authorities
    Route::get('function_authority/new', 'Backend\AdminController@index');
    Route::get('function_authority/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER VOUCHER
    Route::get('voucher/new', 'Backend\AdminController@index');
    Route::get('voucher/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER USER
    Route::get('user/new', 'Backend\AdminController@index');
    Route::get('user/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER AUTHORITY
    Route::get('authority/new', 'Backend\AdminController@index');
    Route::get('authority/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER COMMENT
    Route::get('comments/{multi}', 'Backend\AdminController@index');
    Route::get('comment/new/{multi}', 'Backend\AdminController@index');

    // ROUTER cmpost
    Route::get('cmposts/{multi}', 'Backend\AdminController@index');
    Route::get('cmpost/new/{multi}', 'Backend\AdminController@index');

    // ROUTER PROJECT
    Route::get('project/new', 'Backend\AdminController@index');
    Route::get('project/edit/{multi}', 'Backend\AdminController@index');

    // ROUTER custompage
    Route::get('custompage/new', 'Backend\AdminController@index');
    Route::get('custompage/edit/{multi}', 'Backend\AdminController@index');

});

Route::group(['prefix' => 'product'], function() {

    Route::get('add', 'AdminProductController@add');
    Route::get('edit/{id}', 'AdminProductController@edit');

    Route::post('api_new',['as' => 'api_new', 'uses' => 'AdminProductController@api_new']);

});

// API --------------------------------------------------------

Route::get('api/save_session', 'Backend\api\AuthController@save_session');

// Route::group(['prefix' => 'api', 'middleware' => ['api', 'auth:api']], function () {
Route::group(['prefix' => 'api', 'middleware' => ['api']], function () {
    // Login
    Route::get('login', 'Backend\api\AuthController@login');

    // ADMIN VISITS
    Route::get('adminvisits/statics', 'Backend\api\AdminvisitsController@statics');

    // ADMIN SLIDERS
    Route::get('adminsliders/search_datatable', 'Backend\api\AdminslidersController@search_datatable');
    Route::resource('adminsliders', 'Backend\api\AdminslidersController');

    // ADMIN TBlangs
    Route::get('adminTBlangs/search_datatable', 'Backend\api\AdminTBlangsController@search_datatable');
    Route::resource('adminTBlangs', 'Backend\api\AdminTBlangsController');

    // ADMIN TBabouts
    Route::get('adminTBabouts/search_datatable', 'Backend\api\AdminTBaboutsController@search_datatable');
    Route::put('adminTBabouts/up_TBabout/{id}', 'Backend\api\AdminTBaboutsController@up_TBabout');
    Route::put('adminTBabouts/down_TBabout/{id}', 'Backend\api\AdminTBaboutsController@down_TBabout');
    Route::put('adminTBabouts/change_TBabout/{id}', 'Backend\api\AdminTBaboutsController@change_TBabout');
    Route::resource('adminTBabouts', 'Backend\api\AdminTBaboutsController');

    // ADMIN cmprivates
    Route::get('admincmprivates/search_datatable', 'Backend\api\AdmincmprivatesController@search_datatable');
    Route::resource('admincmprivates', 'Backend\api\AdmincmprivatesController');

    // ADMIN cmprivate2s
    Route::get('admincmprivate2s/search_datatable', 'Backend\api\Admincmprivate2sController@search_datatable');
    Route::resource('admincmprivate2s', 'Backend\api\Admincmprivate2sController');

    // ADMIN Delivery Times
    Route::get('admindeliverytimes/search_datatable', 'Backend\api\AdmindeliverytimesController@search_datatable');
    Route::resource('admindeliverytimes', 'Backend\api\AdmindeliverytimesController');

    // ADMIN COOKING SLIDERS
    Route::get('admincookingsliders/search_datatable', 'Backend\api\AdmincookingslidersController@search_datatable');
    Route::get('admincookingsliders/select2', 'Backend\api\AdmincookingslidersController@select2');
    Route::resource('admincookingsliders', 'Backend\api\AdmincookingslidersController');

    // ADMIN ORDERS
    Route::get('adminorders/search_datatable', 'Backend\api\AdminordersController@search_datatable');
    Route::resource('adminorders', 'Backend\api\AdminordersController');
    Route::put('adminorders/update_status_order/{id}', 'Backend\api\AdminordersController@update_status_order');

    // ADMIN PRODUCTS
    Route::get('adminproducts/search_datatable', 'Backend\api\AdminproductsController@search_datatable');
    Route::get('adminproducts/status', 'Backend\api\AdminproductsController@status');
    Route::put('adminproducts/update_discount/{id}', 'Backend\api\AdminproductsController@update_discount');
    Route::resource('adminproducts', 'Backend\api\AdminproductsController');
    Route::put('adminproducts/up_product/{id}', 'Backend\api\AdminproductsController@up_product');
    Route::put('adminproducts/down_product/{id}', 'Backend\api\AdminproductsController@down_product');
    Route::put('adminproducts/change_product/{id}', 'Backend\api\AdminproductsController@change_product');

     // ADMIN TBdonvis
    Route::get('adminTBdonvis/get_TBdonvi', 'Backend\api\AdminTBdonvisController@get_TBdonvi');
    Route::resource('adminTBdonvis', 'Backend\api\AdminTBdonvisController');

    // ADMIN MUACHUNGS
    Route::get('adminmuachungs/search_datatable', 'Backend\api\AdminmuachungsController@search_datatable');
    Route::get('adminmuachungs/status', 'Backend\api\AdminmuachungsController@status');
    Route::put('adminmuachungs/update_discount/{id}', 'Backend\api\AdminmuachungsController@update_discount');
    Route::resource('adminmuachungs', 'Backend\api\AdminmuachungsController');

    // ADMIN CONTACTS
    Route::get('admincontacts/search_datatable', 'Backend\api\AdmincontactsController@search_datatable');
    Route::resource('admincontacts', 'Backend\api\AdmincontactsController');

    // ADMIN CONTACT PARTNERS
    Route::get('admincontactpartners/search_datatable', 'Backend\api\AdmincontactpartnersController@search_datatable');
    Route::resource('admincontactpartners', 'Backend\api\AdmincontactpartnersController');

    // ADMIN CATEGORIES
    Route::get('admincategories/search_select2', 'Backend\api\AdmincategoriesController@search_select2');
    Route::resource('admincategories', 'Backend\api\AdmincategoriesController');
    Route::put('admincategories/up_categorie/{id}', 'Backend\api\AdmincategoriesController@up_categorie');
    Route::put('admincategories/down_categorie/{id}', 'Backend\api\AdmincategoriesController@down_categorie');
    Route::put('admincategories/change_categorie/{id}', 'Backend\api\AdmincategoriesController@change_categorie');

    // ADMIN CATEGORIES
    Route::get('adminchildcategories/search_select2', 'Backend\api\AdminchildcategoriesController@search_select2');
    Route::resource('adminchildcategories', 'Backend\api\AdminchildcategoriesController');
    Route::put('adminchildcategories/up_categorie/{id}', 'Backend\api\AdminchildcategoriesController@up_categorie');
    Route::put('adminchildcategories/down_categorie/{id}', 'Backend\api\AdminchildcategoriesController@down_categorie');
    Route::put('adminchildcategories/change_categorie/{id}', 'Backend\api\AdminchildcategoriesController@change_categorie');

    // ADMIN COUNTRIES
    Route::get('admincountries/search_select2', 'Backend\api\AdmincountriesController@search_select2');
    Route::get('admincountries/search_datatable', 'Backend\api\AdmincountriesController@search_datatable');
    Route::resource('admincountries', 'Backend\api\AdmincountriesController');

    // ADMIN COMMENTS
    Route::get('admincomments/search_datatable/{id}', 'Backend\api\AdmincommentsController@search_datatable');
    Route::resource('admincomments', 'Backend\api\AdmincommentsController');

    // ADMIN cmposts
    Route::get('admincmposts/search_datatable/{id}', 'Backend\api\AdmincmpostsController@search_datatable');
    Route::resource('admincmposts', 'Backend\api\AdmincmpostsController');

    // ADMIN SETTING
    Route::get('adminsettings/show_info_website', 'Backend\api\AdminsettingsController@show_info_website');
    Route::put('adminsettings/update_info_website/{id}', 'Backend\api\AdminsettingsController@update_info_website');

    // ADMIN BLOGCATEGORIES
    Route::get('adminblogcategories/search_select2', 'Backend\api\AdminblogcategoriesController@search_select2');
    Route::get('adminblogcategories/search_datatable', 'Backend\api\AdminblogcategoriesController@search_datatable');
    Route::resource('adminblogcategories', 'Backend\api\AdminblogcategoriesController');
    Route::post('adminblogcategories/pos_blogcategory', 'Backend\api\AdminblogcategoriesController@pos_blogcategory');

    // ADMIN size
    Route::get('adminsizes/search_select2', 'Backend\api\AdminsizesController@search_select2');
    Route::get('adminsizes/search_datatable', 'Backend\api\AdminsizesController@search_datatable');
    Route::resource('adminsizes', 'Backend\api\AdminsizesController');
    Route::post('adminsizes/pos_size', 'Backend\api\AdminsizesController@pos_blogcategory');

    // ADMIN INFORMATIONCATEGORIES
    Route::get('admininformationcategories/search_select2', 'Backend\api\AdmininformationcategoriesController@search_select2');
    Route::get('admininformationcategories/search_datatable', 'Backend\api\AdmininformationcategoriesController@search_datatable');
    Route::resource('admininformationcategories', 'Backend\api\AdmininformationcategoriesController');

    Route::post('admininformationcategories/pos_informationcategory', 'Backend\api\AdmininformationcategoriesController@pos_informationcategory');

    // ADMIN INFORMATION
    Route::get('admininformation/search_select2', 'Backend\api\AdmininformationsController@search_select2');
    Route::get('admininformation/search_datatable', 'Backend\api\AdmininformationsController@search_datatable');
    Route::post('admininformation/pos_information', 'Backend\api\AdmininformationsController@pos_information');
    Route::resource('admininformation', 'Backend\api\AdmininformationsController');

    // ADMIN PARTNER SUPPORT
    Route::get('adminpartnersupports/search_datatable', 'Backend\api\AdminpartnersupportsController@search_datatable');
    Route::resource('adminpartnersupports', 'Backend\api\AdminpartnersupportsController');
    Route::put('adminpartnersupports/up_partnersupport/{id}', 'Backend\api\AdminpartnersupportsController@up_partnersupport');
    Route::put('adminpartnersupports/down_partnersupport/{id}', 'Backend\api\AdminpartnersupportsController@down_partnersupport');
    Route::put('adminpartnersupports/change_partnersupport/{id}', 'Backend\api\AdminpartnersupportsController@change_partnersupport');

    // ADMIN REGULATION
    Route::get('adminregulations/search_datatable', 'Backend\api\AdminregulationsController@search_datatable');
    Route::post('adminregulations/pos_regulation', 'Backend\api\AdminregulationsController@pos_regulation');
    Route::resource('adminregulations', 'Backend\api\AdminregulationsController');

    // ADMIN BLOGS
    Route::get('adminblogs/search_datatable', 'Backend\api\AdminblogsController@search_datatable');
    Route::post('adminblogs/pos_blog', 'Backend\api\AdminblogsController@pos_blog');
    Route::resource('adminblogs', 'Backend\api\AdminblogsController');
    // ADMIN DonorS
    Route::get('admindonors/search_datatable', 'Backend\api\AdmindonorsController@search_datatable');
    Route::post('admindonors/pos_donor', 'Backend\api\AdmindonorsController@pos_donor');
    Route::resource('admindonors', 'Backend\api\AdmindonorsController');

    // ADMIN custompages
    Route::get('admincustompages/search_datatable', 'Backend\api\AdmincustompagesController@search_datatable');
    Route::put('admincustompages/up_custompage/{id}', 'Backend\api\AdmincustompagesController@up_custompage');
    Route::put('admincustompages/down_custompage/{id}', 'Backend\api\AdmincustompagesController@down_custompage');
    Route::put('admincustompages/change_custompage/{id}', 'Backend\api\AdmincustompagesController@change_custompage');
    Route::resource('admincustompages', 'Backend\api\AdmincustompagesController');

    // ADMIN CHAINSTORES
    Route::get('adminchainstores/search_datatable', 'Backend\api\AdminchainstoresController@search_datatable');
    Route::resource('adminchainstores', 'Backend\api\AdminchainstoresController');

    // ADMIN USERS
    Route::get('adminusers/search_datatable', 'Backend\api\AdminusersController@search_datatable');
    Route::post('adminusers/change_pass', 'Backend\api\AdminusersController@change_pass');
    Route::resource('adminusers', 'Backend\api\AdminusersController');

    // ADMIN DISCOUNTS
    Route::get('admindiscounts/search_datatable', 'Backend\api\AdmindiscountsController@search_datatable');
    Route::resource('admindiscounts', 'Backend\api\AdmindiscountsController');

    // ADMIN Function Authorities
    Route::get('adminfunctionauthories/search_select2_group_url', 'Backend\api\AdminfunctionauthoritiesController@search_select2_group_url');

    Route::get('adminfunctionauthories/search_select2_function', 'Backend\api\AdminfunctionauthoritiesController@search_select2_function');
    Route::get('adminfunctionauthories/search_datatable', 'Backend\api\AdminfunctionauthoritiesController@search_datatable');
    Route::resource('adminfunctionauthories', 'Backend\api\AdminfunctionauthoritiesController');

    // ADMIN VOUCHERS
    Route::get('adminvouchers/search_datatable', 'Backend\api\AdminvouchersController@search_datatable');
    Route::resource('adminvouchers', 'Backend\api\AdminvouchersController');

    // ADMIN AUTHORITY
    Route::get('adminauthorities/search_datatable', 'Backend\api\AdminauthoritiesController@search_datatable');
    Route::resource('adminauthorities', 'Backend\api\AdminauthoritiesController');

    // ADMIN banks
    Route::get('adminbanks/search_datatable', 'Backend\api\AdminbanksController@search_datatable');
    Route::resource('adminbanks', 'Backend\api\AdminbanksController');
});


// ADMIN PROJECTS
Route::get('api/adminprojects/search_datatable', 'Backend\api\AdminprojectsController@search_datatable');
Route::resource('api/adminprojects', 'Backend\api\AdminprojectsController');

// MLANDS
Route::get('api/mlands/search_select2', 'MLandsController@search_select2');

// PROVINCE
Route::get('api/mprovinces/search_select2', 'MProvincesController@search_select2');

// DISTRICT
Route::get('api/mdistricts/search_select2', 'MDistrictsController@search_select2');





