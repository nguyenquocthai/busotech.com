<style>
    .table_export{
        width: 100%
    }
    .table_export tbody{
        opacity: 0;
        visibility: hidden;
        height: 0;
        overflow: hidden;
        display: block;
        position: absolute;
    }
</style>
<table class="table_export">
    <tbody>
        <tr>
            
            <td>Email</td>
            <td>Ngày đăng ký</td>
            
        </tr>
        @if (count($subscribes) != 0)
            @foreach ($subscribes as $subscribe)
            <tr>
                <td>{{$subscribe['email']}}</td>
                <td>{{ date("d/m/Y", strtotime($subscribe['created_at']))}}</td>
            </tr>
            @endforeach
        @else

        @endif
    </tbody>
</table>