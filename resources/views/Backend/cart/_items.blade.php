
<div class="col-sm-5">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Khách hàng</span>
                </div>
            </div>

            <div class="portlet-body form">

                <div class="datatable-scroll table-responsive clearfix">
					<table class="table table-bordered table-hover">
						<tbody>
							<tr>
								<td>Họ tên</td>
								<td>{{$khachhang->name}}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{$khachhang->email}}</td>
							</tr>
							<tr>
								<td>Số điện thoại</td>
								<td>{{$khachhang->phone}}</td>
							</tr>
							<tr>
								<td>Địa chỉ</td>
								<td>{{$khachhang->address}}</td>
							</tr>
							<tr>
								<td>Quận/huyện</td>
								<td>{{$khachhang->huyen}}</td>
							</tr>
							<tr>
								<td>Tỉnh/thành phố</td>
								<td>{{$khachhang->tinh}}</td>
							</tr>
							<!-- <tr>
								<td>Trạng thái</td>
								<td>
									<select class="form-control" name="status">
						                <option value="0">Chưa thanh toán</option>
						                <option value="1">Đã thanh toán</option>
						            </select>
								</td>
							</tr> -->
							
						</tbody>
					</table>
				</div>

            </div>

        </div>
    </div>
    <div class="col-sm-7">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-settings font-green-sharp"></i>
                    <span class="caption-subject bold uppercase">Chi tiết</span>
                </div>
            </div>

            <div class="portlet-body form">

                <div class="datatable-scroll table-responsive clearfix">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Tên sản phẩm</th>
								<th>Hình đại diện</th>
								<th>Số lượng</th>
								
							</tr>
						</thead>
						<tbody>
							@if (count($carts) != 0)
							    @foreach ($carts as $cart)
							    <tr>
									<td>{{@$cart->product->title}}</td>
									<td>
										<div class="table_img">
											<img alt="..." src="{{BladeGeneral::GetImg(['avatar' => @$cart->product->avatar,'data' => 'product', 'time' => @$cart->product->updated_at])}}">
										</div>
									</td>
									<td>{{$cart->soluong}}</td>
									
								</tr>
							    @endforeach
							    <tr style="font-weight: 600">
							    	<td>Thành tiền</td>
							    	<td colspan="3" style="text-align: right;color: red;">{{number_format($khachhang->thanhtien)}} đ</td>
							    </tr>
							@else
							
							@endif
						</tbody>
					</table>
				</div>

            </div>

        </div>
    </div>

