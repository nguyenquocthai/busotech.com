<style>
    .table_export{
        width: 100%
    }
    .table_export tbody{
        opacity: 0;
        visibility: hidden;
        height: 0;
        overflow: hidden;
        display: block;
        position: absolute;
    }
</style>
<table class="table_export">
    <tbody>
        <tr>
            <td>Tên</td>
            <td>Điện thoại</td>
            <td>Email</td>
            <td>Ngày đăng ký</td>
            <td>Nội dung</td>
        </tr>
        @if (count($contacts) != 0)
            @foreach ($contacts as $contact)
            <tr>
                <td>{{$contact['name']}}</td>
                <td>{{$contact['tel']}}</td>
                <td>{{$contact['email']}}</td>
                <td>{{ date("d/m/Y", strtotime($contact['created_at']))}}</td>
                <td>{{$contact['url']}}</td>
            </tr>
            @endforeach
        @else

        @endif
    </tbody>
</table>