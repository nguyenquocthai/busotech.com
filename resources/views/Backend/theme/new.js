WebWorldApp.controller('themes.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'theme';

        // Title block
        $scope.detail_block_title = 'theme';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.theme = {};
            $scope.theme.status = '0';
            $scope.theme.highlight = '0';

            select2s('#theme_cat',{
                commonService: commonService,
                name:'theme_categories',
                have_default: true,
            });

            select2s('#theme_cat1',{
                commonService: commonService,
                name:'theme_categorie1s',
                have_default: true,
            });

            tinymce.remove();
            load_tinymce('#content', null);
            load_tinymce('#content_en', null);
        }

        $('#theme_cat').change(function(event) {
            var id_theme_cat = $(this).val();
            $('#theme_cat1').html('');
            select2s('#theme_cat1',{
                commonService: commonService,
                name:'theme_categorie1s',
                have_default: true,
                where: ['id_theme_cat,=,' + id_theme_cat]
            });
        });

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.theme.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.validate = function(value) {
            return value ? false : true ;
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            if ($("#form_box .file_image").length != 0) {
                if ($("#form_box .file_image").get(0).files[0])
                    files = files.concat($("#form_box .file_image").get(0).files[0]);
            }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            data['content'] = tinymce.get('content').getContent();
            data['content_en'] = tinymce.get('content_en').getContent();

            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_theme', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/themes');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
