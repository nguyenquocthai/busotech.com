WebWorldApp.controller('buso_steps.index', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload', '$routeParams',
    function ($scope, $rootScope, commonService, $location, fileUpload, $routeParams) {

        $rootScope.app.title = 'Lộ trình phát triển sản phẩm';
        $scope.title = 'Lộ trình phát triển sản phẩm';
        $scope.buso_step = {};

        // DATATABLE
        commonService.requestFunction('index_buso_step', $routeParams, function(e) {

            $scope.form = $('#userForm').html();
            $scope.value = e.data;
            $scope.datatable = $('#tbl-data').DataTable({
                "order": [[ 2, "asc" ]],
                columnDefs: [
                    { sortable: false, searchable: false, targets: [ 5,6 ] },
                    { class: 'center-text', targets: [ 2,5,6 ] },
                    { class: 'hidden', targets: [ 0,1 ] }
                ],
                "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                    $(nRow).attr('data-id', aData[0]);
                },
                displayStart: 0,
                displayLength: 5,
                data: e.data,
                "autoWidth": false
            });
            $scope.datatable.columns().search('').draw();
            $scope.datatable.order([2, 'asc']).draw();
        });

        $('#user').change(function(buso_step) {

            if ($(this).val() != $routeParams.id_ctv) {
                if ($(this).val() == 0) {
                    document.location.href = '/admin/buso_steps';
                    return false;
                }
                $('#search_form').submit();
            }
        });

        //-------------------------------------------------------------------------------
        $scope.search_datatable = function() {
            var title = $('.search_title').val() || '';
            $scope.datatable.columns(3).search(title)
                            .draw();
        };

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.btn-edit-row', function(buso_step) {
            var data = {};
            var curenpage = $scope.datatable.page.info().page;
            var id = $(this).attr('data-id');

            $('#form_edit').attr('data-curenpage', curenpage);
            $('#form_edit').attr('data-id', id);
            
            data['status_code'] = "show";
            data['_method'] = 'PUT';
            commonService.requestFunction('update_buso_step/' + id, data, function(e) {
                $('#form_edit').html(e.data);
                select2s('#buso_step_cat',{
                    commonService: commonService,
                    name:'buso_step_cats',
                    have_default: true,
                    selected: $('#buso_step_cat').attr('data-select')
                });
                $('#modal_editrow').modal('show');
            });
        });

        //-------------------------------------------------------------------------------
        $('.btn_editrow').click(function(buso_step) {
            var request = {};
            var data = {};
            var files = [];

            var id_edit = $('#form_edit').attr('data-id');

            var curenpage_string = $('#form_edit').attr('data-curenpage');
            var curenpage = parseInt(curenpage_string, 10);

            var files = [];
            if ($("#form_edit .file_image").length != 0) {
                if ($("#form_edit .file_image").get(0).files[0])
                    files = files.concat($("#form_edit .file_image").get(0).files[0]);
            }

            $("#form_edit").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);

            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";
            request['vitri'] = parseInt($('.item-position.active').html(), 10);
            
            fileUpload.uploadFileToUrl(files, request, 'update_buso_step/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $('#form_edit').html('');
                        $('#modal_editrow').modal('hide');
                        for (var i = 0; i < $scope.value.length; i++) {
                            if($scope.value[i][0] == id_edit){
                                $scope.value[i] = e.row;
                            }
                        }

                        $scope.datatable.clear().draw();
                        $scope.datatable.rows.add($scope.value); // Add new data
                        $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                        $scope.datatable.page( curenpage ).draw( false );
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.delete_row', function(buso_step) {
            var row_id = $(this).attr('data-id');
            var row = $(this).closest('tr');
            confirmPopup('Xóa menu', 'Bạn muốn xóa menu này? ', function() {
                commonService.requestFunction('delete_buso_step/' + row_id + $rootScope.api_token, {}, function(e) {
                    switch (e.code) {
                        case 200:
                            $scope.datatable.row(row).remove().draw( false );
                            commonService.requestFunction('index_buso_step', {}, function(e) {
                                $scope.value = e.data;
                            });
                            break;
                        default:
                            break;
                    }
                });
            });
        });

        //-------------------------------------------------------------------------------
        $('#form_edit').on('click', '.item-position', function(buso_step) {

            var data = {};
            var here = $(this);
            var id = $('#form_edit').attr('data-id');
            var curenpage_string = $('#form_edit').attr('data-curenpage');
            var curenpage = parseInt(curenpage_string, 10);

            data['id_swap'] = $(this).attr('data-id');
            data['status_code'] = 'change';
            data['_method'] = 'PUT';

            if(data['id_swap'] == id){
                return false;
            }

            commonService.requestFunction('update_buso_step/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        $('.item-position.active').attr('data-id',data['id_swap']);
                        here.attr('data-id',id);
                        $('.item-position').removeClass('active');
                        here.addClass('active');
                        commonService.requestFunction('index_buso_step', {}, function(d) {
                            $scope.value = d.data;
                            $scope.datatable.clear().draw();
                            $scope.datatable.rows.add($scope.value); // Add new data
                            $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                            $scope.datatable.page( curenpage ).draw( false );
                        });
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.up', function(buso_step) {

            var curenpage = $scope.datatable.page.info().page;
            var id = $(this).attr('data-id');

            var data = {};
            data['status_code'] = "up";
            data['_method'] = 'PUT';

            $('#tbl-data tbody tr').removeClass('active');
            commonService.requestFunction('update_buso_step/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        for (var y = 0; y < $scope.value.length; y++) {
                            if($scope.value[y][0] == e.idnew){
                                var vtrinew = $scope.value[y][2];
                            }
                            if($scope.value[y][0] == e.idold){
                                var vtriold = $scope.value[y][2];
                            }
                        }
                        for (var i = 0; i < $scope.value.length; i++) {
                            if($scope.value[i][0] == e.idnew){
                                $scope.value[i][1] = e.posnew;
                                $scope.value[i][2] = vtriold;
                            }
                            if($scope.value[i][0] == e.idold){
                                $scope.value[i][1] = e.posold;
                                $scope.value[i][2] = vtrinew;
                            }
                        }

                        $scope.datatable.clear().draw();
                        $scope.datatable.rows.add($scope.value); // Add new data
                        $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                        $scope.datatable.page( curenpage ).draw( false );
                        $('#tbl-data tbody tr[data-id="'+id+'"]').addClass('active');
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.down', function(buso_step) {
            
            var curenpage = $scope.datatable.page.info().page;
            var id = $(this).attr('data-id');

            var data = {};
            data['status_code'] = "down";
            data['_method'] = 'PUT';

            $('#tbl-data tbody tr').removeClass('active');
            commonService.requestFunction('update_buso_step/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        for (var y = 0; y < $scope.value.length; y++) {
                            if($scope.value[y][0] == e.idnew){
                                var vtrinew = $scope.value[y][2];
                            }
                            if($scope.value[y][0] == e.idold){
                                var vtriold = $scope.value[y][2];
                            }
                        }
                        for (var i = 0; i < $scope.value.length; i++) {
                            if($scope.value[i][0] == e.idnew){
                                $scope.value[i][1] = e.posnew;
                                $scope.value[i][2] = vtriold;
                            }
                            if($scope.value[i][0] == e.idold){
                                $scope.value[i][1] = e.posold;
                                $scope.value[i][2] = vtrinew;
                            }
                        }

                        $scope.datatable.clear().draw();
                        $scope.datatable.rows.add($scope.value); // Add new data
                        $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                        $scope.datatable.page( curenpage ).draw( false );
                        $('#tbl-data tbody tr[data-id="'+id+'"]').addClass('active');
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on( 'draw.dt', function () {
            $('.table_img').each(function(index, el) {
                var path = $(this).attr('data-path');
                $(this).html('<img alt="..." src="'+path+'"></img>');
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.btn_status', function(buso_step) {

            var here = $(this);
            var id = $(this).closest('tr').attr('data-id');
            var curenpage = $scope.datatable.page.info().page;

            var data = {};
            data['status_code'] = "change_status";
            data['_method'] = 'PUT';

            commonService.requestFunction('update_buso_step/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        for (var i = 0; i < $scope.value.length; i++) {
                            if($scope.value[i][0] == id){
                                $scope.value[i][5] = e.status;
                            }
                        }

                        $scope.datatable.clear().draw();
                        $scope.datatable.rows.add($scope.value); // Add new data
                        $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                        $scope.datatable.page( curenpage ).draw( false );
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.table_img', function(buso_step) {
            var path = $(this).attr('data-path');
            window.open(path);
        }); 
    }
]);
