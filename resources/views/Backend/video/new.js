WebWorldApp.controller('videos.new', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {
        $rootScope.app.title = 'Tạo mới: video';

        // Title block
        $scope.detail_block_title = 'Chi tiết video';
        $scope.image_block_title = 'Hình ảnh';

        //-------------------------------------------------------------------------------
        $scope.initApp = function () {
            $scope.video = {};
            $scope.video.status = '0';
            // $scope.video.noibat = '1';

            // tinymce.remove();
            // load_tinymce('#content', null);
        }

        //-------------------------------------------------------------------------------
        $scope.slugify = function(str) {
            $scope.video.slug = slugify(str);
        };

        //-------------------------------------------------------------------------------
        $scope.submit = function () {

            var request = {};
            var data = {};
            var files = [];
            // if ($("#form_box .file_image").length != 0) {
            //     if ($("#form_box .file_image").get(0).files[0])
            //         files = files.concat($("#form_box .file_image").get(0).files[0]);
            // }

            $("#form_box").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);
            request['noimg'] = 1;
            // data['content'] = tinymce.get('content').getContent();
            request['value'] = data;
            request['status_code'] = 'store';

            fileUpload.uploadFileToUrl(files, request, 'create_video', function(e) {
                switch (e.code) {
                    case 200:
                        $location.path('/admin/videos');
                        break;
                    default:
                        break;
                }
            });
        }

        //-------------------------------------------------------------------------------
        $scope.back = function() {
            window.history.back();
        };

        //-------------------------------------------------------------------------------
        $scope.$on('$viewContentLoaded', function () {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.initApp();
                });
            }, 200)
        });
    }
]);
