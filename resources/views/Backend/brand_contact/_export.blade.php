<style>
    .table_export{
        width: 100%
    }
    .table_export tbody{
        opacity: 0;
        visibility: hidden;
        height: 0;
        overflow: hidden;
        display: block;
        position: absolute;
    }
</style>
<table class="table_export">
    <tbody>
        <tr>
            <td>Tên</td>
            <td>Điện thoại</td>
            <td>Email</td>
            <td>Ngày đăng ký</td>
            <td>Nội dung thuong hiệu khách đăng ký</td>
        </tr>
        @if (count($brand_contacts) != 0)
            @foreach ($brand_contacts as $brand_contact)
            <tr>
                <td>{{$brand_contact['name']}}</td>
                <td>{{$brand_contact['tel']}}</td>
                <td>{{$brand_contact['email']}}</td>
                <td>{{ date("d/m/Y", strtotime($brand_contact['created_at']))}}</td>
                <td>{{$brand_contact['branding_type']}}</td>
            </tr>
            @endforeach
        @else

        @endif
    </tbody>
</table>