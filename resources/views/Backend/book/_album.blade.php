@if (count($item_albums) != 0)
    @foreach ($item_albums as $item_album)
  	<div data-id="{{$item_album->id}}" class="col-sm-2 item-load-item_album">
		<div class="img-mod">
			<a onclick="delete_item_album({{$item_album->id}})" class="btn btn-xs red" title="Xóa">
			    <i class="fa fa-trash-o"></i> Xóa
			</a>
			<img src="{{BladeGeneral::GetImg(['avatar' => $item_album->name,'data' => 'item_album', 'time' => $item_album->updated_at])}} " alt="">
		</div>
	</div>
    @endforeach
@else
<div class="col-sm-12">Chưa có hình</div>
@endif