<?php 
// echo "<pre>"; print_r($data);die; echo "</pre";
 ?>
<div class="form-group">
    <label>Họ Tên</label>
    <input value="{{$data->name}}" type="text" class="form-control" name="name" readonly>
</div>

<div class="form-group">
    <label class="next-label">Trạng thái</label>
    <select class="form-control" ng-required="true" name="status">
        <option @if($data->status == 0) selected @endif value="0">Đã xem</option>
        <option @if($data->status == 1) selected @endif value="1">Chưa xem</option>
    </select>
</div>
<div class="form-group">
    <label class="next-label">Email</label>
    <input type="text" class="form-control" name="email" readonly value="{{$data->email}}">
</div>
<div class="form-group">
    <label class="next-label">Số điện thoại</label>
    <input type="text" class="form-control" name="tel" readonly value="{{$data->tel}}">
</div>
<div class="form-group">
    <label class="next-label">Tên cửa hàng</label>
    <input type="text" class="form-control" name="name_shop" readonly value="{{$data->name_shop}}">
</div>
<div class="form-group">
    <label class="next-label">Địa chỉ cửa hàng</label>
    <input type="text" class="form-control" name="address_shop" readonly value="{{$data->address_shop}}">
</div>
<div class="form-group">
    <label class="next-label">Ghi chú</label>
    <textarea class="form-control" name="note" rows="3">{{$data->note}}</textarea>
</div>