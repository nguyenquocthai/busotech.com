<style>
    .table_export{
        width: 100%
    }
    .table_export tbody{
        opacity: 0;
        visibility: hidden;
        height: 0;
        overflow: hidden;
        display: block;
        position: absolute;
    }
</style>
<table class="table_export">
    <tbody>
        <tr>
            <td>Tên</td>
            <td>Điện thoại</td>
            <td>Email</td>
            <td>Ngày đăng ký</td>
            <td>Tên cửa hàng</td>
            <td>Địa chỉ cửa hàng</td>
        </tr>
        @if (count($partner_contacts) != 0)
            @foreach ($partner_contacts as $partner_contact)
            <tr>
                <td>{{$partner_contact['name']}}</td>
                <td>{{$partner_contact['tel']}}</td>
                <td>{{$partner_contact['email']}}</td>
                <td>{{ date("d/m/Y", strtotime($partner_contact['created_at']))}}</td>
                <td>{{$partner_contact['name_shop']}}</td>
                <td>{{$partner_contact['address_shop']}}</td>
            </tr>
            @endforeach
        @else

        @endif
    </tbody>
</table>