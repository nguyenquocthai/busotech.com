<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> Admin theme</div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>

<div class="modal fade" id="changepass" tabindex="-1" role="dialog" aria-labelledby="changepassLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="changepassLabel">Change password</h4>
            </div>
            <div class="modal-body">
                <form id="form_change_pass" autocomplete="off">
                    <div class="form-group">
                        <label class="control-label">Old password</label>
                        <input type="password" class="form-control" name="password_old">
                    </div>

                    <div class="form-group">
                        <label class="control-label">New password</label>
                        <input type="password" class="form-control" name="password_new">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button  id="btn_change_pass_admin" type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- END FOOTER -->