<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">

        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>

            <li class="nav-item start ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Admin home</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="/admin" class="nav-link">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Quản lý chung</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">Tổng quan website</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    <li class="nav-item">
                        <a href="{{ url('admin/settings') }}" class="nav-link ">
                            <span class="title">Thông tin website</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/seo_pages') }}" class="nav-link ">
                            <span class="title">Seo Page</span>
                        </a>
                    </li>
                    <!-- <li class="nav-item">
                        <a href="{{ url('admin/db_langs') }}" class="nav-link ">
                            <span class="title">Ngôn ngữ</span>
                        </a>
                    </li> -->

                </ul>
            </li>
            <li class="heading">
                <h3 class="uppercase">Bảng dữ liệu</h3>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Trang chủ</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    <li class="nav-item">
                        <a href="/admin/intro/edit/10" class="nav-link ">Header background</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/services') }}" class="nav-link ">Dịch vụ</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/buso_steps') }}" class="nav-link ">Roadmaps</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/doitacs') }}" class="nav-link ">Đối tác</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Giới thiệu</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    <li class="nav-item">
                        <a href="{{ url('admin/intros') }}" class="nav-link ">Danh sách</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/dinhhuongs') }}" class="nav-link ">Định hướng sản phẩm</a>
                    </li>
                </ul>
            </li>
            
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Nhân sự</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    <li class="nav-item">
                        <a href="{{ url('admin/personnels') }}" class="nav-link ">Danh sách</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/personnel_values') }}" class="nav-link ">Giá trị cốt lõi</a>
                    </li>

                </ul>
            </li>
            
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Dự án đã thực hiện</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/duan_cats') }}" class="nav-link ">Danh mục</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/duans') }}" class="nav-link ">Danh sách</a>
                    </li>

                </ul>
            </li>
            
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Tin tức</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/slider_blogs') }}" class="nav-link ">Slider</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/blog_cats') }}" class="nav-link ">Danh mục</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/blogs') }}" class="nav-link ">Danh sách</a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Thư viện tin</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/sliders') }}" class="nav-link ">Slider</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/library_cats') }}" class="nav-link ">Danh mục</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/librarys') }}" class="nav-link ">Danh sách</a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Buso brand</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/brand_steps') }}" class="nav-link ">Các bước xây dựng thương hiệu</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/brand_duans') }}" class="nav-link ">Dự án thương hiệu</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/brand_items') }}" class="nav-link ">Danh sách thương hiệu</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/brand_contacts') }}" class="nav-link ">Liên hệ thương hiệu</a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Hỗ trợ</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    
                    <li class="nav-item">
                        <a href="{{ url('admin/support_cats') }}" class="nav-link ">Danh mục hướng dẫn sử dụng</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/supports') }}" class="nav-link ">Danh sách hướng dẫn sử dụng</a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('admin/supportfaq_cats') }}" class="nav-link ">Danh mục câu hỏi</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/supportfaqs') }}" class="nav-link ">Danh sách câu hỏi</a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('admin/videos') }}" class="nav-link ">Danh sách video</a>
                    </li>

                </ul>
            </li>
            
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <span class="title">Theme</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/theme_categories') }}" class="nav-link ">Danh mục</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/theme_categorie1s') }}" class="nav-link ">Danh mục cấp 1</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/themes') }}" class="nav-link ">Danh sách</a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Giá web</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/giatenmien_cats') }}" class="nav-link ">Danh mục</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/giatenmiens') }}" class="nav-link ">Danh sách</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/website_feature_cats') }}" class="nav-link ">Danh mục tính năng web</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/website_features') }}" class="nav-link ">Các tính năng</a>
                    </li>
                    
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Giá hosting</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/hosting_advantages') }}" class="nav-link ">Ưu điểm hosting</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/hosting_cats') }}" class="nav-link ">Danh mục</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/hostings') }}" class="nav-link ">Danh sách</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/hosting_feature_cats') }}" class="nav-link ">Danh mục tính năng hosting</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/hosting_features') }}" class="nav-link ">Các tính năng hosting</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/hosting_contacts') }}" class="nav-link ">Đăng ký hosting</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Giá tên miền</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/price_domain_cats') }}" class="nav-link ">Danh mục</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/price_domains') }}" class="nav-link ">Danh sách</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Tuyển dụng</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/departments') }}" class="nav-link ">Phòng ban</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/recruitments') }}" class="nav-link ">Danh sách</a>
                    </li>
                    
                    <li class="nav-item">
                        <a href="{{ url('admin/recruitment_benefits') }}" class="nav-link ">Quyền lợi ứng viên</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/recruitment_contacts') }}" class="nav-link ">Danh sách ứng viên</a>
                    </li>
                </ul>
            </li>
                                
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Chính sách</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/policies') }}" class="nav-link ">Danh sách chính sách</a>
                    </li>
                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <!-- <i class="icon-envelope-letter"></i> -->
                            <span class="title">Đại lý và đối tác</span>
                            <span class="selected"></span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item">
                               <a href="{{ url('admin/partner_intros') }}" class="nav-link ">Giới thiệu đại lý và đối tác</a> 
                            </li>
                            <li class="nav-item">
                               <a href="{{ url('admin/partner_advantages') }}" class="nav-link ">Ưu điểm đại lý và đối tác</a> 
                            </li>
                            <li class="nav-item">
                               <a href="{{ url('admin/partner_steps') }}" class="nav-link ">Các bước đăng ký đại lý và đối tác</a> 
                            </li>
                            <li class="nav-item">
                               <a href="{{ url('admin/partner_contacts') }}" class="nav-link ">Danh sách đăng ký đại lý và đối tác</a> 
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/payment_guides') }}" class="nav-link ">Hướng dẫn thanh toán</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/bank_accs') }}" class="nav-link ">Danh sách thẻ ngân hàng</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <!-- <i class="icon-envelope-letter"></i> -->
                    <span class="title">Liên hệ</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('admin/contacts') }}" class="nav-link ">Danh sách đăng ký theme website</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('admin/subscribes') }}" class="nav-link ">Danh sách đăng ký nhận tin khuyến mãi</a>
                    </li>
                </ul>
            </li>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR