<?php
//-< FRONTEND >--------------------------------------------------------------------------

// Home
Route::get('/', 'Frontend\HomeController@index');
Route::post('/chang_lang', 'Frontend\HomeController@chang_lang');
Route::get('/doi-ngu/{slug}', 'Frontend\HomeController@doingu');
Route::get('/dang-ky-thanh-cong', 'Frontend\HomeController@dangkythanhcong');

// giao diện
Route::get('/giao-dien/{slug}', 'Frontend\ThemeController@detail');
Route::get('/trial/{slug}', 'Frontend\ThemeController@trial');
Route::get('/demo/{slug}', 'Frontend\ThemeController@demo');
Route::get('/danh-muc-giao-dien/{slug}', 'Frontend\ThemeController@category');
Route::get('/giao-dien', 'Frontend\ThemeController@index');

// san pham
Route::get('/san-pham', 'Frontend\ProductController@index');
Route::get('/san-pham/{slug}', 'Frontend\ProductController@detail');
Route::get('/danh-muc-san-pham/{slug}', 'Frontend\ProductController@cat');
Route::get('/gio-hang', 'Frontend\ProductController@viewcart');
// dich vu
Route::get('/busoweb', 'Frontend\ServiceController@busoweb');
// hosting
Route::get('/hosting', 'Frontend\HostingController@index');
Route::get('/hosting-register', 'Frontend\HostingController@register');
// chinh sach
Route::get('/chinh-sach/{slug}', 'Frontend\PolicyController@quydinhsudung');
Route::get('/chinh-sach/chinh-sach-bao-mat', 'Frontend\PolicyController@chinhsachbaomat');
Route::get('/dai-ly-va-doi-tac', 'Frontend\PolicyController@dailyvadoitac');
Route::get('/khach-hang', 'Frontend\PolicyController@khachhang');
Route::get('/huong-dan-thanh-toan', 'Frontend\PolicyController@huongdanthanhtoan');
// giới thiệu
Route::get('/gioi-thieu', 'Frontend\IntroController@index');
Route::get('/nhan-su', 'Frontend\IntroController@personnel');
// brand
Route::get('/brand', 'Frontend\BrandController@index');
Route::get('/branding-form', 'Frontend\BrandController@brandingform');
Route::get('/branding-projects', 'Frontend\BrandController@brandingprojects');
// bang-gia
Route::get('/bang-gia', 'Frontend\PriceController@index');
// ho tro
Route::get('/ho-tro', 'Frontend\SupportController@index');
Route::get('/huong-dan-su-dung/{slug}', 'Frontend\SupportController@guide_cat');
Route::get('/huong-dan-su-dung', 'Frontend\SupportController@guide');
Route::get('/chi-tiet-huong-dan/{slug}', 'Frontend\SupportController@guide_detail');
Route::get('/cau-hoi', 'Frontend\SupportController@faq');
Route::get('/cau-hoi/{slug}', 'Frontend\SupportController@faqcat');
Route::get('/chi-tiet-cau-hoi/{slug}', 'Frontend\SupportController@faqdetail');
// tin tuc
Route::get('/tin-tuc', 'Frontend\BlogController@index');
Route::get('/tat-ca-tin-tuc', 'Frontend\BlogController@catall');
Route::get('/tin-tuc/{slug}', 'Frontend\BlogController@detail');
Route::get('/danh-muc-tin-tuc/{slug}', 'Frontend\BlogController@cat');
// thu vien
Route::get('/thu-vien', 'Frontend\LibraryController@index');
Route::get('/tat-ca-thu-vien', 'Frontend\LibraryController@catall');
Route::get('/thu-vien/{slug}', 'Frontend\LibraryController@detail');
Route::get('/danh-muc-thu-vien/{slug}', 'Frontend\LibraryController@cat');
// tuyen dung
Route::get('/tuyen-dung', 'Frontend\RecruitmentController@index');
Route::get('/tuyen-dung/{slug}', 'Frontend\RecruitmentController@detail');
//contact
Route::get('/lien-he', 'Frontend\ContactController@index');
Route::post('/add_contact', 'Frontend\ContactController@add_contact');
Route::post('/add_contact_theme', 'Frontend\ContactController@add_contact_theme');
Route::post('/check_email', 'Frontend\ContactController@check_email');
Route::post('/add_contact_brand', 'Frontend\ContactController@add_contact_brand');
Route::post('/add_tuyendung', 'Frontend\ContactController@add_tuyendung');
Route::post('/add_contact_hosting', 'Frontend\ContactController@add_contact_hosting');
Route::post('/add_contact_partner', 'Frontend\ContactController@add_contact_partner');
Route::post('/add_subscribe', 'Frontend\ContactController@add_subscribe');
// add comment
Route::post('/add_comment', 'Frontend\CommentController@add_comment');
// carts
Route::post('/add_cart', 'Frontend\cartController@add_cart');
Route::post('/remove_cart', 'Frontend\cartController@remove_cart');
Route::post('/update_cart', 'Frontend\cartController@update_cart');
Route::post('/send_cart', 'Frontend\cartController@send_cart');
Route::get('/show_cart', 'Frontend\cartController@viewcart1');
Route::get('/dat-hang-thanh-cong', 'Frontend\cartController@send_cart');
Route::get('/reponse_payment/{id}', 'Frontend\cartController@reponse_payment');
Route::post('/checkout', 'Frontend\cartController@checkout');
Route::get('/payment_success/{id}', 'Frontend\cartController@payment_success');
Route::get('/payment-status', 'Frontend\cartController@pay_ok');

// nguoidungs
Route::get('/info-account', 'Frontend\NguoidungController@thongtintaikhoan');
Route::get('/create-account', 'Frontend\NguoidungController@taotaikhoan');
Route::get('/register-success', 'Frontend\NguoidungController@dangkythanhcong');
Route::get('/account_active/{active_token}', 'Frontend\NguoidungController@account_active');
Route::get('/dang-xuat', 'Frontend\NguoidungController@dangxuat');
Route::get('/change-pass', 'Frontend\NguoidungController@doimatkhau');
Route::get('/logout', 'Frontend\NguoidungController@logout');
Route::get('/forgot-password', 'Frontend\NguoidungController@forgotpassword');
Route::get('/forget/{token_forget}', 'Frontend\NguoidungController@changepass');

Route::post('/add_nguoidung', 'Frontend\NguoidungController@add_nguoidung');
Route::post('/dangnhap', 'Frontend\NguoidungController@dangnhap');
Route::post('/suataikhoan', 'Frontend\NguoidungController@suataikhoan');
Route::post('/suataikhoan', 'Frontend\NguoidungController@suataikhoan');
Route::post('/mail_forgot_pass', 'Frontend\NguoidungController@mail_forgot_pass');
Route::post('/set_pass', 'Frontend\NguoidungController@set_pass');
Route::post('/login_social', 'Frontend\NguoidungController@login_social');


// dangtin
Route::get('/dang-tin', 'Frontend\DangtinController@index');
Route::get('/post-rental', 'Frontend\DangtinController@dangtinthue');
Route::get('/sua-dang-tin/{slug}', 'Frontend\DangtinController@edit');
Route::get('/contact-rental', 'Frontend\DangtinController@lienhebds');

Route::get('/create-rental', 'Frontend\DangtinController@create_item');
Route::get('/create-rental-step1/{id}', 'Frontend\DangtinController@create_item_b1');
Route::get('/create-rental-step2/{id}', 'Frontend\DangtinController@create_item_b2');
Route::get('/create-rental-step3/{id}', 'Frontend\DangtinController@create_item_b3');

Route::get('/sua-tin-thue/{id}', 'Frontend\DangtinController@edit_item');
Route::get('/get_tq_ti/{id}', 'Frontend\DangtinController@get_tq_ti');
Route::get('/get_tq_ti_item/{id}', 'Frontend\DangtinController@get_tq_ti_item');

Route::post('/api_search', 'Frontend\DangtinController@api_search');
Route::post('/api_dangtin', 'Frontend\DangtinController@api_dangtin');
Route::post('/api_crate_item', 'Frontend\DangtinController@api_crate_item');
Route::post('/api_lienhebds', 'Frontend\DangtinController@api_lienhebds');

// Demogid
Route::get('/store', 'Frontend\StoreController@index');
Route::get('/datatable_store', 'Frontend\StoreController@datatable_store');
Route::get('/datatable_store2', 'Frontend\StoreController@datatable_store2');

// Seach
Route::get('/search', 'Frontend\SearchController@index');

//-< FRONTEND >--------------------------------------------------------------------------


//-< BACKEND >--------------------------------------------------------------------------

Route::get('login', 'Backend\AdminController@login');
Route::group(['prefix' => 'admin', 'middleware' => 'AdminLogin'], function() {

    Route::post('/session', 'Backend\SessionController@index');

    Route::get('/logout', 'Backend\AdminController@logout');

    Route::get('/', 'Backend\AdminController@index');
    Route::get('{multi}', 'Backend\AdminController@index');

    $name_apps = config('general.name_apps');
    foreach ($name_apps as $key => $name) {
	    Route::get( $name.'/new', 'Backend\AdminController@index' );
    	Route::get( $name.'/edit/{multi}', 'Backend\AdminController@index' );
	}
    
});

// API -------------------------------------------------------------
Route::post('/api/adminusers/change_pass', 'Backend\api\AdminusersController@change_pass');
Route::group(['prefix' => 'api', 'middleware' => ['api']], function () {

    // Login
    Route::get('login', 'Backend\api\AuthController@login');
    Route::get('adminselect2s', 'Backend\api\Adminselect2sController@index');

    Route::post('adminusers', 'Backend\api\AdminusersController@change_pass');

    Route::get('save_session', 'Backend\api\AuthController@save_session');
    
    $name_apps = config('general.name_apps');
    foreach ($name_apps as $key => $name) {
	    Route::resource('admin'.$name.'s', 'Backend\api\Admin'.$name.'sController');
	}
    
});

//-< BACKEND >--------------------------------------------------------------------------

// clear cache
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    // Artisan::call('config:cache');
    Artisan::call('view:clear');

    Artisan::call('route:clear');
    
    return "view is cleared";
});