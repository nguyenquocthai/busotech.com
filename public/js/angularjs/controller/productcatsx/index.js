WebWorldApp.controller('productcats.index', ['$scope','$rootScope', 'commonService', '$location', 'fileUpload',
    function ($scope, $rootScope, commonService, $location, fileUpload) {

        $rootScope.app.title = 'Quản trị: loại cho thuê';
        $scope.title = 'Danh sách loại cho thuê';
        $scope.form_add = $('#form_add').html();
        $scope.productcat = {};
        $scope.form = $('#userForm').html();
        
        // DATATABLE
        commonService.requestFunction('index_productcat', {}, function(e) {

            $('#nestable').html(e.nestable);
            $('.hide_productcat').html(e.productcat0);
            // activate Nestable for list 1
            $('#nestable').nestable({
                group: 1,
                maxDepth: 1
            })
            .on('change', updateOutput);

            // output initial serialised data
            updateOutput($('#nestable').data('output', $('#nestable-output')));
        });

        var updateOutput = function(e)
        {
            var list   = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        $('#nestable-menu').on('click', function(e)
        {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });

        //-------------------------------------------------------------------------------
        $scope.search_datatable = function() {
            var title = $('.search_title').val() || '';
            console.log(title);
            $scope.datatable.columns(3).search(title)
                            .draw();
        };

        //-------------------------------------------------------------------------------
        $('.btn_addrow').click(function(event) {
            var request = {};
            var data = {};
            var files = [];
            if ($("#form_add .file_image").get(0).files[0])
                files = files.concat($("#form_add .file_image").get(0).files[0]);

            $("#form_add").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']); 

            request['value'] = data;
            fileUpload.uploadFileToUrl(files, request, 'create_productcat', function(e) {
                switch (e.code) {
                    case 200:
                        reload();
                        $('#modal_addrow').modal('hide');
                        $('#form_add').html($scope.form_add);
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('.nestable-lists').on('click', '.btn-edit-row', function(event) {
            var data = {};
            var id = $(this).attr('data-id');

            $('#form_edit').attr('data-id', id);
            
            data['status_code'] = "show";
            data['_method'] = 'PUT';
            commonService.requestFunction('update_productcat/' + id, data, function(e) {
                $('#form_edit').html(e.data);
                $('#modal_editrow').modal('show');
            });
        });

        //-------------------------------------------------------------------------------
        $('.nestable-lists').on('click', '.btn-status', function(event) {
            var data = {};
            var id = $(this).attr('data-id');

            $('#form_edit').attr('data-id', id);
            
            data['status_code'] = "edit_status";
            data['_method'] = 'PUT';
            commonService.requestFunction('update_productcat/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        reload();
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('.btn_editrow').click(function(event) {
            var request = {};
            var data = {};
            var files = [];

            var id_edit = $('#form_edit').attr('data-id');

            var curenpage_string = $('#form_edit').attr('data-curenpage');
            var curenpage = parseInt(curenpage_string, 10);

            var files = [];
            if ($("#form_edit .file_image").length != 0) {
                if ($("#form_edit .file_image").get(0).files[0])
                    files = files.concat($("#form_edit .file_image").get(0).files[0]);
            }

            $("#form_edit").serializeArray().map(function(x){data[x.name] = x.value;});
            data['slug'] = slugify(data['title']);

            request['value'] = data;
            request['status_code'] = 'edit';
            request['_method'] = "PUT";
            request['vitri'] = parseInt($('.item-position.active').html(), 10);
            
            fileUpload.uploadFileToUrl(files, request, 'update_productcat/' + id_edit + $rootScope.api_token, function(e) {
                switch (e.code) {
                    case 200:
                        $('#form_edit').html('');
                        $('#modal_editrow').modal('hide');
                        reload();
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('.nestable-lists').on('click', '.delete_row', function(event) {
            var row_id = $(this).attr('data-id');
            var row = $(this).closest('tr');
            confirmPopup('Xóa productcat', 'Bạn muốn xóa productcat không ? ', function() {
                commonService.requestFunction('delete_productcat/' + row_id + $rootScope.api_token, {}, function(e) {
                    switch (e.code) {
                        case 200:
                            reload();
                            break;
                        default:
                            break;
                    }
                });
            });
        });

        //-------------------------------------------------------------------------------
        $('#tbl-data').on('click', '.btn_status', function(event) {

            var here = $(this);
            var id = $(this).closest('tr').attr('data-id');
            var curenpage = $scope.datatable.page.info().page;

            var data = {};
            data['status_code'] = "change_status";
            data['_method'] = 'PUT';

            commonService.requestFunction('update_productcat/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        for (var i = 0; i < $scope.value.length; i++) {
                            if($scope.value[i][0] == id){
                                $scope.value[i][6] = e.status;
                            }
                        }

                        $scope.datatable.clear().draw();
                        $scope.datatable.rows.add($scope.value); // Add new data
                        $scope.datatable.columns.adjust().draw(); // Redraw the DataTable
                        $scope.datatable.page( curenpage ).draw( false );
                        break;
                    default:
                        break;
                }
            });
        });

        //-------------------------------------------------------------------------------
        $('.btn-save-row').click(function(event) {
            var data = {};
            var val = $('#nestable-output').val();
            var id = 0;

            data['val'] = val;
            data['status_code'] = 'edit_list';

            commonService.requestFunction('update_productcat/' + id, data, function(e) {
                switch (e.code) {
                    case 200:
                        
                        break;
                    default:
                        break;
                }
            });
        });

        function reload() {
            $('#nestable').nestable('destroy');
            commonService.requestFunction('index_productcat', {}, function(e) {
                $('#nestable').html(e.nestable);
                $('.hide_productcat').html(e.productcat0);
                $('#nestable').nestable({
                    group: 1,
                    maxDepth: 1
                })
                .on('change', updateOutput);
            });
        }
    }
]);
