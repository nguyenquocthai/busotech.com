// load more nha hang
$('#loadMoreprod').click(function(event) {
   var page = $(this).attr('data-page');
   var data = {};
   data['page'] = page;
   $.ajax({
        type: "POST",
        url: '/load_nhahang',
        data: data,
        dataType: 'json',
        success: function(result) {
            console.log(result);
            $('#loadMoreprod').attr('data-page',result.page_current);
            $('#list-prod').append(result.loadmore);
            if(result.next_page ==0){
                $('#loadMoreprod').remove();
            }
        }
    });
});
// load more nha hang cat
$('#loadMoreprodcat').click(function(event) {
   var page = $(this).attr('data-page');
   var id_cat = $(this).attr('data-id-cat');
   var data = {};
   data['page'] = page;
   data['id_cat'] = id_cat;
   $.ajax({
        type: "POST",
        url: '/load_nhahangcat',
        data: data,
        dataType: 'json',
        success: function(result) {
            console.log(result);
            $('#loadMoreprodcat').attr('data-page',result.page_current);
            $('#list-prod').append(result.loadmore);
            if(result.next_page ==0){
                $('#loadMoreprodcat').remove();
            }
        }
    });
});


