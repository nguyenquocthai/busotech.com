//-------------------------------------------------------------------------------
// SETTUP LANG
$(document).on('click', '.lang_pick', function() {

    var here = $(this).closest('.form-group');
    var lang = $(this).attr('data-lang');

    here.find('.lang_pick').removeClass('active');
    $(this).addClass('active');
    
    here.find('.value_lang').removeClass('active');
    here.find('.value_lang[data-lang="'+lang+'"]').addClass('active');

    here.find('.lang_alt').removeClass('active');
    here.find('.lang_alt[data-lang="'+lang+'"]').addClass('active');

});

$(document).on('click', '.pick_lang_muti', function() {

    var here = $(this).closest('.portlet');
    var lang = $(this).attr('data-lang');
    var name = $(this).html();

    here.find('.pick_lang_muti').removeClass('active');
    here.find('.name_mod').html(name);
    $(this).addClass('active');

    here.find('.lang_pick').removeClass('active');
    here.find('.lang_pick[data-lang="'+lang+'"]').addClass('active');
    here.find('.value_lang').removeClass('active');
    here.find('.value_lang[data-lang="'+lang+'"]').addClass('active');

    here.find('.lang_alt').removeClass('active');
    here.find('.lang_alt[data-lang="'+lang+'"]').addClass('active');
});

// DEFAULT MENU
$('.sub-menu .nav-item').click(function() {
    $('.nav-item').removeClass('active');
    $(this).addClass('active');
    $(this).closest('.nav-item.open').addClass('active');
})

//-------------------------------------------------------------------------------
// SELECT ALL
$(document).on('change', '.checkall', function() {
    if ($(this).is(':checked')) {
        $('.checkall').prop('checked', true);
    } else {
        $('.checkall').prop('checked', false);
    }

    $('.item-check').prop('checked', $(this).prop('checked') ? true : false);
    $('.item-check').trigger('change');
})

// SELECT ROW TABLE
$(document).on('change', 'table .checkbox input', function() {
    var count = $('.item-check').filter(function(i, e) {
        return $(e).is(':checked');
    }).length;

    if ($(this).is(':checked')) {
        $(this).parent().parent().parent().addClass('row_selected');
        $('.thead-choose').show();

        if (count === $('.item-check').length) {
            $('.checkall').prop('checked', true);
        }

    } else {
        $('.checkall').prop('checked', false);
        $(this).parent().parent().parent().removeClass('row_selected');

        if (count === 0) {
            $('.thead-choose').hide();
        }
    }

    $('.thead-choose .count-choose > span').html(count);
});

//-------------------------------------------------------------------------------
// DEFAULT DATABLES
try {
    $.fn.dataTable.ext.errMode = 'none';

    // CARE STATE SAVE = TRUE CAUSE NOT SEE CHANGE
    $.extend($.fn.dataTable.defaults, {
        stateSave: true,
        dom: '<"datatable-scroll table-responsive clearfix"ltr><"datatable-footer clearfix"ip>',
        lengthMenu: [[5, 20, 50, 100, -1], [5, 20, 50, 100, 'All']],
        language: {
            url: baseURL + '/public/assets/global/plugins/datatables/vi.json'
        },
        drawCallback: function (settings) {
            var api = this.api();

            // if total record > 0, row count 0, page != 0, always set page to first
            if (settings._iRecordsTotal > 0 && api.rows().count() == 0 && api.page() != 0) {
                api.page('first').state.save();
                $scope.datatable.draw(false);
            }
        }
    });
} catch (e) { }

//-------------------------------------------------------------------------------
// DEFAULT DATE PICKER
if ($.fn.datepicker) {
    $.extend($.fn.datepicker.defaults, {
        autoclose: true,
        clearBtn: false,
        endDate: Infinity,
        format: 'dd/mm/yyyy',
        keyboardNavigation: true,
        language: 'vi',
        orientation: 'bottom auto',
        startDate: -Infinity,
        todayHighlight: true,
    });
}


//-------------------------------------------------------------------------------
function select2_day(e, options) {

    e = e || $('.select2-days');
    options = options || {};

    e.select2();

    if (options.callback)
        options.callback(e);
}


//-------------------------------------------------------------------------------
function select2_tags(e, options) {
    e = e || $('.select2-tags');
    options = options || {};

    e.select2({
        width: '100%',
        tags: true,
        multiple: true,
        language: 'vi',
        tokenSeparators: [',', ' '],
        placeholder: {
            id: '-1',
            text: options.placeholder || '',
        },
        initSelection: function (element, callback) {
            if (options.init_data) {
                for (var i = 0; i < options.init_data.length; i++) {
                    var opt = new Option(options.init_data[i], options.init_data[i]);
                    element.append(opt);
                }

                element.val(options.init_data);
                callback(options.init_data);
            } else {
                callback([]);
            }
        },
    });
}

//-------------------------------------------------------------------------------
function readURL() {
    var newinput =  $(this).parent().parent().parent().find('.portimg ');

    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            reset_inputfile(newinput.next('.delbtn'), true);

            testImage(e.target.result, function (url, result){
                if (result == 'success') {
                    newinput.attr('src', e.target.result).show();
                } else {
                    var base_url = window.location.origin;
                    newinput.attr('src', base_url + '/img/no-image.png').show();
                }
            })
        }

        reader.readAsDataURL(this.files[0]);
    }
}

//-------------------------------------------------------------------------------
function isImage(e) {
  var pattern = /^.*\.{1}(jpg|jpeg|png|gif)$/i;
  return e.length > 0 && pattern.test(e);
}

//-------------------------------------------------------------------------------
function location_image(param) {
    var base_url = window.location.origin;
    switch (param) {
        case 'product_galleries':
            url = '/public/img/upload/product_galleries/';
            break;
        default:
            url = '';
            break;
    }
    return window.location.origin + url;
}

//-------------------------------------------------------------------------------
function reset_inputfile(elm, prserveFileName) {
    if (elm && elm.length > 0) {
        var $input = elm;
        $input.prev('.portimg').attr('src', '').hide();
        if (!prserveFileName) {
            $($input).parent().parent().parent().find('input.fileUpload ').val('');
        }
        elm.remove();
    }
}

//-------------------------------------------------------------------------------
function testImage(url, callback, timeout, options) {
    options = options || {};

    timeout = timeout || 2000;

    var timedOut = false, timer;
    var img = new Image();

    img.onerror = img.onabort = function() {
        if (!timedOut) {
            clearTimeout(timer);
            callback(url, 'error', options);
        }
    };

    img.onload = function() {
        if (!timedOut) {
            clearTimeout(timer);
            callback(url, 'success', options);
        }
    };

    img.src = url;

    timer = setTimeout(function() {
        timedOut = true;
        callback(url, 'timeout', options);
    }, timeout);
}

//-------------------------------------------------------------------------------
function load_tinymce(e,name) {
    e = e || '';
    tinymce.init({
        relative_urls: false,
        remove_script_host: false,
        path_absolute : "/",
        entity_encoding: 'raw',
        selector: e,
        plugins: [
            'advlist lists link image charmap print preview',
            'searchreplace code',
            'insertdatetime media table contextmenu paste responsivefilemanager textcolor'
        ],
        toolbar: 'insertfile undo redo | responsivefilemanager | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview media fullpage | forecolor backcolor emoticons | pagebreak | fontsizeselect',
        fontsize_formats: '8px 10px 12px 14px 16px 18px 20px 24px 36px',
        external_filemanager_path: '/public/filemanager/',
        visualblocks_default_state: true ,
        height: '250',
        init_instance_callback: function (editor) {
            if (name != '') {
                editor.on('keyup', function (e) {
                    $(name).val(tinymce.activeEditor.getContent());
                });
            }
        }
    });
}

//-------------------------------------------------------------------------------
function attr_url_image(url, result, options) {
    options = options || {};

    element = options.element || '.portimg';

    if (result == 'success') {
        $(element).attr('src', url + '?' + (+new Date())).show();
    } else {
        var base_url = window.location.origin;
        $(element).attr('src', base_url + '/public/img/no-image.png').show();
    }
}

// c_tran custom-script

$(document).on('click', '.c-trigger', function(e) {
    $( '.c-btn-submitform' ).trigger( 'click' );
});

$(document).on('click', '.c-ctr-boxform', function(e) {
    $(this).parent().find('.c-boxform').stop().slideToggle();
    $(this).toggleClass('active');
});

$(document).on('click', '.c-ctr-open', function(e) {
    $(this).toggleClass('active');
    $('.c-seachbox-dropdow').toggleClass('active');
});

$(document).mousedown(function(e) {
    var container = $('.c-ctr-open, .c-seachbox-dropdow');
    if (!container.is(e.target) && container.has(e.target).length === 0) {
       $('.c-ctr-open').removeClass('active');
       $('.c-seachbox-dropdow').removeClass('active');
    }
});

$(document).on('click', '.page-sidebar .page-sidebar-wrapper > ul > li > ul.sub-menu > li > a', function(e) {
    $('.page-sidebar .page-sidebar-wrapper > ul > li > ul.sub-menu > li').removeClass('active');
    $('.page-sidebar-wrapper > ul > li').removeClass('active');
    $(this).closest('.page-sidebar-wrapper > ul > li').addClass('active');
    $(this).parent().addClass('active');
});

$(document).on('click', '.page-sidebar .page-sidebar-wrapper>ul>li>a.next-nav-link', function(e) {
    $('.page-sidebar .page-sidebar-wrapper>ul>li>a.next-nav-link').removeClass('active');
    $('.page-sidebar-wrapper > ul > li').removeClass('active');
    $(this).addClass('active');
    $(this).parent().addClass('active');
});

$('.ctr-view-pass').click(function(event) {
    var data = $(this).attr('data-view');
    if(data == 0){
        $('.view-pass-mod').attr({
            type: 'text'
        });
        $(this).attr('data-view',1);
        $(this).html('<i class="fa fa-eye-slash" aria-hidden="true"></i> Hide password');
    }

    if(data == 1){
        $('.view-pass-mod').attr({
            type: 'password'
        });
        $(this).attr('data-view',0);
        $(this).html('<i class="fa fa-eye" aria-hidden="true"></i> See password');
    }
});

// c_tran function
$(document).on('change', '.search_status', function(event) {
    if(this.checked) {
        $(this).val('Activated');
    } else {
        $(this).val('Unactivated');
    }
});

// Upload images reviews
function review(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#img_review').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
$(document).on('change', '#file_image', function(event) {
    review(this);
});

$(document).on('change', '.file_image', function(event) {
    var images = $(this).closest('.file_upload_box').find('.image_review');
    var noimg = "/public/img/no-image.png";
    var file = this.files[0];
    if (typeof(file) === 'undefined') {
        $(this).val('');
        images.attr('src', noimg);
        return false;
    }
    var fileType = file["type"];
    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/svg+xml"];
    if ($.inArray(fileType, ValidImageTypes) < 0) {
        toastr.error('Hình ảnh không hợp lệ', null, {timeOut: 4000});
        $(this).val('');
        images.attr('src', noimg);
        return false;
    }
    if (file.size > 5000000) {
        toastr.error('Dung lượng hình phải bé hơn 5MB', null, {timeOut: 4000});
        $(this).val('');
        images.attr('src', noimg);
        return false;
    }

    var images = $(this).closest('.file_upload_box').find('.image_review');
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            images.attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);
    }
});

$(document).on('click', '#btn_change_pass_admin', function(event) {
    var data = {};
    $("#form_change_pass").serializeArray().map(function(x){data[x.name] = x.value;});
    data['status_code'] = "change_pass";
    showPageLoading();
    $.ajax({
        type: 'POST',
        url: baseURL + 'api/adminusers/change_pass',
        data: data,
        dataType: 'json',
        success: function(result) {
           hidePageLoading();
            switch (result.code) {
                case 200:
                    $('#changepass').modal('hide');
                    toastr.success(result.message);
                    break;
                default:
                    toastr.error(result.error);
                    break;
            }
        }
    });
});

function getIdvideo(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}

//-------------------------------------------------------------------------------
$(document).on('click', '.video_link', function(event) {
    $(this).select();
});

$(document).on('keyup', '.video_link', function(event) {
    var here = $(this).closest('.video_upload_box');
    var link = here.find('.video_link').val();
    var idvideo = getIdvideo(link);
    here.find('.video_id').val(idvideo);
});

$(document).on('click', '.btn_review_video', function(event) {
    var here = $(this).closest('.video_upload_box');
    var link = here.find('.video_link').val();
    if(link == ""){
        toastr.error('Bạn chưa nhập đường dẩn video.');
    } else {
        var idvideo = getIdvideo(link);
        var src = 'https://www.youtube.com/embed/'+idvideo;
        here.find('.video_review iframe').attr('src', src).show();
    }
});

// datevn
function datevn(day) {
    const parts = day.split(/[- :]/);
    const wanted = `${parts[2]}/${parts[1]}/${parts[0]} ${parts[3]}:${parts[4]}`;
    return wanted;
}

// dayvn
function dayvn(day) {
    const parts = day.split(/[- :]/);
    const wanted = `${parts[2]}/${parts[1]}/${parts[0]}`;
    return wanted;
}

function DateTime(day) {
    var d = day.replace(/\//g, '-').replace(/\ /g, '-').split('-');
    var newdata = d[2]+'-'+d[1]+'-'+d[0]+' '+d[3]+':00';
    return newdata;
}

function DateEn(day) {
    var d = day.split('/');
    var newdata = d[2]+'-'+d[1]+'-'+d[0]+' 00:00:00';
    return newdata;
}

//-------------------------------------------------------------------------------
function select2s(e, options) {
    var data = {};
    data['name'] = options.name;
    data['where'] = options.where;
    data['limit'] = options.limit;
    data['title'] = options.title;

    options.commonService.requestFunction('select2', data, function(result) {
        var data = result.data.map(function(item) {
            return {
                id : item.id,
                text : item.title,
                data : item,
            };
        });

        if (options.have_default) {
            data.unshift({
                id : '0',
                text : '--- Chọn ---',
            });
        }

        $(e).select2({
            width: '100%',
            multiple: options.multiple || false,
            language: 'vi',
            tokenSeparators: [',', ' '],
            placeholder: {
                id: '-1',
                text: options.placeholder || '',
            },
            data: data
        });

        if (options.selected) {
            if(options.multiple){
                var obj = jQuery.parseJSON(options.selected);
                $(e).val(obj.map(function(e) { return e; })).trigger('change');
            } else {
                $(e).val(options.selected).trigger('change');
            }
            
        }

        if (options.callback)
            options.callback(e);
    });
}



// add_list_box
$(document).on('click', '.btn_add_item', function(event) {
    var value = $(this).closest('.add_list_box').find('.value').val();
    var here = $(this).closest('.add_list_box');
    if(value == ''){
        toastr.error('Keywords cannot be empty.');
        $(this).closest('.add_list_box').find('input').addClass('error');
        return false;
    }

    var data = '<span class="item_mod"><b>'+value+'</b><i class="far fa-trash-alt del_list"></i></span>';

    $(this).closest('.add_list_box').find('.list_box').append(data);
    $(this).closest('.add_list_box').find('input').val('').removeClass('error');

    var array = [];
    here.find('.item_mod').each(function(index, el) {
        array.push($(this).find('b').html());
    });

    $(this).closest('.add_list_box').find('.json_value').html(JSON.stringify(array));
});

$(document).on('click', '.add_list_box .del_list', function(event) {
    var here = $(this).closest('.add_list_box');
    $(this).closest('.item_mod').remove();
    var array = [];
    here.find('.item_mod').each(function(index, el) {
        array.push($(this).find('b').html());
    });

    here.find('.json_value').html(JSON.stringify(array));
});

function load_list_box(e,value) {
    var array = jQuery.parseJSON( value );
    if (array ==  null) {
        array = [];
    }
    var output = "";
    for (var i = 0; i < array.length; i++) {
        var data = '<span class="item_mod"><b>'+array[i]+'</b><i class="far fa-trash-alt del_list"></i></span>';
        output = output+data;
    }
    $('textarea[name="'+e+'"]').html(value);
    $('.load_'+e).html(output);
}

//-------------------------------------------------------------------------------------
function init_google_map($scope, options) {
  execFunc(function() {
    $scope.$apply(function() {
      // Init map
      if (!$scope.map) {
        var p = { lat: parseFloat(options.pointer.latitude), lng: parseFloat(options.pointer.longitude) };

        if (isNaN(options.pointer.latitude) || isNaN(options.pointer.longitude))
          p = { lat: 10.8230989, lng: 106.6296638 };

        $scope.map = new google.maps.Map($('#googleMap')[0], {
          zoom: 17,
          center: p,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        placeMarker(p);

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');

        var searchBox = new google.maps.places.SearchBox(input);

        $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            $scope.$apply(function() {
              if (!place.geometry) {
                return;
              }

              placeMarker(place.geometry.location);

              options.pointer.latitude = place.geometry.location.lat();
              options.pointer.longitude = place.geometry.location.lng();
              options.pointer.description = place.formatted_address;

              if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
              } else {
                bounds.extend(place.geometry.location);
              }
            });
          });
          $scope.map.fitBounds(bounds);
        });
      }

      //-------------------------------------------------------------------------------------
      google.maps.event.addListener($scope.map, 'click', function(event) {
         placeMarker(event.latLng);

         options.pointer.latitude = event.latLng.lat();
         options.pointer.longitude = event.latLng.lng();
      });

      //-------------------------------------------------------------------------------------
      function placeMarker(location) {
        if (options.marker == null) {
          options.marker = new google.maps.Marker({
            position: location,
            map: $scope.map
          });
        } else {
          options.marker.setPosition(location);
        }
      }
    });
  });
};
// End c_tran function