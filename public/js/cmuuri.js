// load_tienich
$.ajax({
    type: 'GET',
    url: '/datatable_store2',
    success: function(result) {
        var datatable2 = $('#tbl-dataz2').DataTable({
            order: [[ 1, 'desc' ]],
            columnDefs: [
                { sortable: false, searchable: false, targets: [ 0 ] },
                { class: 'hidden', targets: [ 1 ] },
            ],
            displayStart: 0,
            displayLength: 10,
            data: result.value,
            "autoWidth": false
        });
        datatable2.search('').draw();
    }
});
// End load_tienich

//-------------------------------------------------------------------------------
$('#add_tienich').click(function(event) {
    var tienich = [];

    var id = 'key'+$.now();
    var data = $('#data_icon').val();
    var title = $('#title_tienich').val();
    var title_en = $('#title_tienich_en').val();

    if (data == '') {
        alert('Chưa chọn icon');
        return false;
    }

    if (title == '') {
        alert('Chưa nhập tên tiện ích');
        return false;
    }

    if (title_en == '') {
        alert('Chưa nhập tên tiện ích (English)');
        return false;
    }

    tienich['id'] = id;
    tienich['data'] = data;
    tienich['title'] = title;
    tienich['title_en'] = title_en;

    $scope.product1.tienich.push(tienich);
    $('.load-add-tienich').prepend('<li data-id="'+id+'" class="col-sm-3 item-tienich"><div class="item-mod"><span class="icon-mod"><i class="'+data+'"></i></span><span class="title-mod">'+title+'</span><span class="title-mod _en">'+title_en+'</span><i onclick="delete_tienich(&#39;'+id+'&#39;)" class="fas fa-times delete-mod"></i></div></li>');

    $('#data_icon').val('');
    $('#title_tienich').val('');
    $('#title_tienich_en').val('');
});


delete_tienich = function(id) {
    $('.item-tienich[data-id="'+id+'"]').remove();
    $scope.product1.tienich = $scope.product1.tienich.filter(function( obj ) {
        return obj.id !== id;
    });
};

$('#all_id_player').change(function(){
    var check = this.checked ? '1' : '0';
    if (check == 1) {
        $('.check_player').prop('checked', true);
    } else {
        $('.check_player').prop('checked', false);
    }
});

//-------------------------------------------------------------------------------
var addstore = function() {
    var list_ids = [];
    $('.check_player').each(function(i, e) {
        var value = {};
        var check_id = this.checked ? '1' : '0';
        if (check_id == '1') {
            value['data'] = $(this).val();
            value['id'] = $(this).attr('data-id');
            value['title'] = $(this).attr('data-title');
            value['title_en'] = $(this).attr('data-title_en');
            value['value'] = $(this).attr('data-value');
            value['value_en'] = $(this).attr('data-value_en');
            list_ids.push(value);
        }
    });

    if (list_ids == '') {
        alert('Chưa chọn bất kỳ tổng quan...')
        return false;
    }

    list_tongquan(list_ids);

    $('#modal_tongquan').modal('hide');

    $('#all_id_player').prop('checked', false);

    $('.check_player').prop('checked', false);
};


// Tiện ích-------------------------------------------------------------------------
//-------------------------------------------------------------------------------
$('.themtienich').click(function(event) {
    $('#modal_tienich').modal('show');
});

$('#all_id_player2').change(function(){
    var check = this.checked ? '1' : '0';
    if (check == 1) {
        $('.check_player2').prop('checked', true);
    } else {
        $('.check_player2').prop('checked', false);
    }
});

//-------------------------------------------------------------------------------
var addstore2 = function() {
    var list_ids2 = [];
    $('.check_player2').each(function(i, e) {
        var value = {};
        var check_id = this.checked ? '1' : '0';
        if (check_id == '1') {
            value['data'] = $(this).val();
            value['id'] = $(this).attr('data-id');
            value['title'] = $(this).attr('data-title');
            value['title_en'] = $(this).attr('data-title_en');
            list_ids2.push(value);
        }
    });

    if (list_ids2 == '') {
        alert('Please choose at least 1')
        return false;
    }

    list_tienich(list_ids2);

    $('#modal_tienich').modal('hide');

    $('#all_id_player2').prop('checked', false);

    $('.check_player2').prop('checked', false);
};

var grid2 = null;
var docElem2 = document.documentElement;
var demo2 = document.querySelector('.grid-demo2');
var gridElement2 = demo2.querySelector('.grid2');
var filter2Field2 = demo2.querySelector('.filter2-field2');
var searchField2 = demo2.querySelector('.search-field2');
var sortField2 = demo2.querySelector('.sort-field2');
var layoutField2 = demo2.querySelector('.layout-field2');
var addItemsElement2 = demo2.querySelector('.add-more-items2');
var characters2 = 'abcdefghijklmnopqrstuvwxyz';
var filter2Options2 = ['red', 'blue', 'green'];
var dragOrder2 = [];
var uuid2 = 0;
var limit2 = 12;
var filter2FieldValue2;
var sortFieldValue2;
var layoutFieldValue2;
var searchFieldValue2;

function list_tienich(arr2) {
    // Generate new elements.
    var newElems2 = generateElements2(arr2);

    // Set the display of the new elements to "none" so it will be hidden by
    // default.
    newElems2.forEach(function (item) {
        item.style.display = 'none';
    });

    // Add the elements to the grid.
    var newItems = grid2.add(newElems2);

    // Update UI indices.
    updateIndices2();

    // Sort the items only if the drag sorting is not active.
    if (sortFieldValue2 !== 'order') {
        grid2.sort2(sortFieldValue2 === 'title' ? compareItemTitle2 : compareItemColor2);
        dragOrder2 = dragOrder2.concat(newItems);
    }

    // Finally filter2 the items.
    filter2();
}

function initDemo2() {

    initGrid2();

    // Reset field values.
    searchField2.value = '';
    [sortField2, filter2Field2, layoutField2].forEach(function (field) {
        field.value = field.querySelectorAll('option')[0].value;
    });

    // Set inital search query, active filter2, active sort value and active layout.
    searchFieldValue2 = searchField2.value.toLowerCase();
    filter2FieldValue2 = filter2Field2.value;
    sortFieldValue2 = sortField2.value;
    layoutFieldValue2 = layoutField2.value;

    // Search field binding.
    searchField2.addEventListener('keyup', function () {
        var newSearch = searchField2.value.toLowerCase();
        if (searchFieldValue2 !== newSearch) {
            searchFieldValue2 = newSearch;
            filter2();
        }
    });

    // Filter, sort and layout bindings.
    filter2Field2.addEventListener('change', filter2);
    sortField2.addEventListener('change', sort2);
    layoutField2.addEventListener('change', changeLayout2);

    // Add/remove items bindings.
    addItemsElement2.addEventListener('click', addItems2);

    gridElement2.addEventListener('click', function (e) {
        if (elementMatches2(e.target, '.card-remove, .card-remove i')) {
            removeItem2(e);
        }
    });

}

function initGrid2() {

    var dragCounter = 0;
    var arr2 = [];

    grid2 = new Muuri(gridElement2, {
            items: generateElements2(arr2),
            layoutDuration: 400,
            layoutEasing: 'ease',
            dragEnabled: true,
            dragSortInterval: 50,
            dragContainer: document.body,
            dragStartPredicate: function (item, event) {
                var isDraggable = sortFieldValue2 === 'order';
                var isRemoveAction = elementMatches2(event.target, '.card-remove, .card-remove i');
                return isDraggable && !isRemoveAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;
            },
            dragReleaseDuration: 400,
            dragReleseEasing: 'ease'
        })
        .on('dragStart', function () {
            ++dragCounter;
            docElem2.classList.add('dragging');
        })
        .on('dragEnd', function () {
            if (--dragCounter < 1) {
                docElem2.classList.remove('dragging');
            }
        })
        .on('move', updateIndices2)
        .on('sort2', updateIndices2);

}

function filter2() {

    filter2FieldValue2 = filter2Field2.value;
    grid2.filter(function (item) {
        var element = item.getElement();
        var isSearchMatch = !searchFieldValue2 ? true : (element.getAttribute('data-title') || '').toLowerCase().indexOf(searchFieldValue2) > -1;
        var isFilterMatch = !filter2FieldValue2 ? true : (element.getAttribute('data-color') || '') === filter2FieldValue2;
        return isSearchMatch && isFilterMatch;
    });

}

function sort2() {

    // Do nothing if sort value did not change.
    var currentSort = sortField2.value;
    if (sortFieldValue2 === currentSort) {
        return;
    }

    // If we are changing from "order" sorting to something else
    // let's store the drag order.
    if (sortFieldValue2 === 'order') {
        dragOrder2 = grid2.getItems();
    }

    // Sort the items.
    grid2.sort2(
        currentSort === 'title' ? compareItemTitle2 :
        currentSort === 'color' ? compareItemColor2 :
        dragOrder2
    );

    // Update indices and active sort value.
    updateIndices2();
    sortFieldValue2 = currentSort;

}

function addItems2() {

    var arr2 = [];

    // Generate new elements.
    var newElems2 = generateElements2(arr2);

    // Set the display of the new elements to "none" so it will be hidden by
    // default.
    newElems2.forEach(function (item) {
        item.style.display = 'none';
    });

    // Add the elements to the grid.
    var newItems = grid2.add(newElems2);

    // Update UI indices.
    updateIndices2();

    // Sort the items only if the drag sorting is not active.
    if (sortFieldValue2 !== 'order') {
        grid2.sort2(sortFieldValue2 === 'title' ? compareItemTitle2 : compareItemColor2);
        dragOrder2 = dragOrder2.concat(newItems);
    }

    // Finally filter2 the items.
    filter2();

}

function removeItem2(e) {

    var elem = elementClosest2(e.target, '.item_code');
    grid2.hide(elem, {
        onFinish: function (items) {
            var item = items[0];
            grid2.remove(item, {
                removeElements: true
            });
            if (sortFieldValue2 !== 'order') {
                var itemIndex = dragOrder2.indexOf(item);
                if (itemIndex > -1) {
                    dragOrder2.splice(itemIndex, 1);
                }
            }
        }
    });
    updateIndices2();

}

function changeLayout2() {

    layoutFieldValue2 = layoutField2.value;
    grid2._settings.layout = {
        horizontal: false,
        alignRight: layoutFieldValue2.indexOf('right') > -1,
        alignBottom: layoutFieldValue2.indexOf('bottom') > -1,
        fillGaps: layoutFieldValue2.indexOf('fillgaps') > -1
    };
    grid2.layout();

}

//
// Generic helper functions
//

function generateElements2(arr2) {
    var ret = [];
    var last = $('.load_tienich .item_code').length;
    var last2 = parseInt(last, 10);
    var slot = limit2 - last2;
    if (last2 + arr2.length > limit2) {
        alert('Bạn còn ' + slot + ' chổ trống.');
        return ret;
    }
    for (var i = 0; i < arr2.length; i++) {
        ret.push(generateElement2(
            ++uuid2,
            arr2[i].id,
            arr2[i].data,
            arr2[i].title,
            arr2[i].title_en,
        ));
    }
    return ret;
}

function generateElement2(id, id_tq, data, title, title_en) {

    var itemElem = document.createElement('div');
    var classNames = 'item_code h1 w2';
    var itemTemplate = '' +
        '<div class="' + classNames + '" data-id="' + id + '" data-color="green" data-title="' + title + '">' +
        '<div class="item-content">' +
        '<div class="card">' +
        '<div class="card-id">' + id + '</div>' +
        '<div class="card_child">' +
        '<div class="card-id_tq">' + id_tq + '</div>' +
        '<div class="card-id_data"><i class="' + data + '"></i></div>' +
        '<div class="card-title">' + title + '</div>' +
        '<div class="card-title_en">' + title_en + '</div>' +
        '</div>' +
        '<div class="card-remove"><i class="fas fa-trash-alt"></i></div>' +
        '</div>' +
        '</div>' +
        '</div>';

    itemElem.innerHTML = itemTemplate;
    return itemElem.firstChild;
}

function getRandomItem2(collection) {

    return collection[Math.floor(Math.random() * collection.length)];

}

// https://stackoverflow.com/a/7228322
function getRandomInt2(min, max) {

    return Math.floor(Math.random() * (max - min + 1) + min);

}

function generateRandomWord2(length) {

    var ret = '';
    for (var i = 0; i < length; i++) {
        ret += getRandomItem2(characters2);
    }
    return ret;

}

function compareItemTitle2(a, b) {

    var aVal = a.getElement().getAttribute('data-title') || '';
    var bVal = b.getElement().getAttribute('data-title') || '';
    return aVal < bVal ? -1 : aVal > bVal ? 1 : 0;

}

function compareItemColor2(a, b) {

    var aVal = a.getElement().getAttribute('data-color') || '';
    var bVal = b.getElement().getAttribute('data-color') || '';
    return aVal < bVal ? -1 : aVal > bVal ? 1 : compareItemTitle2(a, b);

}

function updateIndices2() {

    grid2.getItems().forEach(function (item, i) {
        item.getElement().setAttribute('data-id', i + 1);
        item.getElement().querySelector('.card-id').innerHTML = i + 1;
    });

}

function elementMatches2(element, selector) {

    var p = Element.prototype;
    return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

}

function elementClosest2(element, selector) {

    if (window.Element && !Element.prototype.closest) {
        var isMatch = elementMatches2(element, selector);
        while (!isMatch && element && element !== document) {
            element = element.parentNode;
            isMatch = element && element !== document && elementMatches2(element, selector);
        }
        return element && element !== document ? element : null;
    } else {
        return element.closest(selector);
    }

}

//
// Fire it up!
//

initDemo2();
var arr2 = [];
list_tienich(arr2);

jQuery(document).ready(function($) {

    if($('#key_edit_project_item').length != 0){
        var id_project_item = $('#key_edit_project_item').val();
        $.ajax({
            type: 'GET',
            url: '/get_tq_ti_item/'+id_project_item,
            success: function(result) {

                result.tongquans.sort(function(a, b) { 
                  return a.vitri - b.vitri  ||  a.id.localeCompare(b.id);
                });

                result.tienichs.sort(function(a, b) { 
                  return a.vitri - b.vitri  ||  a.id.localeCompare(b.id);
                });
                
                list_tienich(result.tienichs);
            }
        });
    }
});